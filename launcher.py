# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

print("PLEASE, BE PATIENT: python may take some time to load some module \n"
      "dependencies (in particular, paramiko).\n"
      "Script may look stuck, but it will be run: don't kill it!\n")
import sys
import logging
# import paramiko

# from fins.src.launchers.ei_launcher import EILauncher as EIL
# from fins.src.launchers.ra_onos_launcher import RAONOSLauncher as RAOL
# from fins.src.launchers.ra_wctrl_launcher import RAWCtrlLauncher as RAWCL
from fins.src.launchers.fins_launcher import FINSLauncher
from fins.src.launchers.rest_launcher import RESTLauncher
from fins.src.launchers.rm_launcher import RMLauncher
from fins.src.launchers.ra_wctrl_launcher import RAWCtrlLauncher
from fins.src.launchers.ra_ovs_launcher import RAOvsLauncher


# MODULES = list()
# if len(sys.argv) > 1:
#     index = 1
#     while index < len(sys.argv):
#
#         MODULES.append(dict(
#             type=sys.argv[index],
#             config_file=sys.argv[index+1]
#         ))
#         index += 2
# else:
#     MODULES.append(dict(
#         type='EI',
#         config_file='fins/confs/ei_local_DEMO.cfg'
#     ))
#
# for module in MODULES:
#     module_type = module['type']
#     config_file = module['config_file']
#     if module_type == 'EI':
#         EIL.launch(config_file)
#     elif module_type == 'RA_ONOS':
#         RAOL.launch(config_file)
#     else:
#         print("Invalid Module type: " + module_type)

# print(str(sys.argv))

if __name__ == '__main__':

    module_tye = None
    config_file = None

    print("FINS launcher ready")
    if len(sys.argv) == 3:
        module_type = sys.argv[1]
        config_file = sys.argv[2]
    elif len(sys.argv) == 2:
        module_type = sys.argv[1]
        if module_type == 'FINS':
            config_file = 'fins/confs/FINS_configuration.cfg'
        # elif module_type == 'RA_ONOS':
        #     config_file = 'fins/confs/RA_configuration_ra_ovs.cfg'
        # elif module_type == 'RA_WCTRL':
        #     config_file = 'fins/confs/RA_configuration_ra_wctrl.cfg'
        # elif module_type == 'EI':
        #     config_file = 'fins/confs/EI_configuration.cfg'
        else:
            print("Missing config file for module " + module_type)
            exit(1)
    else:
        print("Invalid argument number")
        exit(1)


    # if module_type == 'EI':
    #     EIL.launch(config_file)
    # elif module_type == 'RA_ONOS':
    #     RAOL.launch(config_file)
    # elif module_type == 'RA_WCTRL':
    #     RAWCL.launch(config_file)
    # elif module_type == 'FINS':

    if module_type == 'FINS':
        FINSLauncher().launch(config_file)
    elif module_type == 'REST':
        RESTLauncher().launch(config_file)
    elif module_type == 'RM':
        RMLauncher().launch(config_file)
    elif module_type == 'RA_WCTRL':
        RAWCtrlLauncher().launch(config_file)
    elif module_type == 'RA_OVS':
        RAOvsLauncher().launch(config_file)
    else:
        print("Invalid Module type: " + module_type)
