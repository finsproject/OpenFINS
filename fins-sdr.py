#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
fins-sdr.py

Usage:
    fins-sdr.py [-q|-v] [-w|--webgui] [--log <LOG_LEVEL>] [--logfile <LOG_FILE>] [-a|--action <action_name>] [-p|--par <action_params>] [-n|--node <node_name>]
    fins-sdr.py -i [-q|-v] [--log <LOG_LEVEL>] [--logfile <LOG_FILE>] [-a|--action <action_name>] [-p|--par <action_params>] [-n|--node <node_name>]
    fins-sdr.py -f [-q|-v] [--log <LOG_LEVEL>] [--logfile <LOG_FILE>] [--config <FILE>]
    fins-sdr.py -h | --help
    fins-sdr.py --version

Options:
    --logfile <LOG_FILE>        Name of the logfile, [default: fins-sdr.log]
    --config <FILE>             Config File (json), [default: fins-sdr-config.json]
    -i                          Activate interactive mode (ignore action list)
    -f                          Activate file mode (fins-sdr-config.json)
    -n, --node <node_name>      Node name
    -a, --action <action_name>  Action name
    -p, --par <action_params>   Action parameters
    -w, --webgui                REST interface output
    --version                   Show version and exit
    -h, --help                  Show this help message and exit
    -q, --quiet                 Quiet (less text)
    -v, --verbose               Verbose (more text)
    --log <LOG_LEVEL>           Log level, valid options: DEBUG, INFO, WARNING, ERROR, CRITICAL [default: DEBUG]  

Example:
    ./FINS_wishful_agent -v --config ./config.yaml --logfile ./logfile.log 
"""

__docformat__   = "epytext" # http://epydoc.sourceforge.net/manual-epytext.html
__author__      = "Cristina Costa (CREATE-NET FBK)"
__copyright__   = "Copyright (c) 2018, CREATE-NET FBK"
__version__     = "FINS RA Wishful V.1.0"
__email__       = "ccosta@fbk.eu"

# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import requests, logging, os
import pika, json, yaml
from logging import handlers
import time

# ----------------------------------------------------------------------
#  Defaults
# ----------------------------------------------------------------------
#--- Test Default Configuration
DEFAULT_CONF ={
    "host": {
        "ip"   : "localhost",
        "port" : "8888",
        "id"   : "ra_wctrl"
        }
    }
# -------- Logging configuration constants
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(process)d-%(levelname)s-%(message)s'
# LOG_FORMAT          = '%(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
LOG_FORMAT          = '%(asctime)s %(levelname)s: %(name)s.%(funcName)s() - %(message)s'
LOG_FORMAT_OUT      = '%(levelname)s: %(message)s'
LOG_DATE_FORMAT     = '%d-%b-%y %H:%M:%S'
LOG_LEVEL           = 'INFO'
# ----------------------------------------------------------------------
#  Globals
# ----------------------------------------------------------------------
LOG = logging.getLogger('__name__')
LOG.setLevel(logging.DEBUG)
host_config   = dict()

# ----------------------------------------------------------------------
#  Support functions
# ----------------------------------------------------------------------
def validate_config(config):  
    """
    Validates configuration, set defaults if needed
    
    @param config: input configuration
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """  
    LOG.debug("--- Loading Configuration....") 
    # ---- Setting default values ----
    validated_config = DEFAULT_CONF["host"]
    
    try:
        validated_config.update(config["host"])
    except:
        LOG.info(" -- RabbitMq configuration set to default:")
    else:
        LOG.info(" -- RabbitMq configuration loaded:")
        
    # ---- Wishful Agent config values ---
    LOG.info("\t-- ip:   %s", validated_config["ip"])
    LOG.info("\t-- port: %s", str(validated_config["port"]))
    LOG.info("\t-- id:   %s", validated_config["id"])
    
    if "id_sdr" in validated_config:
         validated_config["id"] = validated_config["id_sdr"] 
    return validated_config
    
def load_actions(config):  
    """
    Load actions
    
    @param config: actions
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """  
    LOG.debug("--- Loading actions list ...") 
    validated_list = []
    if not config:
        return validated_list
    if "action_list" in config and type(config["action_list"]) is list:
        for item in config["action_list"]:
            if "action" in item and "action_name" in item["action"]:
                validated_list.append(item)
            else:
                LOG.debug("action {} not valid, skipping ...", repr(item))
    else:
        LOG.debug("action_list not present in config, or format not valid, returning empty list ...")
    return validated_list
        
def send_rest_request(url, action):  
    """
    send_rest_request
    
    @param url: rest host url
    @type  url:    str
    @param action: action
    @type  action: dict
    @return:       rest return value
    @rtype:        dict    
    """
    # --- Send REST request
    if not action or type(action) is not dict:
        return None
        
    try:
        resp = requests.post(url, json=action)
    except requests.exceptions.ConnectionError as e:
        LOG.error('Connection Error: couldn\'t connect to the server [%s], please check if the IP address and PORT number specified in the configuration file correspond to the FINS\' REST service IP and port number', url.split('/')[2])
        raise SystemExit("requests.exceptions.ConnectionError", e)
    except:
        raise
    
    if resp.status_code == 500:
        LOG.error('POST /rest/send_action/ %s %s',str(resp.status_code) , str(resp.reason))
        LOG.error('Please check if the imput action and parameters values are valid, and the id value in the configuration file')
        LOG.error('If the error persist, a restart of the REST server may be needed')
        return 
    elif resp.status_code != 200:
        LOG.error('POST /rest/send_action/ %s %s',str(resp.status_code) , str(resp.reason))
        return 
    else:
        LOG.debug('POST /rest/send_action/ %s %s',str(resp.status_code) , str(resp.reason))
    LOG.info('Action response: \n%s', resp.text)
    try:
        action_resp = json.loads(resp.text)
    except:
        action_resp = yaml.safe_load(resp.text)
        
    return action_resp
# ----------------------------------------------------------------------
#  FINS Wishful Gnuradio Controller REST user interface client 
# ----------------------------------------------------------------------
    
def REST_client_from_json(config = None, *argv, **kwargs):
    """
    FINS Wishful REST simple user interface (GNR) implementation.
    
    Configure from configuration file
        - FINS Configuration
        - Test actions
    
    @param config: configuration parameteres
    @type  config: dict
    """     
    LOG.info("**** FINS Wishful Gnuradio Controller REST Client (from json) ****")  
    
    if type(config) is not dict:
        config = None
    
    # ----- Loading REST Actions  ---- 
    LOG.debug("--- Loading Action list....")    
    action_list = load_actions(config)
    if not action_list:
        raise SystemExit("Action list is empty")        
    
    # ----- Loading Wishful Client Configuration  ----
    LOG.debug("--- Loading FINS Configuration....")    
    host_config   = validate_config(config) 
    url = 'http://' + host_config["ip"] + ':' + host_config["port"] + '/rest/send_action'
    print("-"*100)
    print("Sending actions to RA (id={})".format(host_config["id"]))
    print("Using REST interface: {}".format(url))
    print("-"*100)
    
    LOG.debug("--- Executing action list:") 
    for item in action_list:
        action = dict(module_id = host_config["id"])
        action.update(item)
        
        LOG.debug("Execute action %s", repr(item["action"]))
        if "node" in item:
            node_name = item["node"]["node_name"]
        else:
            node_name = "controller"
        print("Sending action to {}:\n {}".format(node_name, json.dumps(action, indent=2, sort_keys=True)))
        
        input("Press Enter to continue...")
        # --- Send REST request
        action_resp = send_rest_request(url, action)
        LOG.debug('Action response: \n%s', action_resp)
        LOG.debug('Action response from node %s: \n%s', action_resp["node"]["node_name"], action_resp["return_value"])
        print("-"*100)
        input("Press return to continue...")    
    return

def REST_client_interactive(config = None, *argv, **kwargs):
    """
    FINS Wishful REST simple user interface (GNR) implementation.
    
    Configure from configuration file
        - FINS Configuration
        - Test actions
    
    @param config: configuration parameteres
    @type  config: dict
    """     
    LOG.info("**** FINS Wishful Gnuradio Controller REST client (interactive) ****")  
    
    node_name     = None
    action_name   = None
    action_params = {}
    # ----- Init vars  ----   
    if kwargs is not None:
        node_name     = kwargs.get("node_name")
        action_name   = kwargs.get("action_name")
        action_params = kwargs.get("action_params")
        
    if node_name:
        node_name     = node_name[0]
    if action_name:
        action_name   = action_name[0]
    if action_params:
        action_params = action_params[0]
        
    LOG.debug("--- INPUT PAR....")  
    print(node_name,action_name, action_params)
    # ----- Loading Wishful Client Configuration  ----
    LOG.debug("--- Loading FINS Configuration....")    
    host_config   = validate_config(config) 
    
    print("-"*100)
    url = 'http://' + host_config["ip"] + ':' + host_config["port"] + '/rest/send_action'
    print("Sending actions to RA (id={})".format(host_config["id"]))
    print("Using REST interface: {}".format(url))
    print("-"*100)
    print()
    
    # ----- Getting action from input  ----
    action = dict(module_id = host_config["id"])
    action["action"] = dict()
    
    if node_name:
        print("Sending action to {}".format(node_name[0]))
    else:
        node_name = input("Enter the node name: ")
        
    if node_name != "":
        action["node"] = dict(node_name=node_name)
    else:
        node_name == "controller"
    
    
    while not action_name:
        action_name = input("Enter the action name: ")
    action["action"]["action_name"] = action_name
    print()
    
    if not action_params:
        action_params = dict()
        print("Enter the action parameters: ")
        print()
        par = input("\tEnter par name: ")
        while par != "":
            val = input("\tEnter par value: ")
            print()
            try:
                action_params[par]=eval(val)
            except:
                action_params[par]=val
            par = input("\tEnter par name: ")
    action["action"]["action_params"] = action_params 
    
    print("-"*100)
    print("Sending action to {}:\n {}".format(node_name, json.dumps(action, indent=2, sort_keys=True)))
    print("-"*100)
    
    # --- Send REST request
    start = time.time()
    action_resp = send_rest_request(url, action)
    end = time.time()
    LOG.info("Execution time %d", end - start)
    
    return_value = action_resp["return_value"]
    LOG.debug('Action response from node %s: \n%s', action_resp["node"]["node_name"], action_resp["return_value"])
    if return_value.get("error_msg"):
        LOG.info("ERROR: %s", return_value.get("error_msg"))
    if return_value.get("out_msg"):
        LOG.info("OUT: %s", return_value.get("error_msg"))
    if return_value.get("rval"):
        LOG.info("RETURN VALUES: %s", return_value.get("rval"))
    
    return
    
    
def REST_client_command(config = None, *argv, **kwargs):
    """
    FINS Wishful REST simple user interface (GNR) implementation.
    
    Configure from configuration file
        - FINS Configuration
        - Test actions
    
    @param config: configuration parameteres
    @type  config: dict
    """     
    LOG.info("**** FINS Wishful Gnuradio Controller REST client (interactive) ****")  
    
    node_name     = None
    action_name   = None
    action_params = {}
    # ----- Init vars  ----   
    if kwargs is not None:
        node_name     = kwargs.get("node_name")
        action_name   = kwargs.get("action_name")
        action_params = kwargs.get("action_params")
        
    if node_name:
        node_name     = node_name[0]
    if action_name:
        action_name   = action_name[0]
    if action_params:
        action_params = action_params[0]
        if action_params[0]=='[' and action_params[-1]==']':
            params = action_params[1:-1].split(",")
        else:
            # format example '{"names": ["J.J.", "April"], "years": [25, 29]}'
            print(action_params)
            try:
                params = json.loads(action_params)
            except:
                params = action_params

    LOG.debug("--- INPUT PAR....")  
    LOG.debug("pars %s, %s, %s",node_name,action_name, action_params)
    # ----- Loading Wishful Client Configuration  ----
    LOG.debug("--- Loading FINS Configuration....")    
    host_config   = validate_config(config) 
    
    url = 'http://' + host_config["ip"] + ':' + host_config["port"] + '/rest/send_action'
    LOG.debug("Sending actions to RA (id={})".format(host_config["id"]))
    LOG.debug("Using REST interface: {}".format(url))
    
    # ----- Getting action ----
    action = dict(module_id = host_config["id"])            
    if node_name:
        action["node"] = dict(node_name=node_name)
    action["action"] = dict()        
    action["action"]["action_name"] = action_name
    if action_params:
        action["action"]["action_params"] = action_params 

    if action_params:
        action["action"]["action_params"] = params 


    LOG.debug("Sending action to {}:\n {}".format(node_name, json.dumps(action, indent=2, sort_keys=True)))
    
    # --- Send REST request
    start = time.time()
    action_resp = send_rest_request(url, action)
    end = time.time()
    LOG.info("Execution time %d", end - start)
    
    try:
        return_value = action_resp["return_value"]
        if rest_mode:
               #LOG.debug('Action response from node %s: \n%s', action_resp["node"]["node_name"], action_resp["return_value"])
               #if return_value.get("error_msg"):
               #    LOG.info("ERROR: %s", return_value.get("error_msg"))
               #if return_value.get("out_msg"):
               #    LOG.info("OUT: %s", return_value.get("error_msg"))
               #if return_value.get("rval"):
               #    LOG.info("RETURN VALUES: %s", return_value.get("rval"))
               print(json.dumps(return_value))
    except:
        pass
    
    return
# ----------------------------------------------------------------------
#  Main
# ----------------------------------------------------------------------
if __name__ == "__main__":    
    try:
        from docopt import docopt
    except:
        print("""
        Please install docopt using:
            pip3 install docopt==0.6.1
        For more refer to:
        https://github.com/docopt/docopt
        """)
        exit()

    # ---- Process args from command line ----
    args = docopt(__doc__, version=__version__)
    config = None
    
    # ---- Config ----
    config_file   = args.get('--config')        
    action_name   = args.get('--action')
    action_params = args.get('--par')
    node_name     = args['--node']
    int_mode      = args.get('-i')
    file_mode     = args.get('-f')
    if args['--webgui']:
         rest_mode     = True
    else:
         rest_mode     = False
    
    # ---- Logging settings ----  
    if not LOG and  type(LOG) is not logging.Logger: 
        # Create a custom logger
        LOG = logging.getLogger(__name__) 
    
    logfile = args.get('--logfile')        
    try:
        flog_level = getattr(logging, args['--log'])
    except AttributeError as e:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    except:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    
    LOG.warning("Logging to file %s, level set to %s",  logfile, args['--log'])
    
    log_level = logging.getLevelName(LOG_LEVEL)
    if args['--verbose']:
        log_level = logging.DEBUG
    elif args['--quiet']:
        log_level = logging.ERROR  

    # Stream handler
    # If no stream is specified, sys.stderr will be used.
    # c_handler = logging.StreamHandler(stream=sys.stdout)
    c_handler = logging.StreamHandler()
    c_handler.setLevel(flog_level)
    c_format = logging.Formatter(LOG_FORMAT_OUT, LOG_DATE_FORMAT)   
    c_handler.setFormatter(c_format)
    
    # File handler
    f_handler = logging.handlers.RotatingFileHandler(
                  logfile, maxBytes=5000, backupCount=5)
    #f_handler = logging.FileHandler(logfile)
    f_handler.setLevel(log_level)
    f_format = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)
    f_handler.setFormatter(f_format)
    
    # Add handlers to the logger
    LOG.addHandler(c_handler)
    LOG.addHandler(f_handler)     

    
    # ----- Loading configurations from file ---- 
    if config_file is not None and os.path.isfile(config_file):
        LOG.debug("--- Loading configuration from file: <%s>",config_file)
        with open(config_file, 'r') as data_file:
            filename, file_extension = os.path.splitext(config_file)
            if file_extension == ".json":
                config = json.load(data_file) 
            elif file_extension == ".yaml":
                try:
                    import yaml
                except:
                    print("""
                    Please install yaml using:
                        pip3 install pyyaml
                    """)
                    exit()
                config = yaml.safe_load(data_file)
            else:
                LOG.warning("Configuration file type <{}> not supported, using default configuration.".format(file_extension))
    else:
        LOG.warning("--- Warning: configuration file <{}> does not exist, using default configuration.".format(config_file))
    
    # ---- Start FINS Wishful Client ----
    try:
        if int_mode:
            return_value = REST_client_interactive(config, action_name = action_name, action_params = action_params, node_name = node_name)
        elif file_mode:
            return_value = REST_client_from_json(config)
        else:
            return_value = REST_client_command(config, action_name = action_name, action_params = action_params, node_name = node_name)
    except KeyboardInterrupt:
        LOG.info("Keyboard Interrupt,  exiting....")        
    except SystemExit as e:
        LOG.info(e)         
    except:
        raise    
    finally:  
        LOG.info( "Stopping...") 
        # TODO stop all programs ?
