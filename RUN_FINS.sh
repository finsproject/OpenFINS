#!/usr/bin/env bash
sudo chmod 777 fins/scripts/fixed/*.sh
echo "Welcome to the FINS LAUNCHER"
echo ""
echo "FINS provides a configuration tool that can be run to generate a "
echo "configuration file to be used for running the several components of the "
echo "experiment."
echo ""
#echo "Are you interested in using such a feature? [Yes/No]"
#read USE_CONF_GENERATOR
#
#if [[ ${USE_CONF_GENERATOR:0:1} == "Y" ]];
#then
#
#echo "OK, you choose to use the configuration tool."
echo "Now you have to perform some preliminary steps:"
echo ""
echo "STEP 1. CONFIGURATION FILE UPDATE"
echo "Configuration file generation is done by the following python script:"
echo "./fins/conf/config_generator.py"
echo "You need to open it and adjust the component configuration according to"
echo "your experiment"
echo "It is highly recommended to avoid editing field 'used_jfed_vms_file' and"
echo "'jfed_vms_file' in the 'GENERAL SETTING' section during the process."
echo
#echo "When you are done with the editing, press ENTER to proceed to the next step"
#echo
#echo "Press Enter key to proceed..."
#read

echo "STEP 2. UPLOAD THE EXPERIMENT *.mRSpec FILE"
echo "Since you are running your test into IRIS testbed, you should have defined"
echo "it through jFed. In the jFed client, you can download the Experiment rspec"
echo "file in the experiment associated MRSpec tab (Manifest RSpec section)."
echo
echo "NB: If you haven't modified the 'used_jfed_vms_file' and the 'jfed_vms_file'"
echo "params in the GENERAL SETTING section of the config_generator, just follow"
echo "the instructions here below."
echo "OTHERWISE, if you have modified 'jfed_vms_file' remember to save your *RSpec"
echo "file in the path you specified."
echo "If you have set 'used_jfed_vms_file' to 'false', you have disabled the "
echo "*.RSpec file processing support, so you have to update MANUALLY"
echo "config_generator section dedicated to VMS."
echo
echo "Back to the procedure, once you have retrieved that file, please upload it"
echo "as 'experiment.mrspec' into the folder specified in 'jfed_vms_file' param,"
echo " which by default is:"
echo "./fins/confs/tmp/"
echo "NB: DON'T forget to RENAME the file 'experiment.mrspec'"
echo
echo "Press Enter key when you completed the upload: the file will be processed "
echo "and the data in it (mainly jFed node ids and ip addresses) will be used to"
echo "generate the vms description file which is used by FINS to configure its"
echo "components (Experiment Initialiser, Resource Manager and Agents, REST)"
echo
echo "Press Enter key to proceed..."
read
ls ./fins/confs/tmp/
echo
echo "If you have used the default configuration and properly uploaded it, you"
echo "should see listed here above. If it does not appear, please consider to "
echo "abort (Ctrl-C) the procedure and repeat it by running this script again."
echo "If everything is OK, now press Enter key to have the *.RSpec file processed"
echo
#echo "Press Enter key to proceed..."
#read
cd fins/src/support/parser/
python3 rspec_parser.py
cd ../../../../
echo
ls ./fins/confs/tmp/vms_data.cfg
#echo ""
#echo "If the vms data file is there, proceed by pressing Enter key to run the "
#echo "config generator and actually generate the configuration files."
#echo "Otherwise, abort the procedure (Ctrl-C)"
#echo
#echo "Press Enter key to proceed..."
#read
cd fins/confs/
PYTHONPATH=../../ python3 config_generator.py
cd ../../
ls ./fins/confs/*_configuration.cfg
echo ""
echo "Some configuration file some should have appeared listed here above."
echo "If not, abort the procedure (Ctrl-C)"
echo "Otherwise, press Enter key to run the Experiment Initialiser"
echo
echo "Press Enter key to run EI"
read
python3 launcher.py 'FINS'
