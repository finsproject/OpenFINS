#!/usr/bin/env bash

fins_cfg="cfg_FINS_INSTALL_CONFIG.cfg"

. $fins_cfg

if [ -f $FINS_INIT_FILE_NAME ]; then
    echo "INIT FILE $FINS_INIT_FILE_NAME exists: installation already performed"
    read
    exit 1
fi

echo "# fins nstallation $(date)" >> $FINS_INIT_FILE_NAME

if [ -z "${GIT_CLONE_REPO// }" ]; then
    echo "GIT_CLONE_REPO in $fins_cfg is not se, installation aborted: please assign true/false value to it"
    exit 1
else
    if [ $GIT_CLONE_REPO ]; then
        if [ -z "${GIT_REPO// }" ]; then
            echo "git REPOSITORY not specified: please provide one here (without 'https://' part)"
            read GIT_REPO
        fi
        if [ -z "${GIT_BRANCH// }" ]; then
            echo "git BRANCH not specified: by default, all repo will be cloned"
        fi
        if [ -z "${GIT_USERNAME// }" ]; then
            echo "git USERNAME not specified: please provide one here"
            read GIT_USERNAME
        fi
        if [ -z "${GIT_PASSWORD// }" ]; then
            echo "git PASSWORD not specified: please provide one here"
            read GIT_PASSWORD
        fi
        if [ -z "${GIT_FOLDER_NAME// }" ]; then
            echo "FOLDER NAME for the cloned repository not specified: please provide one here"
            read GIT_FOLDER_NAME
        fi

        if [ ${#GIT_USERNAME} -eq 0 ]; then
            FINS_REPO=$GIT_REPO;
        else
            GIT_REPO=${GIT_REPO##"https://"}
            FINS_REPO="https://$GIT_USERNAME:$GIT_PASSWORD@$GIT_REPO"
        fi

        if [ ${#GIT_BRANCH} -eq 0 ]; then
            FINS_BRANCH="";
        else
            GIT_REPO=${GIT_REPO##"https://"}
            FINS_BRANCH="--single-branch -b "$GIT_BRANCH
        fi


        echo "Press Enter to have git cloning $FINS_REPO, branch $GIT_BRANCH into $GIT_FOLDER_NAME"
        echo "cmd: git clone $FINS_BRANCH $FINS_REPO $GIT_FOLDER_NAME"
        read

        sudo apt-get update

        echo "aptget_updated=true" >> $FINS_INIT_FILE_NAME

        sudo apt-get install git -y

        echo "git_installed=true" >> $FINS_INIT_FILE_NAME

        git clone $FINS_BRANCH $FINS_REPO $GIT_FOLDER_NAME
    fi
fi

cd $GIT_FOLDER_NAME
FINS_PATH=$(pwd)

LOG_INTO_INIT=$FINS_PATH"/"$FINS_INIT_FILE_NAME

cp ../$FINS_INIT_FILE_NAME $LOG_INTO_INIT

echo "fins_path=$FINS_PATH" >> $LOG_INTO_INIT

cd fins/scripts/fixed
sudo chmod 777 set_fins_env.sh
./set_fins_env.sh

echo "set_fins_env_run=true" >> $LOG_INTO_INIT

cd -
sudo chmod 777 RUN_FINS.sh
echo ""
echo "Now, verify if everything is configured properly, enter fins folder and then run FINS (RUN_FINS.sh)"