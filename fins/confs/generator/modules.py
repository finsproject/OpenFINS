# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
from fins.confs.generator.basic_fields import StringField, IntField, \
    BoolField, DictField, ListField
from fins.confs.generator.constants import FINSConstants
from fins.confs.generator.validator import Validator
from fins.confs.generator.general import GeneralValidator


class FINSModule:
    def __init__(self, mdl_descriptor, gbl_descriptor=None):

        self.LOG = logging.getLogger(__name__)

        # self.LOG.info(ModuleValidator.KEY__TYPE)
        # self.LOG.info(mdl_descriptor)

        assert ModuleValidator.KEY__TYPE in mdl_descriptor

        mtype = mdl_descriptor[ModuleValidator.KEY__TYPE]

        self._mdl = mdl_descriptor
        self._gbl = gbl_descriptor

        self._validator = None
        if mtype == FINSConstants.TYPE_MOD__EI:
            self._validator = ModuleEIValidator(list(), self._mdl, self._gbl)
        elif mtype == FINSConstants.TYPE_MOD__RM:
            self._validator = ModuleRMValidator(list(), self._mdl, self._gbl)
        elif mtype == FINSConstants.TYPE_MOD__RA or \
            mtype == FINSConstants.TYPE_MOD__RA_ONOS or \
            mtype == FINSConstants.TYPE_MOD__RA_OVS or \
            mtype == FINSConstants.TYPE_MOD__RA_WCTRL:
            self._validator = ModuleRAValidator(list(), self._mdl, self._gbl)
        else:
            self.LOG.warning("Unknown module type (managed as GENERIC module)"
                             ": %s", mtype)
            self._validator = ModuleValidator(list(), self._mdl, self._gbl)

    def get_validated(self):
        if self._validator is None:
            return None
        return self._validator.get_validated()


################################################################################
#      ____                      _        __  __           _       _
#     / ___| ___ _ __   ___ _ __(_) ___  |  \/  | ___   __| |_   _| | ___
#    | |  _ / _ \ '_ \ / _ \ '__| |/ __| | |\/| |/ _ \ / _` | | | | |/ _ \
#    | |_| |  __/ | | |  __/ |  | | (__  | |  | | (_) | (_| | |_| | |  __/
#     \____|\___|_| |_|\___|_|  |_|\___| |_|  |_|\___/ \__,_|\__,_|_|\___|
###
##
#


class ModuleNameField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(ModuleNameField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                if params is not None:
                    if 'mdl' in params:
                        mdl = ModuleValidator(list(), params['mdl'])
                        self.set_value(mdl.get_key(mdl.KEY__TYPE) +
                                       "_" + mdl.get_key(mdl.KEY__ID))
                value = self.value()

            if not super(ModuleNameField, self).validate(value):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class ModuleDescriptionField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(ModuleDescriptionField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                if params is not None:
                    if 'mdl' in params:
                        mdl = ModuleValidator(list(), params['mdl'])
                        self.set_value("(autogenerated description) Module " +
                                       mdl.get_key(mdl.KEY__NAME) +
                                       ": type=" +
                                       mdl.get_key(mdl.KEY__TYPE) +
                                       ", id=" + mdl.get_key(mdl.KEY__ID))
                value = self.value()

            if not super(ModuleDescriptionField, self).validate(value):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class ModuleTypeField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(ModuleTypeField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(ModuleTypeField, self).validate(value):
                return False

            if value not in FINSConstants.MOD_TYPES:
                raise Exception("Invalid module type: " + value +
                                ".\n Valid types: " +
                                str(FINSConstants.MOD_TYPES))

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class ModuleVMField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(ModuleVMField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(ModuleVMField, self).validate(value):
                return False

            if value not in FINSConstants.VM_TYPES:
                if value[:3] == 'VM.':
                    if params is not None:
                        if 'gbl' in params:
                            gbl = GeneralValidator(list(), params['gbl'])
                            vm_number = int(gbl.get_key(gbl.KEY__VM_NUMBER))
                            vm = int(value[3:])
                            if vm >= 0 and vm < vm_number:
                                self.set_value(value)
                            else:
                                raise Exception("Invalid module VM.X format: " +
                                                "X not in [0," +
                                                str(vm_number) + ")")
                        else:
                            raise Exception("VM.X format cannot be applied: " +
                                            "unknown vm total number")
                    else:
                        raise Exception("VM.X format cannot be applied: " +
                                        "unknown vm total number")
                else:
                    raise Exception("Invalid module vm: " + value +
                                    ".\n Valid types: 'VM.X' format or " +
                                    str(FINSConstants.VM_TYPES))

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class ModuleVMWeightField(IntField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(ModuleVMWeightField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(ModuleVMWeightField, self).validate(value):
                return False

            if value < 0:
                self.LOG.warning("Negative vm weight: set to 0 by default")
                value = 0

            elif params is not None:
                if 'gbl' in params:
                    gbl = GeneralValidator(list(), params['gbl'])
                    max_load = int(gbl.get_key(gbl.KEY__VM_MAX_LOAD))
                    if value > max_load:
                        self.LOG.warning("Excessive vm weight: set to max (=" +
                                         str(max_load) + ")")
                        value = max_load
                else:
                    raise Exception("Cannot use vm weight: " +
                                    "unknown vm maximum load")
            else:
                raise Exception("Cannot use vm weight: unknown vm maximum load")

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class ModuleValidator(Validator):
    KEY__NAME = 'name'
    KEY__TYPE = 'type'
    KEY__ID = 'id'
    KEY__VM = 'vm'
    KEY__VM_LOAD = 'vm_weight'
    KEY__DESCRIPTION = 'description'
    KEY__MODULE = 'module'
    KEY__EI_BOOTED = 'ei_booted'
    KEY__EXTRA = 'extra'

    DEFAULT__NAME = None
    DEFAULT__TYPE = None
    DEFAULT__ID = None
    DEFAULT__VM = FINSConstants.TYPE_VM__UBUNTU
    DEFAULT__VM_LOAD = 5
    DEFAULT__DESCRIPTION = None
    DEFAULT__MODULE = ''
    DEFAULT__EI_BOOTED = False
    DEFAULT__EXTRA = dict()

    # _KEYS = [
    #     [KEY__ID, StringField(), True, DEFAULT__ID],
    #     [KEY__TYPE, ModuleTypeField(), True, DEFAULT__TYPE],
    #     [KEY__VM, ModuleVMField(), False, DEFAULT__VM],
    #     [KEY__VM_LOAD, ModuleVMWeightField(), False, DEFAULT__VM_LOAD],
    #     [KEY__NAME, ModuleNameField(), False, DEFAULT__NAME],
    #     [KEY__DESCRIPTION, ModuleDescriptionField(), False,
    #      DEFAULT__DESCRIPTION],
    #     [KEY__MODULE, StringField(), False, DEFAULT__MODULE],
    #     [KEY__EI_BOOTED, BoolField(), False, DEFAULT__EI_BOOTED],
    #     [KEY__EXTRA, DictField(), False, DEFAULT__EXTRA]
    # ]

    def __init__(self, extrakeys=list(), descriptor=None, gbl=None):
        keys = [
            [self.KEY__ID, StringField(), True,
             self.DEFAULT__ID],
            [self.KEY__TYPE, ModuleTypeField(), True,
             self.DEFAULT__TYPE],
            [self.KEY__VM, ModuleVMField(), False,
             self.DEFAULT__VM],
            [self.KEY__VM_LOAD, ModuleVMWeightField(), False,
             self.DEFAULT__VM_LOAD],
            [self.KEY__NAME, ModuleNameField(), False,
             self.DEFAULT__NAME],
            [self.KEY__DESCRIPTION, ModuleDescriptionField(), False,
             self.DEFAULT__DESCRIPTION],
            [self.KEY__MODULE, StringField(), False,
             self.DEFAULT__MODULE],
            [self.KEY__EI_BOOTED, BoolField(), False,
             self.DEFAULT__EI_BOOTED],
            [self.KEY__EXTRA, DictField(), False,
             self.DEFAULT__EXTRA]
        ]
        keys.extend(extrakeys)
        super(ModuleValidator, self).__init__(keys, descriptor)
        self._params = dict(
            gbl=gbl,
            mdl=self._descriptor
        )
        # self.LOG.debug(self._params)


################################################################################
#    _____              ___       _ _   _       _ _
#   | ____|_  ___ __   |_ _|_ __ (_) |_(_) __ _| (_)___  ___ _ __
#   |  _| \ \/ / '_ \   | || '_ \| | __| |/ _` | | / __|/ _ \ '__|
#   | |___ >  <| |_) |  | || | | | | |_| | (_| | | \__ \  __/ |
#   |_____/_/\_\ .__/  |___|_| |_|_|\__|_|\__,_|_|_|___/\___|_|
#              |_|
###
##
#

class ModuleEIValidator(ModuleValidator):
    def __init__(self, extrakeys=list(), descriptor=None, gbl=None):
        keys = list()
        keys.extend(extrakeys)
        super(ModuleEIValidator, self).__init__(keys, descriptor)
        self._params = dict(
            gbl=gbl,
            mdl=self._descriptor
        )

################################################################################
#    ____             __  __
#   |  _ \ ___  ___  |  \/  | __ _ _ __   __ _  __ _  ___ _ __
#   | |_) / _ \/ __| | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
#   |  _ <  __/\__ \ | |  | | (_| | | | | (_| | (_| |  __/ |
#   |_| \_\___||___/ |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|
#                                              |___/
###
##
#

class ModuleRMValidator(ModuleValidator):

    def __init__(self, extrakeys=list(), descriptor=None, gbl=None):
        keys = list()
        keys.extend(extrakeys)
        super(ModuleRMValidator, self).__init__(keys, descriptor)
        self._params = dict(
            gbl=gbl,
            mdl=self._descriptor
        )

################################################################################
#    ____                _                    _
#   |  _ \ ___  ___     / \   __ _  ___ _ __ | |_
#   | |_) / _ \/ __|   / _ \ / _` |/ _ \ '_ \| __|
#   |  _ <  __/\__ \  / ___ \ (_| |  __/ | | | |_
#   |_| \_\___||___/ /_/   \_\__, |\___|_| |_|\__|
#                            |___/
###
##
#

class ModuleRAValidator(ModuleValidator):

    KEY__MANAGED_BY = 'managed_by'

    DEFAULT__MANAGED_BY = None

    _EXTRA_KEYS = [
        [KEY__MANAGED_BY, StringField(), True, DEFAULT__MANAGED_BY],
    ]

    def __init__(self, extrakeys=list(), descriptor=None, gbl=None):
        keys = [
            [self.KEY__MANAGED_BY, StringField(), True,
             self.DEFAULT__MANAGED_BY],
        ]
        keys.extend(extrakeys)
        super(ModuleRAValidator, self).__init__(keys, descriptor)
        self._params = dict(
            gbl=gbl,
            mdl=self._descriptor
        )
