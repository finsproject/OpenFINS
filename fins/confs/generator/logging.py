# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from fins.confs.generator.basic_fields import StringField
from fins.confs.generator.constants import FINSConstants
from fins.confs.generator.validator import Validator


class LoggingFilemodeField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(LoggingFilemodeField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(LoggingFilemodeField, self).validate(value):
                return False

            if value not in FINSConstants.LOG_FILEMODES:
                raise Exception("Invalid logging filemode: " + value +
                                ".\n Valid filemodes: " +
                                str(FINSConstants.LOG_FILEMODES))

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class LoggingLogLevelField(StringField):
    DEFAULT_VALUE = None

    def __init__(self):

        super(LoggingLogLevelField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(LoggingLogLevelField, self).validate(value):
                return False

            if value not in FINSConstants.LOG_LEVELS:
                raise Exception("Invalid logging log level: " + value +
                                ".\n Valid log levels: " +
                                str(FINSConstants.LOG_LEVELS))

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class LoggingValidator(Validator):

    KEY__FILE = 'file'
    KEY__FILEMODE = 'filemode'
    KEY__LOG_LEVEL = 'level'

    DEFAULT__FILE = './fins/logs/fins_default.log'
    DEFAULT__FILEMODE = FINSConstants.FILEMODE_LOG__OVERWRITE
    DEFAULT__LOG_LEVEL = FINSConstants.LEVEL_LOG__DEBUG

    # _KEYS = [
    #     [KEY__FILE, StringField(), False, DEFAULT__FILE],
    #     [KEY__FILEMODE, LoggingFilemodeField(), False, DEFAULT__FILEMODE],
    #     [KEY__LOG_LEVEL, LoggingLogLevelField(), False, DEFAULT__LOG_LEVEL]
    # ]

    def __init__(self, extrakeys=list(), descriptor=None, gbl=None):
        keys = [
            [self.KEY__FILE, StringField(), False,
             self.DEFAULT__FILE],
            [self.KEY__FILEMODE, LoggingFilemodeField(), False,
             self.DEFAULT__FILEMODE],
            [self.KEY__LOG_LEVEL, LoggingLogLevelField(), False,
             self.DEFAULT__LOG_LEVEL]
        ]
        keys.extend(extrakeys)
        super(LoggingValidator, self).__init__(keys, descriptor)
        self._params = dict(
            gbl=gbl,
            lgg=self._descriptor
        )
        # self.LOG.debug(self._params)
