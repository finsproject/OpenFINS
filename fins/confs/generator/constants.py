# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

class FINSConstants:
    TYPE_MOD__EI = 'EI'
    TYPE_MOD__RM = 'RM'
    TYPE_MOD__RA = 'RA'
    TYPE_MOD__RA_ONOS = 'RA.IRIS_ONOS'
    TYPE_MOD__RA_OVS = 'RA.IRIS_OVS'
    TYPE_MOD__RA_WCTRL = 'RA.IRIS_WCTRL'

    MOD_TYPES = [TYPE_MOD__EI,
                 TYPE_MOD__RM,
                 TYPE_MOD__RA,
                 TYPE_MOD__RA_ONOS,
                 TYPE_MOD__RA_OVS,
                 TYPE_MOD__RA_WCTRL]

    TYPE_SRV__RABBITMQ = 'RABBITMQ'
    TYPE_SRV__MONGODB = 'MONGODB'
    TYPE_SRV__FINSREST = 'FINSREST'

    SRV_TYPES = [TYPE_SRV__RABBITMQ,
                 TYPE_SRV__MONGODB,
                 TYPE_SRV__FINSREST]

    TYPE_VM__UBUNTU = 'OS.ubuntu'
    TYPE_VM__USRP_MGR = 'OS.usrp_manager'
    VM_TYPES = [TYPE_VM__UBUNTU, TYPE_VM__USRP_MGR]

    LEVEL_LOG__NOTSET = 'NOTSET'
    LEVEL_LOG__DEBUG = 'DEBUG'
    LEVEL_LOG__INFO = 'INFO'
    LEVEL_LOG__WARNING = 'WARNING'
    LEVEL_LOG__ERROR = 'ERROR'
    LEVEL_LOG__CRITICAL = 'CRITICAL'

    LOG_LEVELS = [LEVEL_LOG__NOTSET,
                  LEVEL_LOG__DEBUG,
                  LEVEL_LOG__INFO,
                  LEVEL_LOG__WARNING,
                  LEVEL_LOG__ERROR,
                  LEVEL_LOG__CRITICAL]

    FILEMODE_LOG__OVERWRITE = 'w'
    FILEMODE_LOG__APPEND = 'a'
    LOG_FILEMODES = [FILEMODE_LOG__OVERWRITE,
                     FILEMODE_LOG__APPEND]

    TYPE_RES__IRIS_VM = 'RES.IRIS_VM'
    TYPE_RES__IRIS_OVS = 'RES.IRIS_OVS'
    TYPE_RES__IRIS_USRP = 'RES.IRIS_USRP'
    TYPE_RES__IRIS_NETCOUPLER = 'RES.IRIS_NETCOUPLER'
    TYPE_RES__IRIS_HYDRA = 'RES.IRIS_HYDRA'
    TYPE_RES__IRIS_HYDRAEMU_NB = 'RES.IRIS_HYDRAEMU_NB'
    TYPE_RES__IRIS_HYDRAEMU_SB_VR = 'RES.IRIS_HYDRAEMU_SB_VR'

    RES_WA_GROUP_NAME__DEFAULT_VALUE = "fins_whishful"
    IRIS_FINS_MAIN_IF__DEFAULT_VALUE = "ens3"

    RES_TYPES = [TYPE_RES__IRIS_VM,
                 TYPE_RES__IRIS_OVS,
                 TYPE_RES__IRIS_USRP,
                 TYPE_RES__IRIS_NETCOUPLER,
                 TYPE_RES__IRIS_HYDRA,
                 TYPE_RES__IRIS_HYDRAEMU_NB,
                 TYPE_RES__IRIS_HYDRAEMU_SB_VR
    ]

    TYPE_RC__ONOS = 'RC.ONOS'
    TYPE_RC__OVS = 'RC.OVS'
    TYPE_RC__WCTRL = 'RC.WISHFUL_CONTROLLER'
    RC_TYPES = [TYPE_RC__ONOS,
                TYPE_RC__OVS,
                TYPE_RC__WCTRL]

    TYPE_LINK__GRE = 'GRE_tunnel'
    TYPE_LINK__SDR = 'SDR_connection'
    TYPE_LINK__SKT = 'socket_connection'
    TYPE_LINK__TAP = 'tap_connection'
    LINK_TYPES = [TYPE_LINK__GRE,
                  TYPE_LINK__SDR,
                  TYPE_LINK__SKT,
                  TYPE_LINK__TAP]

    DIR_LINK__UNI = 'unidirectional'
    DIR_LINK__BI = 'bidirectional'
    LINK_ORIENTATIONS = [DIR_LINK__BI,
                         DIR_LINK__UNI]


