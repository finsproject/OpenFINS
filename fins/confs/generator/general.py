# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from fins.confs.generator.basic_fields import StringField, IntField, BoolField
from fins.confs.generator.validator import Validator


class GeneralValidator(Validator):

    KEY__VM_MAX_LOAD = 'vm_max_load'
    KEY__VM_NUMBER = 'vm_number'
    KEY__STEP_BY_STEP = 'step_by_step'
    KEY__HOSTK_COLLECTOR = 'hostkeys_collector'
    KEY__USED_JFED_VMS_FILE = 'used_jfed_vms_file'
    KEY__JFED_VMS_FILE = 'jfed_vms_file'
    KEY__JFED_USERNAME = 'jfed_username'

    DEFAULT__VM_NUMBER = -1
    DEFAULT__VM_MAX_LOAD = 5
    DEFAULT__STEP_BY_STEP = False
    DEFAULT__HOSTK_COLLECTOR = False
    DEFAULT__USED_JFED_VMS_FILE = True
    DEFAULT__JFED_VMS_FILE = "tmp/vms_data.cfg"
    DEFAULT__JFED_USERNAME = None

    # _KEYS = [
    #     [KEY__VM_NUMBER, IntField(), True, DEFAULT__VM_NUMBER],
    #     [KEY__VM_MAX_LOAD, IntField(), False, DEFAULT__VM_MAX_LOAD],
    #     [KEY__STEP_BY_STEP, BoolField(), False, DEFAULT__STEP_BY_STEP],
    #     [KEY__HOSTK_COLLECTOR, BoolField(), False, DEFAULT__HOSTK_COLLECTOR],
    #     [KEY__USED_JFED_VMS_FILE, BoolField(), False, DEFAULT__USED_JFED_VMS_FILE],
    #     [KEY__JFED_VMS_FILE, StringField(), False, DEFAULT__JFED_VMS_FILE],
    #     [KEY__JFED_USERNAME, StringField(), True, DEFAULT__JFED_USERNAME]
    # ]

    def __init__(self, extrakeys=list(), descriptor=None):
        keys = [
            [self.KEY__VM_NUMBER, IntField(), True,
             self.DEFAULT__VM_NUMBER],
            [self.KEY__VM_MAX_LOAD, IntField(), False,
             self.DEFAULT__VM_MAX_LOAD],
            [self.KEY__STEP_BY_STEP, BoolField(), False,
             self.DEFAULT__STEP_BY_STEP],
            [self.KEY__HOSTK_COLLECTOR, BoolField(), False,
             self.DEFAULT__HOSTK_COLLECTOR],
            [self.KEY__USED_JFED_VMS_FILE, BoolField(), False,
             self.DEFAULT__USED_JFED_VMS_FILE],
            [self.KEY__JFED_VMS_FILE, StringField(), False,
             self.DEFAULT__JFED_VMS_FILE],
            [self.KEY__JFED_USERNAME, StringField(), True,
             self.DEFAULT__JFED_USERNAME]
        ]
        keys.extend(extrakeys)
        super(GeneralValidator, self).__init__(keys, descriptor)
