# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
from abc import ABCMeta, abstractmethod
from fins.confs.generator.basic_fields import StringField


class Validator:
    __metaclass__ = ABCMeta

    # KEY__KEYNAME = 'key'
    #
    # DEFAULT__KEYNAME = "default"
    #
    # _KEYS = [
    #     [KEY__KEYNAME, StringField(), False, DEFAULT__KEYNAME]
    # ]

    def __init__(self, extrakeys, descriptor=None):
        assert isinstance(extrakeys, list)
        keys = []
        keys.extend(extrakeys)
        self._fm = FieldManager(keys)
        self._descriptor = descriptor
        self.LOG = logging.getLogger(__name__)
        self._params = None

    def validate(self, descriptor=None):
        try:
            if descriptor is None:
                descriptor = self._descriptor
            else:
                self._descriptor = descriptor
            d = descriptor

            keys = self._fm.get_keys(True)
            for key in keys:
                if self._fm.is_mandatory(key) and key not in d:
                    raise Exception("Missing mandatory key: " + key)

                elif key not in d:
                    d[key] = self._fm.get_default(key)
                    c = self._fm.get_class(key)
                    # self.LOG.debug("class and key: %s %s", c, key)
                    if not c.validate(d[key], self._params):
                        raise Exception("Default value for key '" + key +
                                        "' is not valid: " + str(d[key]))
                    d[key] = c.value()
                else:
                    c = self._fm.get_class(key)
                    if not c.validate(d[key], self._params):
                        raise Exception("Value for key '" + key +
                                        "' is not valid: " + str(d[key]))
                    d[key] = c.value()
            for key in d:
                if key not in keys:
                    self.LOG.warning("Unknown key '%s' is kept, "
                                     "but not validated: %s", key, str(d[key]))
            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def get_key(self, key):
        if key in self._descriptor:
            return self._descriptor[key]
        else:
            return None

    def get_validated(self, descriptor=None):
        if self.validate(descriptor):
            return self._descriptor
        return None


class FieldManager:

    ID_IDX = 0
    CLASS_IDX = 1
    MANDATORY_IDX = 2
    DEFAULT_IDX = 3

    CLASS_FIELD = "fc"
    MANDATORY_FIELD = "mf"
    DEFAULT_FIELD = "df"

    def __init__(self, fields_description):
        self.LOG = logging.getLogger(__name__)
        fd = fields_description
        self._fd = dict()
        assert isinstance(fd, list)
        for field in fd:
            # self.LOG.debug(field)
            assert isinstance(field, list)
            assert len(field) > 3
            d =dict()
            d[self.CLASS_FIELD] = field[self.CLASS_IDX]
            d[self.MANDATORY_FIELD] = field[self.MANDATORY_IDX]
            d[self.DEFAULT_FIELD] = field[self.DEFAULT_IDX]
            self._fd[field[self.ID_IDX]] = d

    def get_keys(self, as_list=False):
        if as_list:
            return list(self._fd.keys())
        return self._fd.keys()

    def get_class(self, field):
        if field in self._fd.keys():
            return self._fd[field][self.CLASS_FIELD]
        return None

    def get_default(self, field):
        if field in self._fd.keys():
            return self._fd[field][self.DEFAULT_FIELD]
        return None

    def is_mandatory(self, field):
        if field in self._fd.keys():
            return self._fd[field][self.MANDATORY_FIELD]
        return False
