# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
from abc import ABCMeta, abstractmethod


class Field:
    __metaclass__ = ABCMeta

    DEFAULT_VALUE = None

    def __init__(self):
        self._v = self.DEFAULT_VALUE
        self.LOG = logging.getLogger(__name__)

    @abstractmethod
    def validate(self, value, params=None):
        if value is None:
            value = self.value()

        if value is None:
            return False
        self.set_value(value)
        return True

    def value(self):
        return self._v

    def set_value(self, value):
        self._v = value

class StringField(Field):

    DEFAULT_VALUE = ''

    def __init__(self):

        super(StringField, self).__init__()

    def validate(self, value, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(StringField, self).validate(value):
                return False

            if not isinstance(value, str):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class IntField(Field):

    DEFAULT_VALUE = 0

    def __init__(self, value=None):

        super(IntField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(IntField, self).validate(value):
                return False

            if not isinstance(value, int):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class BoolField(Field):
    DEFAULT_VALUE = False

    def __init__(self, value=None):

        super(BoolField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(BoolField, self).validate(value):
                return False

            if not isinstance(value, bool):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False


class DictField(Field):
    DEFAULT_VALUE = False

    def __init__(self, value=None):

        super(DictField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(DictField, self).validate(value):
                return False

            if not isinstance(value, dict):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False

class ListField(Field):
    DEFAULT_VALUE = False

    def __init__(self, value=None):

        super(ListField, self).__init__()

    def validate(self, value=None, params=None):
        try:
            if value is None:
                value = self.value()

            if not super(ListField, self).validate(value):
                return False

            if not isinstance(value, list):
                return False

            self.set_value(value)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False
