# Readme 

## Introduction

This folder contains recipe_libraries for the ```ra_wishful_ctl_module```.
The default path for library recipes is ```./fins/config```, and 
it is stored in ```SliceManager.CONFIG_PATH``` (in the ```fins_radio_slices module```)


## Install

Move files to ```./fins/config``` (if default is used) or to the
directory specified in the  ```SliceManager.CONFIG_PATH```
