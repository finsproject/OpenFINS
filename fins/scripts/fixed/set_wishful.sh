#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y --force-yes netcat
sudo apt-get install -y --force-yes iperf
#sudo apt-get install -y --force-yes vlc
#sudo apt-get install -y --force-yes mplayer
sudo apt-get install -y --force-yes tcpdump
sudo apt-get install -y --force-yes arping
sudo apt-get install -y --force-yes python-pip

sudo pip install --upgrade zmq
sudo pip3 install --upgrade pika
sudo pip3 install --upgrade netifaces
sudo pip3 install --upgrade docopts
sudo pip3 install --upgrade psutil
sudo pip3 install --upgrade gevent