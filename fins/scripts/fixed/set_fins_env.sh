#!/usr/bin/env bash
STARTDATE=$(date)
sudo apt-get update
sudo apt-get install -y --force-yes python3-pip
sudo apt-get install -y --force-yes python-pip
sudo apt-get install -y --force-yes git
sudo apt-get install -y --force-yes dos2unix
sudo apt-get install -y --force-yes build-essential
sudo apt-get install -y --force-yes libssl-dev
sudo apt-get install -y --force-yes libffi-dev
sudo apt-get install -y --force-yes python3-dev
sudo pip3 install --upgrade configobj
sudo pip3 install --upgrade pika==0.13.0b1
sudo pip3 install --upgrade pymongo
sudo pip3 install --upgrade pyYAML
sudo pip3 install --upgrade requests
sudo pip3 install --upgrade tornado==5.1.1
sudo pip3 install --upgrade setuptools
sudo pip3 install --upgrade cryptography
sudo pip3 install --upgrade bcrypt
sudo pip3 install --upgrade pynacl
sudo pip3 install --upgrade paramiko
sudo pip3 install --upgrade docopt==0.6.1
sudo pip install --upgrade zmq
echo "SCRIPT ACTIVITY SUMMARY"
echo 'STARTED @'$STARTDATE
echo 'ENDED   @'$(date)
echo "APT MODULES INSTALLED: python3-pip git dos2unix build-essential libssl-dev libffi-dev python3-dev "
echo "PYTHON MODULES INSTALLED: configobj pika pymongo pyYAML requests tornado setuptools cryptography bcrypt pynacl paramiko docopt"