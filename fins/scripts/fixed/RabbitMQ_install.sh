#!/usr/bin/env bash
STARTDATE=$(date)
sudo apt-get update
sudo apt-get install -y --force-yes python3-pip
sudo apt-get install -y --force-yes erlang-nox
sudo apt-get install -y --force-yes init-system-helpers
sudo apt-get install -y --force-yes socat
sudo apt-get install -y --force-yes adduser
sudo apt-get install -y --force-yes logrotate
sudo apt-get install -y --force-yes rabbitmq-server
echo "SCRIPT ACTIVITY SUMMARY"
echo 'STARTED @'$STARTDATE
echo 'ENDED   @'$(date)
echo "APT MODULES INSTALLED: python3-pip erlang-nox init-system-helpers socat adduser logrotate rabbitmq-server"