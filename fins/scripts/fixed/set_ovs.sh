#!/usr/bin/env bash
if [ -e "/usr/local/share/openvswitch/" ];
then
    sudo /usr/local/share/openvswitch/scripts/ovs-ctl start
    sudo ovs-vsctl --no-wait init
else
    sudo apt-get install -y --force-yes openvswitch-switch
fi

sudo apt-get install -y --force-yes netcat
sudo apt-get install -y --force-yes iperf
#sudo apt-get install -y --force-yes vlc
#sudo apt-get install -y --force-yes mplayer
sudo apt-get install -y --force-yes tcpdump
sudo apt-get install -y --force-yes arping
sudo apt-get install -y --force-yes python-pip
sudo pip install --upgrade zmq