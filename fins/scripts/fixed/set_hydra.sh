#!/usr/bin/env bash

sudo apt-get update

sudo apt-get install -y --force-yes libcanberra-gtk-module
sudo apt-get install -y --force-yes packagekit-gtk3-module

sudo apt-get install -y --force-yes cmake

wget https://raw.githubusercontent.com/jedbrown/cmake-modules/master/FindFFTW.cmake
sudo mv FindFFTW.cmake /usr/share/cmake-3.5/Modules/

git clone https://github.com/maiconkist/gr-hydra.git
cd gr-hydra
mkdir build
cd build
cmake ..
make
sudo make install
sudo ldconfig