#!/usr/bin/env bash
STARTDATE=$(date)
#sudo apt-get update
#sudo apt-get install -y --force-yes python3-pip
#sudo apt-get install -y --force-yes erlang-nox
#sudo apt-get install -y --force-yes init-system-helpers
#sudo apt-get install -y --force-yes socat
#sudo apt-get install -y --force-yes adduser
#sudo apt-get install -y --force-yes logrotate
#sudo apt-get install -y --force-yes rabbitmq-server
sudo service rabbitmq-server start
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server stop
sudo service rabbitmq-server start
sudo rabbitmqctl add_user admin 4dm1n
sudo rabbitmqctl set_user_tags admin administrator
sudo rabbitmqctl set_permissions admin ".*" ".*" ".*"
echo "SCRIPT ACTIVITY SUMMARY"
echo 'STARTED @'$STARTDATE
echo 'ENDED   @'$(date)
echo "APT MODULES INSTALLED: python3-pip erlang-nox init-system-helpers socat adduser logrotate rabbitmq-server"
echo "RabbitMQ Server has been started and admin user configured (password, role and permissions set)"