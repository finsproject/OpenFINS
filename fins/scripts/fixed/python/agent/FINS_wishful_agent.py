#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
FINS_wishful_agent.py

Usage:
    FINS_wishful_agent.py [-q|-v] [--log <LOG_LEVEL>] [--logfile <LOG_FILE>] [--config <CONFIG_FILE>]
    FINS_wishful_agent.py -h | --help
    FINS_wishful_agent.py --version

Options:
    --logfile <LOG_FILE>    Name of the logfile [default: FINS_wishful_agent.log]
    --config <CONFIG_FILE>  Config File (json or yaml) [default: FINS_wishful_config.json]
    --version               Show version and exit
    -h, --help              Show this help message and exit
    -q, --quiet             Quiet (less text)
    -v, --verbose           Verbose (more text)
    --log <LOG_LEVEL>       Log level, valid options: DEBUG, INFO, WARNING, ERROR, CRITICAL [default: DEBUG]  
    
Example:
    ./FINS_wishful_agent -v --config ./config.yaml --logfile ./logfile.log 
"""

__docformat__   = "epytext" # http://epydoc.sourceforge.net/manual-epytext.html
__author__      = "Cristina Costa (CREATE-NET FBK)"
__copyright__   = "Copyright (c) 2018, CREATE-NET FBK"
__version__     = "FINS RA Wishful V.1.0"
__email__       = "ccosta@fbk.eu"

# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
from logging import handlers
import logging
import signal
import sys, os
import yaml, json
import wishful_agent
import psutil, time
# ----------------------------------------------------------------------
#  Defaults
# ----------------------------------------------------------------------
#--- Wishful Agent Default Configuration
DEFAULT_CONF ={

    "agent": {
        "name"          : "default_agent",
        "info"          : "Wishful gnuradio agent",
        "iface"         : "lo",
        "groupName"     : "wishful_1234"
    },
    
    "modules": {
        "discovery": {
            "module":     "wishful_module_discovery_pyre", 
            "class_name": "PyreDiscoveryAgentModule",
            "kwargs": {}
            },
        "gnuradio": {
            "module":     "wishful_module_gnuradio", 
            "class_name": "GnuRadioModule",
            "kwargs": {}
            }
        }
    }
"""
DEFAULT_CONF: 
        
    "agent": Wishful Agent configuration
        "name"      : agent name
        "info"      : agent information
        "iface"     : host interface
        "groupName" : group name for the discovery module

    "controller_modules": Wishful Controller Modules loaded
        "discovery": Discovery Module configuration
        "gnuradio" : Gnuradio Module configuration

    "radio_program_config": Radio Programs configuration parameteres
        "path"               : path for radio program repository 
        "exec_engine"        : execution engine used (e.g. "gnuradio")
        "exec_engine_params" : execution engine parameters, ["program_name", "program_code" , "program_type", "program_port"],
        "program_params"     : list of program parameters, ["parameter_list", "monitor_list","resource_list","tap_ifaces_list"] 
"""

# -------- Logging configuration constants
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(process)d-%(levelname)s-%(message)s'
# LOG_FORMAT          = '%(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
LOG_FORMAT          = '%(asctime)s %(levelname)s: %(name)s.%(funcName)s() - %(message)s'
LOG_FORMAT_OUT      = '%(levelname)s: %(message)s'
LOG_DATE_FORMAT     = '%d-%b-%y %H:%M:%S'
LOG_LEVEL           = 'INFO'

# ----------------------------------------------------------------------
#  Globals
# ----------------------------------------------------------------------
          
LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
agent = None
agent_config   = dict()
modules_config = dict()

# ----------------------------------------------------------------------
#  Support functions
# ----------------------------------------------------------------------
def create_link(protocol, address, port):
    """
    Creates an string for a  protocol url 

    @param protocol: name of the protocol
    @type  protocol: str
    @param address:  address of the server
    @type  address:  str
    @param port:     port number of the server
    @type  port:     int
    @return:         URL
    @rtype:          str
    """
    return protocol + '://' + address + ':' + str(port)
    
def validate_Wishful_agent_config(config):  
    """
    Validates Agent configuration, set defaults if needed
    
    @param config: Agent configuration
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """  
    LOG.info("--- Loading Wishful Agent Configuration....") 
    # ---- Setting default values ----
    validated_config = DEFAULT_CONF["agent"]
    
    try:
        validated_config.update(config["agent"])
    except:
        LOG.info(" -- Wishful Agent configuration set to default:")
    else:
        LOG.info(" -- Wishful Agent configuration loaded from file:")
        
    # ---- Wishful Agent config values ---
    LOG.info("\t-- Name:      %s", validated_config["name"])
    LOG.info("\t-- Info:      %s", str(validated_config["info"]))
    LOG.info("\t-- Interface: %s", str(validated_config["iface"]))
    LOG.info("\t-- Discovery module groupName: %s", validated_config["groupName"])
    
    iface_stats = psutil.net_if_stats().get(validated_config["iface"])
    if iface_stats is None:
        LOG.warning(" -- Warning: Iface %s does not exist", validated_config["iface"])
    elif not iface_stats.isup:
        LOG.warning(" -- Warning: Iface %s is not up", validated_config["iface"])
    
    return validated_config
    
def validate_Wishful_modules_config(agent_config, config=None):
    """
    Validates Whishful Controller configuration,
    set defaults if needed
    
    @param config: Whishful Controller configuration
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """
    LOG.info("--- Loading Wishful Modules Configuration....") 
    
    # ---- Set discovery module default values ---
    default_kwargs = dict()
    default_kwargs["groupName"] = agent_config["groupName"]
    default_kwargs["iface"]     = agent_config["iface"]
    
    validated_config = DEFAULT_CONF["modules"]
    validated_config["discovery"]["kwargs"].update(default_kwargs)
        
    try: config["modules"]
    except:
        LOG.info(" -- Wishful Modules configuration set to default:")
    else:
        validated_config.update(config["modules"])
        LOG.info(" -- Wishful Modules configuration loaded from file:")
        
    for k in validated_config:
        LOG.info("    Loaded %s module", k)
        
    return validated_config
# ----------------------------------------------------------------------
#  FINS Wishful Gnuradio Agent 
# ----------------------------------------------------------------------
    
def FINS_Wishful_Agent(config = None):
    """
    FINS Wishful Agent (GNR) implementation.
    
    Configure controller from configuration file
        - Wishful Agent Configuration
        - Wishful Modules Configuration
    
    @param config: configuration parameteres
    @type  config: dict
    """     
    LOG.info("**** FINS Wishful Gnuradio Agent CONFIG ****")       
     
    # ----- Loading Wishful Agent Configuration  ----
    LOG.debug("--- Loading Wishful Agent Configuration....")    
    agent_config   = validate_Wishful_agent_config(config)  
    modules_config = validate_Wishful_modules_config(agent_config, config)
    
    # ----- Check network configuraiton  ----
    iface_stats = psutil.net_if_stats().get(agent_config["iface"])
    if iface_stats is None:
        LOG.warning(" -- Warning: agent network interface %s does not exist", agent_config["iface"])
        raise SystemExit("SystemExit: Iface {} does not exist or is not up...".format(agent_config["iface"]))
    elif not iface_stats.isup:
        LOG.warning(" -- Warning: agent network interface %s is not up", agent_config["iface"])
        raise SystemExit("SystemExit: Iface {} is not up...".format(agent_config["iface"]))
            
    # ----- Create and configure Wishful Agent  ----
    agent = wishful_agent.Agent()
    agent.set_agent_info(name=agent_config["name"], info=agent_config["info"], iface=agent_config["iface"])

    # ----- Set Up Wishful Agent Modules ----
    for key, value in modules_config.items():
        agent.add_module(moduleName=key, pyModule=value["module"],
                     className=value["class_name"], kwargs=value["kwargs"])
    
    # ---- Start Agent ----
    LOG.info("**** FINS Wishful Gnuradio Agent STARTED ****")  
    LOG.info("Waiting for controller discovery ...")  
    agent.run()

    return

# ----------------------------------------------------------------------
#  Main
# ----------------------------------------------------------------------
if __name__ == "__main__":    
    try:
        from docopt import docopt
    except:
        print("""
        Please install docopt using:
            pip install docopt==0.6.1
        For more refer to:
        https://github.com/docopt/docopt
        """)
        exit()


    # ---- Process args from command line ----
    args = docopt(__doc__, version=__version__)
    
    config = None
    if  args['--config']:
        config_file = args['--config']
    else:
        config_file = CONFIG_FILE
    
    # ---- Logging settings ----  
    logfile = args.get('--logfile')        
    try:
        flog_level = getattr(logging, args['--log'])
    except AttributeError as e:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    except:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    
    LOG.warning("Logging to file %s, level set to %s",  logfile, args['--log'])
    
    log_level = logging.getLevelName(LOG_LEVEL)
    if args['--verbose']:
        log_level = logging.DEBUG
    elif args['--quiet']:
        log_level = logging.ERROR  

    # Stream handler
    # If no stream is specified, sys.stderr will be used.
    # c_handler = logging.StreamHandler(stream=sys.stdout)
    c_handler = logging.StreamHandler()
    c_handler.setLevel(flog_level)
    c_format = logging.Formatter(LOG_FORMAT_OUT, LOG_DATE_FORMAT)   
    c_handler.setFormatter(c_format)
    
    # File handler
    f_handler = logging.handlers.RotatingFileHandler(
                  logfile, maxBytes=5000, backupCount=5)
    #f_handler = logging.FileHandler(logfile)
    f_handler.setLevel(log_level)
    f_format = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)
    f_handler.setFormatter(f_format)

    # Add handlers to the logger
    LOG.addHandler(c_handler)
    #LOG.addHandler(f_handler)     
    logging.basicConfig(filename=logfile, level=log_level, 
                        format  = LOG_FORMAT, 
                        datefmt = LOG_DATE_FORMAT)

    
    # ----- Loading configurations from file ---- 
    if os.path.isfile(config_file):
        LOG.warning("--- Loading configuration from file: <%s>",config_file)
        with open(config_file, 'r') as data_file:
            filename, file_extension = os.path.splitext(config_file)
            if file_extension == ".json":
                config = json.load(data_file) 
            elif file_extension == ".yaml":
                try:
                    import yaml
                except:
                    print("""
                    Please install yaml using:
                        pip install pyyaml
                    """)
                    exit()
                config = yaml.safe_load(data_file)
            else:
                LOG.info("Configuration file type <{}> not supported".format(file_extension))
    else:
        LOG.warning("--- Warning: configuration file <{}> does not exist, using configuration default.".format(config_file))
    
    # ---- Start FINS Wishful Agent ----
    try:
        return_value = FINS_Wishful_Agent(config)
    except KeyboardInterrupt:
        LOG.info("Keyboard Interrupt,  exiting....")   
    except SystemExit as e:
        LOG.info(e)         
    except:
        raise    
    finally:  
        LOG.info( "Stopping...") 
        # TODO stop all programs ?
