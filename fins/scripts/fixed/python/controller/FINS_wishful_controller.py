#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
FINS_Wishful_Controller.py

Usage:
    FINS_Wishful_Controller.py [-q|-v] [--log <LOG_LEVEL>] [--logfile <LOG_FILE>] [--config <CONFIG_FILE>]
    FINS_Wishful_Controller.py -h | --help
    FINS_Wishful_Controller.py --version

Options:
    --logfile <LOG_FILE>    Name of the logfile [default: FINS_wishful_controller.log]
    --config <CONFIG_FILE>  Config File (json or yaml) [default: FINS_wishful_config.json]
    --version               Show version and exit
    -h, --help              Show this help message and exit
    -q, --quiet             Quiet (less text)
    -v, --verbose           Verbose (more text)
    --log <LOG_LEVEL>       Log level, valid options: DEBUG, INFO, WARNING, ERROR, CRITICAL [default: DEBUG]          

Example:
    ./FINS_Wishful_Controller -v --config ./config.yaml 
"""

__docformat__   = "epytext" # http://epydoc.sourceforge.net/manual-epytext.html
__author__      = "Cristina Costa (CREATE-NET FBK)"
__copyright__   = "Copyright (c) 2018, CREATE-NET FBK"
__version__     = "FINS RA Wishful V.1.0"
__email__       = "ccosta@fbk.eu"

# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import sys, os
from logging import handlers
import logging
import json
import gevent, time
import netifaces, psutil

# RabbitMQ imports
import pika

# WiSHFUL imports
import wishful_controller

# FINS imports
from packets import RadioActionPacket

#import datetime
#from itertools import cycle
#import math
#import xmlrpc.client
#import wishful_upis as upis
#import wishful_module_gnuradio
# ----------------------------------------------------------------------
#  Defaults
# ----------------------------------------------------------------------
TIMEOUT = 10
#wishful_agents = {"CU":False, "RU1":False, "RU2":False, "RH1":False, "RH2":False}
wishful_agents = {}
XMLRPC_PORT         = 1234 # default XMLRPC port for gnuradio radio programs

DEFAULT_CONF ={
    "rabbitMQ": {
        "ip"        : "localhost",
        "port"      : 5672,
        "auth"      : {"username":"GNR_NODE","password":"GNR_NODE"},
        "qprefix"   : "qra"
        },
    
    "ra_data": {
        "id"        : "ra_wctl"
        },
        
    "controller": {
        "id"        : "wctrl",
        "name"      : "Controller",
        "info"      : "WiSHFUL Controller",
        "iface"     : "lo",
        "groupName" : "wishful_1234",
        "dl_port"   : "8990",
        "ul_port"   : "8989"
        },

    "modules": {
        "discovery":
            {
            "module":     "wishful_module_discovery_pyre", 
            "class_name": "PyreDiscoveryControllerModule",
            "kwargs": {}
            }
        },

    "radio_program_config": {
        "path"               : "./radio_programs/gnuradio",
        "exec_engine"        : "gnuradio",
        "exec_engine_params" : ["program_name", "program_code" , "program_type", "program_port"],
        "program_params"     : ["parameter_list", "monitor_list","resource_list","tap_ifaces_list"]
        }
}
"""
DEFAULT_CONF: 

    "rabbitMQ": rabbitMQ server configuration
        "ip"        : server ip address
        "port"      : rabbitMQ port e.g. 5672
        "auth"      : rabbitMQ credentials {"username":<user name>,"password":<password>}
        "qprefix"   : queues prefix [default: "qra"], must be the same specified in the RA
        
    "controller": Wishful Controller configuration
        "id"        : controller id
        "name"      : controller name
        "info"      : controller information
        "iface"     : download/upload host interface
        "dl_port"   : downlink port
        "ul_port"   : uplink port
        "groupName" : group name for the discovery module

    "modules": Wishful Controller Modules loaded
        "discovery": Discovery Module configuration

    "radio_program_config": Radio Programs configuration parameteres
        "path"               : path for radio program repository 
        "exec_engine"        : execution engine used (e.g. "gnuradio")
        "exec_engine_params" : execution engine parameters, ["program_name", "program_code" , "program_type", "program_port"],
        "program_params"     : list of program parameters, ["parameter_list", "monitor_list","resource_list","tap_ifaces_list"] 
"""
# -------- Logging configuration constants
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(process)d-%(levelname)s-%(message)s'
# LOG_FORMAT          = '%(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
LOG_FORMAT          = '%(asctime)s %(levelname)s: %(name)s.%(funcName)s() - %(message)s'
LOG_FORMAT_OUT      = '%(levelname)s: %(message)s'
LOG_DATE_FORMAT     = '%d-%b-%y %H:%M:%S'
LOG_LEVEL           = 'INFO'

# -------- Program configuration constants
MIN_NODES_NUMBER    = 1 # minimum number of nodes to be found before starting the experiment loop
SLEEP_TIME          = 2 # sleep time

# ----------------------------------------------------------------------
#  Globals
# ----------------------------------------------------------------------
LOG             = logging.getLogger('pika')
LOG.setLevel(logging.INFO)
LOG             = logging.getLogger()
LOG.setLevel(logging.DEBUG)
controller      = wishful_controller.Controller()
nodes           = []

class NodeInfo:
    RA_ID   = DEFAULT_CONF["ra_data"]["id"]         # default RA Controller ID
    CTRL_ID = DEFAULT_CONF["controller"]["id"]      # default Controller ID
    QPREFIX = DEFAULT_CONF["rabbitMQ"]["qprefix"]   # default prefix for rabbitMQ queues
    
    def __init__ (self, node = None):
        if node is None:
            self.node_obj = None
            self.name     = self.CTRL_ID
        else:
            self.node_obj = node
            self.name     = node.name
        self.from_RA_queue = "{}_from_{}_to_{}_queue".format(self.QPREFIX, self.RA_ID,  self.name)
        self.to_RA_queue   = "{}_from_{}_to_{}_queue".format(self.QPREFIX, self.name,    self.RA_ID)
        self.__queues_declared = False
            
    def __repr__ (self):
        out_msg = "\tNode name   = {}\n".format(self.name)
        if self.node_obj is not None:
            out_msg += "\tNode info   = {}\n".format(self.node_obj.info)
            out_msg += "\tNode id     = {}\n".format(self.node_obj.id)
            out_msg += "\tNode ip     = {}\n".format(self.node_obj.ip)
            out_msg += "\tNode modules     = {}\n".format(self.node_obj.modules)
            out_msg += "\tNode module functions     = {}\n".format(self.node_obj.functions)
            out_msg += "\tNode module generators     = {}\n".format(self.node_obj.generators)
            out_msg += "\tNode interfaces     = {}\n".format(self.node_obj.interfaces)
            out_msg += "\tNode modules_without_iface     = {}\n".format(self.node_obj.modules_without_iface)
        return out_msg

    def get (self):
        return_value = dict()
        return_value["name"] = self.name
        if self.node_obj is not None:
            return_value["info"]                  = self.node_obj.info
            return_value["id"]                    = self.node_obj.id
            return_value["ip"]                    = self.node_obj.ip
            return_value["modules"]               = self.node_obj.modules
            return_value["functions"]             = self.node_obj.functions
            return_value["generators"]            = self.node_obj.generators
            return_value["interfaces"]            = self.node_obj.interfaces
            return_value["modules_without_iface"] = self.node_obj.modules_without_iface
        return return_value

        
    def declare_queues(self, channel):
        LOG.debug("\t-- RabbitMQ channel {}".format(repr(channel)))
        if channel is not None and not self.__queues_declared:
            channel.queue_declare(queue=self.to_RA_queue)
            channel.queue_declare(queue=self.from_RA_queue)
            self.__queues_declared = True
            
    #def delete_queues(self, channel):
        #if channel is not None and self.__queues_declared:
            #channel.queue_delete(queue=self.to_RA_queue)
            #channel.queue_delete(queue=self.from_RA_queue)
            #self.__queues_declared = False
            
class RadioProgramRepo:
    """
    Radio Programs Repository
    
    Istance Attributes:
    radio_programs
    conf_params
    """
    
    def __init__(self, config=dict()):	
        """
        Initialize radio program configuration 

        @param config: program radio list, optional
        @type  config: dict
        """
        LOG.info("--- Init Program Repository ")
        # -------------------
        # Set defaults
        # -------------------
        if config is None:
            config = dict()
            
        self.config_params  = DEFAULT_CONF["radio_program_config"]
        self.radio_programs = dict()
        
        # ---- Load config values ----
        try:
            self.config_params.update(config["radio_program_config"])
        except:
            LOG.info("\t-- Radio program repository path set to default value:")
        else:
            LOG.info("\t-- Radio program repository path set from config file:")
        LOG.info("\t   path: %s" , os.path.abspath(self.config_params["path"]))
        
        # ---- Load programs from config ----
        try:
            for program_name, radio_program in config["radio_programs"].items():
                self.update_radio_program(program_name, radio_program)
        except:
            LOG.debug("\t-- Default programs loaded")
        else:
            LOG.debug("\t-- Radio programs loaded")
            
        # ---- Check if path exists, if not, create it
        if not os.path.exists(self.config_params["path"]):
            os.makedirs(self.config_params["path"])
            LOG.info("\t-- Radio program repository path does not exist, creating it set to default")
                
        return
        
    def update_radio_program(self, program_name, program_config=dict()):	
        """
        Initialize radio program configuration 

        @param program_name:   program name
        @type  program_name:   str
        @param program_config: program radio config
        @type  program_config: dict
        @return:       0 new program added
                       1 program code not loaded
        @rtype:        int
        """
        return_value = 0
        LOG.debug("--- Adding radio program %s to repository", program_name)
        
        program_config = dict((k, program_config[k]) for k in program_config.keys() & (self.config_params["exec_engine_params"] + self.config_params["program_params"]))
        
        if program_name in self.radio_programs:
            program_config['program_port'] = int(program_config.get('program_port',XMLRPC_PORT))
            self.radio_programs[program_name].update(program_config)            
        else:
            program_data = dict()
            program_data.update(dict.fromkeys(self.config_params["exec_engine_params"]))
            program_data.update(dict.fromkeys(self.config_params["program_params"]))
            # program_data['program_args'] = ['',]
            program_data['program_type'] = 'grc'
            program_data['program_port'] = int(XMLRPC_PORT)
            program_data.update(program_config)
            self.radio_programs[program_name] = program_data
        
        if self.radio_programs[program_name]["program_code"] is None:
            try:
                LOG.debug(" -- Program code not present in the repository, loading it from file.")
                self.__load_radio_program(program_name)
            except:
                return_value = 1
                
        return return_value
        
    def delete_radio_program(self, program_name):
        """
        Delete radio program 
        
        @param program_name:   program name
        @type  program_name:   str
        @return:       0 program deleted
                       1 error
        @rtype:        int
        """
        try:
            del self.radio_programs[program_name]
        except:
            return 1
        else:
            return 0 
    
    def get_radio_program_list(self):
        """
        Get radio program list
        
        @return:       program list
        @rtype:        list
        """
        return list(self.radio_programs.keys())
        
        
    def get_radio_program_by_name(self, program_name, config_item=None):
        """
        Get the parameters list for a radio program.
                
        @param program_name: program_name
        @type  program_name: str
        @param config_item:  configuration item
        @type  config_item:  str
        @return:             parameters, None if program_name or config_item not found
        @rtype:              dict(), if config_item None; list or str otherwise
        
        """
        return_value = None
        
        radio_program =  self.radio_programs.get(program_name)
        
        if radio_program is not None:
            # if config_item is None return the program to be executed (retrieve code if needed)
            if config_item is None:
                LOG.info("Getting radio program %s", program_name)
                if radio_program.get("program_code") is None:
                    try:
                        self.__load_radio_program(program_name)
                    except:
                        LOG.info("Couldn't load code for radio program  %s from file", program_name)
                return_value = dict((k, radio_program[k]) for k in self.config_params["exec_engine_params"])
            # loads and return the radio program code
            elif config_item == "program_code":
                LOG.info("Getting radio program %s code", program_name) 
                return_value = radio_program.get("program_code")
                if return_value == None:                   
                    try:
                        return_value = self.__load_radio_program(program_name)
                    except:
                        LOG.info("Couldn't load code for radio program  %s", program_name)
            # returns a value from program_params
            elif config_item in radio_program:
                LOG.info("Getting radio program %s, %s", program_name, config_item)
                return_value = radio_program.get(config_item)
                    
        return return_value
        
    def __load_radio_program(self, program_name):     
        """
        Get the parameters list for a radio program.
                
        @param program_name: program name
        @type  program_name: str
        @return:             program code if successful
                             None if error
        @rtype:              int
        """
        
        radio_program =  self.radio_programs.get(program_name)
        return_value = None
        
        # ---- Load program code from file ----
        if radio_program is not None:      
            LOG.info("--- Loading code for radio program from file <%s.%s>", program_name, radio_program["program_type"])                            
            # ---- Load from file ----
            radio_program.setdefault("program_type", "grc")
            file_name = program_name + "." + radio_program["program_type"]
            cwd = os.getcwd()
            file_name = cwd + '/' + self.config_params["path"] + '/' + file_name   
            
            try:
                with open(file_name, 'rt') as fh:
                    radio_program["program_code"] = fh.read()	
            except:
                raise self.RadioActionError("Couldn't load code, file %s missing or not readable", file_name)
            else:
                return_value = radio_program["program_code"]
                LOG.debug(" -- Loaded program from file %s", file_name)
        else:
            raise self.RadioActionError("Couldn't load code, the program %s does not exist", program_name)	
        
        return return_value
        
    def validate_program_code(self, program_code, program_type):
        """
        Validate radio action and set defaults.
                
        @param program_type: program_type, default None
        @type  program_type: str
        @return:       program code type
        @rtype:        str
        
        """
        if program_code.lstrip().startswith("<?xml"):
            return (program_type == "grc")
        else:
            return (program_type == "py")        
            
# ----------------------------------------------------------------------
#  Wishful Controller Callbacks
# ----------------------------------------------------------------------

@controller.new_node_callback()
def new_node(node_obj):
    """ 
    This function is performed when a new node has been found in the 
    network
    
    @param node_obj: node found
    """
    #for node in nodes:
        #if node.name == node_obj.name:
            #LOG.info("Duplicate node name {}\n node id old {}, node id new {}".format(node.name, node.node_obj.id, node_obj.id))
            #nodes.remove(node)
            #del node 
            #break
            
    node = NodeInfo(node_obj)
    nodes.append(node)	
    if node_obj.name in wishful_agents:
        wishful_agents[node_obj.name] = True
    LOG.info("==> New node appeared:\n{}".format(repr(node)))

@controller.node_exit_callback()
def node_exit(node_obj, reason):
    """ 
    This function is performed when a node, present in the controller 
    node list, leave the experiment. During the experiment, the nodes 
    send "hello packet" to the controller. 
    If the controller do not receives hello packet from a node
    present in the node list, perform this function and the node is 
    been removed
    
    @param node_obj: node leaving the experiment
    @param reason :  exit reason
    """
    # Safety check
    
        
    for node in nodes:
        if node.name == node_obj.name:
            nodes.remove(node)
            #del node #TODO va bene?
            break
    if node_obj.name in wishful_agents:
        wishful_agents[node_obj.name] = False
    LOG.info("NodeExit : NodeID : {} Reason : {}".format(node_obj.id, reason))

# ----------------------------------------------------------------------
#  Support functions
# ----------------------------------------------------------------------

def create_link(protocol, address, port):
    """
    Creates an string for a  protocol url 

    @param protocol: name of the protocol
    @type  protocol: str
    @param address:  address of the server
    @type  address:  str
    @param port:     port number of the server
    @type  port:     int
    @return:         URL
    @rtype:          str
    """
    return protocol + '://' + address + ':' + str(port)
    
def validate_rabbitMQ_config(config):  
    """
    Validates RAbitMQ configuration, set defaults if needed
    
    @param config: RAbitMQ configuration
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """  
    LOG.info("--- Loading RabbitMQ Controller Configuration....") 
    
    # ---- Setting RabbitMQ server default values ----
    validated_config = DEFAULT_CONF["rabbitMQ"]    
    validated_config["ra_id"] = DEFAULT_CONF["ra_data"]["id"]
    validated_config["ctrl_id"] = DEFAULT_CONF["controller"]["id"]
    
    if not config or type(config) is not dict:
        LOG.info(" -- RabbitMQ configuration set to default:")
    else:
        try:
            validated_config["ra_id"]  = config["ra_data"]["id"]
        except:
            LOG.info(" -- RA id set to default value: %s", validated_config["ra_id"])
        else:
            LOG.info(" -- RA id set to: %s", validated_config["ra_id"])
            
        try:
            validated_config["ctrl_id"]  = config["controller"]["id"]
        except:
            LOG.info(" -- Controller id set to default value: %s", validated_config["ctrl_id"])
        else:
            LOG.info(" -- Controller id set to: %s", validated_config["ctrl_id"])
        
        try:
            validated_config.update(config["rabbitMQ"])
        except:
            LOG.info(" -- RabbitMQ configuration set to default:")
        else:
            LOG.info(" -- RabbitMq configuration loaded from file:")
            
        try:
            validated_config["agents"]  = dict()
            for agent in config["agents"]:
                validated_config["agents"][agent] = False
        except:
            validated_config["agents"] = wishful_agents
            LOG.info(" -- Agents set to default value: %s", validated_config["agents"])
        else:
            LOG.info(" --  Agents set to: %s", validated_config["agents"])
        
    # ---- rabbitMQ config values ---
    LOG.info("\t\t-- Server IP: %s", validated_config["ip"])
    LOG.info("\t\t-- Server port: %s", str(validated_config["port"]))
    LOG.info("\t\t-- Credentials: %s", repr(validated_config["auth"]))
    LOG.info("\t\t-- Queue name prefix: %s", validated_config["qprefix"])
    LOG.info("\t\t-- Agents: %s", validated_config["agents"])
    
    return validated_config
    
def validate_Wishful_ctl_config(config=None):
    """
    Validates Whishful Controller configuration,
    set defaults if needed
    
    @param config: Whishful Controller configuration
    @type  config: dict
    @return:       validated configuration
    @rtype:        dict
    """
    LOG.info("--- Loading Wishful Controller Configuration....")
    validated_config = dict()
    
    # ---- Set controller default values ---
    validated_config["controller"] = DEFAULT_CONF["controller"]
    try:
        validated_config["controller"].update(config["controller"])
    except:
        LOG.info(" -- Controller configuration set to default:")
    else:
        LOG.info(" -- Controller configuration loaded from file:")
        
   
    # ---- Check controller values ---
    iface_stats = psutil.net_if_stats().get(validated_config["controller"]["iface"])
    if iface_stats is None or not iface_stats.isup:
        LOG.warning(" -- Warning: Iface %s does not exist or is not up", validated_config["controller"]["iface"])
        
    if 'ip' not in validated_config['controller']:
        assert (validated_config["controller"]["iface"]), "Missing controller network configuration (iface and/or IP address)."
        LOG.debug("Getting the Controller IP from IP on {} interface".format(validated_config["controller"]["iface"]))
        iface_stats = psutil.net_if_stats().get(validated_config["controller"]["iface"])
        if iface_stats is None:
            LOG.warning(" -- Warning: network interface %s does not exist", validated_config["controller"]["iface"])
            raise SystemExit("SystemExit: Iface {} does not exist or is not up...".format(validated_config["controller"]["iface"]))
        elif not iface_stats.isup:
            LOG.warning(" -- Warning: network interface %s is not up", validated_config["controller"]["iface"])
            raise SystemExit("SystemExit: Iface {} is not up...".format(validated_config["controller"]["iface"]))
        validated_config["controller"]["ip"] = netifaces.ifaddresses(validated_config["controller"]["iface"])[netifaces.AF_INET][0]['addr']
        validated_config["controller"]["ip"] = netifaces.ifaddresses(validated_config["controller"]["iface"])[netifaces.AF_INET][0]['addr']
    if 'dl' not in validated_config['controller']:
        validated_config['controller']['dl'] =  create_link('tcp', validated_config["controller"]["ip"], validated_config["controller"]["dl_port"])
    if 'ul' not in validated_config['controller']:
        validated_config['controller']['ul'] =  create_link('tcp', validated_config["controller"]["ip"], validated_config["controller"]["ul_port"])
  
        
    # ---- Set discovery module default values ---
    default_kwargs = dict()
    default_kwargs["iface"]     = validated_config["controller"]["iface"]
    default_kwargs["downlink"]  = validated_config['controller']['dl']
    default_kwargs["uplink"]    = validated_config['controller']['ul']
    default_kwargs["groupName"] = validated_config['controller']['groupName']
    
    validated_config["modules"] = DEFAULT_CONF["modules"] 
    validated_config["modules"]["discovery"]["kwargs"].update(default_kwargs)
    if  config is not None and 'modules' in config:
        validated_config["modules"].update(config["modules"])
        
    # ---- Log config values ---
    LOG.info("\t-- Controller id:        %s", validated_config["controller"]["id"])
    LOG.info("\t-- Controller interface: %s", validated_config["controller"]["iface"])
    LOG.info("\t-- Controller name:      %s", validated_config["controller"]["name"])
    LOG.info("\t-- Controller IP:        %s", validated_config["controller"]["ip"])
    LOG.info("\t-- Controller downlink:  %s", validated_config["controller"]["dl"])
    LOG.info("\t-- Controller uplink:    %s", validated_config["controller"]["ul"])
    LOG.info("\t-- Modules loaded:")
    for k in validated_config["modules"]:
        LOG.info("\t\t- %s", k)
    #LOG.info(json.dumps(validated_config["modules"]))
    
    return validated_config

# ----------------------------------------------------------------------
#  FINS Wishful Controller 
# ----------------------------------------------------------------------
    
def FINS_Wishful_GNR_Controller(config = None):
    """
    FINS Wishful Controller (GNR) implementation.
    
    Configure controller from configuration file
        - Wishful Controler Configuration
        - Wishful Modules Configuration
        - RAbbitMQ Configuration
    
    @param config: configuration parameters 
    @type  config: dict
    """     
    
    
    LOG.info("**** FINS Wishful (GNR) Controller CONFIG ****")      

    # ----- Loading RabbitMQ configuration  ----
    rabbitMQ_config  = dict()    
    rabbitMQ_config  = validate_rabbitMQ_config(config)
    
    # ----- Loading Wishful Controller Configuration  ---- 
    ctl_config = dict() 
    ctl_config = validate_Wishful_ctl_config(config)
    controller.load_config(ctl_config)	  
    
    # ----- Init Program Repository  ----
    radio_programs_repo = RadioProgramRepo(config)
    
    # ----- Init RabbitMQ queues parameters ----
    NodeInfo.RA_ID   = rabbitMQ_config["ra_id"]
    NodeInfo.CTRL_ID = rabbitMQ_config["ctrl_id"]
    NodeInfo.QPREFIX = rabbitMQ_config["qprefix"]
    wishful_agents.update(rabbitMQ_config["agents"])
    
    # ----- Init RabbitMQ controller queue ----
    ctl_node   = NodeInfo()
    nodes.append(ctl_node)
    
    # ----- Connecting RabbitMQ through pika ----
    LOG.info("--- Connecting to RabbitMQ server using pika module....")
    try:
        credentials = pika.PlainCredentials(**rabbitMQ_config["auth"])
        connection  = pika.BlockingConnection(pika.ConnectionParameters(
                           host = rabbitMQ_config["ip"], 
                           port = rabbitMQ_config["port"],
                           socket_timeout = 10,
                           credentials = credentials))
    except:
        raise
    else:
        LOG.info("--- Connected to RabbitMQ, IP address: %s:%d", rabbitMQ_config["ip"], int(rabbitMQ_config["port"]))
                       
    # ----- Setting RabbitMQ channel ----
    try:
        channel = connection.channel()
    except:
        raise
    else:
        LOG.debug("--- RabbitMQ channel created: %s", repr(channel))
    
    # ----- Starting Wishful Controller ----
    LOG.debug("--- Starting Wishful Controller....")    
    for agent in wishful_agents:
        wishful_agents[agent] = False
    try:
        controller.start()  
    except:
        raise
    else:
        LOG.info("**** FINS Wishful (GNR) Controller STARTED ****")      
    
    # ----- Wait for a node to appear ----
    gevent.sleep(10)
        
    # ----- Starting the control LOOP  ----
    LOG.info("--- Starting the EXPERIMENT LOOP....")
    while True:
            LOG.info("Connected nodes: %s", str([ k for k in wishful_agents.keys() if wishful_agents[k] ]))
            success=False
                
            LOG.info("--> Fetching new message from \'%s\' queue:", ctl_node.from_RA_queue)
            method_frame = None
            
            try:
                ctl_node.declare_queues(channel)
                # ----- Get a single message from the AMQP broker.  ----
                method_frame, header_frame, body = channel.basic_get(ctl_node.from_RA_queue)
            except:
                raise
                
            if(method_frame):                                               
                LOG.info("--- New message received from Wishful RA <%s> to node <%s>", rabbitMQ_config["ra_id"], ctl_node.name)
                    
                # ----- Init  ----  
                rval         = None
                error_msg    = None
                return_msg   = None
                
                # ----- Acknowledge received message  ----
                #channel.basic_ack(method_frame.delivery_tag)
                msg = json.loads(json.loads(body.decode('UTF-8')))
                LOG.debug(msg)        
                pkt = RadioActionPacket(msg)
                action = pkt.get_action() 
                action_name   = action.get('action_name')
                action_params = action.get('action_params')
                node          = pkt.get_message().get("node",   dict())
                node_name     = node.get('node_name', NodeInfo.CTRL_ID) 
                     
                # ----- Program actions  ----   
                if action_name: 
                    LOG.info("\t>> Processing action <%s>", action_name)
                    if action_params:
                        LOG.info("\t with params: <%s>", repr(action_params))
                    if node_name:
                        LOG.info("\t on node <%s>...", node_name)
                    else:
                        LOG.info("\t on controller")
                
                # ------------------------------------------------------
                #  Process received message
                # ------------------------------------------------------ 
                if not node_name or node_name == NodeInfo.CTRL_ID:
                    # ----- Controller actions
                    if not action_name:
                        error_msg = "No action specified"
                    elif action_name == "get_nodes":
                        """
                        Action name: get_nodes
                        ===============================
                        Returns connected nodes
                            
                        action_params fields:
                        ---------------------
                        """
                        rval = [ k for k in wishful_agents.keys() if wishful_agents[k] ]
                        if len(rval) > 0:
                            return_msg = "Total nodes found: " + str(len(rval))
                        else:
                            return_msg = "No nodes connected"
                        
                    elif action_name == "add_radio_program":
                        """
                        Action name: add_radio_program
                        ===============================
                        Adds radio program to the repo
                            
                        action_params fields:
                        ---------------------
                            program_name <str>: name of the program to be loaded.
                            program_port <int>: port number of XMLRTP server
                            parameter_list <int>: list of configurable parameters (with set_parameters), if any, default is None
                            monitor_list <int>:   list of monitorable parameters (with get_parameters), if any, default is None
                        """
                        # ---- Check parameters and set defaults ----  
                        try:
                            program_name = action_params["program_name"]
                        except:
                            rval = -1
                            error_msg = "No program name specified"
                        else:
                            rval = radio_programs_repo.update_radio_program(program_name, action_params)    
                            if radio_programs_repo.get_radio_program_by_name(program_name, "program_code") == None:
                                error_msg = "No program code for <{}> is present in the repository".format(str(program_name))
                            return_msg = "Radio program <{}> added to the repository".format(str(program_name))
                        
                    elif action_name == "get_radio_program_list":      
                        """
                        Action name: get_radio_program_list
                        ===================================
                        Gets radio program list.

                        All parameters are passed through the action_params dictionary
                        
                        @param action_params: action dictionary
                        @type  action_params: dict
                        @return:       dictionary with list of program names, 
                                       radio_program_list (iterable).
                        @rtype:        dict
                        
                        """
                        rvals = radio_programs_repo.get_radio_program_list()
                        if rvals == []:
                            return_msg = "No programs in the repository"
                        else:
                            return_msg = "Program list: {}".format(repr(rvals))
                        
                    elif action_name == "get_radio_program":        
                        """
                        Action name: get_radio_program
                        ==============================
                        Gets radio program data.

                        All parameters are passed through the action_params dictionary

                    
                        Keys requested in action dict
                        =============================
                            - program_name <str>: name of the program to be retrived.
                            - get_code <bool>, optional: if True, gets also code, 
                            if stored in the controller. Default is set to False.

                        Raises
                        ======
                            - RadioActionError: if program not found or program_name 
                            not specified.
                        
                        @param action_params: action dictionary
                        @type  action_params: dict
                        @return:       dictionary with radio program data, 
                                       radio_program (dict).
                        @rtype:        dict
                        """
                        # ---- Check parameters and set defaults ----
                        action_params.setdefault("get_code", False)
                        try:
                            program_name     = action_params['program_name']
                        except:
                            rval = -1
                            error_msg = "No program name specified"
                        else:
                            # ---- Get program ----  
                            rval = radio_programs_repo.get_radio_program_by_name(program_name)
                            
                            if rval is None:
                                error_msg = "The program {} does not exist in the repository".format(program_name)
                            else:
                                if not action_params["get_code"]:
                                    rval.pop('program_code', None)
                                
                                return_msg = "Program {} retrieved from repository.".format(program_name)
                        
                    elif action_name == "get_radio_program_info":        
                        """
                        Action name: get_radio_program_info
                        ===================================
                        Gets radio program info.

                        All parameters are passed through the action_params dictionary

                    
                        Keys requested in action dict
                        =============================
                            - program_name <str>: name of the program.
                            - program_info <str>: information requested

                        Raises
                        ======
                            - RadioActionError: if program not found or program_name 
                            not specified.
                        
                        @param action_params: action dictionary
                        @type  action_params: dict
                        @return:       dictionary with radio program data, 
                                       radio_program (dict).
                        @rtype:        dict
                        """
                        # ---- Check parameters and set defaults ----
                        try:
                            program_name     = action_params['program_name']
                        except:
                            rval = -1
                            error_msg = "No program name specified"
                        else:
                            program_info = action_params.get('program_info')
                            rval = radio_programs_repo.get_radio_program_by_name(program_name, program_info)
                            
                            if rval is None:
                                error_msg = "The program {} or {} does not exist in the repository".format(program_name, program_info)
                            else:
                                return_msg = "{} for program {} retrieved from repository.".format(program_info, program_name)
                        
                    elif action_name == "delete_radio_program":        
                        """
                        Action name: delete_radio_program
                        =================================                            
                        Deletes radio program from controller.

                        All parameters are passed through the action_params dictionary

                    
                        Keys requested in action dict
                        =============================
                            - program_name <str>: name of the program to be deleted.

                        Raises
                        ======
                            - RadioActionError: if program not found or program_name 
                            not specified.
                        
                        @param action_params: action dictionary
                        @type  action_params: dict
                        @return:       0, operation sucessful
                                       1, error
                        @rtype:        int
                        """
                        # ---- Check parameters and set defaults ----
                        try:
                            program_name = action_params['program_name']
                        except:
                            rval = -1
                            error_msg = "No program name specified"
                        else:
                            # ---- Delete radio program ---- 
                            if radio_programs_repo.delete_radio_program(program_name):
                                error_msg = "Warning: program {} does not exist in the repository".format(program_name)
                            else:
                                return_msg = "Program {} deleted".format(program_name)
                        
                    elif action_name == "delete_all_radio_programs":        
                        """
                        Action name: delete_all_radio_programs
                        ======================================
                        Deletes all radio programs stored in the controller.

                        
                        @param action_params: action dictionary
                        @type  action_params: dict
                        @return:       dictionary with list of programs delete.
                                       radio_program_list
                        @rtype:        dict
                        
                        """               
                        try:             
                            rval = radio_programs_repo.get_radio_program_list()
                            radio_programs_repo.radio_programs = dict()
                        except:
                            rval = -1
                        
                        return_msg = "All programs deleted from repository"
                        
                    elif action_name == "get_ctrl_config":        
                        """
                        Action name: get_ctrl_config
                        ===================================
                        Send back controller configuration.

                        
                        @return:       dictionary controller configuration
                        @rtype:        dict
                        
                        """               
                        try:             
                            rval = dict()
                            raval["controller"]             = ctl_config
                            raval["rabbitMQ"]               = rabbitMQ_config
                            raval["programs_config_params"] = radio_programs_repo.config_params
                        except:
                            rval = -1
                        
                        return_msg = "Sending controller configuration"
                    elif action_name == "get_node_list":        
                        """
                        Action name: get_ctrl_config
                        ===================================
                        Send back controller configuration.

                        
                        @return:       dictionary controller configuration
                        @rtype:        dict
        
                        """                            
                        rval = []
                        
                        for node in nodes:
                            rval.append(node.name)
                        
                        return_msg = "Sending list of nodes registered in the controller"
                    else:      
                        # ----- Default value returned when radio action is not implemented on node ----
                        rval = None
                        rval = -1
                        error_msg = '>>> Controller action {} not implemented on FINS Wishful Controller'.format(action_name)   
                else:
                    # ----- Node Spefic actions  ----         
                    if node_name in wishful_agents:
                        start_time = time.time()
                        elapsed_time = 0
                        while not wishful_agents[node_name] and elapsed_time < TIMEOUT:
                            elapsed_time = time.time() - start_time
                            LOG.info("Node agent \"%s\" not connected. Waiting %d secs for node to appear...", node_name, TIMEOUT - elapsed_time)
                            gevent.sleep(3)
                        for node in nodes:                       
                            if node.name == node_name:
                                wishful_agents[node_name] = True
                                # ----- Wishful actions  ----                    
                                if not action_name:
                                    error_msg = "No action specified"
                                elif action_name == "get_parameters":
                                    """
                                    Action name: get_parameters
                                    ===========================
                                    Obtain the current radio and MAC radio program configuration 
                                    by getting parameter values.
                                    
                                    action_params fields:
                                    ---------------------
                                        list with parameters names
                                    """            
                                    LOG.info('\t-- Get parameters: {}'.format(action_params))
                                    try:
                                        rval = controller.node(node.node_obj).radio.get_parameters(action_params) 
                                    except Exception as e:
                                        rval = -1
                                        error_msg = repr(e)
                                    except:
                                        rval = -1
                                        error_msg = "Error - {}".format(action_name)
                                    else:
                                        if rval is None:
                                            error_msg = "Warning - {}: no parameters retrieved".format(action_name)
                                        else:
                                            # return_value: dictionary containing the key (string) and value (any) pairs for each parameter.
                                            # If a parameter is not found, the key for that parameter is not present. 
                                            # If no runnnig program is found, returns None
                                            return_msg = "Measured values {}".format(rval)
                                
                                elif action_name == "set_parameters":
                                    """
                                    Action name: set_parameters
                                    ===========================
                                    Set the current radio and MAC radio program 
                                    configuration parameters.
                                    
                                    action_params fields:
                                    ---------------------
                                        dictionary: containing the key (string) and value (any) pairs for each parameter
                                    """                    
                                    LOG.info('\t-- Set parameters: {}'.format(action_params))
                                    try:
                                        rval = controller.node(node.node_obj).radio.set_parameters(action_params)  
                                    except Exception as e:
                                        rval = -1
                                        error_msg = "Error - {}: Unknown variable -> {}".format(action_name, repr(e))
                                    except:
                                        rval = -1
                                        error_msg = "Error - {}".format(action_name)
                                    else:
                                        # return_value: dictionary containing key (string name) error (0 = success, 1=fail, +1=error code) pairs for each parameter.
                                        # (not implemented for gnuradio module)
                                        return_msg = "Set parameters done"
                                
                                elif action_name == "activate_radio_program":
                                    """
                                    Action name: activate_radio_program
                                    ===================================
                                    Stops the current radio program (if any) and enables the execution of the 
                                    radio program specified in the parameter name.
                                    
                                    action_params["params"] fields:
                                    -------------------------------
                                        program_name: name of the radio program 
                                        program_code: (gnuradio m.) code of the radio program 
                                        program_type: (gnuradio m.) default is 'grc'
                                        program_port: (gnuradio m.) XMLRPC server port in radio program,
                                                    the default is default_socket_port
                                    """    
                                    rval = 3 
                                    radio_program = action_params['radio_program']
                                    program_args  = radio_program['program_args']
                                    program_name  = radio_program["program_name"]
                                    radio_programs_repo.update_radio_program(program_name,radio_program)
                                    radio_program = radio_programs_repo.get_radio_program_by_name(program_name)
                                    radio_program['program_args'] = program_args
                                    if radio_program["program_code"] == None:
                                        return_msg = "Warning: no program code for <{}> is present in the controller repository".format(str(program_name))
                                        rval = 1
                                    
                                    LOG.info('\t-- Activate radio program: %s', program_name)
                                    # TODO validate params
                                    try:
                                        rval = controller.node(node.node_obj).radio.activate_radio_program(radio_program)
                                    except FileNotFoundError as e:
                                        # e.g. could not execute grcc compiler for creating the executable file
                                        rval = 4
                                        error_msg = "Error - {} -> {}".format(action_name, repr(e))   
                                        return_msg = "Program {} executable not found on node {} repo (is grcc installed on agent?)".format(radio_program['program_name'], node.name)                            
                                    except TypeError as e:
                                        # e.g. no program_code found present in the agent repo
                                        rval = -1
                                        error_msg = "Error - {} -> {}".format(action_name, repr(e))
                                    except Exception as e:
                                        rval = -1
                                        error_msg = "Error - {} -> {}".format(action_name, repr(e))
                                    except:
                                        rval = -1
                                        error_msg = "Error - {}".format(action_name)
                                    else:
                                        if rval == False:
                                            error_msg = "Error - {} -> failed to start GNURadio program {}".format(action_name, radio_program['program_name'])  
                                            rval = 3
                                        else:
                                            return_msg = "Program {} activated on node {}".format(radio_program['program_name'], node.name)
                                            # rval: variable (int): 0 = success, 1=warning, +1=error code.
                                            # (not implemented for gnuradio)
                                            # e.g error codes:
                                            # 1 Warning: no program code is present in the repository
                                            # 2 Error: program_type error: must be either 'grc' or 'py'
                                            # 3 Error: failed to start GNURadio program or another radio program running on node
                                            # 4 Error: failed to start GNURadio program, program code not present in the agent repo
                                        
                                elif action_name == "deactivate_radio_program":
                                    """
                                    Action name: deactivate_radio_program
                                    =====================================
                                    Stops the radio program specified in the parameter radio_program_name.
                                    
                                    action_params["params"] fields:
                                    -------------------------------
                                        pause <bool>: default value = False (not implemented)
                                        program_name <str>: program to deactivate
                                    """                    
                                    rval = 1
                                    LOG.info('\t-- Deactivate radio program on node %s', node.name)
                                    
                                    if 'program_name' not in action_params:
                                        error_msg = "Program name missing"
                                    else:
                                        pname = action_params["program_name"]                                                                    
                                        try:
                                            rval = controller.node(node.node_obj).radio.deactivate_radio_program(pname)  
                                        except Exception as e:
                                            rval = -1
                                            error_msg = "Error - {}: Unknown variable -> {}".format(action_name, repr(e))
                                        except:
                                            rval = -1
                                            error_msg = "Error - {}".format(action_name)
                                        else:
                                            # return_value: variable (int): 0 = success, 1=fail, +1=error code.
                                            # (not implemented for gnuradio)
                                            # e.g error codes:
                                            # 1 error
                                            # 2 currently running another program
                                            # 3 running or paused radio program; ignore command
                                            if rval is not None and rval > 0:
                                                error_msg = "Error - rval: {}".format(rval)
                                            else:
                                                return_msg = "Program deactivated on node {}:".format(node.name)
                                    
                                elif action_name == "get_running_radio_program":     
                                    """
                                    Action name: get_running_radio_program
                                    =======================================
                                    Get currently running program on node, if any
                                                                    
                                    No parameters expected
                                    """    
                                    rval = None                             
                                    LOG.info('\t-- Get running radio program on node %s', node.name)
                                    try:
                                        rval = controller.node(node.node_obj).radio.get_running_radio_program() 
                                    except Exception as e:
                                        error_msg = repr(e)
                                    except:
                                        error_msg = "Error - {}".format(action_name)
                                    else:
                                        # return_value: name of the running program, returns None if no program is running
                                        if rval is not None:
                                            return_msg = "Program running on node {}: {}".format(node.name, rval)
                                        else:
                                            return_msg = "No program running on node {}".format(node.name)  
                                elif action_name == "get_node_info":     
                                    """
                                    Action name: get_radio_info
                                    =======================================
                                    None info
                                                                    
                                    No parameters expected
                                    """    
                                    LOG.info('\t-- Get node %s info', node.name)
                                    rval = node.get()
                                else:      
                                    # ----- Default value returned when radio action is not implemented on node ----
                                    #rval = -1
                                    rval = None
                                    rval = -1
                                    error_msg = '>>> Radio action {} not implemented on FINS Wishful Controller'.format(action_name)    
                                break
                        else:
                            wishful_agents[node_name] = False
                            rval = -1
                            found = [ k for k in wishful_agents.keys() if wishful_agents[k] ]
                            return_msg = "Nodes found: {} ({}/{})".format(found, len(found), len(wishful_agents))
                            error_msg = "Node agent not connected."
                    else:
                        rval = -1
                        error_msg = "Not an experiment node"
             
                # ----- Acknowledge received message  ----
                channel.basic_ack(method_frame.delivery_tag)
                                        
                LOG.info("--- Action %s processed on node %s.", action_name, node_name)
                
                # ------------------------------------
                #  acknowledge send results back to RA
                # ------------------------------------
                LOG.info("--- Sending reply msg to Wishful Controller RA <%s>", rabbitMQ_config["ra_id"])
                # ----- Build return value  ----
                return_value = dict()
                if rval is not None:
                    return_value["vals"]  = rval
                    LOG.info("\t>> Vals: %s", repr(rval))
                if error_msg is not None:
                    return_value["error_msg"]  = error_msg
                    LOG.info("\t>> Error msg: %s", error_msg)
                if return_msg is not None:
                    return_value["return_msg"] = return_msg
                    LOG.info("\t>> Return msg: %s", return_msg)
                # ----- Build packet  ----
                pkt.set_return_value(return_value)
                pkt.set_sender(rabbitMQ_config["ctrl_id"], True)
                pkt.build_reply(None, None)  
                msg = json.dumps(pkt.get_reply()) 
                #LOG.info(repr(msg))        
                # ----- Send reply  ----
                channel.basic_publish( exchange    = '',
                                       routing_key = ctl_node.to_RA_queue,
                                       body        = msg)
                                                    
            # ----- Wait for new nodes to appear ----
            # LOG.debug("--- Waiting for new nodes to appear...")
            for node in nodes: 
                if node.node_obj is not None:
                    node.node_obj.refresh_hello_timer()
            if not all(wishful_agents.values()):
                LOG.info("Refreshing node list")
                for node, status in wishful_agents.items():
                    if not status:
                        LOG.info("Waiting for node %s to appear....", node)
                gevent.sleep(10)
            else:
                gevent.sleep(2)
    return

# ----------------------------------------------------------------------
#  Main
# ----------------------------------------------------------------------
if __name__ == "__main__":    
    try:
        from docopt import docopt
    except:
        print("""
        Please install docopt using:
            pip install docopt==0.6.1
        For more refer to:
        https://github.com/docopt/docopt
        """)
        exit()
        
    # ---- Process args from command line ----
    args = docopt(__doc__, version=__version__)
    
    config = None
    if  args['--config']:
        config_file = args['--config']
    else:
        config_file = CONFIG_FILE
    
    # ---- Logging settings ----  
    logfile = args.get('--logfile')        
    try:
        flog_level = getattr(logging, args['--log'])
    except AttributeError as e:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    except:
        LOG.warning("Logging level not valid, choose: DEBUG, INFO, WARNING, ERROR, CRITICAL")
        LOG.warning("Logging level set to default [ERROR]")
    
    LOG.warning("Logging to file %s, level set to %s",  logfile, args['--log'])
    
    log_level = logging.getLevelName(LOG_LEVEL)
    if args['--verbose']:
        log_level = logging.DEBUG
    elif args['--quiet']:
        log_level = logging.ERROR  

    # Stream handler
    # If no stream is specified, sys.stderr will be used.
    # c_handler = logging.StreamHandler(stream=sys.stdout)
    c_handler = logging.StreamHandler()
    c_handler.setLevel(log_level)
    c_format = logging.Formatter(LOG_FORMAT_OUT, LOG_DATE_FORMAT)   
    c_handler.setFormatter(c_format)
    
    # File handler
    f_handler = logging.handlers.RotatingFileHandler(
                  logfile, maxBytes=100000, backupCount=5)
    #f_handler = logging.FileHandler(logfile)
    f_handler.setLevel(flog_level)
    f_format = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)
    f_handler.setFormatter(f_format)

    # Add handlers to the logger
    LOG.addHandler(f_handler)     
    LOG.addHandler(c_handler)     
    #logging.basicConfig(filename=logfile, level=log_level, 
                        #format  = LOG_FORMAT, 
                        #datefmt = LOG_DATE_FORMAT)
    
    # ----- Loading configurations from file ----     
    if os.path.isfile(config_file):
        LOG.warning("--- Loading configuration from file: <%s>",config_file)
        with open(config_file, 'r') as data_file:
            filename, file_extension = os.path.splitext(config_file)
            if file_extension == ".json":
                config = json.load(data_file) 
            elif file_extension == ".yaml":
                try:
                    import yaml
                except:
                    print("""
                    Please install yaml using:
                        pip install pyyaml
                    """)
                    exit()
                config = yaml.safe_load(data_file)
            else:
                LOG.info("Configuration file type <{}> not supported".format(file_extension))
    else:
        LOG.warning("--- Warning: configuration file <{}> does not exist, using configuration default.".format(config_file))
        
    # ---- Start FINS Wishful Controller ----
    try:
        return_value = FINS_Wishful_GNR_Controller(config)
    except pika.exceptions.ConnectionClosed as err:
        LOG.error("pika.exceptions.ConnectionClosed: %s", err) 
    except pika.exceptions.AuthenticationError as err:
        LOG.error("pika.exceptions.AuthenticationError: %s", err)
    except pika.exceptions.ProbableAuthenticationError as err:
        LOG.error("pika.exceptions.ProbableAuthenticationError: %s", err)
    except KeyboardInterrupt:
        LOG.info("Keyboard Interrupt, stopping controller....") 
        controller.stop()   
    except SystemExit as e:
        LOG.info(e)         
    except:
        LOG.exception("Error in calling FINS_Wishful_GNR_Controller(), exiting....")     
    finally:
        # TODO stop all programs ?   
        LOG.info( "Stopping...") 
