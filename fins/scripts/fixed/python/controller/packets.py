# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from abc import ABCMeta, abstractmethod
from threading import Lock
import traceback
import logging
import time
import json
import yaml


LOG = logging.getLogger(__name__)


class PacketAbstract:
    __metaclass__ = ABCMeta

    HEADER = "header"
    BODY = "body"

    SENDER = "sender"
    MSG_TYPE = "msg_type"
    MSG_ID = "msg_id"
    RESPONSE_TO = "response_to"
    CREATION_TIME = 'creation_time'

    MSG_TYPE_HELLO = "HELLO"
    MSG_TYPE_RADIO_ACTION = "RADIO_ACTION"
    MSG_TYPE_ACK = "ACK"

    @abstractmethod
    def __init__(self, packet=None):
        if packet is not None:

            assert self.HEADER in packet
            self._header = packet[self.HEADER]

            if 'body' in packet:
                self._body = packet['body']
            else:
                self._body = dict()

        else:
            self._header = dict(
                sender=None,
                msg_type=None,
                msg_id=None,
                response_to=None,
                creation_time=None)
            self._body = dict()

        self._reply_header = dict(
                sender=None,
                msg_type=None,
                msg_id=None,
                response_to=None,
                creation_time=None)
        self._reply_body = dict()

    @abstractmethod
    def build_packet(self, header, body):
        if header is not None:
            for key in header:
                self._header[key] = header[key]
        if body is not None:
            for key in body:
                self._body[key] = body[key]
        self.set_creation_time()

    def get_packet(self):
        # self.validate_header()
        # self.validate_body()
        return dict(header = self._header, body = self._body)

    @abstractmethod
    def build_reply(self, header, body):
        header[self.RESPONSE_TO] = self.get_msg_id()
        if header is not None:
            for key in header:
                self._reply_header[key] = header[key]
        if body is not None:
            for key in body:
                self._reply_body[key] = body[key]
        self.set_creation_time(None, True)

    def get_reply(self):
        # self.validate_header(self._reply_header)
        # self.validate_body(self._reply_body)
        return {'header': self._reply_header, 'body': self._reply_body}

    def validate_header(self, header=None):

        try:
            if header is None:
                header = self._header

            assert self.SENDER in header
            assert isinstance(header[self.SENDER], str)

            assert self.MSG_TYPE in header
            assert isinstance(header[self.MSG_TYPE], str)

            assert self.MSG_ID in header
            assert isinstance(header[self.MSG_ID], str)

            assert self.RESPONSE_TO in header
            assert (header[self.RESPONSE_TO] is None) or \
                   (isinstance(header[self.RESPONSE_TO], str))

            assert self.CREATION_TIME in header
            assert isinstance(header[self.CREATION_TIME], int)

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    @abstractmethod
    def validate_body(self, body=None):

        try:
            if body is None:
                body = self._body

            # Add here validation rules

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    @abstractmethod
    def validate_reply_body(self, body=None):

        try:
            if body is None:
                body = self._reply_body

            # Add here validation rules

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    def get_header(self, reply=False):
        if reply:
            r = self._reply_header
        else:
            r = self._header
        return r

    def get_body(self, reply=False):
        if reply:
            r = self._reply_body
        else:
            r = self._body
        return r

    def get_msg_type(self, reply=False):
        if reply:
            r = self._reply_header[self.MSG_TYPE]
        else:
            r = self._header[self.MSG_TYPE]
        return r

    def get_msg_id(self, reply=False):
        if reply:
            r = self._reply_header[self.MSG_ID]
        else:
            r = self._header[self.MSG_ID]
        return r

    def get_sender(self, reply=False):
        if reply:
            r = self._reply_header[self.SENDER]
        else:
            r = self._header[self.SENDER]
        return r

    def get_response_to(self, reply=False):
        if reply:
            r = self._reply_header[self.RESPONSE_TO]
        else:
            r = self._header[self.RESPONSE_TO]
        return r

    def get_creation_time(self, reply=False):
        if reply:
            r = self._reply_header[self.CREATION_TIME]
        else:
            r = self._header[self.CREATION_TIME]
        return r

    def set_msg_type(self, mtype, reply=False):
        # LOG.debug("type: %s", mtype)
        # LOG.debug("_header: %s", str(self._header))
        if reply:
            self._reply_header[self.MSG_TYPE] = mtype
        else:
            self._header[self.MSG_TYPE] = mtype

    def set_msg_id(self, mid, reply=False):
        if reply:
            self._reply_header[self.MSG_ID] = mid
        else:
            self._header[self.MSG_ID] = mid

    def set_sender(self, sender, reply=False):
        if reply:
            self._reply_header[self.SENDER] = sender
        else:
            self._header[self.SENDER] = sender

    def set_response_to(self, rto=None, reply=False):
        if reply:
            self._reply_header[self.RESPONSE_TO] = rto
        else:
            self._header[self.RESPONSE_TO] = rto

    def set_creation_time(self, ctime=None, reply=False):
        if ctime is None:
            ctime = time.time()
        if reply:
            self._reply_header[self.CREATION_TIME] = ctime
        else:
            self._header[self.CREATION_TIME] = ctime

    def to_string(self):
        return json.dumps({'header': self._header, 'body': self._body},
                          indent=2)


class PacketHandler(object):

    HEADER = "header"
    BODY = "body"

    SENDER = "sender"
    MSG_TYPE = "msg_type"
    MSG_ID = "msg_id"
    RESPONSE_TO = "response_to"
    CREATION_TIME = 'creation_time'

    MSG_TYPE_HELLO = "HELLO"
    MSG_TYPE_RADIO_ACTION = "RADIO_ACTION"
    MSG_TYPE_ACK = "ACK"

    def __init__(self, prefix):
        assert len(prefix) < 16
        self._prefix = prefix
        self._counter = 0
        self._lock = Lock()
        pass

    def get_packet(self, packet_description):
        assert isinstance(packet_description, dict)
        assert self.HEADER in packet_description
        assert self.MSG_TYPE in packet_description[self.HEADER]

        mtype = packet_description[self.HEADER][self.MSG_TYPE]

        if mtype == self.MSG_TYPE_HELLO:
            return HelloPacket(packet_description)

        elif mtype == self.MSG_TYPE_ACK:
            return ACKPacket(packet_description)

        elif mtype == self.MSG_TYPE_RADIO_ACTION:
            return RadioActionPacket(packet_description)

        else:
            raise Exception('Invalid message type: %s', mtype)

    def generate_msg_id(self):
        with self._lock:
            padded_creator = self._prefix.ljust(15, "_")
            padded_number = str(self._counter).rjust(5, "_")
            self._counter = (self._counter+1) % 1000
            return padded_creator+padded_number


class ACKPacket(PacketAbstract):
    def __init__(self, packet=None):
        super(ACKPacket, self).__init__(packet)

        self.set_msg_type(self.MSG_TYPE_ACK)

    def build_packet(self, header, body):
        if header is not None:
            for key in header:
                self._header[key] = header[key]
        if body is not None:
            for key in body:
                self._body[key] = body[key]
        self.set_creation_time()

    def build_reply(self, header, body):
        header[self.RESPONSE_TO] = self.get_msg_id()
        self._reply_header = header
        self._reply_body = body
        self.set_creation_time(None, True)

    def validate_header(self, header=None):
        if super(ACKPacket, self).validate_header(header):
            try:

                assert header[self.RESPONSE_TO] is not None

                return True

            except Exception as e:
                LOG.error("ERROR in header validation: %s", str(e))
                traceback.print_exc()
                return False

        return False

    def validate_body(self, body=None):

        try:
            if body is None:
                body = self._body

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    def validate_reply_body(self, body=None):

        try:
            if body is None:
                body = self._reply_body

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False


class HelloPacket(PacketAbstract):
    def __init__(self, packet=None):
        super(HelloPacket, self).__init__(packet)

        self.set_msg_type(self.MSG_TYPE_HELLO)

    def build_packet(self, header, body):
        if header is not None:
            for key in header:
                self._header[key] = header[key]
        if body is not None:
            for key in body:
                self._body[key] = body[key]
        self.set_creation_time()
        LOG.debug("_header: %s", str(self._header))
        LOG.debug("_body: %s", str(self._body))


    def build_reply(self, header, body):
        if (header is None) or (body is None):
            pkt = ACKPacket().get_packet()
            if header is None:
                header = pkt['header']
            if body is None:
                body = pkt['body']
        header[self.RESPONSE_TO] = self.get_msg_id()
        if header is not None:
            for key in header:
                self._reply_header[key] = header[key]
        if body is not None:
            for key in body:
                self._reply_body[key] = body[key]
        self.set_creation_time(None, True)
        LOG.debug("_reply_header: %s", str(self._reply_header))
        LOG.debug("_reply_body: %s", str(self._reply_body))

    def validate_body(self, body=None):

        try:
            if body is None:
                body = self._body

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    def validate_reply_body(self, body=None):

        try:
            if body is None:
                body = self._reply_body

            return True

        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

#
# RadioActionPacket subclasses here below
#
# Used by ra_wishful_ctl_module.py

class RadioActionPacket(PacketAbstract):    
    def __init__(self, packet=None):
        self.action                  = dict()
        self.action['action_name']   = None
        self.action['action_params'] = dict()
        self.node                    = dict()
        self.node['node_name']       = None
        self.return_value            = dict()
        self.info                    = self.MSG_TYPE_RADIO_ACTION
        super(RadioActionPacket, self).__init__(packet)
        self.set_msg_type(self.MSG_TYPE_RADIO_ACTION)
        if packet is not None:
            if "action" in self._body:
                self.action       = self._body['action']
            if "node" in self._body:
                self.node         = self._body['node']
            if "return_value" in self._body:
                self.return_value = self._body['return_value']
            if "info" in self._body:
                self.info         = self._body['info']
            else:
                if self.action["action_name"] is not None:
                    self.info += "; action name = {}".format(self.action['action_name'])
                if self.node["node_name"] is not None:
                    self.info += "; node name = {}".format(self.node['node_name'])
            
    def __repr__(self):
        return_value = "RadioActionPacket: action name = {}".format(self.action['action_name'])
        if self.node["node_name"] is not None:
            return_value += ", node name = {}".format(self.node['node_name'])
        if self.get_response_to() is None:
            return_value += ", action parameters = {}".format(repr(self.action['params']))
        else:
            return_value += ", return_value = {}".format(repr(self.return_value))
        return return_value
    
    def set_action(self, action=None):
        self.action  = dict()
        self.action['action_name']   = None
        self.action['action_params'] = dict()
        if action is not None:
            if "action_name" in action:
                self.action['action_name'] = action['action_name']
            if "action_params" in action and action['action_params'] is not None:
                self.action['action_params'] = action['action_params']
        self._body['action'] = self.action
        
    def set_node(self, node=None):
        self.node = dict()
        self.node['node_name'] = None
        if node is not None and "node_name" in node:
            self.node['node_name'] = node['node_name']
            
    def set_return_value(self, return_value = None):
        self.return_value  = dict()
        if return_value is not None:
            self.return_value = return_value
        
    def set_info(self, info = None):
        self.info  = "RadioActionPacket"
        if info is not None:
            self.info = info
            
    def get_action(self):
        return self.action
        
    def get_node(self):
        return self.node
        
    def get_return_value(self):
        return self.return_value
        
    def get_info(self):
        return self.info
        
    def get_message(self):
        return dict(action = self.action, node = self.node, return_value = self.return_value)
                
    def build_packet(self, header, body):
        if body is None:   
            body = dict()       
            body['info']   = self.info
            body['node']   = self.node
            body['action'] = self.action
        super(RadioActionPacket, self).build_packet(header, body)
        if body is not None:
            if "action" in self._body:
                self.action       = self._body['action']
            if "node" in self._body:
                self.node         = self._body['node']
            if "return_value" in self._body:
                self.return_value = self._body['return_value']
            if "info" in self._body:
                self.info         = self._body['info']
            else:
                if self.action["action_name"] is not None:
                    self.info += "; action name = {}".format(self.action['action_name'])
                if self.node["node_name"] is not None:
                    self.info += "; node name = {}".format(self.node['node_name'])
        LOG.debug("_header: %s", str(self._header))
        LOG.debug("_body: %s", str(self._body))        
                
    def build_reply(self, header, body):        
        if body is None:
            pkt = ACKPacket().get_packet()
            if header is None:
                header = pkt['header']
            if body is None:
                body = pkt['body'] 
                body['info'] = self.MSG_TYPE_RADIO_ACTION + " REPLAY:" + self.info
                body['return_value'] = self.return_value
                body['node'] = self.node
                body['action'] = dict( action_name = self.action["action_name"] )
       
        header[self.RESPONSE_TO] = self.get_msg_id()
        
        if header is not None:
            for key in header:
                self._reply_header[key] = header[key]
        if body is not None:
            for key in body:
                self._reply_body[key] = body[key]
        self.set_msg_type(self.MSG_TYPE_RADIO_ACTION, True)
        self.set_creation_time(None, True)
        
        LOG.debug("_reply_header: %s", str(self._reply_header))
        LOG.debug("_reply_body: %s", str(self._reply_body))
		
    def validate_body(self, body=None):
        try:
            if body is None:
                body = self._body
            return True
        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False

    def validate_reply_body(self, body=None):
        try:
            if body is None:
                body = self._reply_body
            return True
        except Exception as e:
            LOG.error("ERROR in header validation: %s", str(e))
            traceback.print_exc()
            return False
