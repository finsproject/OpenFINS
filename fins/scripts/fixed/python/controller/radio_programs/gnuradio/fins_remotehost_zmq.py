#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FINS Remote Host (ZMQ push-pull)
# Author: Cristina Costa, FBK CREATE-NET
# Description: v.1.0
# Generated: Tue Jan 22 17:04:22 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import ConfigParser
import SimpleXMLRPCServer
import threading


class fins_remotehost_zmq(gr.top_block):

    def __init__(self, cu="CU", par_cu_host='192.168.5.210', par_cu_port=5000, par_localhost='*', par_mtu=10000, par_packet_len="packet_len", par_ru_port=5100, par_tap="tap0", par_xmlrpcport=1234, par_xmlrpcserver="127.0.0.1", ru="RU1"):
        gr.top_block.__init__(self, "FINS Remote Host (ZMQ push-pull)")

        ##################################################
        # Parameters
        ##################################################
        self.cu = cu
        self.par_cu_host = par_cu_host
        self.par_cu_port = par_cu_port
        self.par_localhost = par_localhost
        self.par_mtu = par_mtu
        self.par_packet_len = par_packet_len
        self.par_ru_port = par_ru_port
        self.par_tap = par_tap
        self.par_xmlrpcport = par_xmlrpcport
        self.par_xmlrpcserver = par_xmlrpcserver
        self.ru = ru

        ##################################################
        # Variables
        ##################################################
        self._ru_port_config = ConfigParser.ConfigParser()
        self._ru_port_config.read('./default')
        try: ru_port = self._ru_port_config.getint('section', "ru_port")
        except: ru_port = par_ru_port
        self.ru_port = ru_port
        self._localhost_config = ConfigParser.ConfigParser()
        self._localhost_config.read('./default')
        try: localhost = self._localhost_config.get(ru, "localhost")
        except: localhost = par_localhost
        self.localhost = localhost
        self._cu_port_config = ConfigParser.ConfigParser()
        self._cu_port_config.read('./default')
        try: cu_port = self._cu_port_config.getint(ru, "cu_port")
        except: cu_port = par_cu_port
        self.cu_port = cu_port
        self._cu_host_config = ConfigParser.ConfigParser()
        self._cu_host_config.read('./default')
        try: cu_host = self._cu_host_config.get(cu, "cu_host")
        except: cu_host = par_cu_host
        self.cu_host = cu_host
        self._tap_config = ConfigParser.ConfigParser()
        self._tap_config.read('./default')
        try: tap = self._tap_config.get(ru, "tap")
        except: tap = par_tap
        self.tap = tap
        self.remoteaddr = remoteaddr = cu_host+":"+str(cu_port)
        self._packet_len_config = ConfigParser.ConfigParser()
        self._packet_len_config.read('./default')
        try: packet_len = self._packet_len_config.get(cu, "packet_len")
        except: packet_len = par_packet_len
        self.packet_len = packet_len
        self._mtu_config = ConfigParser.ConfigParser()
        self._mtu_config.read('default')
        try: mtu = self._mtu_config.getint(ru, 'mtu')
        except: mtu = par_mtu
        self.mtu = mtu
        self.localaddr = localaddr = localhost+":"+str(ru_port)

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_push_sink = zeromq.push_sink(gr.sizeof_char, 1, "tcp://"+localaddr, 100, True, -1)
        self.zeromq_pull_source = zeromq.pull_source(gr.sizeof_char, 1, "tcp://"+remoteaddr, 100, True, -1)
        self.xmlrpc_server = SimpleXMLRPCServer.SimpleXMLRPCServer((par_xmlrpcserver, par_xmlrpcport), allow_none=True)
        self.xmlrpc_server.register_instance(self)
        self.xmlrpc_server_thread = threading.Thread(target=self.xmlrpc_server.serve_forever)
        self.xmlrpc_server_thread.daemon = True
        self.xmlrpc_server_thread.start()
        self.blocks_tuntap_pdu = blocks.tuntap_pdu('tap0', mtu, False)
        self.blocks_tagged_stream_to_pdu = blocks.tagged_stream_to_pdu(blocks.byte_t, packet_len)
        self.blocks_pdu_to_tagged_stream = blocks.pdu_to_tagged_stream(blocks.byte_t, packet_len)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_tagged_stream_to_pdu, 'pdus'), (self.blocks_tuntap_pdu, 'pdus'))    
        self.msg_connect((self.blocks_tuntap_pdu, 'pdus'), (self.blocks_pdu_to_tagged_stream, 'pdus'))    
        self.connect((self.blocks_pdu_to_tagged_stream, 0), (self.zeromq_push_sink, 0))    
        self.connect((self.zeromq_pull_source, 0), (self.blocks_tagged_stream_to_pdu, 0))    

    def get_cu(self):
        return self.cu

    def set_cu(self, cu):
        self.cu = cu
        self._packet_len_config = ConfigParser.ConfigParser()
        self._packet_len_config.read('./default')
        if not self._packet_len_config.has_section(self.cu):
        	self._packet_len_config.add_section(self.cu)
        self._packet_len_config.set(self.cu, "packet_len", str(None))
        self._packet_len_config.write(open('./default', 'w'))
        self._cu_host_config = ConfigParser.ConfigParser()
        self._cu_host_config.read('./default')
        if not self._cu_host_config.has_section(self.cu):
        	self._cu_host_config.add_section(self.cu)
        self._cu_host_config.set(self.cu, "cu_host", str(None))
        self._cu_host_config.write(open('./default', 'w'))

    def get_par_cu_host(self):
        return self.par_cu_host

    def set_par_cu_host(self, par_cu_host):
        self.par_cu_host = par_cu_host
        self.set_cu_host(self.par_cu_host)

    def get_par_cu_port(self):
        return self.par_cu_port

    def set_par_cu_port(self, par_cu_port):
        self.par_cu_port = par_cu_port
        self.set_cu_port(self.par_cu_port)

    def get_par_localhost(self):
        return self.par_localhost

    def set_par_localhost(self, par_localhost):
        self.par_localhost = par_localhost
        self.set_localhost(self.par_localhost)

    def get_par_mtu(self):
        return self.par_mtu

    def set_par_mtu(self, par_mtu):
        self.par_mtu = par_mtu
        self.set_mtu(self.par_mtu)

    def get_par_packet_len(self):
        return self.par_packet_len

    def set_par_packet_len(self, par_packet_len):
        self.par_packet_len = par_packet_len
        self.set_packet_len(self.par_packet_len)

    def get_par_ru_port(self):
        return self.par_ru_port

    def set_par_ru_port(self, par_ru_port):
        self.par_ru_port = par_ru_port
        self.set_ru_port(self.par_ru_port)

    def get_par_tap(self):
        return self.par_tap

    def set_par_tap(self, par_tap):
        self.par_tap = par_tap
        self.set_tap(self.par_tap)

    def get_par_xmlrpcport(self):
        return self.par_xmlrpcport

    def set_par_xmlrpcport(self, par_xmlrpcport):
        self.par_xmlrpcport = par_xmlrpcport

    def get_par_xmlrpcserver(self):
        return self.par_xmlrpcserver

    def set_par_xmlrpcserver(self, par_xmlrpcserver):
        self.par_xmlrpcserver = par_xmlrpcserver

    def get_ru(self):
        return self.ru

    def set_ru(self, ru):
        self.ru = ru
        self._mtu_config = ConfigParser.ConfigParser()
        self._mtu_config.read('default')
        if not self._mtu_config.has_section(self.ru):
        	self._mtu_config.add_section(self.ru)
        self._mtu_config.set(self.ru, 'mtu', str(None))
        self._mtu_config.write(open('default', 'w'))
        self._tap_config = ConfigParser.ConfigParser()
        self._tap_config.read('./default')
        if not self._tap_config.has_section(self.ru):
        	self._tap_config.add_section(self.ru)
        self._tap_config.set(self.ru, "tap", str(None))
        self._tap_config.write(open('./default', 'w'))
        self._localhost_config = ConfigParser.ConfigParser()
        self._localhost_config.read('./default')
        if not self._localhost_config.has_section(self.ru):
        	self._localhost_config.add_section(self.ru)
        self._localhost_config.set(self.ru, "localhost", str(None))
        self._localhost_config.write(open('./default', 'w'))
        self._cu_port_config = ConfigParser.ConfigParser()
        self._cu_port_config.read('./default')
        if not self._cu_port_config.has_section(self.ru):
        	self._cu_port_config.add_section(self.ru)
        self._cu_port_config.set(self.ru, "cu_port", str(None))
        self._cu_port_config.write(open('./default', 'w'))

    def get_ru_port(self):
        return self.ru_port

    def set_ru_port(self, ru_port):
        self.ru_port = ru_port
        self.set_localaddr(self.localhost+":"+str(self.ru_port))

    def get_localhost(self):
        return self.localhost

    def set_localhost(self, localhost):
        self.localhost = localhost
        self.set_localaddr(self.localhost+":"+str(self.ru_port))

    def get_cu_port(self):
        return self.cu_port

    def set_cu_port(self, cu_port):
        self.cu_port = cu_port
        self.set_remoteaddr(self.cu_host+":"+str(self.cu_port))

    def get_cu_host(self):
        return self.cu_host

    def set_cu_host(self, cu_host):
        self.cu_host = cu_host
        self.set_remoteaddr(self.cu_host+":"+str(self.cu_port))

    def get_tap(self):
        return self.tap

    def set_tap(self, tap):
        self.tap = tap

    def get_remoteaddr(self):
        return self.remoteaddr

    def set_remoteaddr(self, remoteaddr):
        self.remoteaddr = remoteaddr

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len

    def get_mtu(self):
        return self.mtu

    def set_mtu(self, mtu):
        self.mtu = mtu

    def get_localaddr(self):
        return self.localaddr

    def set_localaddr(self, localaddr):
        self.localaddr = localaddr


def argument_parser():
    description = 'v.1.0'
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option, description=description)
    parser.add_option(
        "", "--cu", dest="cu", type="string", default="CU",
        help="Set Configuration section in default file for ru1 [default=%default]")
    parser.add_option(
        "", "--par-cu-host", dest="par_cu_host", type="string", default='192.168.5.210',
        help="Set Remote ZMQ cu host [default=%default]")
    parser.add_option(
        "", "--par-cu-port", dest="par_cu_port", type="long", default=5000,
        help="Set ZMQ cu port  [default=%default]")
    parser.add_option(
        "", "--par-localhost", dest="par_localhost", type="string", default='*',
        help="Set Local ZMQ host [default=%default]")
    parser.add_option(
        "", "--par-mtu", dest="par_mtu", type="intx", default=10000,
        help="Set tuntap pdu mtu [default=%default]")
    parser.add_option(
        "", "--par-packet-len", dest="par_packet_len", type="string", default="packet_len",
        help="Set packetlen stream tag [default=%default]")
    parser.add_option(
        "", "--par-ru-port", dest="par_ru_port", type="long", default=5100,
        help="Set ZMQ ru port [default=%default]")
    parser.add_option(
        "", "--par-tap", dest="par_tap", type="string", default="tap0",
        help="Set tap interface for ru [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcport", dest="par_xmlrpcport", type="intx", default=1234,
        help="Set XMLRPC Server port [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcserver", dest="par_xmlrpcserver", type="string", default="127.0.0.1",
        help="Set XMLRPC Server address [default=%default]")
    parser.add_option(
        "", "--ru", dest="ru", type="string", default="RU1",
        help="Set Configuration section in default file for ru1 [default=%default]")
    return parser


def main(top_block_cls=fins_remotehost_zmq, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(cu=options.cu, par_cu_host=options.par_cu_host, par_cu_port=options.par_cu_port, par_localhost=options.par_localhost, par_mtu=options.par_mtu, par_packet_len=options.par_packet_len, par_ru_port=options.par_ru_port, par_tap=options.par_tap, par_xmlrpcport=options.par_xmlrpcport, par_xmlrpcserver=options.par_xmlrpcserver, ru=options.ru)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
