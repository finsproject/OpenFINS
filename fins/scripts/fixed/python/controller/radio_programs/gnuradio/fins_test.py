#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Fins Test
# Generated: Tue Jan 22 17:04:48 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import SimpleXMLRPCServer
import pmt
import threading


class fins_test(gr.top_block):

    def __init__(self, par_tap1="tap1", par_tap2="tap2", program_port=1234):
        gr.top_block.__init__(self, "Fins Test")

        ##################################################
        # Parameters
        ##################################################
        self.par_tap1 = par_tap1
        self.par_tap2 = par_tap2
        self.program_port = program_port

        ##################################################
        # Variables
        ##################################################
        self.tap2 = tap2 = par_tap2
        self.tap1 = tap1 = par_tap1
        self.sync_word2 = sync_word2 = [0, 0, 0, 0, 0, 0, -1, -1, -1, -1, 1, 1, -1, -1, -1, 1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, 0, 1, -1, 1, 1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, -1, 0, 0, 0, 0, 0] 
        self.sync_word1 = sync_word1 = [0., 0., 0., 0., 0., 0., 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 0., 0., 0., 0., 0.]
        self.samprate = samprate = 500000
        self.pilot_symbols = pilot_symbols = ((1, 1, 1, -1,),)
        self.pilot_carriers = pilot_carriers = ((-21, -7, 7, 21,),)
        self.pattern2 = pattern2 = [1, -1, 1, -1]
        self.pattern1 = pattern1 = [0., 1.41421356, 0., -1.41421356]
        self.packet_len = packet_len = "packet_len"
        self.occupied_carriers = occupied_carriers = (range(-26, -21) + range(-20, -7) + range(-6, 0) + range(1, 7) + range(8, 21) + range(22, 27),)
        self.mtu = mtu = 1000
        self.fftlen = fftlen = 64

        ##################################################
        # Blocks
        ##################################################
        self.xmlrpc_server = SimpleXMLRPCServer.SimpleXMLRPCServer(('localhost', program_port), allow_none=True)
        self.xmlrpc_server.register_instance(self)
        self.xmlrpc_server_thread = threading.Thread(target=self.xmlrpc_server.serve_forever)
        self.xmlrpc_server_thread.daemon = True
        self.xmlrpc_server_thread.start()
        self.blocks_message_strobe_0_0 = blocks.message_strobe(pmt.intern("Radio Configuration: \n fftlen: " + str(fftlen) +  "\n samprate: " + str(samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(sync_word1) + "\n sync_word2: " + str(sync_word2) + "\n occupied_carriers: " + str(occupied_carriers)  + "\n pilot_carriers: " + str(pilot_carriers)  + "\n pilot_symbols: " + str(pilot_symbols) +  "\n pattern1: " + str(pattern1) +  "\n pattern2: " + str(pattern2)), 5000)
        self.blocks_message_strobe_0 = blocks.message_strobe(pmt.intern("Packet Configuration:\n MTU: " + str(mtu) + "\n tap1: " + str(tap1) + " tap2: " + str(tap2)  + "\n packet_len: " + str(packet_len)), 5000)
        self.blocks_message_debug_0 = blocks.message_debug()

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_message_strobe_0, 'strobe'), (self.blocks_message_debug_0, 'print'))    
        self.msg_connect((self.blocks_message_strobe_0_0, 'strobe'), (self.blocks_message_debug_0, 'print'))    

    def get_par_tap1(self):
        return self.par_tap1

    def set_par_tap1(self, par_tap1):
        self.par_tap1 = par_tap1
        self.set_tap1(self.par_tap1)

    def get_par_tap2(self):
        return self.par_tap2

    def set_par_tap2(self, par_tap2):
        self.par_tap2 = par_tap2
        self.set_tap2(self.par_tap2)

    def get_program_port(self):
        return self.program_port

    def set_program_port(self, program_port):
        self.program_port = program_port

    def get_tap2(self):
        return self.tap2

    def set_tap2(self, tap2):
        self.tap2 = tap2
        self.blocks_message_strobe_0.set_msg(pmt.intern("Packet Configuration:\n MTU: " + str(self.mtu) + "\n tap1: " + str(self.tap1) + " tap2: " + str(self.tap2)  + "\n packet_len: " + str(self.packet_len)))

    def get_tap1(self):
        return self.tap1

    def set_tap1(self, tap1):
        self.tap1 = tap1
        self.blocks_message_strobe_0.set_msg(pmt.intern("Packet Configuration:\n MTU: " + str(self.mtu) + "\n tap1: " + str(self.tap1) + " tap2: " + str(self.tap2)  + "\n packet_len: " + str(self.packet_len)))

    def get_sync_word2(self):
        return self.sync_word2

    def set_sync_word2(self, sync_word2):
        self.sync_word2 = sync_word2
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_sync_word1(self):
        return self.sync_word1

    def set_sync_word1(self, sync_word1):
        self.sync_word1 = sync_word1
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_samprate(self):
        return self.samprate

    def set_samprate(self, samprate):
        self.samprate = samprate
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_pilot_symbols(self):
        return self.pilot_symbols

    def set_pilot_symbols(self, pilot_symbols):
        self.pilot_symbols = pilot_symbols
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_pilot_carriers(self):
        return self.pilot_carriers

    def set_pilot_carriers(self, pilot_carriers):
        self.pilot_carriers = pilot_carriers
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_pattern2(self):
        return self.pattern2

    def set_pattern2(self, pattern2):
        self.pattern2 = pattern2
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_pattern1(self):
        return self.pattern1

    def set_pattern1(self, pattern1):
        self.pattern1 = pattern1
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.blocks_message_strobe_0.set_msg(pmt.intern("Packet Configuration:\n MTU: " + str(self.mtu) + "\n tap1: " + str(self.tap1) + " tap2: " + str(self.tap2)  + "\n packet_len: " + str(self.packet_len)))

    def get_occupied_carriers(self):
        return self.occupied_carriers

    def set_occupied_carriers(self, occupied_carriers):
        self.occupied_carriers = occupied_carriers
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))

    def get_mtu(self):
        return self.mtu

    def set_mtu(self, mtu):
        self.mtu = mtu
        self.blocks_message_strobe_0.set_msg(pmt.intern("Packet Configuration:\n MTU: " + str(self.mtu) + "\n tap1: " + str(self.tap1) + " tap2: " + str(self.tap2)  + "\n packet_len: " + str(self.packet_len)))

    def get_fftlen(self):
        return self.fftlen

    def set_fftlen(self, fftlen):
        self.fftlen = fftlen
        self.blocks_message_strobe_0_0.set_msg(pmt.intern("Radio Configuration: \n fftlen: " + str(self.fftlen) +  "\n samprate: " + str(self.samprate)  +"\n==================================================\nOFDM configuration:\n sync_word1: " + str(self.sync_word1) + "\n sync_word2: " + str(self.sync_word2) + "\n occupied_carriers: " + str(self.occupied_carriers)  + "\n pilot_carriers: " + str(self.pilot_carriers)  + "\n pilot_symbols: " + str(self.pilot_symbols) +  "\n pattern1: " + str(self.pattern1) +  "\n pattern2: " + str(self.pattern2)))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--par-tap1", dest="par_tap1", type="string", default="tap1",
        help="Set tap interface [default=%default]")
    parser.add_option(
        "", "--par-tap2", dest="par_tap2", type="string", default="tap2",
        help="Set tap interface [default=%default]")
    parser.add_option(
        "", "--program-port", dest="program_port", type="intx", default=1234,
        help="Set xmlrtp port [default=%default]")
    return parser


def main(top_block_cls=fins_test, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(par_tap1=options.par_tap1, par_tap2=options.par_tap2, program_port=options.program_port)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
