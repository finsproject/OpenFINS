#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FINS Central Unit (ZMQ push-pull)
# Author: Cristina Costa, FBK CREATE-NET
# Description: v.1.0
# Generated: Tue Jan 22 17:04:16 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import zeromq
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import ConfigParser
import SimpleXMLRPCServer
import threading


class fins_centralunit_zmq(gr.top_block):

    def __init__(self, cu="CU", par_cu_port0=5000, par_cu_port1=5001, par_localhost='*', par_mtu=10000, par_packet_len="packet_len", par_ru_host0='192.168.5.199', par_ru_host1='192.168.5.202', par_ru_port0=5100, par_ru_port1=5100, par_tap0="tap1", par_tap1="tap2", par_xmlrpcport=1234, par_xmlrpcserver="127.0.0.1", ru0="RU1", ru1="RU2"):
        gr.top_block.__init__(self, "FINS Central Unit (ZMQ push-pull)")

        ##################################################
        # Parameters
        ##################################################
        self.cu = cu
        self.par_cu_port0 = par_cu_port0
        self.par_cu_port1 = par_cu_port1
        self.par_localhost = par_localhost
        self.par_mtu = par_mtu
        self.par_packet_len = par_packet_len
        self.par_ru_host0 = par_ru_host0
        self.par_ru_host1 = par_ru_host1
        self.par_ru_port0 = par_ru_port0
        self.par_ru_port1 = par_ru_port1
        self.par_tap0 = par_tap0
        self.par_tap1 = par_tap1
        self.par_xmlrpcport = par_xmlrpcport
        self.par_xmlrpcserver = par_xmlrpcserver
        self.ru0 = ru0
        self.ru1 = ru1

        ##################################################
        # Variables
        ##################################################
        self._ru_port1_config = ConfigParser.ConfigParser()
        self._ru_port1_config.read('./default')
        try: ru_port1 = self._ru_port1_config.getint(ru1, "ru_port")
        except: ru_port1 = par_ru_port1
        self.ru_port1 = ru_port1
        self._ru_port0_config = ConfigParser.ConfigParser()
        self._ru_port0_config.read('./default')
        try: ru_port0 = self._ru_port0_config.getint(ru0, "ru_port")
        except: ru_port0 = par_ru_port0
        self.ru_port0 = ru_port0
        self._ru_host1_config = ConfigParser.ConfigParser()
        self._ru_host1_config.read('./default')
        try: ru_host1 = self._ru_host1_config.get(ru1, "localhost")
        except: ru_host1 = par_ru_host1
        self.ru_host1 = ru_host1
        self._ru_host0_config = ConfigParser.ConfigParser()
        self._ru_host0_config.read('./default')
        try: ru_host0 = self._ru_host0_config.get(ru0, "localhost")
        except: ru_host0 = par_ru_host0
        self.ru_host0 = ru_host0
        self._localhost_config = ConfigParser.ConfigParser()
        self._localhost_config.read('./default')
        try: localhost = self._localhost_config.get(cu, "localhost")
        except: localhost = par_localhost
        self.localhost = localhost
        self._cu_port1_config = ConfigParser.ConfigParser()
        self._cu_port1_config.read('./default')
        try: cu_port1 = self._cu_port1_config.getint(ru1, "cu_port")
        except: cu_port1 = par_cu_port1
        self.cu_port1 = cu_port1
        self._cu_port0_config = ConfigParser.ConfigParser()
        self._cu_port0_config.read('./default')
        try: cu_port0 = self._cu_port0_config.getint(ru0, "cu_port")
        except: cu_port0 = par_cu_port0
        self.cu_port0 = cu_port0
        self._tap1_config = ConfigParser.ConfigParser()
        self._tap1_config.read('./default')
        try: tap1 = self._tap1_config.get(cu, "tap1")
        except: tap1 = par_tap1
        self.tap1 = tap1
        self._tap0_config = ConfigParser.ConfigParser()
        self._tap0_config.read('./default')
        try: tap0 = self._tap0_config.get(cu, "tap0")
        except: tap0 = par_tap0
        self.tap0 = tap0
        self.remoteaddr1 = remoteaddr1 = ru_host1+":"+str(ru_port1)
        self.remoteaddr0 = remoteaddr0 = ru_host0+":"+str(ru_port0)
        self._packet_len_config = ConfigParser.ConfigParser()
        self._packet_len_config.read('./default')
        try: packet_len = self._packet_len_config.get(cu, "packet_len")
        except: packet_len = par_packet_len
        self.packet_len = packet_len
        self.mtu = mtu = par_mtu
        self.localaddr1 = localaddr1 = localhost+":"+str(cu_port1)
        self.localaddr0 = localaddr0 = localhost+":"+str(cu_port0)

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_push_sink_1 = zeromq.push_sink(gr.sizeof_char, 1, "tcp://"+localaddr1, 100, True, -1)
        self.zeromq_push_sink_0 = zeromq.push_sink(gr.sizeof_char, 1, "tcp://"+localaddr0, 100, True, -1)
        self.zeromq_pull_source_1 = zeromq.pull_source(gr.sizeof_char, 1, "tcp://"+remoteaddr1, 100, True, -1)
        self.zeromq_pull_source_0 = zeromq.pull_source(gr.sizeof_char, 1, "tcp://"+remoteaddr0, 100, True, -1)
        self.xmlrpc_server = SimpleXMLRPCServer.SimpleXMLRPCServer((par_xmlrpcserver, par_xmlrpcport), allow_none=True)
        self.xmlrpc_server.register_instance(self)
        self.xmlrpc_server_thread = threading.Thread(target=self.xmlrpc_server.serve_forever)
        self.xmlrpc_server_thread.daemon = True
        self.xmlrpc_server_thread.start()
        self.blocks_tuntap_pdu_1 = blocks.tuntap_pdu(tap1, mtu, False)
        self.blocks_tuntap_pdu_0 = blocks.tuntap_pdu(tap0, mtu, False)
        self.blocks_tagged_stream_to_pdu_1 = blocks.tagged_stream_to_pdu(blocks.byte_t, packet_len)
        self.blocks_tagged_stream_to_pdu_0 = blocks.tagged_stream_to_pdu(blocks.byte_t, packet_len)
        self.blocks_pdu_to_tagged_stream_1 = blocks.pdu_to_tagged_stream(blocks.byte_t, packet_len)
        self.blocks_pdu_to_tagged_stream_0 = blocks.pdu_to_tagged_stream(blocks.byte_t, packet_len)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_tagged_stream_to_pdu_0, 'pdus'), (self.blocks_tuntap_pdu_0, 'pdus'))    
        self.msg_connect((self.blocks_tagged_stream_to_pdu_1, 'pdus'), (self.blocks_tuntap_pdu_1, 'pdus'))    
        self.msg_connect((self.blocks_tuntap_pdu_0, 'pdus'), (self.blocks_pdu_to_tagged_stream_0, 'pdus'))    
        self.msg_connect((self.blocks_tuntap_pdu_1, 'pdus'), (self.blocks_pdu_to_tagged_stream_1, 'pdus'))    
        self.connect((self.blocks_pdu_to_tagged_stream_0, 0), (self.zeromq_push_sink_0, 0))    
        self.connect((self.blocks_pdu_to_tagged_stream_1, 0), (self.zeromq_push_sink_1, 0))    
        self.connect((self.zeromq_pull_source_0, 0), (self.blocks_tagged_stream_to_pdu_0, 0))    
        self.connect((self.zeromq_pull_source_1, 0), (self.blocks_tagged_stream_to_pdu_1, 0))    

    def get_cu(self):
        return self.cu

    def set_cu(self, cu):
        self.cu = cu
        self._tap1_config = ConfigParser.ConfigParser()
        self._tap1_config.read('./default')
        if not self._tap1_config.has_section(self.cu):
        	self._tap1_config.add_section(self.cu)
        self._tap1_config.set(self.cu, "tap1", str(None))
        self._tap1_config.write(open('./default', 'w'))
        self._tap0_config = ConfigParser.ConfigParser()
        self._tap0_config.read('./default')
        if not self._tap0_config.has_section(self.cu):
        	self._tap0_config.add_section(self.cu)
        self._tap0_config.set(self.cu, "tap0", str(None))
        self._tap0_config.write(open('./default', 'w'))
        self._packet_len_config = ConfigParser.ConfigParser()
        self._packet_len_config.read('./default')
        if not self._packet_len_config.has_section(self.cu):
        	self._packet_len_config.add_section(self.cu)
        self._packet_len_config.set(self.cu, "packet_len", str(None))
        self._packet_len_config.write(open('./default', 'w'))
        self._localhost_config = ConfigParser.ConfigParser()
        self._localhost_config.read('./default')
        if not self._localhost_config.has_section(self.cu):
        	self._localhost_config.add_section(self.cu)
        self._localhost_config.set(self.cu, "localhost", str(None))
        self._localhost_config.write(open('./default', 'w'))

    def get_par_cu_port0(self):
        return self.par_cu_port0

    def set_par_cu_port0(self, par_cu_port0):
        self.par_cu_port0 = par_cu_port0
        self.set_cu_port0(self.par_cu_port0)

    def get_par_cu_port1(self):
        return self.par_cu_port1

    def set_par_cu_port1(self, par_cu_port1):
        self.par_cu_port1 = par_cu_port1
        self.set_cu_port1(self.par_cu_port1)

    def get_par_localhost(self):
        return self.par_localhost

    def set_par_localhost(self, par_localhost):
        self.par_localhost = par_localhost
        self.set_localhost(self.par_localhost)

    def get_par_mtu(self):
        return self.par_mtu

    def set_par_mtu(self, par_mtu):
        self.par_mtu = par_mtu
        self.set_mtu(self.par_mtu)

    def get_par_packet_len(self):
        return self.par_packet_len

    def set_par_packet_len(self, par_packet_len):
        self.par_packet_len = par_packet_len
        self.set_packet_len(self.par_packet_len)

    def get_par_ru_host0(self):
        return self.par_ru_host0

    def set_par_ru_host0(self, par_ru_host0):
        self.par_ru_host0 = par_ru_host0
        self.set_ru_host0(self.par_ru_host0)

    def get_par_ru_host1(self):
        return self.par_ru_host1

    def set_par_ru_host1(self, par_ru_host1):
        self.par_ru_host1 = par_ru_host1
        self.set_ru_host1(self.par_ru_host1)

    def get_par_ru_port0(self):
        return self.par_ru_port0

    def set_par_ru_port0(self, par_ru_port0):
        self.par_ru_port0 = par_ru_port0
        self.set_ru_port0(self.par_ru_port0)

    def get_par_ru_port1(self):
        return self.par_ru_port1

    def set_par_ru_port1(self, par_ru_port1):
        self.par_ru_port1 = par_ru_port1
        self.set_ru_port1(self.par_ru_port1)

    def get_par_tap0(self):
        return self.par_tap0

    def set_par_tap0(self, par_tap0):
        self.par_tap0 = par_tap0
        self.set_tap0(self.par_tap0)

    def get_par_tap1(self):
        return self.par_tap1

    def set_par_tap1(self, par_tap1):
        self.par_tap1 = par_tap1
        self.set_tap1(self.par_tap1)

    def get_par_xmlrpcport(self):
        return self.par_xmlrpcport

    def set_par_xmlrpcport(self, par_xmlrpcport):
        self.par_xmlrpcport = par_xmlrpcport

    def get_par_xmlrpcserver(self):
        return self.par_xmlrpcserver

    def set_par_xmlrpcserver(self, par_xmlrpcserver):
        self.par_xmlrpcserver = par_xmlrpcserver

    def get_ru0(self):
        return self.ru0

    def set_ru0(self, ru0):
        self.ru0 = ru0
        self._ru_port0_config = ConfigParser.ConfigParser()
        self._ru_port0_config.read('./default')
        if not self._ru_port0_config.has_section(self.ru0):
        	self._ru_port0_config.add_section(self.ru0)
        self._ru_port0_config.set(self.ru0, "ru_port", str(None))
        self._ru_port0_config.write(open('./default', 'w'))
        self._ru_host0_config = ConfigParser.ConfigParser()
        self._ru_host0_config.read('./default')
        if not self._ru_host0_config.has_section(self.ru0):
        	self._ru_host0_config.add_section(self.ru0)
        self._ru_host0_config.set(self.ru0, "localhost", str(None))
        self._ru_host0_config.write(open('./default', 'w'))
        self._cu_port0_config = ConfigParser.ConfigParser()
        self._cu_port0_config.read('./default')
        if not self._cu_port0_config.has_section(self.ru0):
        	self._cu_port0_config.add_section(self.ru0)
        self._cu_port0_config.set(self.ru0, "cu_port", str(None))
        self._cu_port0_config.write(open('./default', 'w'))

    def get_ru1(self):
        return self.ru1

    def set_ru1(self, ru1):
        self.ru1 = ru1
        self._ru_port1_config = ConfigParser.ConfigParser()
        self._ru_port1_config.read('./default')
        if not self._ru_port1_config.has_section(self.ru1):
        	self._ru_port1_config.add_section(self.ru1)
        self._ru_port1_config.set(self.ru1, "ru_port", str(None))
        self._ru_port1_config.write(open('./default', 'w'))
        self._ru_host1_config = ConfigParser.ConfigParser()
        self._ru_host1_config.read('./default')
        if not self._ru_host1_config.has_section(self.ru1):
        	self._ru_host1_config.add_section(self.ru1)
        self._ru_host1_config.set(self.ru1, "localhost", str(None))
        self._ru_host1_config.write(open('./default', 'w'))
        self._cu_port1_config = ConfigParser.ConfigParser()
        self._cu_port1_config.read('./default')
        if not self._cu_port1_config.has_section(self.ru1):
        	self._cu_port1_config.add_section(self.ru1)
        self._cu_port1_config.set(self.ru1, "cu_port", str(None))
        self._cu_port1_config.write(open('./default', 'w'))

    def get_ru_port1(self):
        return self.ru_port1

    def set_ru_port1(self, ru_port1):
        self.ru_port1 = ru_port1
        self.set_remoteaddr1(self.ru_host1+":"+str(self.ru_port1))

    def get_ru_port0(self):
        return self.ru_port0

    def set_ru_port0(self, ru_port0):
        self.ru_port0 = ru_port0
        self.set_remoteaddr0(self.ru_host0+":"+str(self.ru_port0))

    def get_ru_host1(self):
        return self.ru_host1

    def set_ru_host1(self, ru_host1):
        self.ru_host1 = ru_host1
        self.set_remoteaddr1(self.ru_host1+":"+str(self.ru_port1))

    def get_ru_host0(self):
        return self.ru_host0

    def set_ru_host0(self, ru_host0):
        self.ru_host0 = ru_host0
        self.set_remoteaddr0(self.ru_host0+":"+str(self.ru_port0))

    def get_localhost(self):
        return self.localhost

    def set_localhost(self, localhost):
        self.localhost = localhost
        self.set_localaddr1(self.localhost+":"+str(self.cu_port1))
        self.set_localaddr0(self.localhost+":"+str(self.cu_port0))

    def get_cu_port1(self):
        return self.cu_port1

    def set_cu_port1(self, cu_port1):
        self.cu_port1 = cu_port1
        self.set_localaddr1(self.localhost+":"+str(self.cu_port1))

    def get_cu_port0(self):
        return self.cu_port0

    def set_cu_port0(self, cu_port0):
        self.cu_port0 = cu_port0
        self.set_localaddr0(self.localhost+":"+str(self.cu_port0))

    def get_tap1(self):
        return self.tap1

    def set_tap1(self, tap1):
        self.tap1 = tap1

    def get_tap0(self):
        return self.tap0

    def set_tap0(self, tap0):
        self.tap0 = tap0

    def get_remoteaddr1(self):
        return self.remoteaddr1

    def set_remoteaddr1(self, remoteaddr1):
        self.remoteaddr1 = remoteaddr1

    def get_remoteaddr0(self):
        return self.remoteaddr0

    def set_remoteaddr0(self, remoteaddr0):
        self.remoteaddr0 = remoteaddr0

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len

    def get_mtu(self):
        return self.mtu

    def set_mtu(self, mtu):
        self.mtu = mtu

    def get_localaddr1(self):
        return self.localaddr1

    def set_localaddr1(self, localaddr1):
        self.localaddr1 = localaddr1

    def get_localaddr0(self):
        return self.localaddr0

    def set_localaddr0(self, localaddr0):
        self.localaddr0 = localaddr0


def argument_parser():
    description = 'v.1.0'
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option, description=description)
    parser.add_option(
        "", "--cu", dest="cu", type="string", default="CU",
        help="Set Configuration section in default file for cu [default=%default]")
    parser.add_option(
        "", "--par-cu-port0", dest="par_cu_port0", type="long", default=5000,
        help="Set ZMQ local port for ru0  [default=%default]")
    parser.add_option(
        "", "--par-cu-port1", dest="par_cu_port1", type="long", default=5001,
        help="Set ZMQ local port for ru0  [default=%default]")
    parser.add_option(
        "", "--par-localhost", dest="par_localhost", type="string", default='*',
        help="Set Local ZMQ host [default=%default]")
    parser.add_option(
        "", "--par-mtu", dest="par_mtu", type="intx", default=10000,
        help="Set tuntap pdu mtu [default=%default]")
    parser.add_option(
        "", "--par-packet-len", dest="par_packet_len", type="string", default="packet_len",
        help="Set packetlen stream tag [default=%default]")
    parser.add_option(
        "", "--par-ru-host0", dest="par_ru_host0", type="string", default='192.168.5.199',
        help="Set ru0 host [default=%default]")
    parser.add_option(
        "", "--par-ru-host1", dest="par_ru_host1", type="string", default='192.168.5.202',
        help="Set ru1 host [default=%default]")
    parser.add_option(
        "", "--par-ru-port0", dest="par_ru_port0", type="long", default=5100,
        help="Set ZMQ port on ru0 [default=%default]")
    parser.add_option(
        "", "--par-ru-port1", dest="par_ru_port1", type="long", default=5100,
        help="Set ZMQ port on ru1 [default=%default]")
    parser.add_option(
        "", "--par-tap0", dest="par_tap0", type="string", default="tap1",
        help="Set tap interface for ru0 [default=%default]")
    parser.add_option(
        "", "--par-tap1", dest="par_tap1", type="string", default="tap2",
        help="Set tap interface for ru1 [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcport", dest="par_xmlrpcport", type="intx", default=1234,
        help="Set XMLRPC Server port [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcserver", dest="par_xmlrpcserver", type="string", default="127.0.0.1",
        help="Set XMLRPC Server address [default=%default]")
    parser.add_option(
        "", "--ru0", dest="ru0", type="string", default="RU1",
        help="Set Configuration section in default file for ru0 [default=%default]")
    parser.add_option(
        "", "--ru1", dest="ru1", type="string", default="RU2",
        help="Set Configuration section in default file for ru1 [default=%default]")
    return parser


def main(top_block_cls=fins_centralunit_zmq, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(cu=options.cu, par_cu_port0=options.par_cu_port0, par_cu_port1=options.par_cu_port1, par_localhost=options.par_localhost, par_mtu=options.par_mtu, par_packet_len=options.par_packet_len, par_ru_host0=options.par_ru_host0, par_ru_host1=options.par_ru_host1, par_ru_port0=options.par_ru_port0, par_ru_port1=options.par_ru_port1, par_tap0=options.par_tap0, par_tap1=options.par_tap1, par_xmlrpcport=options.par_xmlrpcport, par_xmlrpcserver=options.par_xmlrpcserver, ru0=options.ru0, ru1=options.ru1)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
