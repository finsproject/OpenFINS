#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FINS OFDM Radio Link
# Author: Cristina Costa, FBK CREATE-NET
# Description: v.1
# Generated: Tue Jan 22 17:05:03 2019
##################################################

import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from ofdm_modem import ofdm_modem  # grc-generated hier_block
from optparse import OptionParser
import ConfigParser
import SimpleXMLRPCServer
import pmt
import threading
import time


class radio_ofdm(gr.top_block):

    def __init__(self, par_backoff=-15, par_fftlen=64, par_mtu=1000, par_radioconf=2, par_resampling=1, par_rxfreq=2540000000, par_rxgain=0.75, par_samprate=200e3, par_tap="tap0", par_txfreq=2540000000, par_txgain=0.75, par_xmlrpcport=1234, par_xmlrpcserver="127.0.0.1", section='RH'):
        gr.top_block.__init__(self, "FINS OFDM Radio Link")

        ##################################################
        # Parameters
        ##################################################
        self.par_backoff = par_backoff
        self.par_fftlen = par_fftlen
        self.par_mtu = par_mtu
        self.par_radioconf = par_radioconf
        self.par_resampling = par_resampling
        self.par_rxfreq = par_rxfreq
        self.par_rxgain = par_rxgain
        self.par_samprate = par_samprate
        self.par_tap = par_tap
        self.par_txfreq = par_txfreq
        self.par_txgain = par_txgain
        self.par_xmlrpcport = par_xmlrpcport
        self.par_xmlrpcserver = par_xmlrpcserver
        self.section = section

        ##################################################
        # Variables
        ##################################################
        self._uhd_txgain_config = ConfigParser.ConfigParser()
        self._uhd_txgain_config.read('./default')
        try: uhd_txgain = self._uhd_txgain_config.getfloat(section, "txgain")
        except: uhd_txgain = par_txgain
        self.uhd_txgain = uhd_txgain
        self._uhd_rxgain_config = ConfigParser.ConfigParser()
        self._uhd_rxgain_config.read('./default')
        try: uhd_rxgain = self._uhd_rxgain_config.getfloat(section, "rxgain")
        except: uhd_rxgain = par_rxgain
        self.uhd_rxgain = uhd_rxgain
        self._txfreq_config = ConfigParser.ConfigParser()
        self._txfreq_config.read('./default')
        try: txfreq = self._txfreq_config.getfloat(section, "txfreq")
        except: txfreq = par_txfreq
        self.txfreq = txfreq
        self.tap = tap = par_tap
        self._samprate_config = ConfigParser.ConfigParser()
        self._samprate_config.read('./default')
        try: samprate = self._samprate_config.getfloat(section, "samprate")
        except: samprate = par_samprate
        self.samprate = samprate
        self._rxfreq_config = ConfigParser.ConfigParser()
        self._rxfreq_config.read('./default')
        try: rxfreq = self._rxfreq_config.getfloat(section, "rxfreq")
        except: rxfreq = par_rxfreq
        self.rxfreq = rxfreq
        self._resampling_config = ConfigParser.ConfigParser()
        self._resampling_config.read('./default')
        try: resampling = self._resampling_config.getint(section, "resampling")
        except: resampling = par_resampling
        self.resampling = resampling
        self._mtu_config = ConfigParser.ConfigParser()
        self._mtu_config.read('./default')
        try: mtu = self._mtu_config.getint(section, "mtupdu")
        except: mtu = par_mtu
        self.mtu = mtu
        self.len_tag_key = len_tag_key = "packet_len"
        self._fftlen_config = ConfigParser.ConfigParser()
        self._fftlen_config.read('./default')
        try: fftlen = self._fftlen_config.getint(section, "fftlen")
        except: fftlen = par_fftlen
        self.fftlen = fftlen
        self.conf = conf = par_radioconf
        self._backoff_config = ConfigParser.ConfigParser()
        self._backoff_config.read('./default')
        try: backoff = self._backoff_config.getfloat(section, "backoff")
        except: backoff = par_backoff
        self.backoff = backoff

        ##################################################
        # Blocks
        ##################################################
        self.xmlrpc_server = SimpleXMLRPCServer.SimpleXMLRPCServer((par_xmlrpcserver, par_xmlrpcport), allow_none=True)
        self.xmlrpc_server.register_instance(self)
        self.xmlrpc_server_thread = threading.Thread(target=self.xmlrpc_server.serve_forever)
        self.xmlrpc_server_thread.daemon = True
        self.xmlrpc_server_thread.start()
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_samp_rate(samprate)
        self.uhd_usrp_source_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_source_0.set_center_freq(rxfreq, 0)
        self.uhd_usrp_source_0.set_normalized_gain(uhd_rxgain, 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        	len_tag_key,
        )
        self.uhd_usrp_sink_0.set_samp_rate(samprate)
        self.uhd_usrp_sink_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0.set_center_freq(txfreq, 0)
        self.uhd_usrp_sink_0.set_normalized_gain(uhd_txgain, 0)
        self.ofdm_modem_0 = ofdm_modem(
            amplitude=0.05,
            backoff_db=backoff,
            fft_len=fftlen,
            key_length=len_tag_key,
            label="OFDM modem",
            par_radioconf=conf,
            rolloff_length=0,
        )
        self.blocks_tuntap_pdu = blocks.tuntap_pdu(tap, mtu, False)
        self.blocks_null_source_0 = blocks.null_source(gr.sizeof_gr_complex*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_multiply_const = blocks.multiply_const_vcc((10.0**(1.0*backoff/10.0), ))
        self.blocks_message_strobe_mtu = blocks.message_strobe(pmt.intern("MTU: " + str(mtu)), 2000)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_tuntap_pdu, 'pdus'), (self.ofdm_modem_0, 'pdus_in'))    
        self.msg_connect((self.ofdm_modem_0, 'pdus_out'), (self.blocks_tuntap_pdu, 'pdus'))    
        self.connect((self.blocks_multiply_const, 0), (self.blocks_null_sink_0, 0))    
        self.connect((self.blocks_null_source_0, 0), (self.blocks_multiply_const, 0))    
        self.connect((self.ofdm_modem_0, 0), (self.uhd_usrp_sink_0, 0))    
        self.connect((self.uhd_usrp_source_0, 0), (self.ofdm_modem_0, 0))    

    def get_par_backoff(self):
        return self.par_backoff

    def set_par_backoff(self, par_backoff):
        self.par_backoff = par_backoff
        self.set_backoff(self.par_backoff)

    def get_par_fftlen(self):
        return self.par_fftlen

    def set_par_fftlen(self, par_fftlen):
        self.par_fftlen = par_fftlen
        self.set_fftlen(self.par_fftlen)

    def get_par_mtu(self):
        return self.par_mtu

    def set_par_mtu(self, par_mtu):
        self.par_mtu = par_mtu
        self.set_mtu(self.par_mtu)

    def get_par_radioconf(self):
        return self.par_radioconf

    def set_par_radioconf(self, par_radioconf):
        self.par_radioconf = par_radioconf
        self.set_conf(self.par_radioconf)

    def get_par_resampling(self):
        return self.par_resampling

    def set_par_resampling(self, par_resampling):
        self.par_resampling = par_resampling
        self.set_resampling(self.par_resampling)

    def get_par_rxfreq(self):
        return self.par_rxfreq

    def set_par_rxfreq(self, par_rxfreq):
        self.par_rxfreq = par_rxfreq
        self.set_rxfreq(self.par_rxfreq)

    def get_par_rxgain(self):
        return self.par_rxgain

    def set_par_rxgain(self, par_rxgain):
        self.par_rxgain = par_rxgain
        self.set_uhd_rxgain(self.par_rxgain)

    def get_par_samprate(self):
        return self.par_samprate

    def set_par_samprate(self, par_samprate):
        self.par_samprate = par_samprate
        self.set_samprate(self.par_samprate)

    def get_par_tap(self):
        return self.par_tap

    def set_par_tap(self, par_tap):
        self.par_tap = par_tap
        self.set_tap(self.par_tap)

    def get_par_txfreq(self):
        return self.par_txfreq

    def set_par_txfreq(self, par_txfreq):
        self.par_txfreq = par_txfreq
        self.set_txfreq(self.par_txfreq)

    def get_par_txgain(self):
        return self.par_txgain

    def set_par_txgain(self, par_txgain):
        self.par_txgain = par_txgain
        self.set_uhd_txgain(self.par_txgain)

    def get_par_xmlrpcport(self):
        return self.par_xmlrpcport

    def set_par_xmlrpcport(self, par_xmlrpcport):
        self.par_xmlrpcport = par_xmlrpcport

    def get_par_xmlrpcserver(self):
        return self.par_xmlrpcserver

    def set_par_xmlrpcserver(self, par_xmlrpcserver):
        self.par_xmlrpcserver = par_xmlrpcserver

    def get_section(self):
        return self.section

    def set_section(self, section):
        self.section = section
        self._uhd_txgain_config = ConfigParser.ConfigParser()
        self._uhd_txgain_config.read('./default')
        if not self._uhd_txgain_config.has_section(self.section):
        	self._uhd_txgain_config.add_section(self.section)
        self._uhd_txgain_config.set(self.section, "txgain", str(None))
        self._uhd_txgain_config.write(open('./default', 'w'))
        self._uhd_rxgain_config = ConfigParser.ConfigParser()
        self._uhd_rxgain_config.read('./default')
        if not self._uhd_rxgain_config.has_section(self.section):
        	self._uhd_rxgain_config.add_section(self.section)
        self._uhd_rxgain_config.set(self.section, "rxgain", str(None))
        self._uhd_rxgain_config.write(open('./default', 'w'))
        self._txfreq_config = ConfigParser.ConfigParser()
        self._txfreq_config.read('./default')
        if not self._txfreq_config.has_section(self.section):
        	self._txfreq_config.add_section(self.section)
        self._txfreq_config.set(self.section, "txfreq", str(None))
        self._txfreq_config.write(open('./default', 'w'))
        self._samprate_config = ConfigParser.ConfigParser()
        self._samprate_config.read('./default')
        if not self._samprate_config.has_section(self.section):
        	self._samprate_config.add_section(self.section)
        self._samprate_config.set(self.section, "samprate", str(None))
        self._samprate_config.write(open('./default', 'w'))
        self._rxfreq_config = ConfigParser.ConfigParser()
        self._rxfreq_config.read('./default')
        if not self._rxfreq_config.has_section(self.section):
        	self._rxfreq_config.add_section(self.section)
        self._rxfreq_config.set(self.section, "rxfreq", str(None))
        self._rxfreq_config.write(open('./default', 'w'))
        self._mtu_config = ConfigParser.ConfigParser()
        self._mtu_config.read('./default')
        if not self._mtu_config.has_section(self.section):
        	self._mtu_config.add_section(self.section)
        self._mtu_config.set(self.section, "mtupdu", str(None))
        self._mtu_config.write(open('./default', 'w'))
        self._fftlen_config = ConfigParser.ConfigParser()
        self._fftlen_config.read('./default')
        if not self._fftlen_config.has_section(self.section):
        	self._fftlen_config.add_section(self.section)
        self._fftlen_config.set(self.section, "fftlen", str(None))
        self._fftlen_config.write(open('./default', 'w'))
        self._backoff_config = ConfigParser.ConfigParser()
        self._backoff_config.read('./default')
        if not self._backoff_config.has_section(self.section):
        	self._backoff_config.add_section(self.section)
        self._backoff_config.set(self.section, "backoff", str(None))
        self._backoff_config.write(open('./default', 'w'))
        self._resampling_config = ConfigParser.ConfigParser()
        self._resampling_config.read('./default')
        if not self._resampling_config.has_section(self.section):
        	self._resampling_config.add_section(self.section)
        self._resampling_config.set(self.section, "resampling", str(None))
        self._resampling_config.write(open('./default', 'w'))

    def get_uhd_txgain(self):
        return self.uhd_txgain

    def set_uhd_txgain(self, uhd_txgain):
        self.uhd_txgain = uhd_txgain
        self.uhd_usrp_sink_0.set_normalized_gain(self.uhd_txgain, 0)
        	

    def get_uhd_rxgain(self):
        return self.uhd_rxgain

    def set_uhd_rxgain(self, uhd_rxgain):
        self.uhd_rxgain = uhd_rxgain
        self.uhd_usrp_source_0.set_normalized_gain(self.uhd_rxgain, 0)
        	

    def get_txfreq(self):
        return self.txfreq

    def set_txfreq(self, txfreq):
        self.txfreq = txfreq
        self.uhd_usrp_sink_0.set_center_freq(self.txfreq, 0)

    def get_tap(self):
        return self.tap

    def set_tap(self, tap):
        self.tap = tap

    def get_samprate(self):
        return self.samprate

    def set_samprate(self, samprate):
        self.samprate = samprate
        self.uhd_usrp_source_0.set_samp_rate(self.samprate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samprate)

    def get_rxfreq(self):
        return self.rxfreq

    def set_rxfreq(self, rxfreq):
        self.rxfreq = rxfreq
        self.uhd_usrp_source_0.set_center_freq(self.rxfreq, 0)

    def get_resampling(self):
        return self.resampling

    def set_resampling(self, resampling):
        self.resampling = resampling

    def get_mtu(self):
        return self.mtu

    def set_mtu(self, mtu):
        self.mtu = mtu
        self.blocks_message_strobe_mtu.set_msg(pmt.intern("MTU: " + str(self.mtu)))

    def get_len_tag_key(self):
        return self.len_tag_key

    def set_len_tag_key(self, len_tag_key):
        self.len_tag_key = len_tag_key
        self.ofdm_modem_0.set_key_length(self.len_tag_key)

    def get_fftlen(self):
        return self.fftlen

    def set_fftlen(self, fftlen):
        self.fftlen = fftlen
        self.ofdm_modem_0.set_fft_len(self.fftlen)

    def get_conf(self):
        return self.conf

    def set_conf(self, conf):
        self.conf = conf
        self.ofdm_modem_0.set_par_radioconf(self.conf)

    def get_backoff(self):
        return self.backoff

    def set_backoff(self, backoff):
        self.backoff = backoff
        self.ofdm_modem_0.set_backoff_db(self.backoff)
        self.blocks_multiply_const.set_k((10.0**(1.0*self.backoff/10.0), ))


def argument_parser():
    description = 'v.1'
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option, description=description)
    parser.add_option(
        "", "--par-backoff", dest="par_backoff", type="eng_float", default=eng_notation.num_to_str(-15),
        help="Set backoof (db) [default=%default]")
    parser.add_option(
        "", "--par-fftlen", dest="par_fftlen", type="intx", default=64,
        help="Set FFT len [default=%default]")
    parser.add_option(
        "", "--par-mtu", dest="par_mtu", type="intx", default=1000,
        help="Set MTU PDU tap unterface [default=%default]")
    parser.add_option(
        "", "--par-radioconf", dest="par_radioconf", type="intx", default=2,
        help="Set radio modulation configuration [1, 2] [default=%default]")
    parser.add_option(
        "", "--par-resampling", dest="par_resampling", type="intx", default=1,
        help="Set resampling [default=%default]")
    parser.add_option(
        "", "--par-rxfreq", dest="par_rxfreq", type="eng_float", default=eng_notation.num_to_str(2540000000),
        help="Set Rx frequency [default=%default]")
    parser.add_option(
        "", "--par-rxgain", dest="par_rxgain", type="eng_float", default=eng_notation.num_to_str(0.75),
        help="Set Rx gain [default=%default]")
    parser.add_option(
        "", "--par-samprate", dest="par_samprate", type="eng_float", default=eng_notation.num_to_str(200e3),
        help="Set Tx/Rx sample rate [default=%default]")
    parser.add_option(
        "", "--par-tap", dest="par_tap", type="string", default="tap0",
        help="Set tap interface [default=%default]")
    parser.add_option(
        "", "--par-txfreq", dest="par_txfreq", type="eng_float", default=eng_notation.num_to_str(2540000000),
        help="Set Tx frequency [default=%default]")
    parser.add_option(
        "", "--par-txgain", dest="par_txgain", type="eng_float", default=eng_notation.num_to_str(0.75),
        help="Set Tx gain [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcport", dest="par_xmlrpcport", type="intx", default=1234,
        help="Set XMLRPC Server port [default=%default]")
    parser.add_option(
        "", "--par-xmlrpcserver", dest="par_xmlrpcserver", type="string", default="127.0.0.1",
        help="Set XMLRPC Server address [default=%default]")
    parser.add_option(
        "", "--section", dest="section", type="string", default='RH',
        help="Set Configuration section in default file [default=%default]")
    return parser


def main(top_block_cls=radio_ofdm, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(par_backoff=options.par_backoff, par_fftlen=options.par_fftlen, par_mtu=options.par_mtu, par_radioconf=options.par_radioconf, par_resampling=options.par_resampling, par_rxfreq=options.par_rxfreq, par_rxgain=options.par_rxgain, par_samprate=options.par_samprate, par_tap=options.par_tap, par_txfreq=options.par_txfreq, par_txgain=options.par_txgain, par_xmlrpcport=options.par_xmlrpcport, par_xmlrpcserver=options.par_xmlrpcserver, section=options.section)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
