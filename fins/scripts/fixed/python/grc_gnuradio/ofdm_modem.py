# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FINS OFDM Modem
# Author: Cristina Costa, FBK CREATE-NET
# Description: v.1
# Generated: Thu May 30 09:31:59 2019
##################################################

def struct(data): return type('Struct', (object,), data)()
from gnuradio import blocks
from gnuradio import digital
from gnuradio import gr
from gnuradio.filter import firdes
import pmt
import threading
import time


class ofdm_modem(gr.hier_block2):

    def __init__(self, amplitude=0.05, backoff_db=-15, fft_len=64, key_length="packet_len", label="OFDM modem", par_radioconf=1, rolloff_length=0):
        gr.hier_block2.__init__(
            self, "FINS OFDM Modem",
            gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
            gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
        )
        self.message_port_register_hier_in("pdus_in")
        self.message_port_register_hier_out("pdus_out")
        self.message_port_register_hier_out("strobe_rx")
        self.message_port_register_hier_out("strobe_tx")

        ##################################################
        # Parameters
        ##################################################
        self.amplitude = amplitude
        self.backoff_db = backoff_db
        self.fft_len = fft_len
        self.key_length = key_length
        self.label = label
        self.par_radioconf = par_radioconf
        self.rolloff_length = rolloff_length

        ##################################################
        # Variables
        ##################################################
        self.pilot_carriers = pilot_carriers = ((-27, -14, -7, 7, 14, 27),)
        self.pattern2 = pattern2 = [1, -1, 1, -1]
        self.pattern1 = pattern1 = [0., 1.41421356, 0., -1.41421356]
        self.sync_word2 = sync_word2 = [0., 0., 0., 0., 0., 0.,] + pattern2 * ((fft_len-12)/len(pattern2))  +[0., 0., 0., 0., 0., 0.,]
        self.sync_word1 = sync_word1 = [0., 0., 0., 0., 0., 0.,] + pattern1 * ((fft_len-12)/len(pattern1))  +[0., 0., 0., 0., 0., 0.,]
        self.pilot_symbols = pilot_symbols = ((-1,1, 1, -1, -1, -1),)
        self.occupied_carriers = occupied_carriers = (sorted(tuple(set([x for x in range(-26,27)]) - set(pilot_carriers[0]) - set([0,]))),)
        self.tx_rate = tx_rate = 0
        self.tx_goodput = tx_goodput = 0
        self.rx_rate = rx_rate = 0
        self.rx_goodput = rx_goodput = 0
        self.conf2 = conf2 = struct({'freq': 0, 'samprate': 0, 'syncword1': sync_word1, 'syncword2': sync_word2, 'occupiedcarriers': occupied_carriers, 'pilotcarriers': pilot_carriers, 'pilotsymbols': pilot_symbols, 'pattern1': [0., 1.41421356, 0., -1.41421356], 'pattern2': [1, -1, 1, -1], 'fftlen': fft_len, 'lentagkey': key_length, 'cyclicprefixlen': 16, })
        self.conf1 = conf1 = struct({'freq': 0, 'samprate': 0, 'syncword1': (), 'syncword2': (), 'occupiedcarriers': (), 'pilotcarriers': (), 'pilotsymbols': (), 'pattern1': (), 'pattern2': (), 'fftlen': fft_len, 'lentagkey': key_length, 'cyclicprefixlen': 16, })
        self.conf = conf = "conf" + str(par_radioconf)
        self.bytes_written_rx = bytes_written_rx = 0
        self.bytes_rx = bytes_rx = 0

        ##################################################
        # Blocks
        ##################################################
        self.probeiq_tx = blocks.probe_rate(gr.sizeof_gr_complex*1, 500.0, 0.15)
        self.probeiq_rx = blocks.probe_rate(gr.sizeof_gr_complex*1, 500.0, 0.15)
        self.probebits_tx = blocks.probe_rate(gr.sizeof_char*1, 500.0, 0.15)
        self.probebits_rx = blocks.probe_rate(gr.sizeof_char*1, 500.0, 0.15)
        self.digital_ofdm_rx = digital.ofdm_rx(
        	  fft_len=eval(conf+".fftlen"), cp_len=eval(conf+".cyclicprefixlen"),
        	  frame_length_tag_key='frame_'+eval(conf+".lentagkey"),
        	  packet_length_tag_key=eval(conf+".lentagkey"),
        	  bps_header=2,
        	  bps_payload=2,
        	  debug_log=False,
        	  scramble_bits=False
        	 )

        def _tx_rate_probe():
            while True:
                val = self.probeiq_tx.rate()
                try:
                    self.set_tx_rate(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _tx_rate_thread = threading.Thread(target=_tx_rate_probe)
        _tx_rate_thread.daemon = True
        _tx_rate_thread.start()


        def _tx_goodput_probe():
            while True:
                val = self.probebits_tx.rate()
                try:
                    self.set_tx_goodput(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _tx_goodput_thread = threading.Thread(target=_tx_goodput_probe)
        _tx_goodput_thread.daemon = True
        _tx_goodput_thread.start()


        def _rx_rate_probe():
            while True:
                val = self.probeiq_rx.rate()
                try:
                    self.set_rx_rate(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _rx_rate_thread = threading.Thread(target=_rx_rate_probe)
        _rx_rate_thread.daemon = True
        _rx_rate_thread.start()


        def _rx_goodput_probe():
            while True:
                val = self.probebits_rx.rate()
                try:
                    self.set_rx_goodput(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _rx_goodput_thread = threading.Thread(target=_rx_goodput_probe)
        _rx_goodput_thread.daemon = True
        _rx_goodput_thread.start()


        def _bytes_written_rx_probe():
            while True:
                val = self.digital_ofdm_rx.crc.nitems_written(0)
                try:
                    self.set_bytes_written_rx(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _bytes_written_rx_thread = threading.Thread(target=_bytes_written_rx_probe)
        _bytes_written_rx_thread.daemon = True
        _bytes_written_rx_thread.start()


        def _bytes_rx_probe():
            while True:
                val = self.digital_ofdm_rx.crc.nitems_read(0)
                try:
                    self.set_bytes_rx(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (10))
        _bytes_rx_thread = threading.Thread(target=_bytes_rx_probe)
        _bytes_rx_thread.daemon = True
        _bytes_rx_thread.start()

        self.digital_ofdm_tx_0_0 = digital.ofdm_tx(
        	  fft_len=eval(conf+".fftlen"), cp_len=eval(conf+".cyclicprefixlen"),
        	  packet_length_tag_key=eval(conf+".lentagkey"),
        	  bps_header=2,
        	  bps_payload=2,
        	  rolloff=0,
        	  debug_log=False,
        	  scramble_bits=False
        	 )
        self.blocks_tagged_stream_to_pdu_0 = blocks.tagged_stream_to_pdu(blocks.byte_t, key_length)
        self.blocks_pdu_to_tagged_stream = blocks.pdu_to_tagged_stream(blocks.byte_t, key_length)
        self.blocks_multiply_const = blocks.multiply_const_vcc((10.0**(1.0*backoff_db/10.0), ))
        self.blocks_message_strobe_0 = blocks.message_strobe(pmt.intern(label + " Tx" +  "\n Average Tx Rate:" + str(tx_rate) + "\n Tx Goodput: " + str(tx_goodput)), 2000)
        self.blocks_message_strobe = blocks.message_strobe(pmt.intern(label + " Rx" + "\n Average Rx Rate:" + str(rx_rate) + "\n Bytes Rx from channel: " + str(bytes_rx)  + "\n Bytes written: " + str(bytes_written_rx) + "\n Rx Goodput: " + str(rx_goodput)), 2000)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_message_strobe, 'strobe'), (self, 'strobe_rx'))
        self.msg_connect((self.blocks_message_strobe_0, 'strobe'), (self, 'strobe_tx'))
        self.msg_connect((self.blocks_tagged_stream_to_pdu_0, 'pdus'), (self, 'pdus_out'))
        self.msg_connect((self, 'pdus_in'), (self.blocks_pdu_to_tagged_stream, 'pdus'))
        self.connect((self.blocks_multiply_const, 0), (self, 0))
        self.connect((self.blocks_pdu_to_tagged_stream, 0), (self.digital_ofdm_tx_0_0, 0))
        self.connect((self.blocks_pdu_to_tagged_stream, 0), (self.probebits_tx, 0))
        self.connect((self.digital_ofdm_rx, 0), (self.blocks_tagged_stream_to_pdu_0, 0))
        self.connect((self.digital_ofdm_rx, 0), (self.probebits_rx, 0))
        self.connect((self.digital_ofdm_tx_0_0, 0), (self.blocks_multiply_const, 0))
        self.connect((self.digital_ofdm_tx_0_0, 0), (self.probeiq_tx, 0))
        self.connect((self, 0), (self.digital_ofdm_rx, 0))
        self.connect((self, 0), (self.probeiq_rx, 0))

    def get_amplitude(self):
        return self.amplitude

    def set_amplitude(self, amplitude):
        self.amplitude = amplitude

    def get_backoff_db(self):
        return self.backoff_db

    def set_backoff_db(self, backoff_db):
        self.backoff_db = backoff_db
        self.blocks_multiply_const.set_k((10.0**(1.0*self.backoff_db/10.0), ))

    def get_fft_len(self):
        return self.fft_len

    def set_fft_len(self, fft_len):
        self.fft_len = fft_len
        self.set_sync_word2([0., 0., 0., 0., 0., 0.,] + self.pattern2 * ((self.fft_len-12)/len(self.pattern2))  +[0., 0., 0., 0., 0., 0.,] )
        self.set_sync_word1([0., 0., 0., 0., 0., 0.,] + self.pattern1 * ((self.fft_len-12)/len(self.pattern1))  +[0., 0., 0., 0., 0., 0.,] )

    def get_key_length(self):
        return self.key_length

    def set_key_length(self, key_length):
        self.key_length = key_length

    def get_label(self):
        return self.label

    def set_label(self, label):
        self.label = label
        self.blocks_message_strobe_0.set_msg(pmt.intern(self.label + " Tx" +  "\n Average Tx Rate:" + str(self.tx_rate) + "\n Tx Goodput: " + str(self.tx_goodput)))
        self.blocks_message_strobe.set_msg(pmt.intern(self.label + " Rx" + "\n Average Rx Rate:" + str(self.rx_rate) + "\n Bytes Rx from channel: " + str(self.bytes_rx)  + "\n Bytes written: " + str(self.bytes_written_rx) + "\n Rx Goodput: " + str(self.rx_goodput)))

    def get_par_radioconf(self):
        return self.par_radioconf

    def set_par_radioconf(self, par_radioconf):
        self.par_radioconf = par_radioconf
        self.set_conf("conf" + str(self.par_radioconf))

    def get_rolloff_length(self):
        return self.rolloff_length

    def set_rolloff_length(self, rolloff_length):
        self.rolloff_length = rolloff_length

    def get_pilot_carriers(self):
        return self.pilot_carriers

    def set_pilot_carriers(self, pilot_carriers):
        self.pilot_carriers = pilot_carriers
        self.set_occupied_carriers((sorted(tuple(set([x for x in range(-26,27)]) - set(self.pilot_carriers[0]) - set([0,]))),))

    def get_pattern2(self):
        return self.pattern2

    def set_pattern2(self, pattern2):
        self.pattern2 = pattern2
        self.set_sync_word2([0., 0., 0., 0., 0., 0.,] + self.pattern2 * ((self.fft_len-12)/len(self.pattern2))  +[0., 0., 0., 0., 0., 0.,] )

    def get_pattern1(self):
        return self.pattern1

    def set_pattern1(self, pattern1):
        self.pattern1 = pattern1
        self.set_sync_word1([0., 0., 0., 0., 0., 0.,] + self.pattern1 * ((self.fft_len-12)/len(self.pattern1))  +[0., 0., 0., 0., 0., 0.,] )

    def get_sync_word2(self):
        return self.sync_word2

    def set_sync_word2(self, sync_word2):
        self.sync_word2 = sync_word2

    def get_sync_word1(self):
        return self.sync_word1

    def set_sync_word1(self, sync_word1):
        self.sync_word1 = sync_word1

    def get_pilot_symbols(self):
        return self.pilot_symbols

    def set_pilot_symbols(self, pilot_symbols):
        self.pilot_symbols = pilot_symbols

    def get_occupied_carriers(self):
        return self.occupied_carriers

    def set_occupied_carriers(self, occupied_carriers):
        self.occupied_carriers = occupied_carriers

    def get_tx_rate(self):
        return self.tx_rate

    def set_tx_rate(self, tx_rate):
        self.tx_rate = tx_rate
        self.blocks_message_strobe_0.set_msg(pmt.intern(self.label + " Tx" +  "\n Average Tx Rate:" + str(self.tx_rate) + "\n Tx Goodput: " + str(self.tx_goodput)))

    def get_tx_goodput(self):
        return self.tx_goodput

    def set_tx_goodput(self, tx_goodput):
        self.tx_goodput = tx_goodput
        self.blocks_message_strobe_0.set_msg(pmt.intern(self.label + " Tx" +  "\n Average Tx Rate:" + str(self.tx_rate) + "\n Tx Goodput: " + str(self.tx_goodput)))

    def get_rx_rate(self):
        return self.rx_rate

    def set_rx_rate(self, rx_rate):
        self.rx_rate = rx_rate
        self.blocks_message_strobe.set_msg(pmt.intern(self.label + " Rx" + "\n Average Rx Rate:" + str(self.rx_rate) + "\n Bytes Rx from channel: " + str(self.bytes_rx)  + "\n Bytes written: " + str(self.bytes_written_rx) + "\n Rx Goodput: " + str(self.rx_goodput)))

    def get_rx_goodput(self):
        return self.rx_goodput

    def set_rx_goodput(self, rx_goodput):
        self.rx_goodput = rx_goodput
        self.blocks_message_strobe.set_msg(pmt.intern(self.label + " Rx" + "\n Average Rx Rate:" + str(self.rx_rate) + "\n Bytes Rx from channel: " + str(self.bytes_rx)  + "\n Bytes written: " + str(self.bytes_written_rx) + "\n Rx Goodput: " + str(self.rx_goodput)))

    def get_conf2(self):
        return self.conf2

    def set_conf2(self, conf2):
        self.conf2 = conf2

    def get_conf1(self):
        return self.conf1

    def set_conf1(self, conf1):
        self.conf1 = conf1

    def get_conf(self):
        return self.conf

    def set_conf(self, conf):
        self.conf = conf

    def get_bytes_written_rx(self):
        return self.bytes_written_rx

    def set_bytes_written_rx(self, bytes_written_rx):
        self.bytes_written_rx = bytes_written_rx
        self.blocks_message_strobe.set_msg(pmt.intern(self.label + " Rx" + "\n Average Rx Rate:" + str(self.rx_rate) + "\n Bytes Rx from channel: " + str(self.bytes_rx)  + "\n Bytes written: " + str(self.bytes_written_rx) + "\n Rx Goodput: " + str(self.rx_goodput)))

    def get_bytes_rx(self):
        return self.bytes_rx

    def set_bytes_rx(self, bytes_rx):
        self.bytes_rx = bytes_rx
        self.blocks_message_strobe.set_msg(pmt.intern(self.label + " Rx" + "\n Average Rx Rate:" + str(self.rx_rate) + "\n Bytes Rx from channel: " + str(self.bytes_rx)  + "\n Bytes written: " + str(self.bytes_written_rx) + "\n Rx Goodput: " + str(self.rx_goodput)))
