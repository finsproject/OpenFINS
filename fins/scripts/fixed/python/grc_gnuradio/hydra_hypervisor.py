# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FINS HyDRA Hypervisor
# Author: Cristina Costa, FBK CREATE-NET
# Description: v.1
# Generated: Sun Jan  6 16:31:06 2019
##################################################

from gnuradio import blocks
from gnuradio import filter
from gnuradio import gr
from gnuradio.filter import firdes
import hydra


class hydra_hypervisor(gr.hier_block2):

    def __init__(self, backoff_db=-3.5, fftlen=4096, resampling=1, rxfreq=2.49e9, rxfreqvr1=2.55e9 +1e6, rxfreqvr2=2.55e9 +1e6, samprate=2e6, sampratevr1=0.2e6, sampratevr2=0.2e6, txfreq=2.484e9, txfreqvr1=2.55e9 -1e6, txfreqvr2=2.55e9 -1e6):
        gr.hier_block2.__init__(
            self, "FINS HyDRA Hypervisor",
            gr.io_signaturev(3, 3, [gr.sizeof_gr_complex*1, gr.sizeof_gr_complex*1, gr.sizeof_gr_complex*1]),
            gr.io_signaturev(3, 3, [gr.sizeof_gr_complex*1, gr.sizeof_gr_complex*1, gr.sizeof_gr_complex*1]),
        )

        ##################################################
        # Parameters
        ##################################################
        self.backoff_db = backoff_db
        self.fftlen = fftlen
        self.resampling = resampling
        self.rxfreq = rxfreq
        self.rxfreqvr1 = rxfreqvr1
        self.rxfreqvr2 = rxfreqvr2
        self.samprate = samprate
        self.sampratevr1 = sampratevr1
        self.sampratevr2 = sampratevr2
        self.txfreq = txfreq
        self.txfreqvr1 = txfreqvr1
        self.txfreqvr2 = txfreqvr2

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler = filter.rational_resampler_ccc(
                interpolation=resampling,
                decimation=1,
                taps=None,
                fractional_bw=None,
        )
        self.hydra_hydra_source_0 = hydra.hydra_source(2, fftlen, txfreq, samprate,
        	 ((rxfreqvr1, sampratevr1), 
        	 (rxfreqvr2, sampratevr1),
        	 ))
          
        self.hydra_hydra_sink_0 = hydra.hydra_sink(2, fftlen, txfreq, samprate,
        	 ((txfreqvr1, sampratevr1), 
        	 (txfreqvr2, sampratevr2),
        	 ))
          
        self.blocks_multiply_const_vxx_0_0_0 = blocks.multiply_const_vcc((10.0**(1.0*backoff_db/10.0), ))

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_multiply_const_vxx_0_0_0, 0), (self, 2))    
        self.connect((self.hydra_hydra_sink_0, 0), (self.rational_resampler, 0))    
        self.connect((self.hydra_hydra_source_0, 0), (self, 0))    
        self.connect((self.hydra_hydra_source_0, 1), (self, 1))    
        self.connect((self, 0), (self.hydra_hydra_sink_0, 0))    
        self.connect((self, 1), (self.hydra_hydra_sink_0, 1))    
        self.connect((self, 2), (self.hydra_hydra_source_0, 0))    
        self.connect((self.rational_resampler, 0), (self.blocks_multiply_const_vxx_0_0_0, 0))    

    def get_backoff_db(self):
        return self.backoff_db

    def set_backoff_db(self, backoff_db):
        self.backoff_db = backoff_db
        self.blocks_multiply_const_vxx_0_0_0.set_k((10.0**(1.0*self.backoff_db/10.0), ))

    def get_fftlen(self):
        return self.fftlen

    def set_fftlen(self, fftlen):
        self.fftlen = fftlen

    def get_resampling(self):
        return self.resampling

    def set_resampling(self, resampling):
        self.resampling = resampling

    def get_rxfreq(self):
        return self.rxfreq

    def set_rxfreq(self, rxfreq):
        self.rxfreq = rxfreq

    def get_rxfreqvr1(self):
        return self.rxfreqvr1

    def set_rxfreqvr1(self, rxfreqvr1):
        self.rxfreqvr1 = rxfreqvr1
        self.hydra_hydra_source_0.set_central_frequency(0, self.rxfreqvr1)

    def get_rxfreqvr2(self):
        return self.rxfreqvr2

    def set_rxfreqvr2(self, rxfreqvr2):
        self.rxfreqvr2 = rxfreqvr2
        self.hydra_hydra_source_0.set_central_frequency(1, self.rxfreqvr2)

    def get_samprate(self):
        return self.samprate

    def set_samprate(self, samprate):
        self.samprate = samprate

    def get_sampratevr1(self):
        return self.sampratevr1

    def set_sampratevr1(self, sampratevr1):
        self.sampratevr1 = sampratevr1

    def get_sampratevr2(self):
        return self.sampratevr2

    def set_sampratevr2(self, sampratevr2):
        self.sampratevr2 = sampratevr2

    def get_txfreq(self):
        return self.txfreq

    def set_txfreq(self, txfreq):
        self.txfreq = txfreq

    def get_txfreqvr1(self):
        return self.txfreqvr1

    def set_txfreqvr1(self, txfreqvr1):
        self.txfreqvr1 = txfreqvr1
        self.hydra_hydra_sink_0.set_central_frequency(0, self.txfreqvr1)

    def get_txfreqvr2(self):
        return self.txfreqvr2

    def set_txfreqvr2(self, txfreqvr2):
        self.txfreqvr2 = txfreqvr2
        self.hydra_hydra_sink_0.set_central_frequency(1, self.txfreqvr2)
