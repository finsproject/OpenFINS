#!/usr/bin/env bash
STARTDATE=$(date)
sudo rabbitmqctl add_user admin 4dm1n
sudo rabbitmqctl set_user_tags admin administrator
sudo rabbitmqctl set_permissions admin ".*" ".*" ".*"
echo "SCRIPT ACTIVITY SUMMARY"
echo 'STARTED @'$STARTDATE
echo 'ENDED   @'$(date)
echo "RabbitMQ: added user 'admin' and set permissions"