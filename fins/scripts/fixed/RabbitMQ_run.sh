#!/usr/bin/env bash
STARTDATE=$(date)
sudo service rabbitmq-server start
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server stop
sudo service rabbitmq-server start
echo "SCRIPT ACTIVITY SUMMARY"
echo 'STARTED @'$STARTDATE
echo 'ENDED   @'$(date)
echo "RabbitMQ Server stopped and restarted (+ rabbitmq_management plugin enabled)"