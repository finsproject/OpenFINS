# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback
import json
import time
import subprocess

from os import listdir
from os.path import isfile, join

from fins.confs.generator.controllers import ResControllerValidator, \
    ResControllerWCTRLValidator
from fins.confs.generator.services import ServiceRabbitMQValidator
from fins.confs.generator.constants import FINSConstants
from fins.src.support.bash.utils import BashUtils


class FINSControllers:

    WHISFUL_AGENTS_CONTROLLER_DELAY = 20

    def __init__(self, rcs, rs, vms, rmq, username, fins_install_folder):

        self.LOG = logging.getLogger(__name__)

        self._rcs = rcs

        self._rs = rs

        self._vms = vms

        self._rmq = rmq

        self.username = username

        self.fins_install_folder = fins_install_folder

    def configure(self):
        try:
            for index in self._rcs:
                rc = self._rcs[index]
                if not self.configure_by_type(rc):
                    raise Exception("RC configuration failed")
            self.LOG.info("RCs configuration DONE")

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_by_type(self, rc):
        try:
            rc_type = rc[ResControllerValidator.KEY__TYPE]
            if rc_type == FINSConstants.TYPE_RC__WCTRL:
                # self.LOG.info(r_type)
                self.configure_WCTRL(rc)
            else:
                self.LOG.warning("Unknown Resource Controller type: %s",
                                 rc_type)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def generate_wcrtl_config_file(self, rc, file_full_path_name):

        try:
            auth_username = 'admin'
            auth_password = self._rmq[
                ServiceRabbitMQValidator.KEY__USERS]['admin']

            agents = list()
            for index in self._rs:
                r = self._rs[index]
                if r['managed_by'] == rc['id']:

                    vm_idx = r['vm'].strip('VM.')
                    vm_id = self._vms[vm_idx]['id']
                    agents.append(vm_id)

            content = {
                "controller": {
                    "id": rc['id'],
                    "name": rc['name'],
                    "info": rc['description'],
                    "iface": "ens3",
                    "groupName": "finsdemo_2901",
                    "dl_port": "8990",
                    "ul_port": "8989"
                },

                "rabbitMQ": {
                    "ip": self._rmq['ip'],
                    "port": self._rmq['port'],
                    "auth": {"username": auth_username,
                             "password": auth_password},
                    "prefix": "qra"
                },

                "ra_data": {
                    "id": rc['managed_by']
                },

                "agents": agents
            }

            BashUtils.save_into_file(json.dumps(content), file_full_path_name)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def generate_wagent_config_file(self, jfed_vm_name, file_full_path_name):

        try:

            content = {
                "agent": {
                    "groupName":  "finsdemo_2901",
                    "name": jfed_vm_name,
                    "info": "Wishful default FINS agent",
                    "iface": "ens3"
                    }
            }

            BashUtils.save_into_file(json.dumps(content), file_full_path_name)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_WCTRL(self, rc):
        try:

            self.LOG.info(
                "\n\nCONFIGURE WiSHFUL CONTROLLER & AGENTS\n\n")

            self.LOG.info("\n\nWiSHFUL AGENTS get instantiated FIRST\n\n")

            assert (self._configure_WAgents(rc))

            self.LOG.info("\n\nWait %s seconds before "
                          "configuring and running controller",
                          str(self.WHISFUL_AGENTS_CONTROLLER_DELAY))

            time.sleep(self.WHISFUL_AGENTS_CONTROLLER_DELAY)

            self.LOG.info("\n\nWiSHFUL CONTROLLER get instantiated NOW\n\n")

            assert (self._configure_WController(rc))

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False


    def _configure_WAgents(self, rc):

        try:
            self.LOG.info(
                "\n\nCONFIGURE & RUN WiSHFUL AGENTS\n\n"
                "Generate wishful agent scripts & confs for each resource "
                " controlled by wishful controller %s, "
                "transfer files to associated VMs and run all of them in "
                "background",
                rc["id"])

            for index in self._rs:
                r = self._rs[index]
                if r['managed_by'] == rc['id']:

                    # WiSHFUL defaults
                    set_wish_file = 'set_wishful.sh'
                    src_path = self.fins_install_folder + "/" + "fins/scripts/fixed/"

                    if BashUtils.scp_push(set_wish_file, r['ip'], src_path,
                                          '.'):
                        command = 'sudo chmod 777 ' + set_wish_file + ' && ' + \
                                  './' + set_wish_file
                        self.LOG.info("It may take some time... ")
                        response = BashUtils.ssh_command(command, r['ip'])
                        self.LOG.info(
                            "\n\nWishful support installed @" + r['ip'])

                    # WAGENT generate config file

                    wagent_config_file_path = \
                        "/fins/scripts/fixed/python/agent/"
                    wagent_config_file_name = "FINS_wishful_config.json"

                    file_full_path_name = self.fins_install_folder + \
                                          wagent_config_file_path + \
                                          wagent_config_file_name

                    vm_idx = r['vm'].strip('VM.')
                    vm_id = self._vms[vm_idx]['id']

                    assert self.generate_wagent_config_file(vm_id,
                                                            file_full_path_name)

                    # TAR controller folder

                    tar_folder = self.fins_install_folder + \
                                 "/fins/scripts/fixed/python/"
                    tar_file = "agent_" + vm_id + ".tar"

                    BashUtils.run_local("cd " + tar_folder + " && tar -cf " +
                                        tar_file + " " + "agent/ grc_gnuradio/")

                    # WAGENT copy folder config file to WCTRL side

                    assert BashUtils.scp_push(tar_file, r['ip'], tar_folder,
                                              "agent.tar")

                    # UNTAR and RUN Controller in background

                    # assert BashUtils.ssh_bg_command(
                    #     "tar -xf agent.tar && " +
                    #     "cd agent && " +
                    #     "nohup python3 FINS_wishful_agent.py &", r['ip'])

                    cmd = "tar -xf agent.tar; " \
                          "mv grc_gnuradio .grc_gnuradio; " \
                          "cd agent; " \
                          "(nohup python3 FINS_wishful_agent.py &)"
                    assert BashUtils.subprocess_ssh(cmd, self.username,
                                                    r['ip'])

                    self.LOG.info(
                        "\n\nWishful Agent should be running @%s\n", r['ip'])

            return True




        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def _configure_WController(self, rc):

        try:
            self.LOG.info(
                "\n\nCONFIGURE & RUN WiSHFUL CONTROLLER\n\n"
                "Generate wishful controller %s script & conf, "
                "transfer files to associated VM "
                "and run it in background",
                rc["id"]
            )
            # WiSHFUL defaults
            set_wish_file = 'set_wishful.sh'
            src_path = self.fins_install_folder + "/" + "fins/scripts/fixed/"

            if BashUtils.scp_push(set_wish_file, rc['ip'], src_path, '.'):
                command = 'sudo chmod 777 ' + set_wish_file + ' && ' + \
                          './' + set_wish_file
                self.LOG.info("It may take some time... ")
                response = BashUtils.ssh_command(command, rc['ip'])
                self.LOG.info("\n\nWishful support installed @" + rc['ip'])

            # WCTRL generate config file

            wctrl_config_file_path = "/fins/scripts/fixed/python/controller/"
            wctrl_config_file_name = "FINS_wishful_config.json"

            file_full_path_name = self.fins_install_folder + \
                                  wctrl_config_file_path + \
                                  wctrl_config_file_name

            assert self.generate_wcrtl_config_file(rc, file_full_path_name)

            # TAR controller folder

            tar_folder = self.fins_install_folder + \
                         "/fins/scripts/fixed/python/"
            tar_file = "controller.tar"

            BashUtils.run_local("cd " + tar_folder + " && tar -cf " +
                                tar_file + " " + "controller/ grc_gnuradio/")

            # WCTRL copy folder config file to WCTRL side

            assert BashUtils.scp_push(tar_file, rc['ip'], tar_folder, ".")

            # UNTAR and RUN Controller in background

            # assert BashUtils.ssh_bg_command(
            #     "tar -xf " + tar_file + " && " +
            #     "cd controller && " +
            #     "nohup python3 FINS_wishful_controller.py &", rc['ip'])

            # cmd = "ssh -oStrictHostKeyChecking=no -t -f -n %s@%s " \
            #       "\"tar -xf %s; " \
            #       "cd controller; " \
            #       "nohup python3 FINS_wishful_controller.py &\"" % \
            #       ("franto", rc['ip'], tar_file)
            # out = None
            # try:
            #     out = subprocess.run(cmd, shell=True)
            #     self.LOG.debug("\nResponse: %s\n" % out)
            #     # lines = out.decode("utf-8").split('\n')
            # except subprocess.CalledProcessError as e:
            #     self.LOG.error(str(e.output))
            #     traceback.print_exc()
            #     self.LOG.error("CRITICAL ERROR, FINS aborted")
            #     exit(1)

            cmd = "tar -xf %s; " \
                  "mv grc_gnuradio .grc_gnuradio; " \
                  "cd controller; " \
                  "(nohup python3 FINS_wishful_controller.py &)" % tar_file
            assert BashUtils.subprocess_ssh(cmd, self.username, rc['ip'])


            self.LOG.info(
                "\n\nWishful Controller should be running @%s\n", rc['ip'])

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False




    # def generate_wcrtl_config_file(self, rc, file_full_path_name):
    #
    #     KEY__CONTROLLER = "controller"
    #     KEY_CONTROLLER__ID = "id"
    #     KEY_CONTROLLER__NAME = "name"
    #     KEY_CONTROLLER__INFO = "info"
    #     KEY_CONTROLLER__IFACE = "iface"
    #     KEY_CONTROLLER__GROUPNAME = "groupName"
    #     KEY_CONTROLLER__DL_PORT = "dl_port"
    #     KEY_CONTROLLER__UL_PORT = "ul_port"
    #
    #     KEY__RABBITMQ = "rabbitMQ"
    #     KEY_RABBITMQ__IP = "ip"
    #     KEY_RABBITMQ__PORT = "port"
    #     KEY_RABBITMQ__AUTH = "auth"
    #     KEY_RABBITMQ__PREFIX = "prefix"
    #
    #     KEY__RA_DATA = "ra_data"
    #     KEY_RA_DATA__ID = "id"
    #
    #     KEY__RADIO_PROGRAM_CONFIG = "radio_program_config"
    #     KEY_RPC__PATH = "path"
    #     KEY_RPC__EXEC_ENGINE = "exec_engine"
    #     KEY_RPC__EXEC_ENGINE_PARAMS = "exec_engine_params"
    #     KEY_RPC__PROGRAM_PARAMS = "program_params"
    #
    #     KEY__RADIO_PROGRAMS = "radio_programs"
    #
    #     KEY_RP__PROGRAM_NAME = "program_name"
    #     KEY_RP__PROGRAM_CODE = "program_code"
    #     KEY_RP__PROGRAM_TYPE = "program_type"
    #     KEY_RP__PROGRAM_PORT = "program_port"
    #     KEY_RP__PARAMETER_LIST = "parameter_list"
    #     KEY_RP__MONITOR_LIST = "monitor_list"
    #     KEY_RP__RESOURCE_LIST = "resource_list"
    #     KEY_RP__TAP_IFACES_LIST = "tap_ifaces_list"
    #
    #     try:
    #
    #         cfg = dict()
    #         cfg[KEY__CONTROLLER] = dict()
    #         ctrl = cfg[KEY__CONTROLLER]
    #         ctrl[KEY_CONTROLLER__ID] = rc[ResControllerValidator.KEY__ID]
    #         ctrl[KEY_CONTROLLER__NAME] = rc[ResControllerValidator.KEY__NAME]
    #         ctrl[KEY_CONTROLLER__INFO] = \
    #             rc[ResControllerValidator.KEY__DESCRIPTION]
    #         ctrl[KEY_CONTROLLER__IFACE] = \
    #             rc[ResControllerWCTRLValidator.KEY__IFACE]
    #         ctrl[KEY_CONTROLLER__GROUPNAME] = \
    #             rc[ResControllerWCTRLValidator.KEY__GROUP_NAME]
    #         ctrl[KEY_CONTROLLER__DL_PORT] = \
    #             rc[ResControllerWCTRLValidator.KEY__DL_PORT]
    #         ctrl[KEY_CONTROLLER__UL_PORT] = \
    #             rc[ResControllerWCTRLValidator.KEY__UL_PORT]
    #
    #         cfg[KEY__RABBITMQ] = dict()
    #         rmq = cfg[KEY__RABBITMQ]
    #         rmq[KEY_RABBITMQ__IP] = self._rmq['ip']
    #         port = self._rmq[ServiceRabbitMQValidator.KEY__PORT]
    #         rmq[KEY_RABBITMQ__PORT] = port
    #         rmq[KEY_RABBITMQ__AUTH] = dict(
    #             username='admin',
    #             password=self._rmq[ServiceRabbitMQValidator.KEY__USERS]['admin']
    #         )
    #         rmq[KEY_RABBITMQ__PREFIX] = "qrawctrl"
    #
    #         cfg[KEY__RA_DATA] = dict()
    #         rad = cfg[KEY__RA_DATA]
    #         rad[KEY_RA_DATA__ID] = rc[ResControllerValidator.KEY__MANAGED_BY]
    #
    #         cfg[KEY__RADIO_PROGRAM_CONFIG] = dict()
    #         rpc = cfg[KEY__RADIO_PROGRAM_CONFIG]
    #         rpc[KEY_RPC__PATH] = "./radio_programs/gnuradio"
    #         rpc[KEY_RPC__EXEC_ENGINE] = "gnuradio"
    #         rpc[KEY_RPC__EXEC_ENGINE_PARAMS] = [KEY_RP__PROGRAM_NAME,
    #                                             KEY_RP__PROGRAM_CODE,
    #                                             KEY_RP__PROGRAM_TYPE,
    #                                             KEY_RP__PROGRAM_PORT]
    #         rpc[KEY_RPC__PROGRAM_PARAMS] = [KEY_RP__PARAMETER_LIST,
    #                                         KEY_RP__MONITOR_LIST,
    #                                         KEY_RP__RESOURCE_LIST,
    #                                         KEY_RP__TAP_IFACES_LIST]
    #
    #         cfg[KEY__RADIO_PROGRAMS] = dict()
    #         rp = cfg[KEY__RADIO_PROGRAMS]
    #
    #         # rp[KEY_RP__FINS_TEST] = dict()
    #         # ft = rp[KEY_RP__FINS_TEST]
    #         # ft[KEY_RP_FINS_TEST__PARAMETER_LIST] = ["par1", "par2", "par3"]
    #         # ft[KEY_RP_FINS_TEST__MONITOR_LIST] = ["par1", "par2", "par3"]
    #         # ft[KEY_RP_FINS_TEST__RESOURCE_LIST] = ["uhd"]
    #         # ft[KEY_RP_FINS_TEST__TAP_IFACES_LIST] = ["tap1", "tap2"]
    #         # ft[KEY_RP_FINS_TEST__PROGRAM_CODE] = None
    #         # ft[KEY_RP_FINS_TEST__PROGRAM_TYPE] = "grc"
    #         # ft[KEY_RP_FINS_TEST__PROGRAM_PORT] = 1234
    #         #
    #         # rp[KEY_RP__TX] = dict()
    #         # tx = rp[KEY_RP__TX]
    #         # tx[KEY_RP_TX__PROGRAM_NAME] = "tx"
    #         # tx[KEY_RP_TX__PROGRAM_FILE] = "./grc/tx.grc"
    #         # tx[KEY_RP_TX__PROGRAM_TYPE] = "grc"
    #         # tx[KEY_RP_TX__PROGRAM_PORT] = 1235
    #         #
    #         # rp[KEY_RP__RX] = dict()
    #         # rx = rp[KEY_RP__RX]
    #         # rx[KEY_RP_RX__PROGRAM_NAME] = "rx"
    #         # rx[KEY_RP_RX__PROGRAM_FILE] = "./grc/tx.grc"
    #         # rx[KEY_RP_RX__PROGRAM_TYPE] = "grc"
    #         # rx[KEY_RP_RX__PROGRAM_PORT] = 1235
    #
    #         BashUtils.save_into_file(str(cfg), file_full_path_name)
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #         return False

    # def generate_ra_agent_config_file(self, ra_id, file_full_path_name,
    #                                groupname=None, info=None, iface=None):
    #
    #     KEY__GROUPNAME = "groupName"
    #     KEY__NAME = "name"
    #     KEY__INFO = "info"
    #     KEY__IFACE = "iface"
    #
    #     DEFAULT__GROUPNAME = FINSConstants.RES_WA_GROUP_NAME__DEFAULT_VALUE
    #     DEFAULT__INFO = "wishful FINS agent"
    #     DEFAULT__IFACE = FINSConstants.IRIS_FINS_MAIN_IF__DEFAULT_VALUE
    #
    #     if groupname is None:
    #         groupname = DEFAULT__GROUPNAME
    #     if info is None:
    #         info = DEFAULT__INFO
    #     if iface is None:
    #         iface = DEFAULT__IFACE
    #
    #     try:
    #
    #         agent = dict()
    #         agent[KEY__GROUPNAME] = groupname
    #         agent[KEY__NAME] = ra_id
    #         agent[KEY__INFO] = info
    #         agent[KEY__IFACE] = iface
    #
    #         data = dict(
    #             agent=agent
    #         )
    #
    #         BashUtils.save_into_file(json.dumps(data), file_full_path_name)
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #         return False
    #
    # def configure_WCTRL(self, rc_wctrl):
    #
    #     RP_FOLDER = "radio_program"
    #     try:
    #         # Wishful Controller config file
    #         r_id = rc_wctrl[ResControllerValidator.KEY__ID]
    #         path_topology_sdr = "/fins/scripts/generated/topology/sdr"
    #         file_path = self.fins_install_folder + path_topology_sdr
    #         filename = "whishful_controller_config_" + r_id + ".json"
    #         ffn = file_path + "/" + filename
    #         if not self.generate_wcrtl_config_file(rc_wctrl, ffn):
    #             raise Exception("Config file generation FAILED for resource " +
    #                             str(r_id))
    #         r_filename = 'FINS_wishful_config.json'
    #         scp_success = BashUtils.scp_push(filename, rc_wctrl['ip'],
    #                                          file_path, r_filename)
    #         if scp_success:
    #             self.LOG.info("\nWhishful Controller Config file SCP to " +
    #                           str(r_id) + "@" + rc_wctrl['ip'] + ": DONE")
    #         else:
    #             raise Exception("Whishful Controller Config file SCP to" +
    #                             str(r_id) + "@" + rc_wctrl['ip'] +
    #                             ": FAILED")
    #
    #         # Wishful Controller script file
    #         file_path = self.fins_install_folder + "/fins/scripts/fixed/python"
    #         filename = "FINS_wishful_controller.py"
    #         scp_success = BashUtils.scp_push(filename, rc_wctrl['ip'],
    #                                          file_path, ".")
    #         if scp_success:
    #             self.LOG.info("\n\nWishful Controller Python Script file SCP " +
    #                           "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                           ": DONE\n")
    #         else:
    #             raise Exception("Wishful Controller Python Script file SCP " +
    #                             "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                             ": FAILED")
    #
    #         # Wishful Controller packets library file
    #         file_path = self.fins_install_folder + "/fins/scripts/fixed/python"
    #         filename = "FINS_wishful_controller_packets.py"
    #         scp_success = BashUtils.scp_push(filename, rc_wctrl['ip'],
    #                                          file_path, "packets.py")
    #         if scp_success:
    #             self.LOG.info("\n\nWishful Controller Packets Library file SCP " +
    #                           "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                           ": DONE\n")
    #         else:
    #             raise Exception("Wishful Controller Packets Library file SCP " +
    #                             "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                             ": FAILED")
    #
    #         # Wishful Controller radio programs repository
    #         command = "mkdir " + RP_FOLDER
    #         rsp = BashUtils.ssh_command(command, rc_wctrl['ip'])
    #         if rsp is None:
    #             raise Exception("radio_program folder creation on Wishful " +
    #                             "Controller VM(@%s) FAILED",
    #                             rc_wctrl['ip'])
    #         file_path = self.fins_install_folder + "/fins/scripts/fixed/" \
    #                                                "grc/WCTRL_radio_programs"
    #
    #         files = [f for f in listdir(file_path)
    #                  if isfile(join(file_path, f))]
    #
    #         dest_path = RP_FOLDER + "/"
    #         for filename in files:
    #
    #             if filename.endswith(".grc"):
    #                 scp_success = BashUtils.scp_push(filename, rc_wctrl['ip'],
    #                                                  file_path, dest_path)
    #
    #                 if scp_success:
    #                     self.LOG.info(
    #                         "\n\nRadio program file '" + filename + "' SCP " +
    #                         "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                         ": DONE\n")
    #                 else:
    #                     raise Exception(
    #                         "\nRadio program file '" + filename + "' SCP " +
    #                         "to " + str(r_id) + "@" + rc_wctrl['ip'] +
    #                         ": FAILED")
    #
    #         # Wishful Controller RA wishful agent config file
    #         ra_id = rc_wctrl[ResControllerValidator.KEY__MANAGED_BY]
    #         path_topology_sdr = "/fins/scripts/generated/topology/sdr"
    #         file_path = self.fins_install_folder + path_topology_sdr
    #         filename = "FINS_RA_WCTRL_config.json"
    #         file_full_path_name = file_path + "/" + filename
    #
    #         if self.generate_ra_agent_config_file(ra_id, file_full_path_name):
    #             self.LOG.info("\n\nWishful Controller RA '%s' " +
    #                           "wishful agent config file generation: DONE\n" +
    #                           "Saved into: %s\n",
    #                           ra_id,
    #                           file_full_path_name)
    #         else:
    #             raise Exception("Wishful Controller RA '" + ra_id +
    #                             "'wishful agent config file generation FAILED")
    #
    #
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #         return False
