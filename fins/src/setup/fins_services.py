# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback
from fins.src.support.bash.utils import BashUtils as BU


class FINSServices:

    class FinsServiceResponse:

        def __init__(self, success=False, streams=list()):
            self._success = success
            self._streams = streams

        def is_success(self):
            return self._success

        def get_streams(self):
            return self._streams

    INJFOMR_KEY__FILENAME = 'filename'
    INJFOMR_KEY__DESTINATION_IP = 'destination_ip'
    INJFOMR_KEY__DESTINATION_PATH = 'destination_path'
    INJFOMR_KEY__SOURCE_PATH = 'source_path'

    OPERATION__INSTALL = 'INSTALLING'
    OPERATION__RUN = 'LAUNCHING'
    OPERATION__INITIALISE = 'INITIALISING'

    INJFOMR__KEYS = [INJFOMR_KEY__FILENAME,
                     INJFOMR_KEY__DESTINATION_IP,
                     INJFOMR_KEY__DESTINATION_PATH,
                     INJFOMR_KEY__SOURCE_PATH]

    def __init__(self):
        self.LOG = logging.getLogger(__name__)

    def validate_injection_form(self, form):

        try:
            assert isinstance(form, dict)

            for key in self.INJFOMR__KEYS:
                assert key in form
                assert isinstance(form[key], str)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def _inject_and_run(self, inj_form, command):
        try:
            # VALIDATE...

            if not self.validate_injection_form(inj_form):
                raise Exception("Invalid injection form: " + str(inj_form))
            if not isinstance(command, str):
                raise Exception("Command is not str: " + str(type(inj_form)))

            # INJECT...

            scp_transfer_done = BU.scp_push(
                inj_form[self.INJFOMR_KEY__FILENAME],
                inj_form[self.INJFOMR_KEY__DESTINATION_IP],
                inj_form[self.INJFOMR_KEY__SOURCE_PATH],
                inj_form[self.INJFOMR_KEY__DESTINATION_PATH])
            if not scp_transfer_done:
                raise Exception("SCP file transfer FAILED")

            # RUN!
            streams = BU.ssh_command(command,
                                     inj_form[self.INJFOMR_KEY__DESTINATION_IP])

            if streams is None:
                raise Exception("SSH command FAILED")

            return FINSServices.FinsServiceResponse(True, streams)

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return FINSServices.FinsServiceResponse(False, [])

    def _log_setup(self, operation, service_name, inj_form, command):

        self.LOG.info("\n\n%s %s:" +
                      "\nSOURCE: %s%s" +
                      "\nDESTINATION: @%s:%s" +
                      "\nCOMMAND: %s\n",
                      operation,
                      service_name,
                      inj_form[self.INJFOMR_KEY__SOURCE_PATH],
                      inj_form[self.INJFOMR_KEY__FILENAME],
                      inj_form[self.INJFOMR_KEY__DESTINATION_IP],
                      inj_form[self.INJFOMR_KEY__DESTINATION_PATH],
                      command)

    def _prepare_command(self, inj_form):
        command1 = 'cd ' + inj_form[self.INJFOMR_KEY__DESTINATION_PATH]
        command2 = "sudo chmod 777 " + inj_form[self.INJFOMR_KEY__FILENAME]
        command3 = "./" + inj_form[self.INJFOMR_KEY__FILENAME] + ";"
        return command1 + " && " + command2 + " && " + command3

    def setup_RabbitMQ(self, vm_rabbitmq_ip, fed_username, fins_install_dir):
        try:
            service_name = "RabbitMQ Server"
            destination_folder = "/home/" + fed_username

            to_be_installed = True
            to_be_run = True
            to_be_initilised = True

            script_folder = fins_install_dir + "/fins/scripts/fixed/"

            inj_form = dict()

            inj_form[self.INJFOMR_KEY__FILENAME] = None
            inj_form[self.INJFOMR_KEY__DESTINATION_IP] = vm_rabbitmq_ip
            inj_form[self.INJFOMR_KEY__DESTINATION_PATH] = destination_folder
            inj_form[self.INJFOMR_KEY__SOURCE_PATH] = script_folder

            if to_be_installed:

                script_name = "RabbitMQ_install.sh"
                inj_form[self.INJFOMR_KEY__FILENAME] = script_name

                command = self._prepare_command(inj_form)

                self._log_setup(self.OPERATION__INSTALL, service_name,
                                inj_form, command)

                rsp = self._inject_and_run(inj_form, command)
                assert isinstance(rsp, FINSServices.FinsServiceResponse)
                if not rsp.is_success():
                    self.LOG.info('FAILED')
                    raise Exception("INSTALL remote request FAILED")

            if to_be_run:

                script_name = "RabbitMQ_run.sh"
                inj_form[self.INJFOMR_KEY__FILENAME] = script_name

                command = self._prepare_command(inj_form)

                self._log_setup(self.OPERATION__RUN, service_name,
                                inj_form, command)

                rsp = self._inject_and_run(inj_form, command)
                assert isinstance(rsp, FINSServices.FinsServiceResponse)
                if not rsp.is_success():
                    self.LOG.info('FAILED')
                    raise Exception("RUN remote request FAILED")

            if to_be_initilised:

                script_name = "RabbitMQ_init.sh"
                inj_form[self.INJFOMR_KEY__FILENAME] = script_name

                command = self._prepare_command(inj_form)

                self._log_setup(self.OPERATION__INITIALISE, service_name,
                                inj_form, command)

                rsp = self._inject_and_run(inj_form, command)
                assert isinstance(rsp, FINSServices.FinsServiceResponse)
                if not rsp.is_success():
                    self.LOG.info('FAILED')
                    raise Exception("INITIALISE remote request FAILED")

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False
