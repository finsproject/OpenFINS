# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging

LOG = logging.getLogger(__name__)


class VmsLoadBalancer:

    @staticmethod
    def perform(vms, ec, rms, ras, services, rcs, rs, max_weight=5):
        task_allocation = VmsLoadBalancer.check_weights(vms, ec, rms, ras,
                                                        services, rcs, rs,
                                                        max_weight)
        VmsLoadBalancer.update_tasks(task_allocation, max_weight)
        return VmsLoadBalancer.show_assignment(ec, rms, ras, services, rcs, rs)

    @staticmethod
    def check_weights(vms, ec, rms, ras, services, rcs, rs, max_weight=5):
        vm_per_type = dict()
        for index in vms:
            if 'type' in vms[index]:
                t = vms[index]['type']
                if t not in vm_per_type:
                    vm_per_type[t] = dict()
                    vm_per_type[t]['vm_total'] = 0
                    vm_per_type[t]['vms'] = list()
                vm_per_type[t]['vm_total'] += 1
                vm_per_type[t]['vms'].append(vms[index])

        tasks = list()
        tasks.append(ec)
        for rm in rms:
            tasks.append(rms[rm])
        for ra in ras:
            tasks.append(ras[ra])
        for s in services:
            tasks.append(services[s])
        for rc in rcs:
            tasks.append(rcs[rc])
        for r in rs:
            tasks.append(rs[r])

        task_4_type_weight = dict()

        for task in tasks:
            if 'vm_weight' in task:
                t = task['vm']
                if t not in task_4_type_weight:
                    task_4_type_weight[t] = dict()
                w = task['vm_weight']
                if w not in task_4_type_weight[t]:
                    task_4_type_weight[t][w] = dict()
                    task_4_type_weight[t][w]['task_total'] = 0
                    task_4_type_weight[t][w]['weight_total'] = 0
                    task_4_type_weight[t][w]['tasks'] = list()
                task_4_type_weight[t][w]['task_total'] += 1
                task_4_type_weight[t][w]['weight_total'] += int(w)
                task_4_type_weight[t][w]['tasks'].append(task)

        task_allocation = dict()
        for index in vms:
            task_allocation[index] = dict()
            task_allocation[index]['vm'] = vms[index]
            task_allocation[index]['load'] = 0
            task_allocation[index]['assigned_tasks'] = list()

        for t in task_4_type_weight:
            if t[:2] == 'VM':
                vm_index = t[3:]
                #LOG.debug("Specific allocations on VM."+vm_index)
                sum = 0
                for w in task_4_type_weight[t]:
                    sum += int(task_4_type_weight[t][w]['weight_total'])
                if sum > max_weight:
                    LOG.warning("VM[%s] overloaded (%s/%s)",
                                vm_index, sum, max_weight)
                    raise Exception("Task cannot be allocated")
                else:
                    task_allocation[vm_index]['load'] = sum
                    for w in task_4_type_weight[t]:
                        task_allocation[vm_index]['assigned_tasks']. \
                            extend(task_4_type_weight[t][w]['tasks'])
                    #LOG.debug("Specific allocations on VM." + vm_index + "\nTASKS: " + str(task_allocation[vm_index]['assigned_tasks']))

        for t in task_4_type_weight:
            if t[:2] != 'VM':
                # log.debug('t: %s', t)
                for w in range(max_weight, -1, -1):
                    w = str(w)
                    if w in task_4_type_weight[t]:
                        # log.debug('w: %s', w)
                        for task in task_4_type_weight[t][w]['tasks']:
                            load_min = max_weight
                            vm_index = None
                            for index in task_allocation:
                                if t == task_allocation[index]['vm']['type']:
                                    if task_allocation[index]['load'] == 0:
                                        vm_index = index
                                        load_min = task_allocation[index][
                                            'load']
                                        break
                                    elif (task_allocation[index]['load'] <
                                          load_min) \
                                            and \
                                            (task_allocation[index]['load'] +
                                             int(task['vm_weight']) <=
                                             max_weight):
                                        vm_index = index
                                        load_min = task_allocation[index][
                                            'load']
                            if vm_index is None:
                                raise Exception("Task cannot be allocated")
                            else:
                                extraload = int(task['vm_weight'])
                                task_allocation[vm_index]['load'] += extraload
                                task_allocation[vm_index][
                                    'assigned_tasks'].append(
                                    task)

        return task_allocation

    @staticmethod
    def update_tasks(task_allocation, max_weight=5):
        for index in task_allocation:
            ip = task_allocation[index]['vm']['ip']
            vm_id = task_allocation[index]['vm']['id']
            total_load = str(task_allocation[index]['load']) + "/" + \
                         str(max_weight)
            for task in task_allocation[index]['assigned_tasks']:
                task['ip'] = ip
                task['host_ip'] = ip  # TODO: legacy, to be removed
                task['vm_id'] = vm_id
                task['vm_total_load'] = str(
                    task['vm_weight']) + "/" + total_load

    @staticmethod
    def show_assignment(ec, rms, ras, services, rcs, rs, lpadding=30):
        t = '\nModule EC ' + ec['id']
        t = t.ljust(lpadding, " ")
        t = (t + ' is assigned to ' + ec['vm_id'] + ", " +
             ec['ip']).ljust(lpadding * 2, " ")
        s = t + " [L: " + ec['vm_total_load'] + "]"
        for index in rms:
            task = rms[index]
            t = '\nModule RM ' + task['id']
            t = t.ljust(lpadding, " ")
            t = (t + ' is assigned to ' + task['vm_id'] + ", " +
                 task['ip']).ljust(lpadding * 2, " ")
            s += t + " [L: " + task['vm_total_load'] + "]"
        for index in ras:
            task = ras[index]
            t = '\nModule RA ' + task['id']
            t = t.ljust(lpadding, " ")
            t = (t + ' is assigned to ' + task['vm_id'] + ", " +
                 task['ip']).ljust(lpadding * 2, " ")
            s += t + " [L: " + task['vm_total_load'] + "]"
        for index in services:
            task = services[index]
            t = '\nService ' + task['id']
            t = t.ljust(lpadding, " ")
            t = (t + ' is assigned to ' + task['vm_id'] + ", " +
                 task['ip']).ljust(lpadding * 2, " ")
            s += t + " [L: " + task['vm_total_load'] + "]"
        for index in rcs:
            task = rcs[index]
            t = '\nResource Controller ' + task['id']
            t = t.ljust(lpadding, " ")
            t = (t + ' is assigned to ' + task['vm_id'] + ", " +
                 task['ip']).ljust(lpadding * 2, " ")
            s += t + " [L: " + task['vm_total_load'] + "]"
        for index in rs:
            task = rs[index]
            t = '\nResource ' + task['id']
            t = t.ljust(lpadding, " ")
            t = (t + ' is assigned to ' + task['vm_id'] + ", " +
                 task['ip']).ljust(lpadding * 2, " ")
            s += t + " [L: " + task['vm_total_load'] + "]"
        return s

