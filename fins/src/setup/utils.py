# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import time
from fins.src.support.bash.utils import BashUtils
from fins.src.support.bash.script_gen.topology_sg import TopologyScriptGen \
    as TSG
from fins.src.support.bash.script_gen.host_keycollector_sg import \
    HostKeyCollector as HKC

from fins.src.support.bash.script_gen.mongoose_sg import \
     Mongoose
from fins.confs.generator.constants import FINSConstants
from fins.src.support.topology.topology_configurator_sdn import \
    TopologyConfiguratorSDN, OVSTopologyScriptGen

from fins.src.support.ovs.ovs_script_manager import OvsScriptManager as OSM

LOG = logging.getLogger(__name__)

class FinsSetupUtils:

    # @staticmethod
    # def assign_extra_ovs_controller(rcs, candidate_id=None):
    #     xovs_controller = None
    #     if candidate_id is not None:
    #         for index in rcs:
    #             rc = rcs[index]
    #             # LOG.debug("<rc['id']> %s == %s <candidate_id>",
    #             #           rc['id'], candidate_id)
    #             if rc['id'] == candidate_id:
    #                 xovs_controller = rc
    #                 break
    #
    #     if xovs_controller is None:
    #         for index in rcs:
    #             rc = rcs[index]
    #             if rc['type'] == FINSConstants.TYPE_RC__ONOS:
    #                 xovs_controller = rc
    #                 LOG.warning("Assigned first ONOS rc as extra ovs controler")
    #                 break
    #
    #     assert xovs_controller is not None
    #
    #     return xovs_controller

    @staticmethod
    def perform_TopologyConfigurationSDN(rs, rcs, ras, links, fed_username, cwd,
                                         save_folder=None):

        tc = TopologyConfiguratorSDN()

        descriptor = tc.create_bridge_descriptor(rs, rcs, ras, links)

        # scripts = OVSTopologyScriptGen.generate(descriptor)

        osm = OSM()
        scripts = osm.gencode_from_descriptor(descriptor)

        build_scripts = dict()
        for key in scripts:
            bs = "\n".join(scripts[key]['build'])
            build_scripts[key] = bs

        TSG.save(build_scripts, cwd)

        destroy_script = "# !/usr/bin/env bash"
        for key in scripts:
            ip = descriptor[key]['ip']
            ds = " && ".join(scripts[key]['destroy'])
            destroy_script += "\nssh -oStrictHostKeyChecking=no -t " + \
                              fed_username + "@" + \
                              ip + " '" + ds + " && exit '"
        TSG.save_sdn_cleaner(destroy_script, cwd)

        time.sleep(1)

        TSG.do_remote_configuration(rs, cwd)
        # LOG.warning("BRIDGE REMOTE CONFIGURATION DISABLED!!!")

        return dict(
                    descriptor=descriptor,
                    scripts=scripts
                )

    @staticmethod
    def perform_HostKeyCollector(vms, fed_username, fins_install_folder):

        hkc = HKC(vms, fed_username, fins_install_folder)

        hkc.run()

        # script = HKC.generate(vms, fed_username)
        #
        # HKC.save(script, cwd)
        #
        # HKC.run(cwd)

    @staticmethod
    def generate_Mongoose(vms, fed_username, fins_install_folder):
        pk = Mongoose(vms, fed_username, fins_install_folder)

        pk.init()

        # script = HKC.generate(vms, fed_username)
        #
        # HKC.save(script, cwd)
        #
        # HKC.run(cwd)

    @staticmethod
    def add2Mongoose(fed_username, ip, fins_install_folder, cmd):
        Mongoose.add_cmd_to_mongoose(fed_username, ip, fins_install_folder, cmd)
