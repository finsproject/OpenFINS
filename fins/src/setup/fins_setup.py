# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import os
import time
import subprocess
from fins.src.support.bash.script_generator import BashScriptGenerator as BSG
from fins.src.support.bash.ssh_scp import RemoteScriptLauncher as RSL

LOG = logging.getLogger(__name__)


class FinsSetupSupport:

    @staticmethod
    def unlock_hosts(vms, fed_username):
        filename = 'host_knocker.sh'
        filepath = 'fins/scripts/'
        filepn = filepath + filename
        BSG.generate_host_explorer(filepn, vms, fed_username)
        if not os.path.isfile(filepn):
            time.sleep(1)
        bash_command1 = "sudo chmod 777 " + filepn + ";"
        bash_command2 = " ./" + filepn
        subprocess.call(bash_command1, shell=True)
        subprocess.call(bash_command2, shell=True)


    @staticmethod
    def setup_RabbitMQ(rmq_data, fed_username, working_dir):
        vm_rabbitmq_ip = rmq_data['ip']
        script_name = "set_rabbitmq.sh"
        destination_folder = "/home/" + fed_username

        LOG.info("\n\nSetting up RabbitMQ @" + vm_rabbitmq_ip +
                 " with script " + script_name +
                 " run in remote folder " + destination_folder)
        script_folder = working_dir + "/fins/scripts/fixed/"
        injection = RSL.scp_inject(script_name, vm_rabbitmq_ip,
                                   script_folder, destination_folder)
        if injection:
            bash_command1 = 'cd '+destination_folder + " && "
            bash_command2 = "sudo chmod 777 " + script_name + " && "
            bash_command3 = "./" + script_name + ";"
            bash_command = bash_command1 + bash_command2 + bash_command3
            LOG.info("It may take some time... ")
            response = RSL.ssh_run_remote_command(bash_command, vm_rabbitmq_ip)
            # LOG.info("It may take several minutes... ")
            # LOG.debug("STDOUT:\n" + str(response[1].read()))
            # LOG.debug("STDERR:\n" + str(response[2].read()))
            LOG.info("\n\nRabbitMQ server should be running @" + vm_rabbitmq_ip)
            return True

        else:

            return False

    @staticmethod
    def setup_MongoDB(mdb_data, fed_username, working_dir):
        vm_mongodb_ip = mdb_data['ip']
        script_name = "set_mongodb.sh"
        destination_folder = "/home/" + fed_username

        LOG.info("\n\nSetting up MongoDB @" + vm_mongodb_ip +
                 " with script " + script_name +
                 " run in remote folder " + destination_folder)
        script_folder = working_dir + "/fins/scripts/fixed/"
        injection = RSL.scp_inject(script_name, vm_mongodb_ip,
                                   script_folder, destination_folder)
        if injection:
            bash_command1 = 'cd ' + destination_folder + " && "
            bash_command2 = "sudo chmod 777 " + script_name + " && "
            bash_command3 = "./" + script_name + ";"
            bash_command = bash_command1 + bash_command2 + bash_command3
            LOG.info("It may take some time... ")
            response = RSL.ssh_run_remote_command(bash_command, vm_mongodb_ip)
            # LOG.info("It may take several minutes... ")
            # LOG.debug("STDOUT:\n" + str(response[1].read()))
            # LOG.debug("STDERR:\n" + str(response[2].read()))
            LOG.info("\n\nMongoDB should be running @" + vm_mongodb_ip)
            return True

        else:

            return False

    @staticmethod
    def setup_ONOS(onos_data, fed_username, working_dir):
        vm_onos_ip = onos_data['ip']
        script_name = "set_onos.sh"
        destination_folder = "/home/" + fed_username

        LOG.info("\n\nSetting up ONOS @" + vm_onos_ip +
                 " with script " + script_name +
                 " run in remote folder " + destination_folder)
        script_folder = working_dir + "/fins/scripts/fixed/"
        injection = RSL.scp_inject(script_name, vm_onos_ip,
                                   script_folder, destination_folder)
        if injection:
            bash_command1 = 'cd ' + destination_folder + " && "
            bash_command2 = "sudo chmod 777 " + script_name + " && "
            bash_command3 = "./" + script_name + ";"
            bash_command = bash_command1 + bash_command2 + bash_command3
            LOG.info("It may take some time... ")
            response = RSL.ssh_run_remote_command(bash_command, vm_onos_ip)
            #
            # LOG.debug("STDOUT:\n" + str(response[1].read()))
            # LOG.debug("STDERR:\n" + str(response[2].read()))
            LOG.info("\n\nONOS should be running @" + vm_onos_ip)
            return True

        else:

            return False

    @staticmethod
    def setup_OVS_support(ovs_data, fed_username, working_dir):
        vm_ovs_ip = ovs_data['ip']
        script_name = "set_ovs.sh"
        destination_folder = "/home/" + fed_username

        LOG.info("\n\nSetting up OVS support @" + vm_ovs_ip +
                 " with script " + script_name +
                 " run in remote folder " + destination_folder)
        script_folder = working_dir + "/fins/scripts/"
        injection = RSL.scp_inject(script_name, vm_ovs_ip,
                                   script_folder, destination_folder)
        if injection:
            bash_command1 = 'cd ' + destination_folder + " && "
            bash_command2 = "sudo chmod 777 " + script_name + " && "
            bash_command3 = "./" + script_name + ";"
            bash_command = bash_command1 + bash_command2 + bash_command3
            LOG.info("It may take some time... ")
            response = RSL.ssh_run_remote_command(bash_command, vm_ovs_ip)
            # LOG.info("It may take several minutes... ")
            # LOG.debug("STDOUT:\n" + str(response[1].read()))
            # LOG.debug("STDERR:\n" + str(response[2].read()))
            LOG.info("\n\nOVS support and switches should be " +
                     "installed & configured @" + vm_ovs_ip)
            return True

        else:

            return False

    @staticmethod
    def setup_topology(rs, links, controller_ip=None, controller_port=None):
        TYPE_OVS = 'RES.IRIS_OVS'
        TYPE_USRP = 'RES.IRIS_USRP'
        TYPE_VM = 'RES.IRIS_VM'

        resources = dict()
        if_counter = dict()
        for index in rs:
            res = rs[index]
            res_type = res['type']
            res_id = res['id']

            if_counter[res_id] = 0

            bridges = dict()
            bridge_id = "br_" + res_id
            if res_type == TYPE_VM:
                if_counter[res_id] += 1
                internal = dict(
                    int1=dict(
                        of_port=if_counter[res_id]
                    )
                )
                br = dict(
                    interfaces=dict(
                        internal=internal,
                        gre=dict()
                    )
                )
                bridges[bridge_id] = br

            elif res_type == TYPE_OVS:
                br = dict(
                    interfaces=dict(
                        gre=dict()
                    )
                )
                bridges[bridge_id] = br

            elif res_type == TYPE_USRP:
                # tap = ['tap1', 'tap2']
                if_counter[res_id] += 2
                tap = dict(
                    tap1=dict(
                        of_port=if_counter[res_id]
                    ),
                    tap2=dict(
                        of_port=if_counter[res_id]
                    )
                )
                br = dict(
                    interfaces=dict(
                        tap=tap,
                        gre=dict()
                    )
                )
                bridges[bridge_id] = br
            if (controller_ip is not None) and (controller_port is not None):
                bridges[bridge_id]['controller'] = dict(
                    ip=controller_ip,
                    port=controller_port
                )

            else:
                print("Invalid Resource Type: %s", res_type)

            resources[res_id] = dict(
                bridges=bridges
            )

        for index in links:
            link = links[index]
            src = link['source']
            dst = link['destination']

            res_src = None
            for index in rs:
                res = rs[index]
                if res['id'] == src:
                    res_src = res
                    break
            assert res_src is not None
            if_counter[src] += 1

            res_dst = None
            for index in rs:
                res = rs[index]
                if res['id'] == dst:
                    res_dst = res
                    break
            assert res_dst is not None
            if_counter[dst] += 1

            gre_src = dict(
                remote_ip=res_dst['ip'],
                of_port=if_counter[src],
                terminal_id=dst
            )
            gre_src_name = "gre_to_" + dst

            gre_dst = dict(
                remote_ip=res_src['ip'],
                of_port=if_counter[dst],
                terminal_id=src
            )
            gre_dst_name = "gre_to_" + src

            br_id = "br_" + src
            gre_src_list = resources[src]['bridges'][br_id]['interfaces']['gre']
            gre_src_list[gre_src_name] = gre_src

            br_id = "br_" + dst
            gre_dst_list = resources[dst]['bridges'][br_id]['interfaces']['gre']
            gre_dst_list[gre_dst_name] = gre_dst

        return resources