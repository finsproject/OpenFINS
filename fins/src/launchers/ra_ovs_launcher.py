# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.


import configobj
import os
import time
import traceback

from importlib import import_module
from fins.src.support.rabbit_mq.rmq_manager import RMQManager
from fins.src.support.log.log_manager import LogManager
from fins.src.setup.vms_loadbalancer import VmsLoadBalancer as VLB
from fins.src.setup.utils import FinsSetupUtils as FSU
# from fins.src.setup.fins_setup import FinsSetupSupport as FSS ## TODO: review it
from fins.src.setup.fins_services import FINSServices
from fins.src.setup.fins_controllers import FINSControllers
from fins.confs.generator.constants import FINSConstants
from fins.src.support.bash.utils import BashUtils as BU

from fins.src.support.topology.topology_configuration_sdr import \
    TopologyConfiguratorSDR

from fins.src.launchers.basic_launcher import BasicLauncher
from fins.confs.generator.controllers import ResControllerValidator
from fins.confs.generator.resources import ResourceWAValidator
from fins.src.support.comm.rest import Rest


class RAOvsLauncher(BasicLauncher):

    def __init__(self):

        super(RAOvsLauncher, self).__init__()

        self._rmq_d = None
        self._ra_data_d = None

        self.working_dir = None

        self.keepalive = True

    def set_fins_vars_from_config(self):
        try:
            if self.config is None:
                raise Exception("Config file not yet assigned")
            elif not isinstance(self.config, dict):
                raise Exception("Config file not yet assigned")
            else:
                self._rmq_d = self.config['RMQ']
                self._ra_data_d = self.config['RA_DATA']

                self.working_dir = os.getcwd()

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_ra_ovs_setup(self):

        try:
            section = "RA OVS setup"

            self.open_section(section)

            module = self._ra_data_d['module']
            mod = import_module('fins.src.modules.ra.' + module)
            class_inst = getattr(mod, 'ModuleAgent')

            ra_module = class_inst(self._rmq_d,
                                   self._ra_data_d)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_keepalive_loop(self):
        try:
            section = "Keep Alive loop"

            self.open_section(section)

            counter = 0
            reminder_interval = 300
            loop_step = 1
            try:
                while self.keepalive:
                    counter += loop_step
                    time.sleep(loop_step)

                    if counter == reminder_interval:
                        s = 'REMINDER: Ctrl+C to STOP RA_WCTRL ' + \
                            self._ra_data_d['id']
                        counter = 0
                        self.LOG.debug("\n\n%s",
                                       LogManager.frame_msg(s, "X"))
            except KeyboardInterrupt:
                pass

            self.close_section(section, 'EXITED')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch(self, config_file, init_file="__init__.fins"):

        super(RAOvsLauncher, self).launch(config_file)

        # Set other variables

        self.open_section("Retrieve and configure config vars")

        if not self.set_fins_vars_from_config():
            raise Exception("Failed vars configuration")

        self.close_section("Retrieve and configure config vars", 'DONE')

        # Splash

        LogManager.splash_fins_RA(self.LOG)

        # RA_WCTRL setup

        if not self.perform_ra_ovs_setup():
            raise Exception("RA OVS setup failed")

        # Keep Alive loop

        time.sleep(5)

        if not self.perform_keepalive_loop():
            raise Exception("Keep Alive loop closed in abnormal way")
