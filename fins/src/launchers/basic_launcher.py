# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import configobj
import logging
from abc import ABCMeta, abstractmethod
from fins.src.support.log.log_manager import LogManager


class BasicLauncher:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.LOG = None
        self.config = None
        self._log_d = None

    def configure_log(self, log_level, log_file, filemode):
        # LOG_FORMAT = ('%(levelname) -8s %(asctime)s %(name) -45s %(funcName) '
        #               '-20s %(lineno) -5d: %(message)s')
        LOG_FORMAT = ('%(levelname) -8s %(name) -45s %(funcName) '
                      '-20s %(lineno) -5d: %(message)s')

        level = logging.getLevelName(log_level)
        logging.basicConfig(filename=log_file,
                            filemode=filemode,
                            level=level)

        console = logging.StreamHandler()

        console.setLevel(level)
        formatter = logging.Formatter(LOG_FORMAT)

        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)

        for key in logging.Logger.manager.loggerDict:
            if key.find('fins') < 0:
                # print("Filtered: " + key)
                logging.getLogger(key).setLevel(logging.INFO)
            else:
                # print("NOT Filtered: " + key + "[" +
                #       str(key.find('fins')) + "]")
                pass

        lg = logging.getLogger(__name__)
        return lg

    def breakpoint(self, wait_for_input, hint=''):
        if wait_for_input:
            input("\n>>> Break point"
                  "\n" + hint +
                  "\n\nPress Enter to continue...")

    def open_section(self, section):
        self.LOG.info("\n\n%s", LogManager.frame_msg(
            section + "...",
            "."))

    def close_section(self, section, response):
        self.LOG.info("\n\n%s", LogManager.frame_msg(
            section + ": " + response,
            "#"))

    @abstractmethod
    def launch(self, config_file):

        # Retrieve config file

        self.config = configobj.ConfigObj(config_file)

        print(str(self.config))

        self._log_d = self.config['LOGGING']

        self.LOG = self.configure_log(self._log_d['level'],
                                      self._log_d['file'],
                                      self._log_d['filemode'])

        self.LOG.info("ConfigFile has been properly retrieved")
        self.LOG.info("LOG has been enabled")
