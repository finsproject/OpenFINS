# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.


import configobj
import os
import time
import traceback
import subprocess
import json

from configobj import ConfigObj
from importlib import import_module
from fins.src.support.rabbit_mq.rmq_manager import RMQManager
from fins.src.support.log.log_manager import LogManager
from fins.src.setup.vms_loadbalancer import VmsLoadBalancer as VLB
from fins.src.setup.utils import FinsSetupUtils as FSU
# from fins.src.setup.fins_setup import FinsSetupSupport as FSS ## TODO: review it
from fins.src.setup.fins_services import FINSServices
from fins.src.setup.fins_controllers import FINSControllers
from fins.confs.generator.constants import FINSConstants
from fins.src.support.bash.utils import BashUtils as BU

from fins.src.support.topology.topology_configuration_sdr import \
    TopologyConfiguratorSDR

from fins.src.launchers.basic_launcher import BasicLauncher
from fins.confs.generator.controllers import ResControllerValidator
from fins.confs.generator.resources import ResourceWAValidator
from fins.confs.generator.modules import ModuleRAValidator
from fins.src.support.comm.rest import Rest


class FINSLauncher(BasicLauncher):

    def __init__(self):

        super(FINSLauncher, self).__init__()

        self._gbl_d = None
        self._vms_d = None
        self._ei_d = None
        self._rms_d = None
        self._ras_d = None
        self._services_d = None
        self._rcs_d = None
        self._rs_d = None
        self._links_d = None

        self._rmq_d = None
        self._mongo_d = None
        self._rest_d = None

        self.step_by_step = None
        self.vm_max_load = None
        self.fed_username = None

        self.working_dir = None

        self.rmq_manager = None

        self.fins_bridge_descriptor = None

        self.keepalive = True

    def sbs_next_breakpoint(self, hint):
        self.breakpoint(
            self.step_by_step,
            "NEXT STEP: " + hint
        )

    def set_fins_vars_from_config(self):
        try:
            if self.config is None:
                raise Exception("Config file not yet assigned")
            elif not isinstance(self.config, dict):
                raise Exception("Config file not yet assigned")
            else:
                self._gbl_d = self.config['GLOBAL']
                self._vms_d = self.config['VMs']
                self._ei_d = self.config['MODULES']['EI']
                self._rms_d = self.config['MODULES']['RMs']
                self._ras_d = self.config['MODULES']['RAs']
                self._services_d = self.config['SERVICES']
                self._rcs_d = self.config['RESOURCES']['RCs']
                self._rs_d = self.config['RESOURCES']['Rs']
                self._links_d = self.config['TOPOLOGY']['LINKS']

                self._rmq_d = self._services_d['RABBITMQ']
                self._mongo_d = self._services_d['MONGODB']
                self._rest_d = self._services_d['FINSREST']

                self.step_by_step = True if (
                            self._gbl_d['step_by_step'] == 'True') else False
                self.vm_max_load = int(self._gbl_d['vm_max_load'])
                self.fed_username = self._gbl_d['jfed_username']

                self.working_dir = os.getcwd()

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_load_balancing(self):
        try:
            section = "VMs load balancing"

            self.open_section(section)

            response = VLB.perform(self._vms_d,
                                   self._ei_d,
                                   self._rms_d,
                                   self._ras_d,
                                   self._services_d,
                                   self._rcs_d,
                                   self._rs_d,
                                   self.vm_max_load)

            self.LOG.info('\n'
                          '\nLOAD BALANCER PROPOSED ALLOCATION'
                          '\n' + response + '\n')

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_mongoose_creation(self):
        try:
            section = "Mongoose creation"

            self.open_section(section)

            FSU.generate_Mongoose(self._vms_d,
                                  self.fed_username,
                                  self.working_dir)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_hostkey_collection(self):
        try:
            section = "Host-key collection"

            self.open_section(section)

            if self._gbl_d['hostkeys_collector'] == 'True':
                FSU.perform_HostKeyCollector(self._vms_d,
                                             self.fed_username,
                                             self.working_dir)
            else:
                self.LOG.info('Host keys already performed')

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_rabbitmq_setup(self):

        try:
            section = "RabbitMQ setup"

            fs = FINSServices()

            self.open_section(section)

            if self._rmq_d['ei_booted'] == 'True':
                response = fs.setup_RabbitMQ(self._rmq_d['host_ip'],
                                             self.fed_username,
                                             self.working_dir)
                if not response:
                    self.LOG.warning("Something went wrong with RABBITMQ setup")
            else:
                self.LOG.info("\n\nRabbitMQ server already up and running @" +
                              self._rmq_d['ip'])

            adj_rmq = dict(
                host_ip=self._rmq_d['host_ip'],
                port=self._rmq_d['port'],
                config_port=self._rmq_d['config_port'],
                username='admin',
                password=self._rmq_d['users']['admin'],
                log_level=self._rmq_d['log_level']
            )

            self.rmq_manager = RMQManager(adj_rmq, True)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

        return False

    def perform_mongodb_setup(self):

        try:
            section = "MongoDB setup"

            self.open_section(section)

            self.LOG.warning(section + " not impemented")

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

        return False

    def _fill_arp_tables(self, bridge_descriptor, fed_username, cwd):
        try:

            ARP_DONE = "ARP RUN & DONE!"
            ifs = dict()

            for key in bridge_descriptor:
                brd = bridge_descriptor[key]
                ip = brd['ip']
                cmd = "(ip -o link | awk '{print \$2,\$(NF-2)}') 2>&1"
                response = BU.ssh_get_remote_output(cmd, fed_username, ip)

                indent = "    "
                summary = ""
                if response is None:
                    response = list()
                for idx, line in enumerate(response):
                    s = line.strip(' \t\n\r')
                    summary += indent + s + "\n"
                    response[idx] = s
                # cmd = "ssh -oStrictHostKeyChecking=no -t %s@%s " \
                #       "\"(ip -o link | awk '{print \$2,\$(NF-2)}') 2>&1\"" % \
                #       (fed_username, ip)
                # out = None
                # try:
                #     out = subprocess.check_output(cmd, shell=True)
                # except subprocess.CalledProcessError as e:
                #     self.LOG.error(str(e.output))
                #     traceback.print_exc()
                #     self.LOG.error("CRITICAL ERROR, FINS aborted")
                #     exit(1)
                #
                # indent = "    "
                # response = ""
                # lines = out.decode("utf-8").split('\n')
                # for idx, line in enumerate(lines):
                #     s = line.strip(' \t\n\r')
                #     response += indent + s + "\n"
                #     lines[idx] = s
                self.LOG.debug("\n\nCollected Interfaces @%s:\n%s", ip,
                               summary)
                # self.LOG.debug("\nInterfaces @%s: %s\n" % (ip, out))
                # lines = out.decode("utf-8").split('\n')

                # Retrieve all internal interfaces defined
                for line in response:
                    if len(line) == 0:
                        break
                    splitted = line.split(':', 1)
                    if_name = splitted[0].split('@', 1)
                    if isinstance(if_name, list):
                        if_name = if_name[0].strip()
                    if_mac = splitted[1].strip()

                    for iface in brd['interfaces']['int']:
                        if iface['id'] == if_name:
                            ip_exploded = iface['ip'].split(".")
                            if not isinstance(ip_exploded, list):
                                self.LOG.error(ip_exploded)
                                raise Exception("ip_exploded is not a list")
                            assert len(ip_exploded) == 4
                            if ip_exploded[0] not in ifs:
                                ifs[ip_exploded[0]] = dict()
                            ifs0 = ifs[ip_exploded[0]]
                            if ip_exploded[1] not in ifs0:
                                ifs0[ip_exploded[1]] = dict()
                            ifs1 = ifs0[ip_exploded[1]]
                            if ip_exploded[2] not in ifs1:
                                ifs1[ip_exploded[2]] = dict()
                            ifs2 = ifs1[ip_exploded[2]]
                            ifs2[ip_exploded[3]] = dict(
                                name=if_name,
                                ip=iface['ip'],
                                vm_ip=ip,
                                mac=if_mac
                            )
                            break
            # Create update
            cmds = dict()
            for k0 in ifs:
                for k1 in ifs[k0]:
                    for k2 in ifs[k0][k1]:
                        subnet = ifs[k0][k1][k2]
                        for k3 in subnet:
                            vm_ip = subnet[k3]['vm_ip']
                            if vm_ip not in cmds:
                                cmds[vm_ip] = list()
                            for x in subnet:
                                if x != k3:
                                    cmd = "sudo arp -i %s -s %s %s" % \
                                          (str(subnet[k3]['name']),
                                           str(subnet[x]['ip']),
                                           str(subnet[x]['mac']))
                                    cmds[vm_ip].append(cmd)

            # Create files
            for vm_ip in cmds:
                code = "\n".join(cmds[vm_ip])
                code += "\necho \"" + ARP_DONE + "\""
                full_path = cwd + "/" + \
                            "fins/scripts/generated/topology/sdn/arp/"
                filename = "ARP_%s.sh" % vm_ip
                file_fullpath_name = full_path + filename
                BU.save_into_file(code, file_fullpath_name)

                self.LOG.info("\n\nARP configuration for %s GENERATED: \n%s" %
                              (vm_ip, file_fullpath_name))

                if BU.scp_push(filename, vm_ip, full_path, '.'):
                    command = 'sudo chmod 777 ' + filename + ' && ' + \
                              './' + filename
                    response = BU.ssh_command(command, vm_ip)
                    self.LOG.info("\n\nARP TABLE (rady2be) configured @" + vm_ip)

            return True
        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

        return False

    def perform_sdn_topology_configuration(self):
        try:
            section = "SDN Topology configuration"

            self.open_section(section)
            rsp = FSU.perform_TopologyConfigurationSDN(self._rs_d,
                                                       self._rcs_d,
                                                       self._ras_d,
                                                       self._links_d,
                                                       self.fed_username,
                                                       self.working_dir)

            self.fins_bridge_descriptor = rsp['descriptor']
            # self.LOG.debug(
            #     'FINS BRIDGE CONFIGURATION: \n\n%s\n',
            #     json.dumps(
            #         self.fins_bridge_descriptor, sort_keys=True, indent=3)
            # )

            #self.fins_bridge_descriptor = self.enhance_bridge_decriptor()
            #self.fins_bridge_descriptor = self.enhance_bridge_decriptor()

            self._fill_arp_tables(self.fins_bridge_descriptor,
                                  self.fed_username,
                                  self.working_dir)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def enhance_bridge_decriptor(self, descriptor=None):
        if descriptor is None:
            descriptor = self.fins_bridge_descriptor
            if descriptor is None:
                return False
        for index in descriptor:
            br = descriptor[index]
            # self.LOG.debug("br id: %s", br['id'])
            br_ip = br['ip']
            # self.LOG.debug("br ip: %s", br['ip'])
            if 'gre' in br['interfaces']:
                gre_ifs = br['interfaces']['gre']
                for gre_if in gre_ifs:
                    # self.LOG.debug("   gre id: %s", gre_if['id'])
                    remote_ip = gre_if['remote_ip']
                    # self.LOG.debug("   gre r_ip: %s", remote_ip)
                    for r_index in descriptor:
                        r_br = descriptor[r_index]
                        # self.LOG.debug("       r_br id: %s", r_br['id'])
                        # self.LOG.debug("       r_br ip: %s", r_br['ip'])
                        if r_br['ip'] == remote_ip:
                            # self.LOG.debug("          match FOUND!")
                            r_gre_ifs = r_br['interfaces']['gre']
                            for r_gre_if in r_gre_ifs:
                                # self.LOG.debug("          r_gre r_ip: %s", r_gre_if['remote_ip'])
                                if r_gre_if['remote_ip'] == br_ip:
                                    # self.LOG.debug("             Assigning..")
                                    gre_if['remote_br_id'] = r_br['id']
                                    gre_if['remote_br_fins'] = r_br['associated_resource']
                                    gre_if['remote_br_jfed'] = r_br['associated_resource_jfed']
                                    gre_if['remote_of_port'] = r_gre_if['of_port']
                                    break
                            break
        return descriptor

    # def perform_sdr_topology_configuration(self):
    #
    #     try:
    #         section = "SDR Topology configuration"
    #
    #         self.open_section(section)
    #
    #         self.LOG.info("Generating wishful agent scripts & confs for "
    #                       "each resource controlled by wishful controller")
    #
    #         tc_sdr = TopologyConfiguratorSDR(self._rs_d,
    #                                          self._rcs_d,
    #                                          self.working_dir)
    #
    #         tc_sdr.configure()
    #
    #         self.close_section(section, 'DONE')
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #     return False

    def _show_active_wishful_components(self):
        WHISFUL_PREFIX_C = "FINS_wishful_controller.py"
        WHISFUL_PREFIX_A = "FINS_wishful_agent.py"

        wish_list = list()
        for rcindex in self._rcs_d:
            rc = self._rcs_d[rcindex]
            rc_type = rc[ResControllerValidator.KEY__TYPE]
            if rc_type == FINSConstants.TYPE_RC__WCTRL:
                vm_id = rc['vm_id']
                rc_id = rc['id']
                cmd = "ps -elf | grep %s" % WHISFUL_PREFIX_C
                ps = BU.ssh_get_remote_output(cmd, self.fed_username, rc['ip'])
                if ps is not None:
                    if isinstance(ps, str):
                        ps = [ps]
                    for idx, line in enumerate(ps):
                        fields = line.split()
                        # self.LOG.debug(fields)
                        if (len(fields) > 1) and ("grep" not in line):
                            # desc = [
                            #     "  [C]",
                            #     "[" + vm_id + "]",
                            #     fields[11],
                            #     "PID=" + fields[3],
                            #     fields[15]
                            # ]
                            desc = '    {:8s} {:16s} {:5s} {:6s} {:26s}'.format(
                                "[" + vm_id + "]", rc['ip'], fields[11],
                                fields[3], fields[15])
                            # line = "[C][%s] %s" %(vm_id, line)
                            # wish_list.append(" ".join(desc))
                            wish_list.append(desc)
                for rindex in self._rs_d:
                    r = self._rs_d[rindex]
                    if r['managed_by'] == rc_id:
                        vm_id = r['vm_id']
                        cmd = "ps -elf | grep %s" % WHISFUL_PREFIX_A
                        ps = BU.ssh_get_remote_output(cmd, self.fed_username,
                                                      r['ip'])
                        if ps is not None:
                            if isinstance(ps, str):
                                ps = [ps]
                            for idx, line in enumerate(ps):
                                fields = line.split()
                                # self.LOG.debug(fields)
                                if (len(fields) > 1) and ("grep" not in line):
                                    # desc = [
                                    #     "    [A]",
                                    #     "[" + vm_id + "]",
                                    #     fields[11],
                                    #     "PID=" + fields[3],
                                    #     fields[15]
                                    # ]
                                    # line = "[A][%s] %s" % (vm_id, line)
                                    desc = '    {:8s} {:16s} {:5s} ' \
                                           '{:6s} {:26s}'.\
                                        format(
                                            "[" + vm_id + "]", r['ip'],
                                            fields[11], fields[3], fields[15])
                                    # wish_list.append(" ".join(desc))
                                    wish_list.append(desc)
        self.LOG.info("\n\nCurrently active "
                      "WiSHFUL Controllers and Agents:\n%s\n",
                      str("\n".join(wish_list)))


    def perform_rcs_setup(self):

        try:

            # for index in self._rcs_d:
            #     rc = self._rcs_d[index]
            #     rc_type = rc[ResControllerValidator.KEY__TYPE]
            #     if rc_type == FINSConstants.TYPE_RC__ONOS:
            #         if not self.perform_rc_onos_setup():
            #             raise Exception("RC ONOS setup failed")
            #     elif rc_type == FINSConstants.TYPE_RC__WCTRL:
            #         if not self.perform_rc_wctrl_setup():
            #             raise Exception("RC WCTRL setup failed")
            #     else:
            #         raise Exception("Unknown RC type: " + rc_type)

            section = "RCs setup"
            self.open_section(section)

            fc = FINSControllers(self._rcs_d, self._rs_d, self._vms_d,
                                 self._rmq_d, self.fed_username,
                                 self.working_dir)

            if not fc.configure():
                raise Exception("RCs setup failed")

            self._show_active_wishful_components()

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

        return False

    # def perform_rc_onos_setup(self):
    #
    #     try:
    #         section = "RC ONOS setup"
    #
    #         self.open_section(section)
    #
    #         self.LOG.warning(section + " not impemented")
    #
    #         self.close_section(section, 'DONE')
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #     return False
    #
    # def perform_rc_wctrl_setup(self):
    #
    #     try:
    #         section = "RC WiSHFUL Controller setup"
    #
    #         self.open_section(section)
    #
    #         self.LOG.warning(section + " not impemented")
    #
    #         self.close_section(section, 'DONE')
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #     return False

    def perform_ei_setup(self):

        try:
            section = "EI setup"

            self.open_section(section)

            module = self._ei_d['module']
            mod = import_module('fins.src.modules.ei.' + module)
            class_inst = getattr(mod, 'ModuleManager')

            ei_module = class_inst(self.rmq_manager,
                                   self._ei_d,
                                   self._rms_d,
                                   self._ras_d,
                                   self._rest_d,
                                   self.enhance_bridge_decriptor()
                                   )

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def perform_conf_files_creation(self):

        try:

            for index in self._ras_d:
                ra = self._ras_d[index]
                ra_type = ra['type']
                self.LOG.debug("RA type: %s", ra_type)
                if ra_type == FINSConstants.TYPE_MOD__RA_WCTRL:
                    self.create_ra_wctrl_conf_file(ra)
                elif ra_type == FINSConstants.TYPE_MOD__RA_OVS:
                    self.create_ra_ovs_conf_file(ra)

            self.create_rest_conf_file()

            for index in self._rms_d:
                rm = self._rms_d[index]
                self.create_rm_conf_file(rm)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def create_ra_wctrl_conf_file(self, ra):

        LOG_FILE_PREFIX = "./fins/logs/log_ra_wctrl__"
        CFG_FILE_PREFIX = "./fins/confs/conf_ra_wctrl__"
        try:
            section = "Creating config file for RA WCTRL " + ra['id']

            self.open_section(section)

            log_filename = LOG_FILE_PREFIX + ra['id'] + ".log"
            conf_filename = CFG_FILE_PREFIX + ra['id'] + ".cfg"

            config = ConfigObj(indent_type='    ')
            config.filename = conf_filename

            log = dict(
                file=log_filename,
                filemode="w",
                level="DEBUG"
            )

            config['LOGGING'] = log

            rmq = dict(
                host_ip=self._rmq_d['host_ip'],
                port=self._rmq_d['port'],
                config_port=self._rmq_d['config_port'],
                username='admin',
                password=self._rmq_d['users']['admin'],
                log_level=self._rmq_d['log_level']
            )

            config['RMQ'] = rmq

            wctrl = None
            for index in self._rcs_d:
                rc = self._rcs_d[index]
                if rc['type'] == FINSConstants.TYPE_RC__WCTRL:
                    wctrl = rc
                    break

            nodes = dict()
            for index in self._rs_d:
                r = self._rs_d[index]
                if r['managed_by'] == wctrl['id']:
                    vm_idx = r['vm'].strip('VM.')
                    vm_id = self._vms_d[vm_idx]['id']
                    node = dict()
                    node['node_ip'] = self._vms_d[vm_idx]['ip']
                    node['type'] = r['type']
                    node['taps'] = r['taps']
                    node['description'] = r['description']
                    nodes[vm_id] = node


            ra_data = dict(
                id=ra['id'],
                type=ra['type'],
                module=ra['module'],
                ei_id=self._ei_d['id'],
                rm_id=ra['managed_by'],
                rest_id=self._rest_d['id'],
                ctrl_id=wctrl['id'],
                nodes=nodes
            )

            config['RA_DATA'] = ra_data



            # TODO: add list of associated resources?
            # rs = dict()
            # for index in self._rs_d:
            #     r = self._rs_d[index]
            #     rm_id = ra[ModuleRAValidator.KEY__MANAGED_BY]
            #     self.LOG.debug("RM id check: '%s' =?= '%s'", rm_id, rm['id'])
            #     if rm_id == rm['id']:
            #         ras[str(index)] = dict()
            #         ra_id = ra[ModuleRAValidator.KEY__ID]
            #         ras[str(index)]['id'] = ra_id
            #         ra_type = ra[ModuleRAValidator.KEY__TYPE]
            #         ras[str(index)]['type'] = ra_type
            #
            # config['RMs'] = rs

            config.write()

            self.LOG.info("\n\nRA WCTRL '%s' conf file saved in: %s\n",
                          ra['id'], conf_filename)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def create_rest_conf_file(self):

        LOG_FILE_PREFIX = "./fins/logs/log_rest__"
        CFG_FILE_PREFIX = "./fins/confs/conf_rest__"

        rest = self._rest_d
        try:
            section = "Creating config file for REST " + rest['id']

            self.open_section(section)

            log_filename = LOG_FILE_PREFIX + rest['id'] + ".log"
            conf_filename = CFG_FILE_PREFIX + rest['id'] + ".cfg"

            config = ConfigObj(indent_type='    ')
            config.filename = conf_filename

            log = dict(
                file=log_filename,
                filemode="w",
                level="DEBUG"
            )

            config['LOGGING'] = log

            rmq = dict(
                host_ip=self._rmq_d['host_ip'],
                port=self._rmq_d['port'],
                config_port=self._rmq_d['config_port'],
                username='admin',
                password=self._rmq_d['users']['admin'],
                log_level=self._rmq_d['log_level']
            )

            config['RMQ'] = rmq

            rest_data = dict(
                id=rest['id'],
                type=rest['type'],
                port=rest['port'],
                host_ip=rest['host_ip']
            )

            config['REST_DATA'] = rest_data

            ras = dict()
            for index in self._ras_d:
                ra = self._ras_d[index]
                ras[index] = dict()
                ras[index]['id'] = ra['id']

            config['RAs'] = ras

            rms = dict()
            for index in self._rms_d:
                rm = self._rms_d[index]
                rms[index] = dict()
                rms[index]['id'] = rm['id']

            config['RMs'] = rms

            ei = dict()
            ei['id'] = self._ei_d['id']

            config['EI'] = ei

            config.write()

            self.LOG.info("\n\nREST '%s' conf file saved in: %s\n",
                          rest['id'], conf_filename)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def create_ra_ovs_conf_file(self, ra):

        LOG_FILE_PREFIX = "./fins/logs/log_ra_ovs__"
        CFG_FILE_PREFIX = "./fins/confs/conf_ra_ovs__"
        try:
            section = "Creating config file for RA OVS " + ra['id']

            self.open_section(section)

            log_filename = LOG_FILE_PREFIX + ra['id'] + ".log"
            conf_filename = CFG_FILE_PREFIX + ra['id'] + ".cfg"

            config = ConfigObj(indent_type='    ')
            config.filename = conf_filename

            log = dict(
                file=log_filename,
                filemode="w",
                level="DEBUG"
            )

            config['LOGGING'] = log

            rmq = dict(
                host_ip=self._rmq_d['host_ip'],
                port=self._rmq_d['port'],
                config_port=self._rmq_d['config_port'],
                username='admin',
                password=self._rmq_d['users']['admin'],
                log_level=self._rmq_d['log_level']
            )

            config['RMQ'] = rmq

            ra_data = dict(
                id=ra['id'],
                type=ra['type'],
                module=ra['module'],
                ei_id=self._ei_d['id'],
                rm_id=ra['managed_by'],
                rest_id=self._rest_d['id']
            )

            config['RA_DATA'] = ra_data

            config.write()

            self.LOG.info("\n\nRA OVS '%s' conf file saved in: %s\n",
                          ra['id'], conf_filename)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def create_rm_conf_file(self, rm):

        LOG_FILE_PREFIX = "./fins/logs/log_rm__"
        CFG_FILE_PREFIX = "./fins/confs/conf_rm__"

        try:
            section = "Creating config file for RM " + rm['id']

            self.open_section(section)

            log_filename = LOG_FILE_PREFIX + rm['id'] + ".log"
            conf_filename = CFG_FILE_PREFIX + rm['id'] + ".cfg"

            config = ConfigObj(indent_type='    ')
            config.filename = conf_filename

            log = dict(
                file=log_filename,
                filemode="w",
                level="DEBUG"
            )

            config['LOGGING'] = log

            rmq = dict(
                host_ip=self._rmq_d['host_ip'],
                port=self._rmq_d['port'],
                config_port=self._rmq_d['config_port'],
                username='admin',
                password=self._rmq_d['users']['admin'],
                log_level=self._rmq_d['log_level']
            )

            config['RMQ'] = rmq

            rm_data = dict(
                id=rm['id'],
                type=rm['type'],
                module=rm['module']
            )

            config['RM_DATA'] = rm_data

            rest = dict()
            rest['id'] = self._rest_d['id']

            config['REST'] = rest

            ei = dict()
            ei['id'] = self._ei_d['id']

            config['EI'] = ei

            ras = dict()
            for index in self._ras_d:
                ra = self._ras_d[index]
                rm_id = ra[ModuleRAValidator.KEY__MANAGED_BY]
                self.LOG.debug("RM id check: '%s' =?= '%s'", rm_id, rm['id'])
                if rm_id == rm['id']:
                    ras[str(index)] = dict()
                    ra_id = ra[ModuleRAValidator.KEY__ID]
                    ras[str(index)]['id'] = ra_id
                    ra_type = ra[ModuleRAValidator.KEY__TYPE]
                    ras[str(index)]['type'] = ra_type

            config['RAs'] = ras

            config.write()

            self.LOG.info("\n\nREST '%s' conf file saved in: %s\n",
                          rest['id'], conf_filename)

            self.close_section(section, 'DONE')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch_REST(self, conf_file=None):
        try:
            CFG_FILE_PREFIX = "./fins/confs/conf_rest__"

            rest = self._rest_d
            if conf_file is None:
                conf_file = CFG_FILE_PREFIX + rest['id'] + ".cfg"

            cmd1 = "cd " + self.working_dir
            cmd2 = "nohup python3 launcher.py REST " + conf_file + " &"
            cmd = cmd1 + "  && " + cmd2
            if BU.ssh_bg_command(cmd, rest['ip']) is None:
                return False

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch_RAs(self):
        try:

            for index in self._ras_d:
                ra = self._ras_d[index]
                ra_type = ra['type']
                if ra_type == FINSConstants.TYPE_MOD__RA_WCTRL:
                    # self.create_ra_wctrl_conf_file(ra)
                    self.launch_RA_WCTRL(ra)
                    # self.LOG.warning("RA_WCTRL LAUNCH: SKIPPED")
                    # self.LOG.warning("\nRun it manually from fins folder:\n "
                    #                  "python3 launcher.py "
                    #                  "RA_WCTRL fins/confs/"
                    #                  "conf_ra_wctrl__ra_wctrl.cfg ")
                elif ra_type == FINSConstants.TYPE_MOD__RA_OVS:
                    self.launch_RA_OVS(ra)
                    # self.LOG.warning("RA_OVS LAUNCH: SKIPPED")
                    # self.LOG.warning("\nRun it manually from fins folder:\n "
                    #                  "python3 launcher.py "
                    #                  "RA_OVS fins/confs/"
                    #                  "conf_ra_ovs__ra_ovs.cfg ")
                else:
                    self.LOG.warning("Unknown RA type: %s", ra_type)
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch_RA_OVS(self, ra, conf_file=None):
        try:
            CFG_FILE_PREFIX = "./fins/confs/conf_ra_ovs__"

            if conf_file is None:
                conf_file = CFG_FILE_PREFIX + ra['id'] + ".cfg"

            cmd1 = "cd " + self.working_dir
            cmd2 = "(nohup python3 launcher.py RA_OVS " + conf_file + " &)"
            cmd = cmd1 + "  ; " + cmd2
            # if BU.ssh_bg_command(cmd, ra['ip']) is None:
            #     return False
            if BU.subprocess_ssh(cmd, self.fed_username, ra['ip']) is None:
                return False
            
            self.LOG.warning("RA_OVS LAUNCH: launched")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch_RA_WCTRL(self, ra, conf_file=None):
        try:
            CFG_FILE_PREFIX = "./fins/confs/conf_ra_wctrl__"

            if conf_file is None:
                conf_file = CFG_FILE_PREFIX + ra['id'] + ".cfg"

            cmd1 = "cd " + self.working_dir
            cmd2 = "(nohup python3 launcher.py RA_WCTRL " + conf_file + " &)"
            cmd = cmd1 + "  ; " + cmd2
            # if BU.ssh_bg_command(cmd, ra['ip']) is None:
            #     return False
            if BU.subprocess_ssh(cmd, self.fed_username, ra['ip']) is None:
                return False

            self.LOG.warning("RA_WCTRL LAUNCH: launched")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    # def perform_rest_setup(self):
    #     try:
    #         section = "FINS REST setup"
    #
    #         self.open_section(section)
    #
    #         adj_rest = dict(
    #             id=self._rest_d['id'],
    #             port=self._rest_d['port'],
    #             type=self._rest_d['type'],
    #             host_ip=self._rest_d['host_ip']
    #
    #         )
    #
    #         adj_rmq = dict(
    #             host_ip=self._rmq_d['host_ip'],
    #             port=self._rmq_d['port'],
    #             config_port=self._rmq_d['config_port'],
    #             username='admin',
    #             password=self._rmq_d['users']['admin'],
    #             log_level=self._rmq_d['log_level']
    #         )
    #
    #         rest = Rest(adj_rest, adj_rmq,
    #                     self._ras_d,
    #                     self._rms_d,
    #                     self._ei_d)
    #         rest.start()
    #
    #         self.close_section(section, 'DONE')
    #
    #         return True
    #
    #     except Exception as e:
    #         self.LOG.error(str(e))
    #         traceback.print_exc()
    #
    #         return False

    def perform_keepalive_loop(self):
        try:
            section = "Stand-by loop"

            self.open_section(section)

            counter = 0
            reminder_interval = 300
            loop_step = 1
            try:
                while self.keepalive:
                    if counter == reminder_interval:
                        s = 'REMINDER: FINS is CURRENTLY RUNNING!' \
                            ' CTRL-C to STOP it'
                        counter = 0
                        self.LOG.debug("\n\n%s",
                                       LogManager.frame_msg(s, "X"))
                    counter += loop_step
                    time.sleep(loop_step)

            except KeyboardInterrupt:
                pass

            self.close_section(section, 'EXITED')

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def launch(self, config_file, init_file="__init__.fins"):

        super(FINSLauncher, self).launch(config_file)

        # Set other variables

        self.open_section("Retrieve and configure config vars")

        if not self.set_fins_vars_from_config():
            raise Exception("Failed vars configuration")

        self.close_section("Retrieve and configure config vars", 'DONE')

        # Verify init file

        self.open_section("Check FINS proper installation")

        if os.path.isfile(init_file):
            cwd2 = configobj.ConfigObj(init_file)['fins_path']
            if self.working_dir != cwd2:
                raise Exception("Install path != current working dir. \n"
                                "If you are running into a sym link, "
                                "update " + init_file)
        else:
            raise Exception("No init file (" + init_file +
                            "): have you installed FINS?")

        self.close_section("Check FINS proper installation", 'OK')

        # Splash

        LogManager.splash_fins(self.LOG)

        self.sbs_next_breakpoint("VMs LOAD BALANCING")

        # VMs load balancing

        if not self.perform_load_balancing():
            raise Exception("Load balancing failed")

        self.sbs_next_breakpoint("MONGOOSE CREATION")

        # Mongoose creation

        if not self.perform_mongoose_creation():
            raise Exception("Mongoose creation failed")

        self.sbs_next_breakpoint("HOST KEY COLLECTION")

        # Host Key collection

        if not self.perform_hostkey_collection():
            raise Exception("Host key failed")

        self.sbs_next_breakpoint("RABBITMQ SETUP")

        # RabbitMQ setup

        if not self.perform_rabbitmq_setup():
            raise Exception("RabbitMQ setup failed")

        # self.sbs_next_breakpoint("MONGODB SETUP")
        #
        # # MONGODB setup
        #
        # if not self.perform_mongodb_setup:
        #     raise Exception("MongoDB setup failed")

        self.sbs_next_breakpoint("RCs SETUP")

        # RCs setup

        if not self.perform_rcs_setup():
            raise Exception("RCs setup failed")

        # self.LOG.warning("RCs SETUP: SKIPPED")

        self.sbs_next_breakpoint("SDN TOPOLOGY CONFIGURATION")

        # SDN Topology configuration

        if not self.perform_sdn_topology_configuration():
            raise Exception("SDN Topology configuration failed")

        # self.sbs_next_breakpoint("SDR TOPOLOGY CONFIGURATION")
        #
        # # SDR Topology configuration
        #
        # # if not self.perform_sdr_topology_configuration():
        # #     raise Exception("SDR Topology configuration failed")
        #
        # self.LOG.warning("SDR Topology configuration: SKIPPED")

        self.sbs_next_breakpoint("EI setup")

        # EI Setup

        if not self.perform_ei_setup():
            raise Exception("EI Setup failed")

        self.sbs_next_breakpoint("CONF FILE CREATION")

        # Configuration File generation

        if not self.perform_conf_files_creation():
            raise Exception("Configuration file generation")

        self.sbs_next_breakpoint("FINS REST LAUNCH")

        # FINS REST setup

        if not self.launch_REST():
            raise Exception("FINS REST setup failed")
        # self.LOG.warning("FINS REST LAUNCH: SKIPPED")

        self.sbs_next_breakpoint("RAs LAUNCH")

        # RAs launch

        if not self.launch_RAs():
            raise Exception("RAs setup failed")

        self.sbs_next_breakpoint("KEEPALIVE LOOP")

        # Keep Alive loop

        time.sleep(5)

        if not self.perform_keepalive_loop():
            raise Exception("Keep Alive loop closed in abnormal way")
