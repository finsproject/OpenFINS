# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import json
import requests
import logging

from requests.auth import HTTPBasicAuth
from fins.src.support.comm.response_wrapper import ResponseWrapper

LOG = logging.getLogger(__name__)


class ONOSInstruction:

    TYPE_L2MODIFICATION = "L2MODIFICATION"
    TYPE_OUTPUT = "OUTPUT"
    STYPE_VLAN_PUSH = "VLAN_PUSH"
    STYPE_VLAN_POP = "VLAN_POP"
    STYPE_VLAN_ID = "VLAN_ID"


    @staticmethod
    def create_vlan_push():
        return dict(
            type=ONOSInstruction.TYPE_L2MODIFICATION,
            subtype=ONOSInstruction.STYPE_VLAN_PUSH
        )

    @staticmethod
    def create_vlan_pop():
        return dict(
            type=ONOSInstruction.TYPE_L2MODIFICATION,
            subtype=ONOSInstruction.STYPE_VLAN_POP
        )

    @staticmethod
    def create_vlan_id(vlan_id):
        return dict(
            type=ONOSInstruction.TYPE_L2MODIFICATION,
            subtype=ONOSInstruction.STYPE_VLAN_ID,
            vlanId=vlan_id
        )

    @staticmethod
    def create_output(port_number):
        return dict(
            type=ONOSInstruction.TYPE_OUTPUT,
            port=port_number
        )


class ONOSCriterion:

    TYPE_IN_PORT = "IN_PORT"
    TYPE_VLAN_VID ="VLAN_VID"

    @staticmethod
    def create_in_port(port_number):
        return dict(
            type=ONOSCriterion.TYPE_IN_PORT,
            port=port_number
        )

    @staticmethod
    def create_vlan_vid(vlan_vid):
        return dict(
            type=ONOSCriterion.TYPE_VLAN_VID,
            vlanId=vlan_vid
        )


class ONOS_Ruler:

    DEFAULT_PRIORITY = 50000

    @staticmethod
    def set_source_rules(device_id, table_id, vlan_id, in_port, out_port,
                         priority=DEFAULT_PRIORITY,
                         timeout=0,
                         is_permanent=True,
                         deferred=list()):

        # As source of the ougoing slice messages ...

        rule1 = ONOS_Ruler._assign_vlan_and_output(device_id, table_id, vlan_id,
                                                   in_port, out_port, priority,
                                                   timeout, is_permanent,
                                                   deferred)

        # ... and as destination of the incoming slice messages
        # (switched in_/out_ port)

        rule2 = ONOS_Ruler._assign_output_and_remove_vlan(device_id, table_id,
                                                          vlan_id, out_port,
                                                          in_port, priority,
                                                          timeout, is_permanent,
                                                          deferred)

        source_rules = list()

        source_rules.append(rule1)
        source_rules.append(rule2)

        return source_rules

    @staticmethod
    def set_switch_rules(device_id, table_id, vlan_id, in_port, out_port,
                         priority=DEFAULT_PRIORITY,
                         timeout=0,
                         is_permanent=True,
                         deferred=list()):
        # As intermediate point for the incoming slice messages from one side...

        rule1 = ONOS_Ruler._assign_output_by_vlan(device_id, table_id, vlan_id,
                                                  in_port, out_port, priority,
                                                  timeout, is_permanent,
                                                  deferred)

        # ... and as intermediate point for the incoming slice messages from the
        # other side (switched in_/out_ port)

        rule2 = ONOS_Ruler._assign_output_by_vlan(device_id, table_id, vlan_id,
                                                  out_port, in_port, priority,
                                                  timeout, is_permanent,
                                                  deferred)

        switch_rules = list()

        switch_rules.append(rule1)
        switch_rules.append(rule2)

        return switch_rules

    @staticmethod
    def set_destination_rules(device_id, table_id, vlan_id, in_port, out_port,
                              priority=DEFAULT_PRIORITY,
                              timeout=0,
                              is_permanent=True,
                              deferred=list()):

        # As destination of the incoming slice messages ...

        rule1 = ONOS_Ruler._assign_output_and_remove_vlan(device_id, table_id,
                                                          vlan_id, in_port,
                                                          out_port, priority,
                                                          timeout, is_permanent,
                                                          deferred)

        # ... and as source of the outgoing slice messages
        # (switched in_/out_ port)

        rule2 = ONOS_Ruler._assign_vlan_and_output(device_id, table_id, vlan_id,
                                                   out_port, in_port, priority,
                                                   timeout, is_permanent,
                                                   deferred)

        destination_rules = list()

        destination_rules.append(rule1)
        destination_rules.append(rule2)

        return destination_rules

    @staticmethod
    def _assign_vlan_and_output(device_id, table_id, vlan_id, in_port, out_port,
                               priority=DEFAULT_PRIORITY,
                               timeout=0,
                               is_permanent=True,
                               deferred=list()):
        instructions = list()
        instructions.append(ONOSInstruction.create_vlan_push())
        instructions.append(ONOSInstruction.create_vlan_id(vlan_id))
        instructions.append(ONOSInstruction.create_output(out_port))

        treatment = dict(
            instructions=instructions,
            deferred=deferred
        )

        criteria = list()
        criteria.append(ONOSCriterion.create_in_port(in_port))

        selector = dict(
            criteria=criteria
        )

        return dict(priority=priority,
                    timeout=timeout,
                    isPermanent=is_permanent,
                    tableId=table_id,
                    deviceId=device_id,
                    treatment=treatment,
                    selector=selector)

    @staticmethod
    def _assign_output_by_vlan(device_id, table_id, vlan_id, in_port, out_port,
                              priority=DEFAULT_PRIORITY,
                              timeout=0,
                              is_permanent=True,
                              deferred=list()):
        instructions = list()
        instructions.append(ONOSInstruction.create_output(out_port))

        treatment = dict(
            instructions=instructions,
            deferred=deferred
        )

        criteria = list()
        criteria.append(ONOSCriterion.create_in_port(in_port))
        criteria.append(ONOSCriterion.create_vlan_vid(vlan_id))

        selector = dict(
            criteria=criteria
        )

        return dict(priority=priority,
                    timeout=timeout,
                    isPermanent=is_permanent,
                    tableId=table_id,
                    deviceId=device_id,
                    treatment=treatment,
                    selector=selector)

    @staticmethod
    def _assign_output_and_remove_vlan(device_id, table_id, vlan_vid, in_port, out_port,
                               priority=DEFAULT_PRIORITY,
                               timeout=0,
                               is_permanent=True,
                               deferred=list()):
        instructions = list()
        instructions.append(ONOSInstruction.create_vlan_pop())
        instructions.append(ONOSInstruction.create_output(out_port))

        treatment = dict(
            instructions=instructions,
            deferred=deferred
        )

        criteria = list()
        criteria.append(ONOSCriterion.create_in_port(in_port))
        criteria.append(ONOSCriterion.create_vlan_vid(vlan_vid))

        selector = dict(
            criteria=criteria
        )

        return dict(priority=priority,
                    timeout=timeout,
                    isPermanent=is_permanent,
                    tableId=table_id,
                    deviceId=device_id,
                    treatment=treatment,
                    selector=selector)


class ONOS_REST_Inquirer:

    FIELDS_DEVICE = ['id', 'type', 'driver', 'hw', 'available', 'ports',
                     'annotations']
    FIELDS_DEVICE_ANNOTATIONS = ['managementAddress', 'protocol']
    FIELDS_PORT = ['port', 'portSpeed', 'annotations', 'isEnabled']
    FIELDS_PORT_ANNOTATIONS = ['portName', 'adminState']

    def __init__(self, ip="127.0.0.1", port=8181,
                 username='onos', password='rocks'):
        self._ip = ip
        self._port = port
        self._username = username
        self._password = password

        self._controlled_devices = None

    def post_activate_app(self, app_name):
        return dict(
            path='/applications/' + app_name + '/active',
            type='POST'
        )

    def post_flowrules(self, rules_descriptor,
                       app_id='org.fins.rules'):
        assert isinstance(app_id, str)
        assert isinstance(rules_descriptor, dict)
        assert 'flows' in rules_descriptor
        return dict(
            path='/flows?appId=' + app_id,
            type='POST',
            json=rules_descriptor
        )

    def get_flowrules_by_app(self, app_id):
        assert isinstance(app_id, str)
        return dict(
            path='/flows/application/' + app_id,
            type='GET'
        )

    def delete_flowrules_by_app(self, app_id):
        assert isinstance(app_id, str)
        return dict(
            path='/flows/application/' + app_id,
            type='DELETE'
        )

    def get_devices(self):
        return dict(
                    path='/devices',
                    type='GET'
                )

    def get_device_ports(self, device_id):
        assert isinstance(device_id, str)
        return dict(
                    path='/devices/' + device_id + '/ports',
                    type='GET'
                )

    def _get_devices_ids(self):
        rsp = self.request(self.get_devices())
        isinstance(rsp, ResponseWrapper)

        ids = list()
        if rsp.is_response():
            if rsp.status_code() == 200:
                devs = rsp.text_json()['devices']
                for dev in devs:
                    did = dev['id']
                    ids.append(did)

        return ids

    def _process_port(self, port):
        assert isinstance(port, dict)

        p = dict()
        for field in self.FIELDS_PORT:
            if field in port:
                if field == 'annotations':
                    for subfield in self.FIELDS_PORT_ANNOTATIONS:
                        if subfield in port[field]:
                            p[subfield] = port[field][subfield]
                        else:
                            LOG.warning("Expected port subfield is missing:"
                                        " %s",
                                        subfield)
                else:
                    p[field] = port[field]
            else:
                LOG.warning("Expected port field is missing: %s", port)

        return p

    def _process_device(self, device):
        assert isinstance(device, dict)

        processed_dev = dict()
        for field in self.FIELDS_DEVICE:
            if field in device:
                if field == 'annotations':
                    for subfield in self.FIELDS_DEVICE_ANNOTATIONS:
                        if subfield in device[field]:
                            processed_dev[subfield] = device[field][subfield]
                        else:
                            LOG.warning("Expected device subfield is missing:"
                                        " %s",
                                        subfield)
                elif field == 'ports':
                    processed_dev[field] = dict()
                    for port in device[field]:
                        p = self._process_port(port)
                        processed_dev[field][p['port']] = p
                else:
                    processed_dev[field] = device[field]
            else:
                LOG.warning("Expected device field is missing: %s", field)

        return processed_dev

    def set_controlled_devices(self):

        ids = self._get_devices_ids()

        # LOG.debug(ids)

        self._controlled_devices = dict()
        for i in ids:
            rsp = self.request(self.get_device_ports(i))
            if rsp.is_response():
                if rsp.status_code() == 200:
                    dev = rsp.text_json()
                    # LOG.debug(dev)
                    device = self._process_device(dev)
                    # LOG.debug(device)

                    device_name = None
                    if 'ports' in device:
                        if 'local' in device['ports']:
                            device_name = device['ports']['local']['portName']
                    assert device_name is not None

                    self._controlled_devices[device_name] = device

    def get_controlled_devices(self):
        if self._controlled_devices is None:
            self.get_controlled_devices()
        return self._controlled_devices

    def request(self, params):
        rest_url = "http://" + self._ip + ":" + str(self._port) + "/onos/v1"
        auth = HTTPBasicAuth(self._username, self._password)
        t = params['type']
        rw = None
        if t == 'GET':
            try:
                response = requests.get(
                    rest_url + params['path'],
                    auth=auth
                )

                rw = ResponseWrapper(response)
            except Exception as e:
                rw = ResponseWrapper(e, ResponseWrapper.TYPE_EXCEPTION)
        elif t == 'DELETE':
            try:
                response = requests.delete(
                    rest_url + params['path'],
                    auth=auth
                )

                rw = ResponseWrapper(response)
            except Exception as e:
                rw = ResponseWrapper(e, ResponseWrapper.TYPE_EXCEPTION)
        elif t == 'POST':
            try:
                response = None
                if 'data' in params:
                    response = requests.post(
                        rest_url + params['path'],
                        auth=auth,
                        data=params['data']
                    )
                elif 'json' in params:
                    # LOG.debug('Json case')
                    response = requests.post(
                        rest_url + params['path'],
                        auth=auth,
                        json=params['json']
                    )
                else:
                    response = requests.post(
                        rest_url + params['path'],
                        auth=auth
                    )

                rw = ResponseWrapper(response)
            except Exception as e:
                # LOG.debug(e)
                rw = ResponseWrapper(e, ResponseWrapper.TYPE_EXCEPTION)
        return rw

    def is_rest_ready(self):
        params = self.get_devices()
        rw = self.request(params)
        LOG.debug(rw.read_response())
        LOG.debug(rw.read_exception())
        if rw.is_response():
            if rw.status_code() == 200:
                return True
        return False












