# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import requests


class ResponseWrapper:

    TYPE_RESPONSE = 'R'
    TYPE_EXCEPTION = 'E'

    def __init__(self, outcome, rtype=TYPE_RESPONSE):
        assert rtype == self.TYPE_RESPONSE or \
               rtype == self.TYPE_EXCEPTION

        self._outcome = outcome
        self._type = rtype

    def is_exception(self):
        return self._type == self.TYPE_EXCEPTION

    def is_response(self):
        return self._type == self.TYPE_RESPONSE

    def read_response(self):
        if self.is_response():
            return self._outcome
        else:
            return None

    def read_exception(self):
        if self.is_exception():
            return self._outcome
        else:
            return None

    def status_code(self):
        if self.is_response():
            return self._outcome.status_code
        else:
            return None

    def text(self):
        if self.is_response():
            return self._outcome.text
        else:
            return None

    def text_json(self):
        if self.is_response():
            return self._outcome.json()
        else:
            return None
