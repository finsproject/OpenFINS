# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import traceback
import logging
from threading import Lock

LOG = logging.getLogger(__name__)


class SwitchBoard:

    PUBLISH = 'publish'
    CONSUME = 'consume'
    MANAGEMENT = 'management'

    QUEUES = 'queues'
    DIRECTION = 'direction'
    PURPOSE = 'purpose'
    ENDPOINT = 'endpoint'
    LOCK = 'lock'

    def __init__(self):

        self.console = dict()

    def add_channel(self, terminal_id, queue_name, direction, purpose):

        if purpose is None:
            purpose = self.MANAGEMENT

        try:
            assert isinstance(terminal_id, str)
            assert isinstance(queue_name, str)
            assert isinstance(direction, str)
            assert isinstance(purpose, str)
            assert (direction == self.PUBLISH) or (direction == self.CONSUME)

            if terminal_id not in self.console:
                self.console[terminal_id] = dict()

            if self.QUEUES not in self.console[terminal_id]:
                self.console[terminal_id][self.QUEUES] = dict()

            self.console[terminal_id][self.QUEUES][queue_name] = dict()
            self.console[terminal_id][self.QUEUES][queue_name][
                self.DIRECTION] = direction
            self.console[terminal_id][self.QUEUES][queue_name][
                self.PURPOSE] = purpose
            self.console[terminal_id][self.LOCK] = Lock()

            return True

        except Exception as e:
            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()

            return False

    def get_queue_name(self, terminal_id, direction, purpose=None):

        if purpose is None:
            purpose = self.MANAGEMENT

        try:
            assert isinstance(terminal_id, str)
            assert isinstance(direction, str)
            assert isinstance(purpose, str)
            assert (direction == self.PUBLISH) or (direction == self.CONSUME)

            if terminal_id not in self.console:
                return None

            for queue_name in self.console[terminal_id][self.QUEUES]:
                qdata = self.console[terminal_id][self.QUEUES][queue_name]
                if (qdata[self.DIRECTION] == direction) and \
                   (qdata[self.PURPOSE] == purpose):
                    return queue_name

            return None

        except Exception as e:
            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()

            return None

    def get_terminal_id(self, queue_name):

        assert isinstance(queue_name, str)

        for terminal_id in self.console:
            for queue in self.console[terminal_id][self.QUEUES]:
                if queue == queue_name:
                    return terminal_id

        return None

    def get_terminal_id_list(self):
        return list(self.console.keys())

    def get_terminal_queues(self, terminal_id, purpose=None):

        if purpose is None:
            purpose = self.MANAGEMENT

        assert isinstance(terminal_id, str)
        assert isinstance(purpose, str)
        assert terminal_id in self.console

        queues_dict = dict()
        for queue_name in self.console[terminal_id][self.QUEUES]:
            qdata = self.console[terminal_id][self.QUEUES][queue_name]
            if qdata[self.PURPOSE] == purpose:
                queues_dict[queue_name] = qdata[self.DIRECTION]

        return queues_dict

    def to_str(self):
        s = ''
        for terminal_id in self.console:
            for queue_name in self.console[terminal_id][self.QUEUES]:
                qdata = self.console[terminal_id][self.QUEUES][queue_name]
                s = s + "CHANNEL [%s][%s]: %s <%s>\n" % (
                    terminal_id,
                    queue_name,
                    qdata[self.DIRECTION],
                    qdata[self.PURPOSE])
        return s

    def assign_rmq_endpoint(self, terminal_id, rmq_endpoint):
        self.console[terminal_id][self.ENDPOINT] = rmq_endpoint

    def send_to(self, terminal_id, msg, purpose=None):

        if purpose is None:
            purpose = self.MANAGEMENT

        direction = self.PUBLISH

        queue = self.get_queue_name(terminal_id, direction, purpose)
        LOG.info("send to: %s, %s, %s", terminal_id, direction, purpose)
        assert queue is not None

        self.console[terminal_id][self.ENDPOINT].send(queue, msg)

    def reply_to(self, queue_name, reply_msg=True):

        # LOG.debug("queue_name: %s", queue_name)
        terminal_id = self.get_terminal_id(queue_name)
        assert terminal_id is not None

        queue_dest = self.get_queue_name(terminal_id, self.PUBLISH)
        assert queue_dest is not None

        self.console[terminal_id][self.ENDPOINT].send(queue_dest, reply_msg)

    def lock(self, terminal_id):
        return self.console[terminal_id][self.LOCK]

    def change_callback(self, terminal_id, callback, purpose=None):

        if purpose is None:
            purpose = self.MANAGEMENT

        direction = self.CONSUME

        queue = self.get_queue_name(terminal_id, direction, purpose)
        assert queue is not None

        self.console[terminal_id][self.ENDPOINT].change_callback(callback, queue)

