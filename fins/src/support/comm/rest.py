# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import tornado.escape
import tornado.ioloop
import tornado.web
from tornado import gen
import asyncio
from fins.src.support.rabbit_mq.rmq_manager import RMQManager
from fins.src.support.comm.switchboard import SwitchBoard
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.packets import PacketHandler, \
    HelloPacket, ACKPacket, RadioActionPacket, OvsActionPacket

from threading import Thread
import json
import logging
import time
import traceback
import yaml

LOG = logging.getLogger(__name__)

rest_rmq_manager = None
rest_switchboard = None
rest_packet_handler = None
rest_exchange_name = None
rest_id = None


class Welcome(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    @gen.coroutine
    def get(self, path):
        try:
            LOG.info(path)
            self.write("Welcome into FINS REST interface")
            self.finish()
        except Exception as e:
            self.set_status(500)
            raise


class SendHello(tornado.web.RequestHandler):
    completed = False

    def get(self, path=None):
        try:
            LOG.info(path)
            self.write("Ma ciao!")
            self.finish()
        except Exception as e:
            self.set_status(500)
            raise

    def post(self, *args, **kwargs):

        global rest_switchboard
        global rest_rmq_manager
        global rest_exchange_name
        global rest_id

        try:
            request_data = json.loads(self.request.body.decode('utf-8'))
            LOG.debug(request_data)

            if request_data.keys() != {'module_id'}:
                self.set_status(400)
                return

            module_id = request_data['module_id']

            with rest_switchboard.lock(module_id):
                # Create endpoint ep (with self.process_message as callback)

                # assert isinstance(rest_rmq_manager, RMQManager)
                # assert isinstance(rest_switchboard, SwitchBoard)
                #
                # queue_desc = rest_switchboard.get_terminal_queues(module_id)
                # ep = rest_rmq_manager.create_endpoint(rest_exchange_name,
                #                                       queue_desc,
                #                                       self.process_message)
                # ep.start()
                # rest_switchboard.assign_rmq_endpoint(module_id, ep)

                # send command to module

                if module_id == 'ra_onos':
                    rest_switchboard.change_callback(module_id,
                                                     self.process_message)
                else:
                    rest_switchboard.change_callback(module_id,
                                                     self.process_message2)

                pkt = HelloPacket()
                pkt.build_packet(
                    dict(
                        sender=rest_id,
                        msg_id=rest_packet_handler.generate_msg_id()
                    ),
                    dict(
                        text="You're under a REST!"
                    ))

                rest_switchboard.send_to(module_id, pkt.get_packet())
                # wait for response
                while not self.completed:
                    time.sleep(0.1)

                # Close endpoint ep (ep.join())

                # ep.join()
                # LOG.info("ep.join() invoked")
                # rest_switchboard.assign_rmq_endpoint(module_id, None)

                # ADD HERE TODO
                self.set_status(200)
                return


        except Exception as e:
            self.set_status(500)
            raise

        pass

    def process_message(self, queue, message):

        global rest_packet_handler

        # LOG.debug('message class: %s', type(message).__name__)
        # message = eval(message)

        # assert message['header']
        # assert message['header']['sender']
        # assert message['header']['type']
        # assert message['header']['mid']
        # assert message['header']['creation_time']
        # assert message['body']

        packet = rest_packet_handler.get_packet(message)

        LOG.debug('PROCESS MESSAGE 1')
        LOG.debug("\n\nRECEIVED msg:\n%s\n",
                  packet.to_string())

        try:
            self.completed = True

        except Exception as e:

            traceback.print_exc()
            LOG.error(str(e))
        pass

    def process_message2(self, queue, message):

        global rest_packet_handler

        # LOG.debug('message class: %s', type(message).__name__)
        # message = eval(message)

        # assert message['header']
        # assert message['header']['sender']
        # assert message['header']['type']
        # assert message['header']['mid']
        # assert message['header']['creation_time']
        # assert message['body']

        packet = rest_packet_handler.get_packet(message)

        LOG.debug('PROCESS MESSAGE 2')
        LOG.debug("\n\nRECEIVED msg:\n%s\n",
                  packet.to_string())

        try:
            self.completed = True

        except Exception as e:

            traceback.print_exc()
            LOG.error(str(e))
        pass


class SendAction(tornado.web.RequestHandler):
    completed = False

    def get(self, path=None):
        try:
            LOG.info(path)
            self.write("Ma ciao!")
            self.finish()
        except Exception as e:
            self.set_status(500)
            raise

    def post(self, *args, **kwargs):

        global rest_switchboard
        global rest_rmq_manager
        global rest_exchange_name
        global rest_id

        try:
            request_data = json.loads(self.request.body.decode('utf-8'))
            LOG.debug(request_data)

            # DEBUGGED
            # if request_data.keys() != {'module_id'}:
            if 'module_id' not in request_data:
                self.set_status(400)
                return

            module_id = request_data['module_id']

            with rest_switchboard.lock(module_id):
                # Create endpoint ep (with self.process_message as callback)

                # assert isinstance(rest_rmq_manager, RMQManager)
                # assert isinstance(rest_switchboard, SwitchBoard)
                #
                # queue_desc = rest_switchboard.get_terminal_queues(module_id)
                # ep = rest_rmq_manager.create_endpoint(rest_exchange_name,
                #                                       queue_desc,
                #                                       self.process_message)
                # ep.start()
                # rest_switchboard.assign_rmq_endpoint(module_id, ep)

                # send command to module
                self.return_packet = None

                pkt = HelloPacket()

                if module_id == 'ra_wctrl':
                    rest_switchboard.change_callback(module_id,
                                                     self.process_message)
                    pkt = RadioActionPacket()
                    pkt_body = dict()
                    if "node" in request_data:
                        pkt_body['node'] = request_data['node']
                    if "action" in request_data:
                        pkt_body['action'] = request_data['action']

                    pkt.build_packet(
                        dict(
                            sender=rest_id,
                            msg_id=rest_packet_handler.generate_msg_id()
                        ),
                        pkt_body
                    )
                if module_id == 'ra_ovs':
                    rest_switchboard.change_callback(module_id,
                                                     self.process_message)
                    pkt = OvsActionPacket()
                    pkt_body = dict()
                    if "action" in request_data:
                        pkt_body['action'] = request_data['action']
                    if "params" in request_data:
                        pkt_body['params'] = request_data['params']

                    pkt.build_packet(
                        dict(
                            sender=rest_id,
                            msg_id=rest_packet_handler.generate_msg_id()
                        ),
                        pkt_body
                    )
                else:
                    rest_switchboard.change_callback(module_id,
                                                     self.process_message2)


                LOG.info(yaml.dump(pkt.get_packet()))
                rest_switchboard.send_to(module_id, pkt.get_packet())

                # wait for response
                while not self.completed:
                    time.sleep(0.1)

                # Close endpoint ep (ep.join())
                # ep.join()
                # LOG.info("ep.join() invoked")
                # rest_switchboard.assign_rmq_endpoint(module_id, None)

                # ---- write response ----
                if self.return_packet.get_msg_type() == self.return_packet.MSG_TYPE_RADIO_ACTION:
                    return_msg = dict()
                    return_msg["info"] = self.return_packet.get_info()
                    return_msg["action"] = self.return_packet.get_action()
                    return_msg["node"] = self.return_packet.get_node()
                    return_msg[
                        "return_value"] = self.return_packet.get_return_value()
                elif self.return_packet.get_msg_type() == self.return_packet.MSG_TYPE_OVS_ACTION:
                    b = self.return_packet.get_body()
                    return_msg = dict()
                    if 'info' in b:
                        return_msg["info"] = b['info']
                    if 'action' in b:
                        return_msg["action"] = b['action']
                else:
                    return_msg = repr(self.return_packet)
                self.write(yaml.dump(return_msg))  # TODO
                self.set_status(200)
                return


        except Exception as e:
            self.set_status(500)
            raise

        pass

    def process_message(self, queue, message):

        global rest_packet_handler

        # LOG.debug('message class: %s', type(message).__name__)
        # message = eval(message)

        # assert message['header']
        # assert message['header']['sender']
        # assert message['header']['type']
        # assert message['header']['mid']
        # assert message['header']['creation_time']
        # assert message['body']

        packet = rest_packet_handler.get_packet(message)

        LOG.debug('PROCESS MESSAGE 1')
        LOG.debug("\n\nRECEIVED msg:\n%s\n",
                  packet.to_string())

        # ---- Save return packet ----
        self.return_packet = packet

        try:
            self.completed = True
        except Exception as e:
            traceback.print_exc()
            LOG.error(str(e))

        pass

    def process_message2(self, queue, message):

        global rest_packet_handler

        # LOG.debug('message class: %s', type(message).__name__)
        # message = eval(message)

        # assert message['header']
        # assert message['header']['sender']
        # assert message['header']['type']
        # assert message['header']['mid']
        # assert message['header']['creation_time']
        # assert message['body']

        packet = rest_packet_handler.get_packet(message)

        LOG.debug('PROCESS MESSAGE 2')
        LOG.debug("\n\nRECEIVED msg:\n%s\n",
                  packet.to_string())

        # ---- Save return packet ----
        self.return_packet = packet

        try:
            self.completed = True
        except Exception as e:
            traceback.print_exc()
            LOG.error(str(e))
        pass


class Rest(Thread):
    def __init__(self, rest_data, rmq_data, ras_data, rms_data, ei_data):
        super(Rest, self).__init__()

        global rest_rmq_manager
        global rest_switchboard
        global rest_packet_handler
        global rest_exchange_name
        global rest_id

        self.ioloop = None
        self.application = None
        self._rmq_data = rmq_data
        self._rest_data = rest_data
        self._rms_data = rms_data
        self._ras_data = ras_data
        self._ei_data = ei_data

        rest_rmq_manager = RMQManager(self._rmq_data)

        rest_switchboard = SwitchBoard()

        rest_id = rest_data['id']

        rest_packet_handler = PacketHandler(rest_id)

        rest_exchange_name = self.create_exchange()

        self.create_exchange()

        self.register_module_queues()

        self.assign_rmq_endpoints()

        self.show_switchboard()

    def show_switchboard(self):

        global rest_switchboard

        LOG.info("\n\nREST SWITCHBOARD:\n%s", rest_switchboard.to_str())

    def create_exchange(self):

        global rest_rmq_manager
        global rest_id

        rmq_rc = rest_rmq_manager.rmq_rest_client()

        exchange_name = RMQUtils.get_fins_exchange_name(rest_id)

        rmq_rc.create_exchange(exchange_name, True)

        return exchange_name

    def register_queue(self, terminal_id, queue_name, direction,
                       purpose=None):

        global rest_switchboard

        rest_switchboard.add_channel(terminal_id,
                                     queue_name,
                                     direction,
                                     purpose)

    def register_module_queues(self):

        global rest_rmq_manager
        global rest_switchboard
        global rest_id

        LOG.info("")
        LOG.info("Registering all REST queues...")

        ras_data = self._ras_data
        rms_data = self._rms_data
        ei_data = self._ei_data
        # rmq_rc = rest_rmq_manager.rmq_rest_client()

        for key in ras_data:
            ra_id = ras_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(rest_id, ra_id)
            # rmq_rc.create_queue(queue)

            assert rest_switchboard.get_queue_name(ra_id, "publish") is None
            self.register_queue(ra_id, queue, "publish")

            queue = RMQUtils.get_fins_queue_name(ra_id, rest_id)
            # rmq_rc.create_queue(queue)

            assert rest_switchboard.get_queue_name(ra_id, "consume") is None
            self.register_queue(ra_id, queue, "consume")

        for key in rms_data:
            rm_id = rms_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(rest_id, rm_id)
            # rmq_rc.create_queue(queue)

            assert rest_switchboard.get_queue_name(rm_id, "publish") is None
            self.register_queue(rm_id, queue, "publish")

            queue = RMQUtils.get_fins_queue_name(rm_id, rest_id)
            # rmq_rc.create_queue(queue)

            assert rest_switchboard.get_queue_name(rm_id, "consume") is None
            self.register_queue(rm_id, queue, "consume")

        # ei_data:
        ei_id = ei_data['id']

        queue = RMQUtils.get_fins_queue_name(rest_id, ei_id)
        # rmq_rc.create_queue(queue)

        assert rest_switchboard.get_queue_name(ei_id, "publish") is None
        self.register_queue(ei_id, queue, "publish")

        queue = RMQUtils.get_fins_queue_name(ei_id, rest_id)
        # rmq_rc.create_queue(queue)

        assert rest_switchboard.get_queue_name(ei_id, "consume") is None
        self.register_queue(ei_id, queue, "consume")

    def assign_rmq_endpoints(self):

        global rest_switchboard
        global rest_rmq_manager
        global rest_exchange_name

        LOG.info("")
        LOG.info("Assigning a RabbitMQ endpoint for each associated module")

        ids = rest_switchboard.get_terminal_id_list()

        for terminal_id in ids:
            queue_desc = rest_switchboard.get_terminal_queues(terminal_id)
            ep = rest_rmq_manager.create_endpoint(rest_exchange_name,
                                                  queue_desc,
                                                  None)

            rest_switchboard.assign_rmq_endpoint(terminal_id, ep)

            ep.start()

    def run(self):
        try:
            asyncio.set_event_loop(asyncio.new_event_loop())
            self.application = tornado.web.Application([
                (r"/rest/send_hello", SendHello),
                (r"/rest/send_action", SendAction),
                (r"/", Welcome)])
            # self.application = tornado.web.Application([
            #     (r"/rest/send_action", SendAction),
            #     (r"/", Welcome)])

            # LOG.debug(self._rest_data)

            LOG.debug('listening on ' + self._rest_data['host_ip']
                      + ' port ' + str(self._rest_data['port']))
            self.application.listen(self._rest_data['port'],
                                    self._rest_data['host_ip'])

            self.ioloop = tornado.ioloop.IOLoop()
            self.ioloop.instance().start()
        except Exception as e:
            traceback.print_exc()
            LOG.error(e)
        finally:
            LOG.info('Rest no more running')
        # tornado.ioloop.IOLoop.instance().start()

    def join(self, timeout=None):
        try:
            self.ioloop.current().stop()
            super(Rest, self).join(timeout)
            LOG.info('Rest interface closed')
        except Exception as e:
            traceback.print_exc()
            LOG.error(e)
        finally:
            LOG.info('Rest somehow terminated')

