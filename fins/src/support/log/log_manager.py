# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging


class LogManager:

    LOG_FORMAT = ('%(levelname) -8s %(asctime)s %(name) -45s %(funcName) '
                  '-20s %(lineno) -5d: %(message)s')

    @staticmethod
    def configure_log(log_level, log_file, filemode, log_format=LOG_FORMAT,
                      name=__name__):
        level = logging.getLevelName(log_level)
        logging.basicConfig(filename=log_file,
                            filemode=filemode,
                            level=level)

        console = logging.StreamHandler()


        console.setLevel(level)
        formatter = logging.Formatter(log_format)

        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)

        return logging.getLogger(name)

    @staticmethod
    def splash_fins(log, mode='slant'):
        # http://www.network-science.de/ascii/

        if mode == 'slant':
            # log.info("    ___________   _______")
            # log.info("   / ____/  _/ | / / ___/")
            # log.info("  / /_   / //  |/ /\__ \ ")
            # log.info(" / __/ _/ // /|  /___/ / ")
            # log.info("/_/   /___/_/ |_//____/  ")
            # log.info('')
            log.info("\n"
                     "\n    ___________   _______"
                     "\n   / ____/  _/ | / / ___/"
                     "\n  / /_   / //  |/ /\__ \ "
                     "\n / __/ _/ // /|  /___/ / "
                     "\n/_/   /___/_/ |_//____/  "
                     "\n"
                     "\nFederating Interface for Network Slicing"
                     "\n    by FBK Create-Net (2018)"
                     "\n")

        elif mode == 'doom':
            log.info("______ _____ _   _  _____  ")
            log.info("|  ___|_   _| \ | |/  ___| ")
            log.info("| |_    | | |  \| |\ `--.  ")
            log.info("|  _|   | | | . ` | `--. \ ")
            log.info("| |    _| |_| |\  |/\__/ / ")
            log.info("\_|    \___/\_| \_/\____/  ")
            log.info('')

        # log.info('Federating Interface for Network Slicing')
        # log.info('    by FBK Create-Net (2018)')
        log.info('')

    def splash_fins_RA(log, mode='slant'):
        # http://www.network-science.de/ascii/

        if mode == 'slant':
            log.info("\n"
                     "\n    ____               ___                    __ "
                     "\n   / __ \___  _____   /   | ____ ____  ____  / /_"
                     "\n  / /_/ / _ \/ ___/  / /| |/ __ `/ _ \/ __ \/ __/"
                     "\n / _, _/  __(__  )  / ___ / /_/ /  __/ / / / /_  "
                     "\n/_/ |_|\___/____/  /_/  |_\__, /\___/_/ /_/\__/  "
                     "\n                         /____/                  "
                     "\n"
                     "\nFederating Interface for Network Slicing"
                     "\n    by FBK Create-Net (2018)"
                     "\n")

        # log.info('Federating Interface for Network Slicing')
        # log.info('    by FBK Create-Net (2018)')
        log.info('')

    def splash_fins_REST(log, mode='slant'):
        # http://www.network-science.de/ascii/

        if mode == 'slant':
            log.info("\n"
                     "\n    ____  _________________"
                     "\n   / __ \/ ____/ ___/_  __/"
                     "\n  / /_/ / __/  \__ \ / /   "
                     "\n / _, _/ /___ ___/ // /    "
                     "\n/_/ |_/_____//____//_/     "
                     "\n"
                     "\nFederating Interface for Network Slicing"
                     "\n    by FBK Create-Net (2018)"
                     "\n")

        # log.info('Federating Interface for Network Slicing')
        # log.info('    by FBK Create-Net (2018)')
        log.info('')

    def splash_fins_RM(log, mode='slant'):
        # http://www.network-science.de/ascii/

        if mode == 'slant':
            log.info(
             "\n"
             "\n    ____               __  ___                                 "
             "\n   / __ \___  _____   /  |/  /___ _____  ____ _____ ____  _____"
             "\n  / /_/ / _ \/ ___/  / /|_/ / __ `/ __ \/ __ `/ __ `/ _ \/ ___/"
             "\n / _, _/  __(__  )  / /  / / /_/ / / / / /_/ / /_/ /  __/ /    "
             "\n/_/ |_|\___/____/  /_/  /_/\__,_/_/ /_/\__,_/\__, /\___/_/     "
             "\n                                            /____/             "
             "\n"
             "\nFederating Interface for Network Slicing"
             "\n    by FBK Create-Net (2018)"
             "\n")

        # log.info('Federating Interface for Network Slicing')
        # log.info('    by FBK Create-Net (2018)')
        log.info('')

    @staticmethod
    def frame_msg(msg, c="*", offset_spaces=0):

        offset = ""
        for r in range(offset_spaces):
            offset += " "
        if not isinstance(msg, str):
            msg = str(msg)
        l = len(msg) + 4
        s = ""
        starline = "" + offset
        for r in range(l):
            starline += c
        padding = "" + offset
        for r in range(l):
            if r == 0 or r == l - 1:
                padding += c
            else:
                padding += " "
        phrase = offset + c + " " + msg + " " + c
        frame = starline + "\n" + \
                padding + "\n" + \
                phrase + "\n" + \
                padding + "\n" + \
                starline + "\n"

        return frame
