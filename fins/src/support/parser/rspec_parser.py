# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import xml.etree.ElementTree as ET
import logging
from configobj import ConfigObj

level = logging.getLevelName("DEBUG")
logging.basicConfig(filename='rspec_parser.log',
                    filemode='w',
                    level=level)
console = logging.StreamHandler()
LOG_FORMAT = ('%(levelname) -8s %(asctime)s %(name) -45s %(funcName) '
              '-20s %(lineno) -5d: %(message)s')
# LOG_FORMAT ='%(asctime)-15s %(name)-40s: %(levelname)-8s %(message)s'
console.setLevel(level)
formatter = logging.Formatter(LOG_FORMAT)

console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
LOG = logging.getLogger(__name__)

class RSpecParser:

    @staticmethod
    def mrspec(filename):
        tree = ET.parse(filename)
        root = tree.getroot()

        vms = list()
        #LOG.debug(root.tag)
        #LOG.debug(root.attrib)
        for node in root:
            client_id = node.get('client_id')
            user_ip = node[0][2].get('for')
            index = user_ip.find('@')
            user = user_ip[:index]
            index2 = user_ip.find(':')
            ip = user_ip[index+1:index2]
            vm = dict()
            vm['id'] = client_id
            vm['ip'] = ip
            vm['type'] = 'OS.ubuntu'
            vms.append(vm)
        return vms

if __name__ == '__main__':
    config = ConfigObj(indent_type='    ')
    config.filename = "../../../confs/tmp/vms_data.cfg"
    vms_data = RSpecParser.mrspec('../../../confs/tmp/experiment.mrspec')
    counter = 0
    config['VMS'] = dict()
    for vm in vms_data:
        config['VMS'][str(counter)] = vm
        counter += 1
    config.write()
    LOG.info("\n\n"+str(vms_data))
    LOG.info("\n\nVMS data has been saved into file %s (--> fins/confs)"
             "\n"
             "\n***************************************************************"
             "\n*>> Run config_generator_DEMO.py to update FINS config file <<*"
             "\n***************************************************************",
             config.filename)


    # configr = ConfigObj(config.filename)
    # data = configr['VMS']
    # LOG.info("\n\n" + str(data))


