# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import time
import json
from threading import Lock

from fins.src.support.onos.utils import ONOS_Ruler
from fins.src.support.onos.utils import ONOS_REST_Inquirer as ORI
from fins.src.support.onos.utils import ResponseWrapper

from fins.src.support.topology.topology_manager import TopologyManager

from fins.src.support.log.log_manager import LogManager

LOG = logging.getLogger(__name__)

class Slice:

    class SliceStatus:

        INVALID = 'invalid'
        UNDEFINED = 'undefined'
        INITIALISING = 'initialising'
        INITIALISED = 'initialised'
        ACTIVATING = 'activating'
        ACTIVE = 'active'
        DEACTIVATING = 'deactivating'
        DEACTIVATED = 'deactivated'
        DELETING = 'deleting'

        def __init__(self):
            self._lock = Lock()
            self._value = self.UNDEFINED

        def _set_value(self, value):
            with self._lock:
                self._value = value

        def value(self):
            return self._value

        def invalidate(self):
            self._set_value(self.INVALID)
            LOG.warning("Status set forcibly to %s", self.INVALID)

        def is_invalid(self):
            return self.INVALID == self.value()

        def is_initialised(self):
            return self.INITIALISED == self.value()

        def is_active(self):
            return self.ACTIVE == self.value()

        def is_deactivated(self):
            return self.DEACTIVATED == self.value()

        # Just for rollback purposes
        def undefine(self):
            value = self.value()
            self._set_value(self.UNDEFINED)
            LOG.warning("Invalid transition %s -> %s",
                        value, self.UNDEFINED)

        def initialise(self, undo=False):
            value = self.value()
            if value == self.UNDEFINED or \
               value == self.INITIALISED or \
               value == self.DEACTIVATED or undo:
                self._set_value(self.INITIALISING)
                return True
            else:
                LOG.error("Invalid transition %s -> %s",
                          value, self.INITIALISING)
                return False

        def initialised(self, undo=False):
            value = self.value()
            if value == self.INITIALISING or undo:
                self._set_value(self.INITIALISED)
                return True
            LOG.error("Invalid transition %s -> %s",
                      value, self.INITIALISING)
            return False

        def activating(self, undo=False):
            value = self.value()
            if value == self.INITIALISED or \
               value == self.DEACTIVATED or undo:
                self._set_value(self.ACTIVATING)
                return True
            else:
                LOG.error("Invalid transition %s -> %s",
                          value, self.ACTIVATING)
                return False

        def activated(self, undo=False):
            value = self.value()
            if value == self.ACTIVATING or undo:
                self._set_value(self.ACTIVE)
                return True
            LOG.error("Invalid transition %s -> %s",
                      value, self.ACTIVE)
            return False

        def deactivating(self, undo=False):
            value = self.value()
            if value == self.ACTIVE or undo:
                self._set_value(self.DEACTIVATING)
                return True
            else:
                LOG.error("Invalid transition %s -> %s",
                          value, self.DEACTIVATING)
                return False

        def deactivated(self, undo=False):
            value = self.value()
            if value == self.DEACTIVATING or undo:
                self._set_value(self.DEACTIVATED)
                return True
            LOG.error("Invalid transition %s -> %s",
                      value, self.DEACTIVATED)
            return False

        def deleting(self, undo=False):
            value = self.value()
            if value == self.UNDEFINED or \
               value == self.INITIALISED or \
               value == self.DEACTIVATED or undo:
                self._set_value(self.DELETING)
                return True
            else:
                LOG.error("Invalid transition %s -> %s",
                          value, self.DELETING)
            return False

    FIELD__NAME = 'name'
    FIELD__DESCRIPTION = 'description'
    FIELD__VLAN_ID = 'vlan_id'
    FIELD__APP_ID = 'app_id'
    FIELD__TABLE_ID = 'table_id'
    FIELD__PATH = 'path'
    FIELD__EXPLICIT_PATH = 'explicit_path'
    FIELD__CAPACITY = 'capacity'
    FIELD__FLOWRULES = 'flowrules'

    DEFAULT_CAPACITY = 0
    DEFAULT_TABLE_ID = 6

    def __init__(self, descriptor=None):

        self.name = None
        self.description = None
        self.vlan_id = None
        self.table_id = None
        self.app_id = None
        self.path = None
        self.explicit_path = None
        self.capacity = None
        self.flowrules = list()
        self.active_flows = list()
        self.status = self.SliceStatus()

        if descriptor is not None:
            self.initialise(descriptor)

    def is_invalid(self):
        return self.status.is_invalid()

    def is_initialised(self):
        return self.status.is_initialised()

    def is_running(self):
        return self.status.is_active()

    def is_stopped(self):
        return self.status.is_deactivated()

    def generate_app_id(self, code):
        return "org.fins.slice" + code + ".rules"

    def generate_slice_name(self, code):
        return "Slice_" + code

    def initialise(self, descriptor):

        try:
            self.status.initialise()

            if self.FIELD__EXPLICIT_PATH not in descriptor:
                raise Exception("No '" + self.FIELD__EXPLICIT_PATH +
                                "' field in descriptor")

            self.explicit_path = descriptor[self.FIELD__EXPLICIT_PATH]

            if self.FIELD__VLAN_ID not in descriptor:
                raise Exception("No '" + self.FIELD__VLAN_ID +
                                "' field in descriptor")
            vlan_id = str(descriptor[self.FIELD__VLAN_ID])
            self.vlan_id = vlan_id

            if self.FIELD__FLOWRULES not in descriptor:
                raise Exception("No '" + self.FIELD__FLOWRULES +
                                "' field in descriptor")
            self.flowrules = descriptor[self.FIELD__FLOWRULES]

            if self.FIELD__PATH in descriptor:
                self.path = descriptor[self.FIELD__PATH]

            if self.FIELD__NAME in descriptor:
                self.name = descriptor[self.FIELD__NAME]
            else:
                self.name = self.generate_slice_name(self.vlan_id)

            if self.FIELD__DESCRIPTION in descriptor:
                self.description = descriptor[self.FIELD__DESCRIPTION]
            else:
                # self.description = "A slice from here to there using " \
                #                    "vlan id " + self.vlan_id
                self.description = self.stringified_path(self.explicit_path)

            if self.FIELD__TABLE_ID in descriptor:
                self.table_id = descriptor[self.FIELD__TABLE_ID]
            else:
                self.table_id = self.DEFAULT_TABLE_ID

            if self.FIELD__CAPACITY in descriptor:
                self.capacity = descriptor[self.FIELD__CAPACITY]
            else:
                self.capacity = self.DEFAULT_CAPACITY

            if self.FIELD__APP_ID in descriptor:
                self.app_id = descriptor[self.FIELD__APP_ID]
            else:
                self.app_id = self.generate_app_id(self.vlan_id)

            self.status.initialised()

            s = "Slice " + self.name + " INITIALISED"
            LOG.debug("\n\n%s", LogManager.frame_msg(s, "+", 4))

        except Exception as e:
            LOG.error(e)
            self.vlan_id = None
            self.path = None
            self.capacity = None
            self.flowrules = list()
            self.status.undefine()

    def activate(self, onos_inquirer):

        try:
            if not isinstance(onos_inquirer, ORI):
                raise Exception("onos inquirer is not instance of " +
                                "class ONOS_REST_Inquirer")

            self.status.activating()

            flow_count = len(self.flowrules)

            rsp = onos_inquirer.request(
                onos_inquirer.post_flowrules(dict(flows=self.flowrules),
                                             self.app_id))
            assert isinstance(rsp, ResponseWrapper)

            if rsp.is_response():
                LOG.debug(rsp.read_response())
                LOG.debug(rsp.text_json())
            else:
                LOG.debug(rsp.read_exception())

            flows_activated = False
            counter = 0
            LOG.debug("Wait for flows to be activated")
            while not flows_activated:

                rsp = onos_inquirer.request(
                    onos_inquirer.get_flowrules_by_app(self.app_id))
                assert isinstance(rsp, ResponseWrapper)

                if rsp.is_response():
                    # LOG.debug(rsp.read_response())
                    # LOG.debug(rsp.text_json())
                    fr = rsp.text_json()
                    self.active_flows = fr['flows']
                    active_flows = len(self.active_flows)
                    LOG.debug("Activated flows: %s [expected: %s]",
                              active_flows, flow_count)
                    if active_flows < flow_count:
                        counter += 5
                        time.sleep(5)
                        if counter > 60:
                            raise Exception("Some of the added rules are "
                                            "still missing after more than "
                                            "60 s")
                    else:
                        flows_activated = True

                else:
                    LOG.debug(rsp.read_exception())

            self.status.activated()

            s = "Slice " + self.name + " ACTIVATED"
            LOG.debug("\n\n%s", LogManager.frame_msg(s, "+", 4))

            return True

        except Exception as e:
            LOG.error(e)
            self.status.invalidate()
            return False

    def deactivate(self, onos_inquirer):

        try:
            if not isinstance(onos_inquirer, ORI):
                raise Exception("onos inquirer is not instance of " +
                                "class ONOS_REST_Inquirer")

            self.status.deactivating()

            rsp = onos_inquirer.request(
                onos_inquirer.delete_flowrules_by_app(self.app_id))
            assert isinstance(rsp, ResponseWrapper)

            if rsp.is_response():
                LOG.debug(rsp.read_response())
                # It returns a void confirm message, so nothing to be logged
            else:
                LOG.debug(rsp.read_exception())

            rules_removed = False
            counter = 0
            LOG.debug("Wait for rules to be removed")
            while not rules_removed:

                rsp = onos_inquirer.request(
                    onos_inquirer.get_flowrules_by_app(self.app_id))
                assert isinstance(rsp, ResponseWrapper)

                if rsp.is_response():
                    # LOG.debug(rsp.read_response())
                    # LOG.debug(rsp.text_json())
                    fr = rsp.text_json()
                    self.active_flows = fr['flows']
                    active_flows = len(self.active_flows)
                    LOG.debug("Still active flows: %s [expected: 0]",
                              active_flows)
                    if len(self.active_flows) != 0:
                        counter += 5
                        time.sleep(5)
                        if counter > 60:
                            raise Exception("Some of the deleting rules are "
                                            "still there even after more than "
                                            "60 s")
                    else:
                        rules_removed = True
                else:
                    LOG.debug(rsp.read_exception())

            self.status.deactivated()

            s = "Slice " + self.name + " DEACTIVATED"
            LOG.debug("\n\n%s", LogManager.frame_msg(s, "+", 4))

            return True

        except Exception as e:
            LOG.error(e)
            self.status.invalidate()
            return False

    def delete(self):

        try:

            self.status.deleting()

            return True

        except Exception as e:
            LOG.error(e)
            self.status.invalidate()
            return False



    def generate_descriptor(self, stringyfied=False):

        descriptor = dict(
            name=self.name,
            description=self.description,
            vlan_id=self.vlan_id,
            table_id=self.table_id,
            app_id=self.app_id,
            path=self.path,
            explicit_path=self.explicit_path,
            capacity=self.capacity,
            flowrules=self.flowrules,
            flows_running=self.active_flows,
            status=self.status.value()
        )

        descriptor = json.dumps(descriptor)

        if not stringyfied:
            return json.loads(descriptor)

        return descriptor

    @staticmethod
    def stringified_path(explicit_path):
        s = ""
        counter = 0
        for step in explicit_path:
            node = step['node']
            port = step['port']
            item = "[" + str(node)
            if port is not None:
                item += ":" +str(port)
            item += "]"
            if counter == 0:
                s += item + " "
            else:
                s += "--> " + item + " "
            counter += 1

        return s


class SliceManager:
    
    def __init__(self, topology_manager):
        self._topology_manager = None
        if isinstance(topology_manager, TopologyManager):
            self._topology_manager = topology_manager
        assert (self._topology_manager, TopologyManager)

        # if isinstance(onos_inquirer, ORI):
        #     self._onos_inquirer = onos_inquirer
        # else:
        #     self._onos_inquirer = ORI(onos_inquirer['ip'],
        #                               onos_inquirer['rest_port'])

        self.vlan_register = list()
        self.slice_repository = dict()

    def is_vlan_already_registered(self, vlan_id):
        tot = self.vlan_register.count(vlan_id)
        if tot == 0:
            return False
        elif tot == 1:
            return True
        else:
            LOG.error("vlan id %s have been registered multiple times", vlan_id)
            return True

    def add_vlan_to_register(self, vlan_id):
        if self.is_vlan_already_registered(vlan_id):
            return False
        else:
            self.vlan_register.append(vlan_id)
            return True

    def add_slice_to_repository(self, slc):
        try:
            if not isinstance(slc, Slice):
                raise Exception('slice is not instance of class Slice')

            if self.is_slice_already_stored(slc):
                return False

            vlan_id = slc.vlan_id
            self.slice_repository[vlan_id] = slc

            return True

        except Exception as e:
            LOG.error(e)
            return False

    def remove_slice_from_repository(self, vlan_id):
        try:
            vlan_id = str(vlan_id)
            if vlan_id not in self.slice_repository:
                LOG.warning("Slice with vlan id " + vlan_id + " is not stored")
                return True
            self.slice_repository.pop(vlan_id, None)

            return True

        except Exception as e:
            LOG.error(e)
            return False

    def is_slice_already_stored(self, vlan_id):
        try:
            vlan_id = str(vlan_id)
            if vlan_id in self.slice_repository:
                return True
            return False
        except Exception as e:
            LOG.error(e)
            raise


    def get_first_free_vlan_id(self, grater_than=99):
        self.vlan_register.sort()
        length = len(self.vlan_register)
        ida = 0
        idb = 0
        for index in range(length - 1):
            ida = int(self.vlan_register[index])
            idb = int(self.vlan_register[index + 1])
            if ida < grater_than:
                ida = grater_than
            if idb - ida > 1:
                return idb - 1
        if idb < grater_than:
            return grater_than + 1
        return idb + 1


    def eval_explicit_path(self, path_request):

        return self._topology_manager.eval_explicit_path(path_request)

    def generate_slice_flowrules(self, explicit_path, vlan_id, table_id):

        cep = self._topology_manager.compact_explicit_path(explicit_path)

        LOG.debug("\nCompact Explicit Path:\n\n%s\n", str(cep))

        flows = list()

        for index in range(len(cep)):
            did = cep[index]['onos_did']

            rules = list()

            if index == 0:
                rules = ONOS_Ruler.set_source_rules(
                                did, table_id, vlan_id,
                                cep[index]['in_port'],
                                cep[index]['out_port'])

            elif index == (len(cep) - 1):
                rules = ONOS_Ruler.set_destination_rules(
                    did, table_id, vlan_id,
                    cep[index]['in_port'],
                    cep[index]['out_port'])

            else:
                rules = ONOS_Ruler.set_switch_rules(
                    did, table_id, vlan_id,
                    cep[index]['in_port'],
                    cep[index]['out_port'])

            flows.extend(rules)

        return flows

    # def onos_post_flowrules(self, flow_rules, app_id):
    #
    #     rsp = self._onos_inquirer.request(
    #             self._onos_inquirer.post_flowrules(dict(flows=flow_rules),
    #                                                app_id))
    #     assert isinstance(rsp, ResponseWrapper)
    #     return rsp
    #
    # def onos_get_flowrules(self, app_id):
    #
    #     rsp = self._onos_inquirer.request(
    #         self._onos_inquirer.get_flowrules_by_app(app_id))
    #     assert isinstance(rsp, ResponseWrapper)
    #     return rsp
    #
    # def onos_delete_flowrules(self, app_id):
    #
    #     rsp = self._onos_inquirer.request(
    #         self._onos_inquirer.delete_flowrules_by_app(app_id))
    #     # assert isinstance(rsp, ResponseWrapper)
    #     return rsp

    def initialise_slice(self, descriptor, vlan_checked=True):
        try:
            if not vlan_checked:
                vlan_id = descriptor['vlan_id']
                if self.is_slice_already_stored(vlan_id):
                    raise Exception('A slice with vlan id ' + str(vlan_id) +
                                    " is already stored")
            slc = Slice(descriptor)
            if not slc.status.is_initialised():
                return False

            self.add_slice_to_repository(slc)

            return True

        except Exception as e:
            LOG.error(e)
            return False

    def activate_slice(self, vlan_id, onos_inquirer):
        try:
            vlan_id = str(vlan_id)
            if not self.is_slice_already_stored(vlan_id):
                    raise Exception('A slice with vlan id ' + vlan_id +
                                    " is not stored")
            slc = self.slice_repository[vlan_id]
            assert isinstance(slc, Slice)
            if not (slc.status.is_initialised() or slc.status.is_deactivated()):
                raise Exception('Slice with vlan id ' + vlan_id +
                                " status is " + slc.status.value())
            slc.activate(onos_inquirer)

            if not slc.status.is_active():
                return False

            return True

        except Exception as e:
            LOG.error(e)
            return False

    def deactivate_slice(self, vlan_id, onos_inquirer):
        try:
            vlan_id = str(vlan_id)
            if not self.is_slice_already_stored(vlan_id):
                    raise Exception('A slice with vlan id ' + vlan_id +
                                    " is not stored")
            slc = self.slice_repository[vlan_id]
            assert isinstance(slc, Slice)
            if not (slc.status.is_active()):
                raise Exception('Slice with vlan id ' + vlan_id +
                                " status is " + slc.status.value())
            slc.deactivate(onos_inquirer)

            if not slc.status.is_deactivated():
                return False

            return True

        except Exception as e:
            LOG.error(e)
            return False










