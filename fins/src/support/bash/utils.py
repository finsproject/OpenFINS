# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import subprocess
import traceback

from fins.src.support.bash.ssh_scp import RemoteScriptLauncher as RSL

LOG = logging.getLogger(__name__)

class BashUtils:

    ##################
    # > FILE MANAGEMENT
    #

    @staticmethod
    def save_into_file(data, file_full_path_name, append=False):

        try:
            mode = 'w+'
            if append:
                mode = 'a+'
            file = open(file_full_path_name, mode)
            file.write(data)
            file.close()

            return True

        except Exception as e:
            traceback.print_exc()

            return False

    ##################
    # > RUN LOCALLY
    #

    @staticmethod
    def run_local(bash_command):
        LOG.debug("\n\nRun command (locally): \n%s\n", bash_command)
        subprocess.call(bash_command, shell=True)

    ##################
    # > SSH
    #

    @staticmethod
    def ssh_command(command, destination_ip):
        #  TODO: move here ssh_scp.py RSL method
        return RSL.ssh_run_remote_command(command,
                                          destination_ip)

    @staticmethod
    def ssh_bg_command(command, destination_ip):
        #  TODO: move here ssh_scp.py RSL method
        return RSL.ssh_run_remote_bg_command(command,
                                             destination_ip)
    @staticmethod
    def subprocess_ssh(command, username, destination_ip):
        return RSL.subprocess_ssh(command, username, destination_ip)

    @staticmethod
    def ssh_get_remote_output(command, username, destination_ip):
        return RSL.ssh_get_remote_output(command, username, destination_ip)

    ##################
    # > SCP
    #

    @staticmethod
    def scp_push(file_name, destination_ip, source_path, destination_path):
        #  TODO: move here ssh_scp.py RSL method
        return RSL.scp_inject(file_name,
                              destination_ip,
                              source_path,
                              destination_path)

    @staticmethod
    def scp_pull(file_name, destination_ip, source_path, destination_path):
        #  TODO: move here ssh_scp.py RSL method
        return RSL.scp_retrieve_remote_file(file_name,
                                            destination_ip,
                                            source_path,
                                            destination_path)

    ##################
    # > OTHER
    #

    @staticmethod
    def make_a_bg_command(command):
        return 'nohup ' + command + ' &'
