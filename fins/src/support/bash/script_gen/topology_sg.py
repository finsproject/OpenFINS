# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import datetime
import logging
import os
import json

from fins.src.support.bash.utils import BashUtils as BU
# from fins.src.setup.utils import FinsSetupUtils as FSU
from fins.confs.generator.constants import FINSConstants

LOG = logging.getLogger(__name__)


class TopologyScriptGen:

    #################
    # > TOPOLOGY DESCRIPTOR
    #

    IFTYPE_GRE = 'GRE'
    IFTYPE_INT = 'INTERNAL'
    IFTYPE_TAP = 'TAP'

    SCRIPT_FOLDER = "fins/scripts/generated/topology/"

    @staticmethod
    def _assign_fins_ifid(iftype, params):
        assert isinstance(params, dict)
        if iftype == TopologyScriptGen.IFTYPE_GRE:
            assert 'destination' in params
            return "gre_" + params['destination']
        elif iftype == TopologyScriptGen.IFTYPE_TAP:
            assert 'index' in params
            extra = ''
            if 'extra' in params:
                if params['extra']:
                    extra = 'x_'
            return "ft_" + extra + params['index']
        elif iftype == TopologyScriptGen.IFTYPE_INT:
            assert 'index' in params
            extra = ''
            if 'extra' in params:
                if params['extra']:
                    extra = 'x_'
            return "fi_" + extra + params['index']
        else:
            raise Exception("Invalid iftype: %s", iftype)

    @staticmethod
    def _assign_fins_brid(id):
        return 'fbr_' + id

    @staticmethod
    def create_bridge_descriptor(rs, rcs, links, extra_controller_id):

        TYPE_OVS = FINSConstants.TYPE_RES__IRIS_OVS
        TYPE_VM = FINSConstants.TYPE_RES__IRIS_VM
        TYPE_USRP = FINSConstants.TYPE_RES__IRIS_USRP

        KEY_RES__RC_ID  = 'rc_id'

        bridges = dict()

        for idx in rs:

            r = rs[idx]

            # get resource Id and Type
            r_id = r['id']
            r_type = r['type']

            if not ((r_type == TYPE_VM) or
                    (r_type == TYPE_OVS) or
                    (r_type == TYPE_USRP)):
                raise Exception('Unknown resource type: %s', r_type)

            br = dict()
            br['id'] = TopologyScriptGen._assign_fins_brid(r_id)
            br['associated_resource'] = r_id
            br['associated_resource_jfed'] = r['vm_id']
            br['ip'] = r['ip']

            # Assign Controller

            rc_id = None
            if KEY_RES__RC_ID in r:
                rc_id = r[KEY_RES__RC_ID]
            else:
                rc_id = extra_controller_id

            assert rc_id is not None

            for index in rcs:
                rc = rcs[index]
                if rc['id'] == rc_id:
                    br['controller'] = dict()
                    br['controller']['ip'] = rc['ip']
                    br['controller']['port'] = rc['extra']['port']

            assert 'controller' in br

            # Assign Interfaces

            br['interfaces'] = dict()

            br['interfaces']['gre'] = list()
            br['interfaces']['tap'] = list()
            br['interfaces']['int'] = list()

            # Set Internal Ifs (extra)

            if 'extra' in r:
                ex = r['extra']
                if 'extra_intifs' in ex:
                    eifs = ex['extra_intifs']
                    for ifid in eifs:
                        intif = dict(
                            id=ifid,
                            ip=eifs[ifid]
                        )
                        br['interfaces']['int'].append(intif)
                if 'extra_taps' in ex:
                    taps = ex['extra_taps']
                    for tapid in taps:
                        tap = dict(
                            id=tapid
                        )
                        br['interfaces']['tap'].append(tap)

            bridges[r_id] = br

        # Add GRE ifs (from links)

        for idx in links:

            link = links[idx]

            src_id = link['source']
            dst_id = link['destination']
            src = None
            dst = None
            for index in rs:
                r = rs[index]
                if r['id'] == src_id:
                    src = r
                elif r['id'] == dst_id:
                    dst = r
            assert src is not None
            assert dst is not None

            bridge = bridges[src_id]
            gre_src = dict(
                id=TopologyScriptGen._assign_fins_ifid(
                    TopologyScriptGen.IFTYPE_GRE,
                    dict(
                        destination=dst_id
                    )
                ),
                remote_ip=dst['ip']
            )
            bridge['interfaces']['gre'].append(gre_src)

            bridge = bridges[dst_id]
            gre_dst = dict(
                id=TopologyScriptGen._assign_fins_ifid(
                    TopologyScriptGen.IFTYPE_GRE,
                    dict(
                        destination=src_id
                    )
                ),
                remote_ip=src['ip']
            )
            bridge['interfaces']['gre'].append(gre_dst)

        # Assign of_port number to each port in each bridge

        for r_id in bridges:

            bridge = bridges[r_id]
            counter = 1
            for ifgre in bridge['interfaces']['gre']:
                ifgre['of_port'] = counter
                counter += 1

            for iftap in bridge['interfaces']['tap']:
                iftap['of_port'] = counter
                counter += 1

            for ifint in bridge['interfaces']['int']:
                ifint['of_port'] = counter
                counter += 1

        return bridges


    @staticmethod
    def create_full_topology_descriptor(fins_descriptor, onos_descriptor,
                                        dictionarized=True):
        fd = json.loads(json.dumps(fins_descriptor))
        od = json.loads(json.dumps(onos_descriptor))

        ftd = list()
        for vm_id in fd:
            vm = fd[vm_id]
            vm_descriptor = dict()
            vd = vm_descriptor
            vd['id'] = dict(
                fins=vm['associated_resource'],
                jfed=vm['associated_resource_jfed']
            )
            vd['ip'] = vm['ip']
            vd['bridge'] = dict()
            vd['bridge']['controller'] = vm['controller']
            vd['bridge']['name'] = vm['id']
            bn = vd['bridge']['name']
            vd['bridge']['onos'] = dict()
            for field in od[bn]:
                if field != 'ports':
                    vd['bridge']['onos'][field] = od[bn][field]
            vd['bridge']['ports'] = dict()

            for iftype in vm['interfaces']:
                for iface in vm['interfaces'][iftype]:
                    port = dict()
                    port['id'] = iface['id']
                    port['type'] = iftype
                    port['of_port'] = str(iface['of_port'])
                    if iftype == 'gre':
                        port['remote'] = dict()
                        port['remote']['ip'] = iface['remote_ip']
                    elif iftype == 'int':
                        port['ip'] = iface['ip']

                    vd['bridge']['ports'][port['of_port']] = port
            ftd.append(vd)

        for vm in ftd:
            for idx in vm['bridge']['ports']:
                port = vm['bridge']['ports'][idx]
                if port['type'] == 'gre':
                    for remote_vm in ftd:
                        if remote_vm['ip'] == port['remote']['ip']:
                            port['remote']['vm_id'] = remote_vm['id']
                            for ridx in remote_vm['bridge']['ports']:
                                rport = remote_vm['bridge']['ports'][ridx]
                                if (rport['type'] == 'gre') and \
                                   (rport['remote']['ip'] == vm['ip']):
                                    port['remote']['of_port'] = rport['of_port']
        if not dictionarized:
            return ftd

        ftd_dict = dict()
        for vm in ftd:
            ftd_dict[vm['id']['fins']] = vm

        return ftd_dict


    #################
    # > TOPOLOGY SCRIPTS
    #

    # Echo

    @staticmethod
    def _echo_create_bridge(bridge_id, separator='\n'):
        return "echo \"Creating bridge " + bridge_id + "\"" + \
               separator

    @staticmethod
    def _echo_create_if(iftype, bridge_id, params, separator='\n'):

        assert isinstance(params, dict)

        assert 'id' in params
        if_id = params['id']

        assert 'of_port' in params
        of_port = params['of_port']

        rip = ''
        if iftype == 'GRE':
            assert 'remote_ip' in params
            rip = ' (remote ip: ' + params['remote_ip'] + ')'

        return "echo \"Creating and adding " + iftype + rip + " " + if_id + \
               " to bridge " + bridge_id + "[of port: " + str(of_port) + "]\"" \
               + separator

    @staticmethod
    def _echo_set_userspace_datapath(separator='\n'):

        return "echo \"Set Userspace datapath\"" + separator

    @staticmethod
    def _echo_set_protocols(protocols, separator='\n'):

        plist = ""
        for p in protocols:
            plist += str(p) + " "

        return "echo \"Set supported protocols: \"" + plist + separator



    @staticmethod
    def _echo_set_controller(bridge_id, controller, separator="\n"):
        assert 'ip' in controller
        ip = controller['ip']
        assert 'port' in controller
        port = controller['port']
        return "echo \"Assign " + bridge_id + \
               " to controller running @" + ip + ":" + port + "\"" + \
               separator

    # Bridge

    @staticmethod
    def _add_bridge(bridge_id, separator="\n"):
        return "sudo ovs-vsctl add-br " + bridge_id + separator

    @staticmethod
    def _activate_bridge(bridge_id, separator="\n"):
        return "sudo ifconfig " + bridge_id + " up" + separator

    @staticmethod
    def _set_bridge_datatpath(bridge_id, datapath, separator="\n"):
        return "sudo ovs-vsctl set bridge " + bridge_id + \
               " datapath_type=" + datapath + separator

    @staticmethod
    def _set_bridge_protocols(bridge_id, protocols, separator="\n"):
        assert isinstance(protocols, list)
        plist = str(protocols[0])
        for index in range(1, len(protocols)):
            plist += "," + str(protocols[index])
        return "sudo ovs-vsctl set bridge " + bridge_id + \
               " protocols=" + plist + separator

    # Interface

    @staticmethod
    def _activate_if(if_id, if_ip=None, separator="\n"):
        if if_ip is None:
            if_ip = ""
        else:
            if_ip = " " + if_ip
        return "sudo ifconfig " + if_id + if_ip + " up" + separator

    # Descriptor processing

    @staticmethod
    def _process_controller(descriptor, bridge_id, separator="\n"):
        assert 'ip' in descriptor
        ip = descriptor['ip']
        assert 'port' in descriptor
        port = descriptor['port']

        echo = TopologyScriptGen._echo_set_controller(bridge_id, descriptor)

        script = "sudo ovs-vsctl set-controller " + bridge_id +\
                 " tcp:" + ip + ":" + str(port) + separator

        return echo + script + separator

    @staticmethod
    def _process_ifint(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']
        assert 'ip' in descriptor
        if_ip = descriptor['ip']

        echo = TopologyScriptGen._echo_create_if('INT', bridge_id, descriptor)

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  ' -- set interface ' + if_id + ' type=internal' + separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        script3 = TopologyScriptGen._activate_if(if_id, if_ip)

        return echo + script1 + script2 + script3 + separator

    @staticmethod
    def _process_ifints(descriptor, bridge_id):
        script = ''
        for ifint in descriptor:
            script += TopologyScriptGen._process_ifint(ifint, bridge_id)
        return script

    @staticmethod
    def _process_iftap(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']

        echo = TopologyScriptGen._echo_create_if('TAP', bridge_id, descriptor)

        script0 = 'sudo ip tuntap add mode tap ' + if_id + separator

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        script3 = TopologyScriptGen._activate_if(if_id)

        return echo + script0 + script1 + script2 + script3 + separator

    @staticmethod
    def _process_iftaps(descriptor, bridge_id):
        script = ''
        for iftap in descriptor:
            script += TopologyScriptGen._process_iftap(iftap, bridge_id)
        return script

    @staticmethod
    def _process_ifgre(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'remote_ip' in descriptor
        remote_ip = descriptor['remote_ip']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']

        echo = TopologyScriptGen._echo_create_if('GRE', bridge_id, descriptor)

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  ' -- set interface ' + if_id + ' type=gre ' +\
                  'options:remote_ip=' + remote_ip +\
                  separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        return echo + script1 + script2 + separator

        # script3 = TopologyScriptGen._activate_if(if_id)
        # return echo + script1 + script2 + script3 + separator

    @staticmethod
    def _process_ifgres(descriptor, bridge_id):
        script = ''
        for ifgre in descriptor:
            script += TopologyScriptGen._process_ifgre(ifgre, bridge_id)
        return script

    @staticmethod
    def _process_interfaces(descriptor, bridge_id):

        assert 'gre' in descriptor
        ifgre = descriptor['gre']
        script = TopologyScriptGen._process_ifgres(ifgre, bridge_id)

        assert 'tap' in descriptor
        iftap = descriptor['tap']
        script += TopologyScriptGen._process_iftaps(iftap, bridge_id)

        assert 'int' in descriptor
        ifint = descriptor['int']
        script += TopologyScriptGen._process_ifints(ifint, bridge_id)

        return script

    @staticmethod
    def _process_bridge(descriptor, separator="\n"):
        assert 'id' in descriptor
        bridge_id = descriptor['id']

        echo = TopologyScriptGen._echo_create_bridge(bridge_id)

        script1 = TopologyScriptGen._add_bridge(bridge_id) + separator

        script2 = TopologyScriptGen._activate_bridge(bridge_id) + separator

        echo2 = TopologyScriptGen._echo_set_userspace_datapath()

        script3 = TopologyScriptGen._set_bridge_datatpath(bridge_id, 'netdev')\
                  + separator

        protocols = ["OpenFlow10", "OpenFlow11", "OpenFlow12", "OpenFlow13"]
        echo3 = TopologyScriptGen._echo_set_protocols(protocols)

        script4 = TopologyScriptGen._set_bridge_protocols(bridge_id, protocols)\
                  + separator

        assert 'controller' in descriptor
        script5 = ''
        if descriptor['controller'] is not None:
            controller = descriptor['controller']
            script5 = TopologyScriptGen._process_controller(controller,
                                                            bridge_id)

        assert 'interfaces' in descriptor
        interfaces = descriptor['interfaces']
        script6 = TopologyScriptGen._process_interfaces(interfaces, bridge_id)

        script7 = TopologyScriptGen._activate_bridge(bridge_id) + separator

        return echo + script1 + script2 + \
               echo2 + script3 + echo3 + script4 + script5 + script6 + script7

    @staticmethod
    def generate(descriptor, separator="\n"):
        assert isinstance(descriptor, dict)
        result = dict()
        for resource_id in descriptor:
            bridge = descriptor[resource_id]

            LOG.debug(bridge)

            assert 'ip' in bridge
            resource_ip = bridge['ip']
            comment = "# BRIDGE CONFIGURATION SCRIPT FOR " + resource_id + \
                      "@" + resource_ip + separator
            comment += "# generated by code @" + str(datetime.datetime.now()) \
                       + separator + separator
            echo = "echo \"Installing OpenVSwitch switch\"" + separator
            apt1 = 'sudo apt-get update' + separator
            apt2 = 'sudo apt-get install -y --force-yes openvswitch-switch' + \
                   separator
            script = TopologyScriptGen._process_bridge(bridge)
            result[resource_id] = comment + separator +\
                                  echo + apt1 + apt2 + separator + \
                                  script
        return result

    @staticmethod
    def save(scripts, cwd, fins_path=SCRIPT_FOLDER):
        if fins_path == None:
            fins_path = TopologyScriptGen.SCRIPT_FOLDER
        for resource_id in scripts:
            script = scripts[resource_id]
            file_fullpath_name = cwd + "/" +\
                                 fins_path + \
                                 resource_id + ".sh"
            # LOG.debug("file path: %s", file_fullpath_name)
            BU.save_into_file(script, file_fullpath_name)

    @staticmethod
    def save_sdn_cleaner(script, cwd, fins_path=SCRIPT_FOLDER):
        if fins_path == None:
            fins_path = TopologyScriptGen.SCRIPT_FOLDER
        file_fullpath_name = cwd + "/" +\
                             fins_path + "sdn_cleaner.sh"
        BU.save_into_file(script, file_fullpath_name)

    @staticmethod
    def do_remote_configuration(rs, cwd, fins_path=SCRIPT_FOLDER):
        if fins_path == None:
            fins_path = TopologyScriptGen.SCRIPT_FOLDER
        for index in rs:
            r = rs[index]
            if r['ei_booted'] == 'False':
                continue
            r_id = r['id']
            r_ip = r['ip']
            init_file = r_id + ".sh"
            folder = cwd + "/" + fins_path
            if os.path.isfile(folder+init_file):

                set_ovs_file = 'set_ovs.sh'
                src_path = cwd + "/" + "fins/scripts/fixed/"

                if BU.scp_push(set_ovs_file, r_ip, src_path, '.'):
                    command = 'sudo chmod 777 ' + set_ovs_file + ' && ' + \
                              './' + set_ovs_file
                    LOG.info("It may take some time... ")
                    response = BU.ssh_command(command, r_ip)
                    LOG.info("\n\nOVS support installed @" + r_ip)

                if BU.scp_push(init_file, r_ip, folder, '.'):

                    command = 'sudo chmod 777 ' + init_file + ' && ' + \
                              './' + init_file
                    LOG.info("It may take some time... ")
                    response = BU.ssh_command(command, r_ip)
                    LOG.info("\n\nOVS bridge should have been " +
                             "created & configured @" + r_ip)


                    # command = 'sudo chmod 777 ' + init_file
                    # response = BU.ssh_command(command, r_ip)
                    # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # LOG.debug("STDERR:\n" + str(response[2].read()))
                    #
                    # # command = 'sudo ./' + init_file + ' && ' + 'touch done_it'
                    # command = 'ls -l ' + ' && ' + './' + init_file
                    # response = BU.ssh_command(command, r_ip)
                    # # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # for line in response[1].read().splitlines():
                    #     LOG.debug("STDOUT: %s", line)
                    # LOG.debug("STDERR:\n" + str(response[2].read()))
                    #
                    # command = 'touch done_it_again'
                    # response = BU.ssh_command(command, r_ip)
                    # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # LOG.debug("STDERR:\n" + str(response[2].read()))

                else:

                    LOG.error('Failed configuration script ' + init_file +
                              ' transmission')

            else:
                LOG.warning("NO Topology configuration file for resource %s@%s",
                            r_id, r_ip)
