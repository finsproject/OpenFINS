# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
import logging
import os

from fins.src.support.bash.utils import BashUtils as BU

LOG = logging.getLogger(__name__)


class Mongoose:

    SCRIPT_FOLDER = 'fins/scripts/generated/mongoose/'
    FILE_NAME = 'mongoose.sh'

    def __init__(self, vms, fed_username, fins_install_folder):
        self._vms = vms
        self._user = fed_username
        self._fins_folder = fins_install_folder

    def generate(self, vms=None, fed_username=None):
        if vms is None:
            vms = self._vms
        if fed_username is None:
            fed_username = self._user
        script = "# !/usr/bin/env bash"
        for index in vms:
            ip = vms[index]['ip']
            script += "\nssh -oStrictHostKeyChecking=no -t " + \
                      fed_username + "@" + ip + " 'pkill python && exit'"

        return script

    def save(self, script=None, fins_install_folder=None,
             fins_path=None, file_name=None):
        if script is None:
            script = self.generate()
        if fins_path is None:
            fins_path = self.SCRIPT_FOLDER
        if file_name is None:
            file_name = self.FILE_NAME
        if fins_install_folder is None:
            fins_install_folder = self._fins_folder
        full_filename = fins_install_folder + "/" + fins_path + \
                        file_name

        BU.save_into_file(script, full_filename)

    def init(self, fins_install_folder=None, filename=None,
             fins_path=None):
        if fins_install_folder is None:
            fins_install_folder = self._fins_folder
        if fins_path is None:
            fins_path = self.SCRIPT_FOLDER
        if filename is None:
            filename = self.FILE_NAME
        self.save(self.generate(self._vms, self._user), fins_install_folder,
                  fins_path, filename)

    @staticmethod
    def add_cmd_to_mongoose(fed_username, ip, fins_install_folder, cmd):

        script = "\nssh -oStrictHostKeyChecking=no -t " + \
                  fed_username + "@" + ip + " '" + cmd + " && exit'"

        full_filename = fins_install_folder + "/" + Mongoose.SCRIPT_FOLDER + \
                        Mongoose.FILE_NAME

        BU.save_into_file(script, full_filename, True)
