# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.


class BashScriptGenerator:

    def __init__(self):
        pass

    @staticmethod
    def generate_host_explorer(filename, vms, fed_username):
        try:
            s = "# !/usr/bin/env bash"
            for index in vms:
                ip = vms[index]['ip']
                s += "\nssh -oStrictHostKeyChecking=no -t " + \
                     fed_username + "@" + ip + " 'exit'"

            file = open(filename, 'w')
            file.write(s)
            file.close()

            return True

        except Exception as e:

            return False
