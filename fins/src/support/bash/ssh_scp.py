# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import paramiko
import time
from fins.third_party.scp.scp import SCPClient
import traceback
import logging
import subprocess

LOG = logging.getLogger(__name__)

class NoParamikoDEBUGFilter(logging.Filter):
    def filter(self, record):
        # print("record.name: " + str(record.name))
        # print("record.levelname: " + str(record.levelname))
        if record.levelname == 'DEBUG':
            return False
        return True

class RemoteScriptLauncher:
    def __init__(self):
        pass

    @staticmethod
    def scp_inject(file_name, destination_ip, source_path, destination_path):

        paramiko.util.get_logger('paramiko.transport').addFilter(
            NoParamikoDEBUGFilter())
        try:

            assert isinstance(file_name, str)
            assert isinstance(destination_ip, str)
            assert isinstance(source_path, str)
            assert isinstance(destination_path, str)

            if not source_path.endswith("/"):
                source_path += "/"
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(destination_ip)
            scp = SCPClient(client.get_transport())
            scp.put(source_path + file_name, remote_path=destination_path)
            scp.close()
            client.close()

            return True

        except Exception as e:

            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()

            return False

    @staticmethod
    def ssh_run_remote_command(command, destination_ip):
        client = None
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(destination_ip)
            LOG.debug('\nREMOTE COMMAND: %s', command)
            stdin, stdout, stderr = client.exec_command(command)
            std_out = ""
            indent = "    "
            out_list = []
            for line in stdout.read().splitlines():
                s = str(line.decode("utf-8"))
                std_out += indent + s + "\n"
                out_list.append(s)
            LOG.debug("\nSTANDARD OUTPUT: \n%s", std_out)
            std_err = ""
            err_list = []
            for line in stderr.read().splitlines():
                s = str(line.decode("utf-8"))
                std_err += indent + s + "\n"
                err_list.append(s)
            LOG.debug("\nSTANDARD ERROR: \n%s", std_err)
            client.close()
            return [[], out_list, err_list]

        except Exception as e:
            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            if client is not None:
                client.close()

            return None

    @staticmethod
    def ssh_run_remote_bg_command(command, destination_ip):
        client = None
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(destination_ip)
            LOG.debug('\nREMOTE COMMAND: %s', command)
            transport = client.get_transport()
            channel = transport.open_session()
            channel.exec_command(command)
            channel.close()
            client.close()
            return True

        except Exception as e:
            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            if client is not None:
                client.close()

            return False

    @staticmethod
    def subprocess_ssh(command, username, destination_ip):
        try:
            ssh = "nohup ssh -oStrictHostKeyChecking=no " \
                  "-oForwardAgent=yes -t -f -n %s@%s " % \
              (username, destination_ip)
            cmd = "\"" + command + "\""

            out = subprocess.run(ssh + cmd, shell=True)
            LOG.debug("\nResponse: %s\n" % out)
            return True
        except subprocess.CalledProcessError as e:
            LOG.error(str(e.output))
            traceback.print_exc()
            return False

    @staticmethod
    def ssh_get_remote_output(command, username, destination_ip):
        try:
            ssh = "ssh -oStrictHostKeyChecking=no " \
                  "-oForwardAgent=yes -t -f -n %s@%s " % \
                  (username, destination_ip)
            cmd = "\"" + command + "\""
            out = subprocess.check_output(ssh + cmd, shell=True)
            lines = out.decode("utf-8").split('\n')
            for idx, line in enumerate(lines):
                s = line.strip(' \t\n\r')
                lines[idx] = s
            return lines
        except subprocess.CalledProcessError as e:
            LOG.error(str(e.output))
            traceback.print_exc()
            return None

    @staticmethod
    def scp_retrieve_remote_file(file_name, source_ip, source_path,
                                 destination_path):
        try:

            assert isinstance(file_name, str)
            assert isinstance(source_ip, str)
            assert isinstance(source_path, str)
            assert isinstance(destination_path, str)

            if not source_path.endswith("/"):
                source_path += "/"
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(source_ip)
            scp = SCPClient(client.get_transport())
            scp.get(source_path + file_name, local_path=destination_path)
            scp.close()
            client.close()

            return True

        except Exception as e:

            LOG.error("ERROR: %s", str(e))
            traceback.print_exc()

            return False


    @staticmethod
    def perform(script_name, script_source_path, dest_ip, dest_path, report_name, report_saving_path):
        # TRANSFER
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.connect(dest_ip)
        scp = SCPClient(client.get_transport())
        scp.put(script_source_path + script_name, remote_path=dest_path)

        # EXECUTE
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.connect(dest_ip)
        command1 = 'cd ' + dest_path + ';'
        command2 = 'sudo chmod 777 ' + script_name + ';'
        command3 = './' + script_name
        command = command1 + command2 + command3
        stdin, stdout, stderr = client.exec_command(command)
        # LOG.debug(stdout.read())

        time.sleep(1)

        # CHECK
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.connect(dest_ip)
        command1 = 'cd ' + dest_path + ';'
        command2 = 'ls ' + report_name + ';'
        command = command1 + command2
        stdin, stdout, stderr = client.exec_command(command)
        # LOG.debug(stdout.read())
        result = stdout.read()
        if 'No such file or directory' in result:
            raise Exception("Script failed!")

        # RETRIEVE
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.connect(dest_ip)
        scp = SCPClient(client.get_transport())
        scp.get(dest_path+report_name, local_path=report_saving_path)

