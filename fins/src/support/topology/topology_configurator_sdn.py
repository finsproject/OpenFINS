# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import datetime
import os

from fins.confs.generator.constants import FINSConstants
from fins.confs.generator.resources import ResourceValidator, \
    ResourceSDNValidator
from fins.confs.generator.controllers import ResControllerValidator
from fins.confs.generator.modules import ModuleRAValidator
from fins.confs.generator.links import LinkValidator

from fins.src.support.bash.utils import BashUtils as BU


class TopologyConfiguratorSDN:
    IFTYPE_GRE = 'GRE'
    IFTYPE_INT = 'INTERNAL'
    IFTYPE_TAP = 'TAP'

    def __init__(self):
        self.LOG = logging.getLogger(__name__)

    @staticmethod
    def assign_bridge_id(resource_id):
        return 'fbr_' + resource_id

    @staticmethod
    def _assign_gre_id(destination):
        return "gre_" + str(destination)

    @staticmethod
    def assign_bridge_controller(managed_by, rcs, ras):

        ID = ResControllerValidator.KEY__ID
        TYPE = ResControllerValidator.KEY__TYPE
        for idx in rcs:
            rc = rcs[idx]
            rc_id = rc[ID]
            if rc_id == managed_by:
                rc_type = rc[TYPE]
                if rc_type == FINSConstants.TYPE_RC__ONOS:
                    controller = dict(
                        ip=rc['ip'],
                        port=rc['port']
                    )
                    return controller
                else:
                    # Other switch controllers are not yet managed
                    return None

        ID = ModuleRAValidator.KEY__ID
        TYPE = ModuleRAValidator.KEY__TYPE
        for idx in ras:
            ra = ras[idx]
            ra_id = ra[ID]
            if ra_id == managed_by:
                ra_type = ra[TYPE]
                if ra_type == FINSConstants.TYPE_MOD__RA_OVS:
                    # RA OVS inject directly commands into bridge
                    return None
                else:
                    # Other switch controllers are not yet managed
                    return None

        # Unknown controller: NO controller
        return None

    @staticmethod
    def fill_OVS_bridge_descriptor(r, rs, rcs, ras, links):

        br = dict()

        br['associated_resource'] = r['id']
        br['associated_resource_jfed'] = r['vm_id']
        br['ip'] = r['ip']

        # Userspace datapath?
        USDP = ResourceSDNValidator.KEY__USERSPACE_DP
        r_usdp = r[USDP]
        br['userspace_dp'] = r_usdp

        # Assign Controller

        br['controller'] = None

        MANAGED_BY = ResourceValidator.KEY__MANAGED_BY
        if MANAGED_BY in r:
            r_manby = r[MANAGED_BY]
            br['controller'] = \
                TopologyConfiguratorSDN.assign_bridge_controller(r_manby,
                                                                 rcs,
                                                                 ras)

        # Assign Interfaces

        br['interfaces'] = dict()

        br['interfaces']['gre'] = list()
        br['interfaces']['tap'] = list()
        br['interfaces']['int'] = list()

        # Add Additional interfaces/ports

        EXTRA_INTS = ResourceSDNValidator.KEY__EXTRA_INTS
        EXTRA_TAPS = ResourceSDNValidator.KEY__EXTRA_TAPS

        if EXTRA_INTS in r:
            eits = r[EXTRA_INTS]
            for ifid in eits:
                intif = dict(
                    id=ifid,
                    ip=eits[ifid]
                )
                br['interfaces']['int'].append(intif)

        if EXTRA_TAPS in r:
            taps = r[EXTRA_TAPS]
            for tapid in taps:
                tap = dict(
                    id=tapid
                )
                br['interfaces']['tap'].append(tap)

        # Add GRE ifs (from links)

        for idx in links:
            link = links[idx]
            TYPE = LinkValidator.KEY__TYPE
            SRC_ID = LinkValidator.KEY__SOURCE
            DST_ID = LinkValidator.KEY__DESTINATION
            ORIENT = LinkValidator.KEY__ORIENTATION
            ID = ResourceValidator.KEY__ID

            if link[TYPE] == FINSConstants.TYPE_LINK__GRE:
                src_id = link[SRC_ID]
                dst_id = link[DST_ID]
                r_id = r[ID]

                if src_id == r_id:
                    dst = None
                    for index in rs:
                        if rs[index][ID] == dst_id:
                            dst = rs[index]
                            break
                    if dst is None:
                        raise Exception("Destination not among resources: " +
                                        dst_id)
                    gre_link = dict(
                        id=TopologyConfiguratorSDN._assign_gre_id(dst_id),
                        remote_ip=dst['ip']
                    )
                    br['interfaces']['gre'].append(gre_link)
                elif link[ORIENT] == FINSConstants.DIR_LINK__BI and \
                    dst_id == r_id:
                        src = None
                        for index in rs:
                            if rs[index][ID] == src_id:
                                src = rs[index]
                                break
                        if src is None:
                            raise Exception("Source not among resources: " +
                                            src_id)
                        gre_link = dict(
                            id=TopologyConfiguratorSDN._assign_gre_id(src_id),
                            remote_ip=src['ip']
                        )
                        br['interfaces']['gre'].append(gre_link)

        # Add OFport number (automatically)
        counter = 1
        for ifgre in br['interfaces']['gre']:
            ifgre['of_port'] = counter
            counter += 1

        for iftap in br['interfaces']['tap']:
            iftap['of_port'] = counter
            counter += 1

        for ifint in br['interfaces']['int']:
            ifint['of_port'] = counter
            counter += 1

        return br

    def create_bridge_descriptor(self, rs, rcs, ras, links):

        bridges = dict()

        for idx in rs:

            r = rs[idx]

            ID = ResourceValidator.KEY__ID
            TYPE = ResourceValidator.KEY__TYPE

            r_id = r[ID]
            r_type = r[TYPE]

            if r_type == FINSConstants.TYPE_RES__IRIS_USRP or \
               r_type == FINSConstants.TYPE_RES__IRIS_HYDRA or \
               r_type == FINSConstants.TYPE_RES__IRIS_HYDRAEMU_NB or \
               r_type == FINSConstants.TYPE_RES__IRIS_HYDRAEMU_SB_VR:
                self.LOG.debug("Resource type: %s (IGNORED)", r_type)

            # OVS bridges
            elif r_type == FINSConstants.TYPE_RES__IRIS_VM or \
                 r_type == FINSConstants.TYPE_RES__IRIS_OVS or \
                 r_type == FINSConstants.TYPE_RES__IRIS_NETCOUPLER:

                self.LOG.debug("Resource type: %s (TO BE PROCESSED)", r_type)

                bridge_id = TopologyConfiguratorSDN.assign_bridge_id(r_id)

                if bridge_id in bridges:
                    # TODO: handle it
                    raise Exception('Duplicated bridge id: ' + bridge_id)

                br = TopologyConfiguratorSDN.fill_OVS_bridge_descriptor(r, rs,
                                                                        rcs, ras,
                                                                        links)

                br['id'] = bridge_id

                bridges[r_id] = br

            else:
                self.LOG.debug("Unknown Resource type: %s (IGNORED)", r_type)

        return bridges


LOG = logging.getLogger(__name__)


class OVSTopologyScriptGen:

    SCRIPT_FOLDER = "fins/scripts/generated/topology/"

    # Echo

    @staticmethod
    def _echo_create_bridge(bridge_id, separator='\n'):
        return "echo \"Creating bridge " + bridge_id + "\"" + \
               separator

    @staticmethod
    def _echo_create_if(iftype, bridge_id, params, separator='\n'):

        assert isinstance(params, dict)

        assert 'id' in params
        if_id = params['id']

        assert 'of_port' in params
        of_port = params['of_port']

        rip = ''
        if iftype == 'GRE':
            assert 'remote_ip' in params
            rip = ' (remote ip: ' + params['remote_ip'] + ')'

        return "echo \"Creating and adding " + iftype + rip + " " + if_id + \
               " to bridge " + bridge_id + "[of port: " + str(of_port) + "]\"" \
               + separator

    @staticmethod
    def _echo_set_userspace_datapath(separator='\n'):

        return "echo \"Set Userspace datapath\"" + separator

    @staticmethod
    def _echo_set_protocols(protocols, separator='\n'):

        plist = ""
        for p in protocols:
            plist += str(p) + " "

        return "echo \"Set supported protocols: \"" + plist + separator

    @staticmethod
    def _echo_set_controller(bridge_id, controller, separator="\n"):
        assert 'ip' in controller
        ip = controller['ip']
        assert 'port' in controller
        port = controller['port']
        return "echo \"Assign " + bridge_id + \
               " to controller running @" + ip + ":" + port + "\"" + \
               separator

    # Bridge

    @staticmethod
    def _add_bridge(bridge_id, separator="\n"):
        return "sudo ovs-vsctl add-br " + bridge_id + separator

    @staticmethod
    def _activate_bridge(bridge_id, separator="\n"):
        return "sudo ifconfig " + bridge_id + " up" + separator

    @staticmethod
    def _set_bridge_datatpath(bridge_id, datapath, separator="\n"):
        return "sudo ovs-vsctl set bridge " + bridge_id + \
               " datapath_type=" + datapath + separator

    @staticmethod
    def _set_bridge_protocols(bridge_id, protocols, separator="\n"):
        assert isinstance(protocols, list)
        plist = str(protocols[0])
        for index in range(1, len(protocols)):
            plist += "," + str(protocols[index])
        return "sudo ovs-vsctl set bridge " + bridge_id + \
               " protocols=" + plist + separator

    # Interface

    @staticmethod
    def _activate_if(if_id, if_ip=None, separator="\n"):
        if if_ip is None:
            if_ip = ""
        else:
            if_ip = " " + if_ip
        return "sudo ifconfig " + if_id + if_ip + " up" + separator

    # Descriptor processing

    @staticmethod
    def _process_controller(descriptor, bridge_id, separator="\n"):
        assert 'ip' in descriptor
        ip = descriptor['ip']
        assert 'port' in descriptor
        port = descriptor['port']

        echo = OVSTopologyScriptGen._echo_set_controller(bridge_id, descriptor)

        script = "sudo ovs-vsctl set-controller " + bridge_id + \
                 " tcp:" + ip + ":" + str(port) + separator

        return echo + script + separator

    @staticmethod
    def _process_ifint(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']
        assert 'ip' in descriptor
        if_ip = descriptor['ip']

        echo = OVSTopologyScriptGen._echo_create_if('INT', bridge_id, descriptor)

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  ' -- set interface ' + if_id + ' type=internal' + separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        script3 = OVSTopologyScriptGen._activate_if(if_id, if_ip)

        return echo + script1 + script2 + script3 + separator

    @staticmethod
    def _process_ifints(descriptor, bridge_id):
        script = ''
        for ifint in descriptor:
            script += OVSTopologyScriptGen._process_ifint(ifint, bridge_id)
        return script

    @staticmethod
    def _process_iftap(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']

        echo = OVSTopologyScriptGen._echo_create_if('TAP', bridge_id, descriptor)

        script0 = 'sudo ip tuntap add mode tap ' + if_id + separator

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        script3 = OVSTopologyScriptGen._activate_if(if_id)

        return echo + script0 + script1 + script2 + script3 + separator

    @staticmethod
    def _process_iftaps(descriptor, bridge_id):
        script = ''
        for iftap in descriptor:
            script += OVSTopologyScriptGen._process_iftap(iftap, bridge_id)
        return script

    @staticmethod
    def _process_ifgre(descriptor, bridge_id, separator="\n"):
        assert 'id' in descriptor
        if_id = descriptor['id']
        assert 'remote_ip' in descriptor
        remote_ip = descriptor['remote_ip']
        assert 'of_port' in descriptor
        of_port = descriptor['of_port']

        echo = OVSTopologyScriptGen._echo_create_if('GRE', bridge_id, descriptor)

        script1 = 'sudo ovs-vsctl add-port ' + bridge_id + ' ' + if_id + \
                  ' -- set interface ' + if_id + ' type=gre ' + \
                  'options:remote_ip=' + remote_ip + \
                  separator

        script2 = 'sudo ovs-vsctl set Interface ' + if_id + \
                  ' ofport_request=' + str(of_port) + separator

        return echo + script1 + script2 + separator

        # script3 = OVSTopologyScriptGen._activate_if(if_id)
        # return echo + script1 + script2 + script3 + separator

    @staticmethod
    def _process_ifgres(descriptor, bridge_id):
        script = ''
        for ifgre in descriptor:
            script += OVSTopologyScriptGen._process_ifgre(ifgre, bridge_id)
        return script

    @staticmethod
    def _process_interfaces(descriptor, bridge_id):

        assert 'gre' in descriptor
        ifgre = descriptor['gre']
        script = OVSTopologyScriptGen._process_ifgres(ifgre, bridge_id)

        assert 'tap' in descriptor
        iftap = descriptor['tap']
        script += OVSTopologyScriptGen._process_iftaps(iftap, bridge_id)

        assert 'int' in descriptor
        ifint = descriptor['int']
        script += OVSTopologyScriptGen._process_ifints(ifint, bridge_id)

        return script

    @staticmethod
    def _process_bridge(descriptor, separator="\n"):
        assert 'id' in descriptor
        bridge_id = descriptor['id']

        echo = OVSTopologyScriptGen._echo_create_bridge(bridge_id)

        script1 = OVSTopologyScriptGen._add_bridge(bridge_id) + separator

        script2 = OVSTopologyScriptGen._activate_bridge(bridge_id) + separator

        # REMOVED: NO MORE NEEDED
        #
        # echo2 = OVSTopologyScriptGen._echo_set_userspace_datapath()
        #
        # script3 = OVSTopologyScriptGen._set_bridge_datatpath(bridge_id, 'netdev') \
        #           + separator
        echo2 = ""
        script3 = ""

        protocols = ["OpenFlow10", "OpenFlow11", "OpenFlow12", "OpenFlow13"]
        echo3 = OVSTopologyScriptGen._echo_set_protocols(protocols)

        script4 = OVSTopologyScriptGen._set_bridge_protocols(bridge_id, protocols) \
                  + separator

        assert 'controller' in descriptor
        script5 = ''
        if descriptor['controller'] is not None:
            controller = descriptor['controller']
            script5 = OVSTopologyScriptGen._process_controller(controller,
                                                            bridge_id)

        assert 'interfaces' in descriptor
        interfaces = descriptor['interfaces']
        script6 = OVSTopologyScriptGen._process_interfaces(interfaces, bridge_id)

        script7 = OVSTopologyScriptGen._activate_bridge(bridge_id) + separator

        return echo + script1 + script2 + \
               echo2 + script3 + echo3 + script4 + script5 + script6 + script7

    @staticmethod
    def generate(descriptor, separator="\n"):
        assert isinstance(descriptor, dict)
        result = dict()
        for resource_id in descriptor:
            bridge = descriptor[resource_id]

            # LOG.debug(bridge)

            assert 'ip' in bridge
            resource_ip = bridge['ip']
            comment = "# BRIDGE CONFIGURATION SCRIPT FOR " + resource_id + \
                      "@" + resource_ip + separator
            comment += "# generated by code @" + str(datetime.datetime.now()) \
                       + separator + separator
            # echo = "echo \"Installing OpenVSwitch switch\"" + separator
            # apt1 = 'sudo apt-get update' + separator
            # # apt2 = 'sudo apt-get install -y --force-yes openvswitch-switch' + \
            # #        separator
            #
            # apt2 = "if [ -e \"/usr/local/share/openvswitch/\" ];" + separator +\
            #        "then" + separator +\
            #        "    sudo /usr/local/share/openvswitch/scripts/ovs-ctl start" + separator +\
            #        "    sudo ovs-vsctl --no-wait init" + separator +\
            #        "else" + separator +\
            #        "    apt-get install -y --force-yes openvswitch-switch=2.9.0" + separator +\
            #        "fi"+ separator

            script = OVSTopologyScriptGen._process_bridge(bridge)
            result[resource_id] = comment + separator + script
                                  # echo + apt1 + apt2 + separator + \
                                  # script
        return result

    @staticmethod
    def save(scripts, cwd, fins_path=SCRIPT_FOLDER):
        if fins_path == None:
            fins_path = OVSTopologyScriptGen.SCRIPT_FOLDER
        for resource_id in scripts:
            script = scripts[resource_id]
            file_fullpath_name = cwd + "/" + \
                                 fins_path + \
                                 resource_id + ".sh"
            # LOG.debug("file path: %s", file_fullpath_name)
            BU.save_into_file(script, file_fullpath_name)

    @staticmethod
    def do_remote_configuration(rs, cwd, fins_path=SCRIPT_FOLDER):
        if fins_path == None:
            fins_path = OVSTopologyScriptGen.SCRIPT_FOLDER
        for index in rs:
            r = rs[index]
            if r['ei_booted'] == 'False':
                continue
            r_id = r['id']
            r_ip = r['ip']
            init_file = r_id + ".sh"
            folder = cwd + "/" + fins_path
            if os.path.isfile(folder + init_file):

                if BU.scp_push(init_file, r_ip, folder, '.'):

                    command = 'sudo chmod 777 ' + init_file + ' && ' + \
                              './' + init_file
                    LOG.info("It may take some time... ")
                    response = BU.ssh_command(command, r_ip)
                    LOG.info("\n\nOVS support and switches should be " +
                             "installed & configured @" + r_ip)
                    # command = 'sudo chmod 777 ' + init_file
                    # response = BU.ssh_command(command, r_ip)
                    # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # LOG.debug("STDERR:\n" + str(response[2].read()))
                    #
                    # # command = 'sudo ./' + init_file + ' && ' + 'touch done_it'
                    # command = 'ls -l ' + ' && ' + './' + init_file
                    # response = BU.ssh_command(command, r_ip)
                    # # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # for line in response[1].read().splitlines():
                    #     LOG.debug("STDOUT: %s", line)
                    # LOG.debug("STDERR:\n" + str(response[2].read()))
                    #
                    # command = 'touch done_it_again'
                    # response = BU.ssh_command(command, r_ip)
                    # LOG.debug("STDOUT:\n" + str(response[1].read()))
                    # LOG.debug("STDERR:\n" + str(response[2].read()))

                else:

                    LOG.error('Failed configuration script ' + init_file +
                              ' transmission')

            else:
                LOG.warning("NO Topology configuration file for resource %s@%s",
                            r_id, r_ip)









