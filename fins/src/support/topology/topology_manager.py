# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import json
from fins.src.support.system.utils import fins_copy_dict

LOG = logging.getLogger(__name__)

class FullTopology:

    class FieldId:

        FIELD__FINS = 'fins'
        FIELD__JFED = 'jfed'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__FINS,
                                 self.FIELD__JFED]
            self.valid_types = dict()
            self.valid_types[self.FIELD__FINS] = [str, None]
            self.valid_types[self.FIELD__JFED] = [str, None]

            self.fins_id = None
            self.jfed_id = None

            try:
                if descriptor is None:
                    return
                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):
                        self.fins_id = descriptor[self.FIELD__FINS]
                        self.jfed_id = descriptor[self.FIELD__JFED]
                    else:
                        raise Exception('descriptor parameter is not a ' +
                                        'valid dict')
                else:
                    raise Exception('descriptor parameter is not None ' +
                                    'or dict: ' +
                                    str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: " +
                            "expected " +
                            str(self.valid_types[field]) + ", found " +
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            rsp = dict()
            rsp[self.FIELD__FINS] = self.fins_id
            rsp[self.FIELD__JFED] = self.jfed_id
            return rsp

    class FieldController:

        FIELD__IP = 'ip'
        FIELD__PORT = 'port'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__IP,
                                 self.FIELD__PORT]
            self.valid_types = dict()
            self.valid_types[self.FIELD__IP] = [str]
            self.valid_types[self.FIELD__PORT] = [str]

            self.ip = None
            self.port = None

            try:
                if descriptor is None:
                    return

                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):
                        self.ip = descriptor[self.FIELD__IP]
                        self.port = descriptor[self.FIELD__PORT]
                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not None or dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: " +
                            "expected " +
                            str(self.valid_types[field]) + ", found "+
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            rsp = dict()
            rsp[self.FIELD__IP] = self.ip
            rsp[self.FIELD__PORT] = self.port
            return rsp

    class FieldOnos:

        FIELD__PROTOCOL = 'protocol'
        FIELD__ID = 'id'
        FIELD__MAN_ADDR = 'managementAddress'
        FIELD__DRIVER = 'driver'
        FIELD__AVAILABLE = 'available'
        FIELD__TYPE = 'type'
        FIELD__HW = 'hw'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__PROTOCOL,
                                 self.FIELD__ID,
                                 self.FIELD__MAN_ADDR,
                                 self.FIELD__DRIVER,
                                 self.FIELD__AVAILABLE,
                                 self.FIELD__TYPE,
                                 self.FIELD__HW]

            self.valid_types = dict()
            self.valid_types[self.FIELD__PROTOCOL] = [str]
            self.valid_types[self.FIELD__ID] = [str]
            self.valid_types[self.FIELD__MAN_ADDR] = [str]
            self.valid_types[self.FIELD__DRIVER] = [str]
            self.valid_types[self.FIELD__AVAILABLE] = [bool]
            self.valid_types[self.FIELD__TYPE] = [str]
            self.valid_types[self.FIELD__HW] = [str]

            self.protocol = None
            self.oid = None
            self.man_addr = None
            self.driver = None
            self.available = None
            self.otype = None
            self.hw = None

            try:
                if descriptor is None:
                    return

                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):

                        self.protocol = descriptor[self.FIELD__PROTOCOL]
                        self.oid = descriptor[self.FIELD__ID]
                        self.man_addr = descriptor[self.FIELD__MAN_ADDR]
                        self.driver = descriptor[self.FIELD__DRIVER]
                        self.available = descriptor[self.FIELD__AVAILABLE]
                        self.otype = descriptor[self.FIELD__TYPE]
                        self.hw = descriptor[self.FIELD__HW]
                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not None or dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: "
                            "expected " +
                            str(self.valid_types[field]) + ", found "+
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            descriptor = dict()
            descriptor[self.FIELD__PROTOCOL] = self.protocol
            descriptor[self.FIELD__ID] = self.oid
            descriptor[self.FIELD__MAN_ADDR] = self.man_addr
            descriptor[self.FIELD__DRIVER] = self.driver
            descriptor[self.FIELD__AVAILABLE] = self.available
            descriptor[self.FIELD__TYPE] = self.otype
            descriptor[self.FIELD__HW] = self.hw

            return descriptor

    class FieldPort:

        FIELD__OFPORT = 'of_port'
        FIELD__ID = 'id'
        FIELD__TYPE = 'type'
        FIELD__IP = 'ip'
        FIELD__REMOTE = 'remote'

        TYPE__GRE = 'gre'
        TYPE__INT = 'int'
        TYPE__TAP = 'tap'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__OFPORT,
                                 self.FIELD__ID,
                                 self.FIELD__TYPE,
                                 self.FIELD__IP,
                                 self.FIELD__REMOTE]

            self.valid_types = dict()
            self.valid_types[self.FIELD__OFPORT] = [str, int]
            self.valid_types[self.FIELD__ID] = [str]
            self.valid_types[self.FIELD__TYPE] = [str]
            self.valid_types[self.FIELD__IP] = [None, str]
            self.valid_types[self.FIELD__REMOTE] = [None, dict]

            self.optional_fields = dict()
            self.optional_fields[self.TYPE__GRE] = [self.FIELD__IP]
            self.optional_fields[self.TYPE__INT] = [self.FIELD__REMOTE]
            self.optional_fields[self.TYPE__TAP] = [self.FIELD__IP,
                                                    self.FIELD__REMOTE]

            self.ofport = None
            self.pid = None
            self.ptype = None
            self.ip = None
            self.remote = None

            try:

                if descriptor is None:
                    return

                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):

                        self.ofport = descriptor[self.FIELD__OFPORT]
                        self.pid = descriptor[self.FIELD__ID]
                        self.ptype = descriptor[self.FIELD__TYPE]
                        self.ip = descriptor[self.FIELD__IP]
                        self.remote = descriptor[self.FIELD__REMOTE]
                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not None or dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        ptype = descriptor[self.FIELD__TYPE]
                        if field not in self.optional_fields[ptype]:
                            raise Exception("Descriptor is missing field '" +
                                            field + "'")

                    if field in descriptor:
                        valid = False
                        for vtype in self.valid_types[field]:
                            if vtype is None:
                                if descriptor[field] is None:
                                    valid = True
                                    break
                            elif isinstance(descriptor[field], vtype):
                                valid = True
                                break
                        if not valid:
                            raise Exception(
                                "Field '" + field + "' type is not valid: " +
                                "expected " +
                                str(self.valid_types[field]) + ", found " +
                                str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            descriptor = dict()
            descriptor[self.FIELD__OFPORT] = self.ofport
            descriptor[self.FIELD__ID] = self.pid
            descriptor[self.FIELD__TYPE] = self.ptype
            descriptor[self.FIELD__IP] = self.ip
            descriptor[self.FIELD__REMOTE] = self.remote

            return descriptor

    class FieldRemote:

        FIELD__OFPORT = 'of_port'
        FIELD__VMID = 'vm_id'
        FIELD__IP = 'ip'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__OFPORT,
                                 self.FIELD__VMID,
                                 self.FIELD__IP]

            self.valid_types = dict()
            self.valid_types[self.FIELD__OFPORT] = [str, int]
            self.valid_types[self.FIELD__VMID] = [dict]
            self.valid_types[self.FIELD__IP] = [str]

            self.ofport = None
            self.vmid = None
            self.ip = None

            try:

                if descriptor is None:
                    return

                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):

                        self.ofport = descriptor[self.FIELD__OFPORT]
                        self.vmid = descriptor[self.FIELD__VMID]
                        self.ip = descriptor[self.FIELD__IP]
                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not None nor dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        LOG.debug("Current descriptor: %s", str(descriptor))
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: "
                            "expected " +
                            str(self.valid_types[field]) + ", found "+
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            descriptor = dict()
            descriptor[self.FIELD__OFPORT] = self.ofport
            descriptor[self.FIELD__VMID] = self.vmid
            descriptor[self.FIELD__IP] = self.ip

            return descriptor

    class FieldBridge:

        FIELD__CONTROLLER = 'controller'
        FIELD__ONOS = 'onos'
        FIELD__NAME = 'name'
        FIELD__PORTS = 'ports'


        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__CONTROLLER,
                                 self.FIELD__ONOS,
                                 self.FIELD__NAME,
                                 self.FIELD__PORTS]

            self.valid_types = dict()
            self.valid_types[self.FIELD__CONTROLLER] = [dict]
            self.valid_types[self.FIELD__ONOS] = [dict]
            self.valid_types[self.FIELD__NAME] = [str]
            self.valid_types[self.FIELD__PORTS] = [dict]

            self.controller = None
            self.onos = None
            self.name = None
            self.ports = None

            try:
                if descriptor is None:
                    return

                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):

                        self.controller = descriptor[self.FIELD__CONTROLLER]
                        self.onos = descriptor[self.FIELD__ONOS]
                        self.name = descriptor[self.FIELD__NAME]
                        self.ports = descriptor[self.FIELD__PORTS]
                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not None or dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: "
                            "expected " +
                            str(self.valid_types[field]) + ", found "+
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            descriptor = dict()
            descriptor[self.FIELD__CONTROLLER] = self.controller
            descriptor[self.FIELD__ONOS] = self.onos
            descriptor[self.FIELD__NAME] = self.name
            descriptor[self.FIELD__PORTS] = self.ports

            return descriptor

    class FieldVM:

        FIELD__BRIDGE = 'bridge'
        FIELD__ID = 'id'
        FIELD__IP = 'ip'

        def __init__(self, descriptor=None):

            self.valid_fields = [self.FIELD__BRIDGE,
                                 self.FIELD__ID,
                                 self.FIELD__IP]

            self.valid_types = dict()
            self.valid_types[self.FIELD__BRIDGE] = [dict]
            self.valid_types[self.FIELD__ID] = [dict]
            self.valid_types[self.FIELD__IP] = [str]

            self.bridge = None
            self.vid = None
            self.ip = None

            try:
                if descriptor is None:
                    return
                if isinstance(descriptor, dict):
                    if self.validate_descriptor(descriptor):

                        self.bridge = descriptor[self.FIELD__BRIDGE]
                        self.vid = descriptor[self.FIELD__ID]
                        self.ip = descriptor[self.FIELD__IP]

                    else:
                        raise Exception(
                            'descriptor parameter is not a valid dict')
                else:
                    raise Exception(
                        'descriptor parameter is not a dict: ' +
                        str(type(descriptor)))

            except Exception as e:
                LOG.error(e)

        def validate_descriptor(self, descriptor):

            try:

                if not isinstance(descriptor, dict):
                    raise Exception('Descriptor type is not dict: ' +
                                    str(type(descriptor)))

                for field in self.valid_fields:
                    if field not in descriptor:
                        raise Exception("Descriptor is missing field '" +
                                        field + "'")

                    valid = False
                    for vtype in self.valid_types[field]:
                        if vtype is None:
                            if descriptor[field] is None:
                                valid = True
                                break
                        elif isinstance(descriptor[field], vtype):
                            valid = True
                            break
                    if not valid:
                        raise Exception(
                            "Field '" + field + "' type is not valid: "
                            "expected " +
                            str(self.valid_types[field]) + ", found "+
                            str(type(descriptor[field])))

                return True

            except Exception as e:
                LOG.error(e)
                return False

        def get_descriptor(self):

            descriptor = dict()
            descriptor[self.FIELD__BRIDGE] = self.bridge
            descriptor[self.FIELD__ID] = self.vid
            descriptor[self.FIELD__IP] = self.ip

            return descriptor

    def __init__(self, descriptor):
        self.validate_descriptor(descriptor)
        self._full_topology_descriptor = descriptor
        pass

    def validate_descriptor(self, descriptor):
        try:
            if not isinstance(descriptor, dict):
                raise Exception('Descriptor is not a dict')

            for vm_id in descriptor:
                # LOG.debug("Processing vm_id %s", vm_id)
                vm = descriptor[vm_id]
                fvm = self.FieldVM()
                if not fvm.validate_descriptor(vm):
                    raise Exception('Invalid VM structure')

                vid = vm[fvm.FIELD__ID]
                fvid = self.FieldId()
                if not fvid.validate_descriptor(vid):
                    raise Exception('Invalid VM.ID structure')
                if not vm[fvm.FIELD__ID][fvid.FIELD__FINS] == vm_id:
                    raise Exception('Invalid VM index assignment')

                bridge = vm[fvm.FIELD__BRIDGE]
                fbridge = self.FieldBridge()
                if not fbridge.validate_descriptor(bridge):
                    raise Exception('Invalid VM.BRIDGE structure')
                controller = bridge[fbridge.FIELD__CONTROLLER]
                fctrl = self.FieldController()
                if not fctrl.validate_descriptor(controller):
                    raise Exception('Invalid VM.BRIDGE.CONTROLLER structure')
                onos = bridge[fbridge.FIELD__ONOS]
                fonos = self.FieldOnos()
                if not fonos.validate_descriptor(onos):
                    raise Exception('Invalid VM.BRIDGE.ONOS structure')
                ports = bridge[fbridge.FIELD__PORTS]
                if not isinstance(ports, dict):
                    raise Exception('Invalid VM.BRIDGE.PORTS structure')
                for ofport in ports:
                    # LOG.debug("Processing ofport %s", ofport)
                    port = ports[ofport]
                    fport = self.FieldPort()
                    if not fport.validate_descriptor(port):
                        raise Exception('Invalid VM.BRIDGE.PORTS.PORT '
                                        'structure')
                    if not port[fport.FIELD__OFPORT] == ofport:
                        raise Exception('Invalid VM.BRIDGE.PORTS.PORT index '
                                        'assignment')
                    if fport.FIELD__REMOTE in port:
                        remote = port[fport.FIELD__REMOTE]
                        frem = self.FieldRemote()
                        if not frem.validate_descriptor(remote):
                            raise Exception(
                                'Invalid VM.BRIDGE.PORTS.PORT.REMOTE structure')
                        vm_id = remote[frem.FIELD__VMID]
                        fvmid = self.FieldId()
                        if not fvmid.validate_descriptor(vm_id):
                            raise Exception(
                                'Invalid VM.BRIDGE.PORTS.PORT.REMOTE.ID '
                                'structure')
            return True

        except Exception as e:
            LOG.error(e)
            return False

    def nav_get_all_resources(self):

        _ftd = self._full_topology_descriptor

        return _ftd

    def nav_get_resource(self, vm_id):

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id]

    def nav_get_resource_id(self, vm_id, jfed=True):

        fid = self.FieldVM.FIELD__ID
        ffins = self.FieldId.FIELD__FINS
        fjfed = self.FieldId.FIELD__JFED

        _ftd = self._full_topology_descriptor

        if jfed:
            return _ftd[vm_id][fid][fjfed]

        return _ftd[vm_id][fid][ffins]

    def nav_get_resource_ip(self, vm_id):

        fip = self.FieldVM.FIELD__IP

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id][fip]

    def nav_get_onos_id(self, vm_id):

        fbridge = self.FieldVM.FIELD__BRIDGE
        fonos = self.FieldBridge.FIELD__ONOS
        fonos_id = self.FieldOnos.FIELD__ID

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id][fbridge][fonos][fonos_id]

    def nav_get_ports(self, vm_id):

        fbridge = self.FieldVM.FIELD__BRIDGE
        fports = self.FieldBridge.FIELD__PORTS

        return self._full_topology_descriptor[vm_id][fbridge][fports]

    def nav_get_port_type(self, vm_id, ofport):

        fbridge = self.FieldVM.FIELD__BRIDGE
        fports = self.FieldBridge.FIELD__PORTS
        ftype = self.FieldPort.FIELD__TYPE

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id][fbridge][fports][ofport][ftype]

    def nav_is_port_gre(self, vm_id, ofport):

        t = self.FieldPort.TYPE__GRE

        return t == self.nav_get_port_type(vm_id, ofport)

    def nav_get_remote_vm_id(self, vm_id, ofport, fins_id=True):

        fbridge = self.FieldVM.FIELD__BRIDGE
        fports = self.FieldBridge.FIELD__PORTS
        fremote = self.FieldPort.FIELD__REMOTE
        fvid = self.FieldRemote.FIELD__VMID
        fins = self.FieldId.FIELD__FINS
        jfed = self.FieldId.FIELD__JFED

        tag = fins
        if not fins_id:
            tag = jfed

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id][fbridge][fports][ofport][fremote][fvid][tag]

    def nav_get_remote_ofport(self, vm_id, ofport,):

        fbridge = self.FieldVM.FIELD__BRIDGE
        fports = self.FieldBridge.FIELD__PORTS
        fremote = self.FieldPort.FIELD__REMOTE
        fofport = self.FieldRemote.FIELD__OFPORT

        _ftd = self._full_topology_descriptor

        return _ftd[vm_id][fbridge][fports][ofport][fremote][fofport]



class PathRequest:

    class VMEncoding:
        FINS = 'fins'
        JFED = 'jfed'
        IP = 'ip'

        def __init__(self):
            self.valid_values = [self.FINS, self.JFED, self.IP]

        def validate(self, encoding):
            try:
                if not isinstance(encoding, str):
                    raise Exception("vm encoding is not a string")

                for valid in self.valid_values:
                    if valid == encoding:
                        return True

                raise Exception("Invalid vm encoding: " + encoding)

            except Exception as e:
                LOG.error(e)
                return False

    class PortEncoding:
        NAME = 'name'
        OFPORT = 'ofport'
        IP = 'ip'

        def __init__(self):
            self.valid_values = [self.NAME, self.OFPORT, self.IP]

        def validate(self, encoding):
            try:
                if not isinstance(encoding, str):
                    raise Exception("Port encoding is not a string")

                for valid in self.valid_values:
                    if valid == encoding:
                        return True

                raise Exception("Invalid vm encoding: " + encoding)

            except Exception as e:
                LOG.error(e)
                return False

    class PathEncoding:
        STRLIST = 'strlist'
        DICTLIST = 'dictlist'

        NODE = 'node'
        PORT = 'port'

        def __init__(self):
            self.valid_values = [self.STRLIST, self.DICTLIST]
            self.step_values = [self.NODE, self.PORT]

        def validate(self, encoding, instance=None):
            try:

                if not isinstance(encoding, str):
                    raise Exception("Port encoding is not a string")

                for valid in self.valid_values:
                    if valid == encoding:
                        if instance is None:
                            return True
                        if not isinstance(instance, list):
                            raise Exception("Path field is not a list")
                        if encoding == self.STRLIST:
                            for step in instance:
                                if not self._validate_step(encoding, step):
                                    raise Exception("Invalid step for "
                                                    "encoding" + encoding)
                        elif encoding == self.DICTLIST:
                            for step in instance:
                                if not self._validate_step(encoding, step):
                                    raise Exception("Invalid step for "
                                                    "encoding" + encoding)
                        return True

                raise Exception("Unknown encoding: " + encoding)

            except Exception as e:
                LOG.error(e)
                return False

        def _validate_step(self, encoding, step):
            try:
                if encoding == self.STRLIST:
                    return isinstance(step, str)
                elif encoding == self.DICTLIST:
                    if not isinstance(step, dict):
                        raise Exception("Path step is not a dict")
                    for field in self.step_values:
                        if field in step:
                            if field == self.NODE:
                                if not isinstance(step[field], str):
                                    raise Exception("Step field '" + field +
                                                    "' is not a string")
                            elif field == self.PORT:
                                if not (step[field] is None or
                                   isinstance(step[field], str) or
                                   isinstance(step[field], int)):
                                    raise Exception("Step field '" + field +
                                                    "' is not a string or "
                                                    "an int or None: " +
                                                    str(type(step[field])))
                        else:
                            raise Exception("Missing field in path step: " +
                                            field)
                    return True

                raise Exception("Unknown encoding: " + encoding)

            except Exception as e:
                LOG.error(e)
                return False

    class StepItem:
        FIELD_NODE = 'node'
        FIELD_PORT = 'port'

        def __init__(self):
            self.field_values = [not self.FIELD_NODE, self.FIELD_PORT]

        def validate(self, step):
            try:
                if not isinstance(step, dict):
                    raise Exception("Step is not a dictionary")

                for field in self.field_values:
                    if field not in step:
                        raise Exception("Mandatory field " + field +
                                        "is missing")
                    if field == self.FIELD_NODE:
                        if not isinstance(step[field], str):
                            raise Exception("Field " + field + "is not string")
                    elif field == self.FIELD_PORT:
                        if isinstance(step[field], str):
                            raise Exception("Field " + field + "is not string")

                return True

            except Exception as e:
                LOG.error(e)
                return False

    class DescriptorFields:
        VM_ENCODING = 'vm_encoding'
        PORT_ENCODING = 'port_encoding'
        PATH_ENCODING = 'path_encoding'
        PATH = 'path'

        def list_values(self, section=None):
            if section is None:
                return [self.VM_ENCODING, self.PORT_ENCODING,
                        self.PATH_ENCODING, self.PATH]

    def __init__(self, descriptor=None):
        self.vm_encoding = None
        self.port_encoding = None
        self.path_encoding = None
        self.path = None

        if descriptor is not None:
            if self.validate_descriptor(descriptor):
                DF = self.DescriptorFields()
                self.vm_encoding = descriptor[DF.VM_ENCODING]
                self.port_encoding = descriptor[DF.PORT_ENCODING]
                self.path_encoding = descriptor[DF.PATH_ENCODING]
                self.path = descriptor[DF.PATH]
            else:
                pass

    def validate_descriptor(self, descriptor):
        if not isinstance(descriptor, dict):
            return False
        try:
            DF = self.DescriptorFields()
            for field in DF.list_values():
                if field not in descriptor:
                    raise Exception('Missing mandatory field: ' + field)

                if field == DF.VM_ENCODING:
                    VM_ENC = self.VMEncoding()
                    if not VM_ENC.validate(descriptor[field]):
                        raise Exception("Invalid field '" + field +
                                        "' value: " + descriptor[field])

                elif field == DF.PORT_ENCODING:
                    PO_ENC = self.PortEncoding()
                    if not PO_ENC.validate(descriptor[field]):
                        raise Exception("Invalid field '" + field +
                                        "' value: " + descriptor[field])

                elif field == DF.PATH_ENCODING:
                    PA_ENC = self.PathEncoding()
                    if not PA_ENC.validate(descriptor[field]):
                        raise Exception("Invalid field '" + field +
                                        "' value: " + descriptor[field])

                elif field == DF.PATH:
                    PA_ENC = self.PathEncoding()
                    if not PA_ENC.validate(descriptor[DF.PATH_ENCODING],
                                           descriptor[DF.PATH]):
                        raise Exception("Invalid field '" + field)

            return True

        except Exception as e:
            LOG.error(e)
            return False

    def get_field__vm_encoding(self):
        return self.vm_encoding

    def get_field__port_encoding(self):
        return self.port_encoding

    def get_field__path_encoding(self):
        return self.path_encoding

    def get_field__path(self):
        return self.path


class TopologyManager:

    def __init__(self, fins_sdn_config, onos_descriptor):

        # create full totology descriptor

        ftd = self.generate_full_topology_descriptor(fins_sdn_config,
                                                     onos_descriptor)

        self._full_topology = FullTopology(ftd)
        pass

    def generate_full_topology_descriptor(self, fins_descriptor,
                                          onos_descriptor, dictionarized=True):
        fd = json.loads(json.dumps(fins_descriptor))
        od = json.loads(json.dumps(onos_descriptor))

        ftd = list()
        for vm_id in fd:
            vm = fd[vm_id]
            vm_descriptor = dict()
            vd = vm_descriptor
            vd['id'] = dict(
                fins=vm['associated_resource'],
                jfed=vm['associated_resource_jfed']
            )
            vd['ip'] = vm['ip']
            vd['bridge'] = dict()
            vd['bridge']['controller'] = vm['controller']
            vd['bridge']['name'] = vm['id']
            bn = vd['bridge']['name']
            vd['bridge']['onos'] = dict()
            for field in od[bn]:
                if field != 'ports':
                    vd['bridge']['onos'][field] = od[bn][field]
            vd['bridge']['ports'] = dict()

            for iftype in vm['interfaces']:
                for iface in vm['interfaces'][iftype]:
                    port = dict()
                    port['id'] = iface['id']
                    port['type'] = iftype
                    port['of_port'] = str(iface['of_port'])
                    if iftype == 'gre':
                        port['remote'] = dict()
                        port['remote']['ip'] = iface['remote_ip']
                    elif iftype == 'int':
                        port['ip'] = iface['ip']

                    vd['bridge']['ports'][port['of_port']] = port
            ftd.append(vd)

        for vm in ftd:
            for idx in vm['bridge']['ports']:
                port = vm['bridge']['ports'][idx]
                if port['type'] == 'gre':
                    for remote_vm in ftd:
                        if remote_vm['ip'] == port['remote']['ip']:
                            port['remote']['vm_id'] = remote_vm['id']
                            for ridx in remote_vm['bridge']['ports']:
                                rport = remote_vm['bridge']['ports'][ridx]
                                if (rport['type'] == 'gre') and \
                                   (rport['remote']['ip'] == vm['ip']):
                                    port['remote']['of_port'] = rport['of_port']
        if not dictionarized:
            return ftd

        ftd_dict = dict()
        for vm in ftd:
            ftd_dict[vm['id']['fins']] = vm

        return ftd_dict

    def eval_explicit_path(self, path_request, compact=False):

        pr = path_request
        if isinstance(path_request, dict):
            pr = PathRequest(path_request)
        elif not isinstance(path_request, PathRequest):
            raise Exception("Invalid path request type: " +
                            str(type(path_request)))

        assert isinstance(pr, PathRequest)

        path = fins_copy_dict(pr.get_field__path())

        evaluated_path = self._eval_path_encoding(pr.get_field__path_encoding(),
                                                  path)

        if evaluated_path is None:
            return None

        evaluated_path = self._eval_vm_encoding(pr.get_field__vm_encoding(),
                                                  evaluated_path)

        if evaluated_path is None:
            return None

        evaluated_path = self._eval_port_encoding(pr.get_field__port_encoding(),
                                                  evaluated_path)

        if evaluated_path is None:
            return None

        explicit_path = self._derive_explicit_path(evaluated_path)

        if compact:

            return self.compact_explicit_path(explicit_path)

        return explicit_path

    def _derive_explicit_path(self, evaluated_path):

        _ft = self._full_topology

        explicit_path = []

        steps = len(evaluated_path)

        SI = PathRequest.StepItem

        for index in range(steps):
            if index == (steps - 1):
                explicit_path.append(evaluated_path[index])
                continue
            if index == 0:
                explicit_path.append(evaluated_path[index])

            node_from = evaluated_path[index][SI.FIELD_NODE]
            node_to = evaluated_path[index + 1][SI.FIELD_NODE]

            found = False
            # ports = self._topology_descriptor[node_from]['bridge']['ports']
            ports = _ft.nav_get_ports(node_from)
            for ofport in ports:

                # if ports[ofport]['type'] == 'gre':
                if _ft.nav_is_port_gre(node_from, ofport):
                    # if ports[ofport]['remote']['vm_id']['fins'] == node_to:
                    if node_to == _ft.nav_get_remote_vm_id(node_from, ofport):
                        step = dict()
                        step[SI.FIELD_NODE] = node_from
                        step[SI.FIELD_PORT] = ofport
                        explicit_path.append(step)
                        step = dict()
                        step[SI.FIELD_NODE] = node_to
                        # step[SI.FIELD_PORT] = ports[ofport]['remote']['of_port']
                        step[SI.FIELD_PORT] = \
                            _ft.nav_get_remote_ofport(node_from, ofport)
                        explicit_path.append(step)
                        found = True
                        break
            if not found:
                raise Exception('Missing connection between ' +
                                str(evaluated_path[index]) + ' and ' +
                                str(evaluated_path[index + 1]))

        return explicit_path


    def compact_explicit_path(self, explicit_path):

        steps = len(explicit_path)

        cep = []

        for index in range(int(steps/2)):

            node_id = explicit_path[2*index]['node']

            # onos_device_id = \
            #     self._topology_descriptor[node_id]['bridge']['onos']['id']

            onos_device_id = self._full_topology.nav_get_onos_id(node_id)

            node = dict(
                node=node_id,
                onos_did=onos_device_id,
                in_port=explicit_path[2*index]['port'],
                out_port=explicit_path[2*index+1]['port']
            )

            cep.append(node)

        return cep

    def _eval_path_encoding(self, encoding, path):

        # PATH ENCODING
        try:

            PE = PathRequest.PathEncoding
            SI = PathRequest.StepItem

            evaluated_path = list()

            if encoding == PE.STRLIST:

                for step in path:

                    components = str.split(step, ':')
                    if len(components) > 2:
                        raise Exception('Invalid path step: %s', step)
                    node = components[0]
                    port = None
                    if len(components) == 2:
                        port = components[1]

                    xstep = dict()
                    xstep[SI.FIELD_NODE] = node
                    xstep[SI.FIELD_PORT] = port
                    evaluated_path.append(xstep)

            elif encoding == PE.DICTLIST:

                evaluated_path = path

            LOG.debug("Evaluated Path (path encoding): \n%s", evaluated_path)

        except Exception as e:

            LOG.error(e)
            return None

        return evaluated_path

    def _eval_vm_encoding(self, encoding, path):

        # VM ENCODING

        try:

            _ft = self._full_topology

            VE = PathRequest.VMEncoding
            SI = PathRequest.StepItem

            if encoding == VE.FINS:

                for step in path:
                    fins_id = step[SI.FIELD_NODE]
                    if fins_id not in _ft.nav_get_all_resources():
                        raise Exception('Unknown FINS node id: %s', fins_id)

            elif encoding == VE.JFED:

                for step in path:
                    jfed_id = step[SI.FIELD_NODE]

                    found = None
                    for fins_id in _ft.nav_get_all_resources():
                        node_jid = _ft.nav_get_resource_id(fins_id)
                        if node_jid == jfed_id:
                            found = fins_id
                            break
                    if found is None:
                        raise Exception('Unknown jFed node id: %s', jfed_id)
                    step[SI.FIELD_NODE] = found

            elif encoding == VE.IP:

                for step in path:
                    ip = step[SI.FIELD_NODE]

                    found = None
                    for fins_id in _ft.nav_get_all_resources():
                        node_ip = _ft.nav_get_resource_ip(fins_id)
                        if ip == node_ip:
                            found = fins_id
                            break
                    if found is None:
                        raise Exception('Unknown node IP: %s', ip)
                    step[SI.FIELD_NODE] = found

            LOG.debug("Evaluated Path (vm encoding): \n%s", path)

        except Exception as e:

            LOG.error(e)
            return None

        return path

    def _eval_port_encoding(self, encoding, path):

        # PORT ENCODING

        try:

            _ft = self._full_topology

            PE = PathRequest.PortEncoding
            SI = PathRequest.StepItem

            if encoding == PE.OFPORT:

                for step in path:
                    if step[SI.FIELD_PORT] is None:
                        continue

                    vm_id = step[SI.FIELD_NODE]
                    ofport = str(step[SI.FIELD_PORT])

                    ports = _ft.nav_get_ports(vm_id)
                    if ofport not in ports:
                        raise Exception('Unknown ofport for node %s: %s',
                                        vm_id, ofport)

            elif encoding == PE.NAME:

                for step in path:
                    if step[SI.FIELD_PORT] is None:
                        continue

                    vm_id = step[SI.FIELD_NODE]
                    port_id = str(step[SI.FIELD_NODE])

                    found = False
                    ports = _ft.nav_get_ports(vm_id)
                    for ofport in ports:

                        if ports[ofport]['id'] == port_id:
                            step[SI.FIELD_PORT] = ofport
                            found = True
                            break

                    if not found:
                        raise Exception('Unknown port id for node %s: %s',
                                        vm_id, port_id)

            elif encoding == PE.IP:

                for step in path:
                    if step[SI.FIELD_PORT] is None:
                        continue

                    vm_id = step[SI.FIELD_NODE]
                    port_ip = str(step[SI.FIELD_NODE])

                    found = False
                    ports = _ft.nav_get_ports(vm_id)
                    for ofport in ports:
                        if 'ip' not in ports[ofport]:
                            continue
                        if ports[ofport]['ip'] == port_ip:
                            step[SI.FIELD_PORT] = ofport
                            found = True
                            break

                    if not found:
                        raise Exception('Unknown port ip for node %s: %s',
                                        vm_id, port_ip)

            LOG.debug("Decoded Path (port encoding): \n%s", path)

        except Exception as e:

            LOG.error(e)
            return None

        return path
