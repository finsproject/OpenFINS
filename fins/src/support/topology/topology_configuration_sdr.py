# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback

from fins.confs.generator.resources import ResourceValidator,\
    ResourceWAValidator
from fins.confs.generator.controllers import ResControllerValidator
from fins.confs.generator.constants import FINSConstants
from fins.src.support.bash.utils import BashUtils


class TopologyConfiguratorSDR:

    def __init__(self, rs, rcs, fins_install_folder):

        self.LOG = logging.getLogger(__name__)

        self._rs = []
        self._rcs = []
        self._select_sdr_controllers(rcs)
        self._select_sdr_resources(rs)
        self.fins_install_folder = fins_install_folder

    def _select_sdr_controllers(self, rcs):
        try:
            for index in rcs:
                rc = rcs[index]
                rc_type = rc[ResControllerValidator.KEY__TYPE]
                if self._is_sdr_controller_by_type(rc_type):
                    self._rcs.append(rc)

                    return True

            return False

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def _is_sdr_controller_by_type(self, rc_type):
        if rc_type == FINSConstants.TYPE_RC__WCTRL:
            return True
        return False

    def _is_sdr_controller_by_id(self, rc_id):
        try:
            for rc in self._rcs:
                if rc_id == rc[ResControllerValidator.KEY__ID]:
                    return True
            return False

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def _select_sdr_resources(self, rs):

        for index in rs:
            r = rs[index]
            managed_by = r[ResourceValidator.KEY__MANAGED_BY]
            if self._is_sdr_controller_by_id(managed_by):
                self._rs.append(r)

    def configure(self):
        try:
            for r in self._rs:
                if not self.configure_by_type(r):
                    raise Exception("SDR configuration failed")
            self.LOG.info("SDR configuration DONE: " +
                          "all whishful agent scripts & confs are in place")

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_by_type(self, resource_descriptor):
        try:

            r = resource_descriptor

            r_type = r[ResourceValidator.KEY__TYPE]
            if r_type == FINSConstants.TYPE_RES__IRIS_USRP:
                # self.LOG.info(r_type)
                self.configure_USRP(r)
            elif r_type == FINSConstants.TYPE_RES__IRIS_HYDRA:
                # self.LOG.info(r_type)
                self.configure_HYDRA(r)
            elif r_type == FINSConstants.TYPE_RES__IRIS_HYDRAEMU_NB:
                # self.LOG.info(r_type)
                self.configure_HYDRAEMU_NB(r)
            elif r_type == FINSConstants.TYPE_RES__IRIS_HYDRAEMU_SB_VR:
                # self.LOG.info(r_type)
                self.configure_HYDRAEMU_SB_VR(r)
            else:
                self.LOG.warning("Unknown resource type: %s", r_type)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def generate_agent_config_file(self, resource, file_full_path_name,
                                   info=None, iface=None):

        KEY__GROUPNAME = "groupName"
        KEY__NAME = "name"
        KEY__INFO = "info"
        KEY__IFACE = "iface"

        DEFAULT__INFO = "wishful FINS agent"
        DEFAULT__IFACE = FINSConstants.IRIS_FINS_MAIN_IF__DEFAULT_VALUE

        if info is None:
            info = DEFAULT__INFO
        if iface is None:
            iface = DEFAULT__IFACE

        try:

            agent = dict()
            agent[KEY__GROUPNAME] = resource[ResourceWAValidator.KEY__GROUP_NAME]
            agent[KEY__NAME] = resource[ResourceValidator.KEY__ID]
            agent[KEY__INFO] = info
            agent[KEY__IFACE] = iface

            data = dict(
                agent=agent
            )

            BashUtils.save_into_file(str(data), file_full_path_name)

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def set_whishful_agent_files(self, wa_resource):
        try:
            r_id = wa_resource[ResourceValidator.KEY__ID]
            path_topology_sdr = "/fins/scripts/generated/topology/sdr"
            file_path = self.fins_install_folder + path_topology_sdr
            filename = "agent_config_" + r_id + ".json"
            ffn = file_path + "/" + filename
            if not self.generate_agent_config_file(wa_resource, ffn):
                raise Exception("Config file generation FAILED for resource " +
                                str(r_id))
            r_filename = 'FINS_wishful_config.json'
            scp_success = BashUtils.scp_push(filename, wa_resource['ip'],
                                             file_path, r_filename)
            if scp_success:
                self.LOG.info("\n\nWhishful Agent Config file SCP to " +
                              str(r_id) + "@" + wa_resource['ip'] + ": DONE\n")
            else:
                raise Exception("Whishful Agent Config file SCP to" +
                                str(r_id) + "@" + wa_resource['ip'] +
                                ": FAILED")

            file_path = self.fins_install_folder + "/fins/scripts/fixed/python"
            filename = "FINS_wishful_agent.py"
            scp_success = BashUtils.scp_push(filename, wa_resource['ip'],
                                             file_path, ".")
            if scp_success:
                self.LOG.info("\n\nWishful Agent Python Script file SCP to " +
                              str(r_id) + "@" + wa_resource['ip'] + ": DONE\n")
            else:
                raise Exception("Wishful Agent Python Script file SCP to " +
                                str(r_id) + "@" + wa_resource['ip'] +
                                ": FAILED")

            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_USRP(self, resource):
        try:
            if not self.set_whishful_agent_files(resource):
                raise Exception("FAILED setting wishful agent files")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_HYDRA(self, resource):
        try:
            if not self.set_whishful_agent_files(resource):
                raise Exception("FAILED setting wishful agent files")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_HYDRAEMU_NB(self, resource):
        try:
            if not self.set_whishful_agent_files(resource):
                raise Exception("FAILED setting wishful agent files")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False

    def configure_HYDRAEMU_SB_VR(self, resource):
        try:
            if not self.set_whishful_agent_files(resource):
                raise Exception("FAILED setting wishful agent files")
            return True

        except Exception as e:
            self.LOG.error(str(e))
            traceback.print_exc()

            return False
