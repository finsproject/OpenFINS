# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
from fins.src.support.ovs.ovs_script_manager import OvsScriptManager as OSM

class QoSQueue:
    DEFAULT__MIN_RATE = 0
    DEFAULT__MAX_RATE = 0
    DEFAULT__UUID = None
    DEFAULT__SLICE_ID = None

    def __init__(self,
                 min_rate=DEFAULT__MIN_RATE,
                 max_rate=DEFAULT__MAX_RATE,
                 slice_id=DEFAULT__SLICE_ID,
                 uuid=DEFAULT__UUID):

        self.min_rate = self.DEFAULT__MIN_RATE
        self.max_rate = self.DEFAULT__MAX_RATE
        self.slice_id = self.DEFAULT__SLICE_ID
        self.uuid = self.DEFAULT__UUID

        self.set__min_rate(min_rate)
        self.set__max_rate(max_rate)
        self.set__slice_id(slice_id)
        self.set__uuid(uuid)

    def set__min_rate(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.min_rate = str(value)
        else:
            self.min_rate = self.DEFAULT__MIN_RATE

    def set__max_rate(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.max_rate = str(value)
        else:
            self.max_rate = self.DEFAULT__MAX_RATE

    def set__slice_id(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.slice_id = str(value)
        else:
            self.slice_id = self.DEFAULT__SLICE_ID

    def set__uuid(self, value):

        if isinstance(value, str):
            self.uuid = value
        else:
            self.uuid = self.DEFAULT__UUID

    def get__min_rate(self):
        return self.min_rate

    def get__max_rate(self):
        return self.max_rate

    def get__slice_id(self):
        return self.slice_id

    def get__uuid(self):
        return self.uuid

    def to_dict(self):
        return dict(
            min_rate=self.min_rate,
            max_rate=self.max_rate,
            slice_id=self.slice_id,
            uuid=self.uuid
        )



class IfQoS:

    DEFAULT__MIN_RATE = 0
    DEFAULT__MAX_RATE = 0
    DEFAULT__INGRESS_BURST = 0
    DEFAULT__INGRESS_RATE = 0
    DEFAULT__BRIDGE_VM = None
    DEFAULT__BRIDGE_NAME = None
    DEFAULT__IF_NAME = None
    DEFAULT__UUID = None

    def __init__(self,
                 min_rate=DEFAULT__MIN_RATE,
                 max_rate=DEFAULT__MAX_RATE,
                 ingress_burst=DEFAULT__INGRESS_BURST,
                 ingress_rate=DEFAULT__INGRESS_RATE,
                 bridge_vm=None,
                 bridge_name=None,
                 if_name=None,
                 uuid=None,
                 queues=dict()
                 ):

        self.min_rate = self.DEFAULT__MIN_RATE
        self.max_rate = self.DEFAULT__MAX_RATE
        self.ingress_burst = self.DEFAULT__INGRESS_BURST
        self.ingress_rate = self.DEFAULT__INGRESS_RATE
        self.bridge_vm = self.DEFAULT__BRIDGE_VM
        self.bridge_name = self.DEFAULT__BRIDGE_NAME
        self.if_name = self.DEFAULT__IF_NAME
        self.uuid = self.DEFAULT__UUID
        self.queues = dict()

        self.set__min_rate(min_rate)
        self.set__max_rate(max_rate)
        self.set__ingress_burst(ingress_burst)
        self.set__ingress_rate(ingress_rate)

        self.set__bridge_vm(bridge_vm)
        self.set__bridge_name(bridge_name)
        self.set__if_name(if_name)
        self.set__uuid(uuid)
        self.set__queues(queues)

    def set__min_rate(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.min_rate = str(value)
        else:
            self.min_rate = self.DEFAULT__MIN_RATE

    def set__max_rate(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.max_rate = str(value)
        else:
            self.max_rate = self.DEFAULT__MAX_RATE

    def set__ingress_burst(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.ingress_burst = str(value)
        else:
            self.ingress_burst = self.DEFAULT__INGRESS_BURST

    def set__ingress_rate(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.ingress_rate = str(value)
        else:
            self.ingress_rate = self.DEFAULT__INGRESS_RATE

    def set__bridge_vm(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.bridge_vm = str(value)
        else:
            self.bridge_vm = self.DEFAULT__BRIDGE_VM

    def set__bridge_name(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.bridge_name = str(value)
        else:
            self.bridge_name = self.DEFAULT__BRIDGE_NAME

    def set__if_name(self, value):

        if isinstance(value, str) or isinstance(value, int):
            self.if_name = str(value)
        else:
            self.if_name = self.DEFAULT__IF_NAME

    def set__uuid(self, value):

        if isinstance(value, str):
            self.uuid = value
        else:
            self.uuid = self.DEFAULT__UUID

    def set__queues(self, value):

        if isinstance(value, dict):
            if self._are_qosqueue(value):
                self.queues = value
            elif self._are_dict(value):
                self.queues = self._into_qosqueue_dict(value)
            else:
                dict()
        else:
            self.queues = dict()

    def _are_qosqueue(self, d):
        for key in d:
            if not isinstance(d[key], QoSQueue):
                return False
        return True

    def _are_dict(self, d):
        for key in d:
            if not isinstance(d[key], dict):
                return False
        return True

    def _into_qosqueue_dict(self, descriptor):

        qd = dict()
        for key in descriptor:
            d = descriptor[key]
            min_rate = None
            max_rate = None
            slice_id = None
            uuid = None
            if "min_rate" in d:
                min_rate = d["min_rate"]
            if "max_rate" in d:
                max_rate = d["max_rate"]
            if "slice_id" in d:
                slice_id = d["slice_id"]
            if "uuid" in d:
                uuid = d["uuid"]
            qd[key] = QoSQueue(min_rate, max_rate, slice_id, uuid)
        return qd

    def get__min_rate(self):
        return self.min_rate

    def get__max_rate(self):
        return self.max_rate

    def get__ingress_burst(self):
        return self.ingress_burst

    def get__ingress_rate(self):
        return self.ingress_rate

    def get__bridge_name(self):
        return self.bridge_name

    def get__bridge_vm(self):
        return self.bridge_vm

    def get__if_name(self):
        return self.if_name

    def get__uuid(self):
        return self.uuid

    def get__queues(self):
        return self.queues

    def to_dict(self):
        return dict(
            min_rate=self.min_rate,
            max_rate=self.max_rate,
            uuid=self.uuid,

            ingress_burst=self.ingress_burst,
            ingress_rate=self.ingress_rate,
            bridge_vm=self.bridge_vm,
            bridge_name=self.bridge_name,
            if_name=self.if_name,

            queues=self._queues_to_dict()
        )

    def _queues_to_dict(self):
        d = dict()
        queues = self.get__queues()
        for key in queues:
            queue = queues[key]
            assert isinstance(queue, QoSQueue)
            d[key] = queue.to_dict()
        return d


    def add__queue(self, queue, index=-1, update=False):
        if not isinstance(queue, QoSQueue):
            if isinstance(queue, dict):
                min_rate = 0
                if "min_rate" in queue:
                    min_rate = queue["min_rate"]
                max_rate = 0
                if "max_rate" in queue:
                    max_rate = queue["max_rate"]
                uuid = None
                if "uuid" in queue:
                    uuid = queue["uuid"]
                slice_id = None
                if "slice_id" in queue:
                    slice_id = queue["slice_id"]
                queue = QoSQueue(min_rate, max_rate, slice_id, uuid)
            else:
                return False
        assert isinstance(queue, QoSQueue)

        if not isinstance(index, int):
            if isinstance(index, str):
                index = int(index)
            else:
                return False
        assert isinstance(index, int)

        queues = self.get__queues()
        if index < 0:
            index = self._first_free_queue_index()
            queues[str(index)] = queue
        else:
            if str(index) in queues:
                if update:
                    queues[str(index)] = queue
                else:
                    return False
            else:
                queues[str(index)] = queue
        return True

    def _first_free_queue_index(self):
        keys = list(self.get__queues().keys())
        keys.sort()
        length = len(keys)
        if int(keys[length-1]) < length:
            return length
        else:
            previous = 0
            for key in keys:
                current = int(key)
                if current - previous > 1:
                    return previous + 1
                else:
                    previous = current
            return previous + 1

    def remove_queue(self, index):
        queues = self.get__queues()
        if str(index) in queues:
            return queues.pop(str(index), None)
        return None

    def remove_queue_by_slice_id(self, slice_id):
        index = self._get_queue_index_by_slice_id(slice_id)
        if index is not None:
            return self.remove_queue(index)
        else:
            return None

    def _get_queue_index_by_slice_id(self, slice_id):
        queues = self.get__queues()
        for key in queues:
            queue = queues[key]
            assert isinstance(queue, QoSQueue)
            if str(queue.get__slice_id()) == str(slice_id):
                return str(key)
        return None

    def update_uid_by_slice_id(self, uuid, slice_id):
        index = self._get_queue_index_by_slice_id(slice_id)
        if index is not None:
            queue = self.get__queues()[index]
            assert isinstance(queue, QoSQueue)
            queue.set__uuid(uuid)
            return True
        return False


    def generate_ovs__set_qos(self):
        if self.get__if_name() == self.DEFAULT__IF_NAME:
            return ""
        osm = OSM()
        return osm.set_port_qos(self.get__if_name(),
                                self.get__max_rate(),
                                self.get__min_rate(),
                                self.get__queues())

    def generate_ovs__update_qos(self, params):
        assert isinstance(params, dict)
        if "ingress_burst" in params:
            self.set__ingress_burst(params["ingress_burst"])
        if "ingress_rate" in params:
            self.set__ingress_rate(params["ingress_rate"])
        if "min_rate" in params:
            self.set__min_rate(params["min_rate"])
        if "max_rate" in params:
            self.set__max_rate(params["max_rate"])
        if "queues" in params:
            pass


if __name__ == '__main__':

    level = logging.getLevelName("DEBUG")
    logging.basicConfig(filename='ovsbridgeman.log',
                        filemode='w',
                        level=level)
    console = logging.StreamHandler()
    LOG_FORMAT = ('%(levelname) -8s %(lineno) -5d: %(message)s')
    # LOG_FORMAT ='%(asctime)-15s %(name)-40s: %(levelname)-8s %(message)s'
    console.setLevel(level)
    formatter = logging.Formatter(LOG_FORMAT)

    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    LOG = logging.getLogger(__name__)

    queues = {
        "1": dict(
            uuid="zxcvbnm",
            slice_id=1111
        ),
        "2": dict(
            uuid="asdfghjkl",
            slice_id=2222
        ),
        "3": dict(
            max_rate=1000000,
            slice_id=3333
        ),
        "4": dict(
            max_rate=2000000,
            slice_id=4444
        )
    }

    qos = IfQoS()
    qos.set__queues(queues)
    LOG.info(qos.to_dict())
    LOG.info(qos.add__queue(dict(
            max_rate=5000000,
            slice_id="XXXX"
        )))
    LOG.info(qos.to_dict())
    rsp = qos.update_uid_by_slice_id("franto", "XXXX")
    LOG.info(rsp)
    LOG.info(qos.to_dict())
    rsp = qos.remove_queue(5)
    if rsp is not None:
        assert isinstance(rsp, QoSQueue)
        rsp = rsp.to_dict()
    LOG.info(rsp)

