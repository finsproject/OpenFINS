# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback


class OVSBridgeManager:

    OVS_VSCTL = "ovs-vsctl"
    SET = 'set'
    BRIDGE = 'bridge'
    ADD_BR = 'add-br'
    SUDO = 'sudo'
    DATAPATH_TYPE = 'datapath_type'
    DATAPATH_TYPE__NETDEV = 'netdev'
    DATAPATH_TYPE__USERSPACE = DATAPATH_TYPE__NETDEV
    PROTOCOLS = 'protocols'
    PROTOCOL_OPENFLOW10 = 'OpenFlow10'
    PROTOCOL_OPENFLOW11 = 'OpenFlow11'
    PROTOCOL_OPENFLOW12 = 'OpenFlow12'
    PROTOCOL_OPENFLOW13 = 'OpenFlow13'
    PROTOCOL_OPENFLOW14 = 'OpenFlow14'
    PROTOCOL_OPENFLOW15 = 'OpenFlow15'
    PROTOCOLS_DEFAULT_LIST = [PROTOCOL_OPENFLOW10,
                              PROTOCOL_OPENFLOW11,
                              PROTOCOL_OPENFLOW12,
                              PROTOCOL_OPENFLOW13]
    SET_CONTROLLER = 'set-controller'
    DEL_CONTROLLER = 'del-controller'
    IFCONFIG = 'ifconfig'
    IFCONFIG_UP = 'up'
    IFCONFIG_DOWN = 'down'

    ADD_PORT = 'add-port'
    TYPE = 'TYPE'
    PORT_TYPE__GRE = 'gre'
    PORT_TYPE__INTERNAL = 'internal'
    PORT_TYPE__TAP = 'tap'
    INTERFACE = 'interface'
    OPTIONS_REMOTEIP = 'options:remote_ip'

    def __init__(self, bridge_name):
        self._name = bridge_name
        self.LOG = logging.getLogger(__name__)

    def get_name(self):
        return self._name

    def validate_bridge_name(self, name):
        try:
            if name is not None:
                if not isinstance(name, str):
                    self.LOG.warning("Invalid name type (not str): %s",
                                    str(type(name)))
                    return False
            else:
                self.LOG.warning("name is None")
                return False

            return True

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return False

    def validate_ifconfig_value(self, value):
        values = [self.IFCONFIG_UP, self.IFCONFIG_DOWN]
        for v in values:
            if v == value:
                return True
        return False

    def validate_protocol(self, protocol):
        try:
            protocols = [self.PROTOCOL_OPENFLOW10,
                         self.PROTOCOL_OPENFLOW11,
                         self.PROTOCOL_OPENFLOW12,
                         self.PROTOCOL_OPENFLOW13,
                         self.PROTOCOL_OPENFLOW14,
                         self.PROTOCOL_OPENFLOW15]

            if not isinstance(protocol, str):
                self.LOG.warning("Invalid protocol type (not str): %s",
                                 str(type(protocol)))
                return False

            if protocol not in protocols:
                self.LOG.warning("Invalid protocol: %s \n Valid values: %s",
                                 str(type(protocol)),
                                 protocols)
                return False

            return True

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return False

    def _assemble_cmd(self, components):
        try:
            if not isinstance(components, list):
                raise Exception("Invalid components (not list): " +
                        str(type(components)))
            cmd = " ".join(map(str, components))

            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_create_bridge(self, bridge_name=None, sudo=True):
        try:
            br_name = self._name
            # self.LOG.debug(br_name)
            if bridge_name is not None:
                # self.LOG.debug(bridge_name)
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.OVS_VSCTL,
                                      self.ADD_BR,
                                      br_name])

            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_ifconfig_bridge(self, value=IFCONFIG_UP,
                            bridge_name=None, sudo=True):
        try:
            br_name = self._name
            if bridge_name is not None:
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            if not self.validate_ifconfig_value(value):
                raise Exception("Invalid ifconfig value : " +
                                str(value))

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.IFCONFIG,
                                      br_name,
                                      value])
            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def _cmd_set_bridge(self, bridge_name=None, sudo=True):
        try:

            br_name = self._name
            if bridge_name is not None:
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.OVS_VSCTL,
                                      self.SET,
                                      self.BRIDGE,
                                      br_name])
            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_set_datapath(self, datapath_type=DATAPATH_TYPE__USERSPACE,
                         bridge_name=None, sudo=True):

        try:

            set_cmd = self._cmd_set_bridge(bridge_name, sudo)
            if set_cmd == "":
                raise Exception("Failure during bridge set config")

            if not isinstance(datapath_type, str):
                raise Exception("Invalid datapath_type type (not str): " +
                                str(type(datapath_type)))
            dpt_cmd = self.DATAPATH_TYPE + "=" + datapath_type

            cmd = self._assemble_cmd([set_cmd, dpt_cmd])

            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_set_protocols(self, protocols=PROTOCOLS_DEFAULT_LIST,
                         bridge_name=None, sudo=True):

        try:

            set_cmd = self._cmd_set_bridge(bridge_name, sudo)
            if set_cmd == "":
                raise Exception("Failure during bridge set config")

            if not isinstance(protocols, list):
                raise Exception("Invalid protocols type (not list): " +
                                str(type(protocols)))
            for protocol in protocols:
                if not self.validate_protocol(protocol):
                    raise Exception("Invalid protocol: " + str(type(protocol)))

            prc_cmd = self.PROTOCOLS + "=" + ",".join(map(str, protocols))

            cmd = self._assemble_cmd([set_cmd, prc_cmd])

            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_set_controller(self, protocol, ip, port=6653,
                           bridge_name=None, sudo=True):

        try:

            br_name = self._name
            if bridge_name is not None:
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            ctr_cmd = protocol + ":" + ip + ":" + str(port)

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.OVS_VSCTL,
                                      self.SET_CONTROLLER,
                                      br_name,
                                      ctr_cmd])
            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def cmd_delete_controller(self, bridge_name=None, sudo=True):

        try:

            br_name = self._name
            if bridge_name is not None:
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.OVS_VSCTL,
                                      self.DEL_CONTROLLER,
                                      br_name])
            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def _cmd_add_port_p1(self, port_name, bridge_name=None, sudo=True):
        try:

            br_name = self._name
            if bridge_name is not None:
                if not self.validate_bridge_name(bridge_name):
                    raise Exception("Invalid bridge name (not str): " +
                                    str(type(bridge_name)))
                br_name = bridge_name

            sudo_cmd = ""
            if sudo:
                sudo_cmd = self.SUDO

            if not isinstance(port_name, str):
                raise Exception("Invalid port name (not str): " +
                                str(type(port_name)))

            cmd = self._assemble_cmd([sudo_cmd,
                                      self.OVS_VSCTL,
                                      self.ADD_PORT,
                                      br_name,
                                      port_name])
            return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""

    def _cmd_add_port_p2(self, if_name, if_type, remoteip=None):
        try:
            if not isinstance(if_name, str):
                raise Exception("Invalid port name type (not str): " +
                                str(type(if_name)))

            if not isinstance(if_type, str):
                raise Exception("Invalid port_type type (not str): " +
                                str(type(if_type)))

            if remoteip is not None:
                if not isinstance(remoteip, str):
                    raise Exception("Invalid remote_ip type (not str): " +
                                    str(type(remoteip)))

            if if_type == self.PORT_TYPE__INTERNAL:

                type_cmd = self.TYPE + "=" + if_type

                cmd = self._assemble_cmd([self.SET,
                                          self.INTERFACE,
                                          if_name,
                                          type_cmd])
                return cmd

            elif if_type == self.PORT_TYPE__GRE:

                type_cmd = self.TYPE + "=" + if_type

                if remoteip is None:
                    raise Exception("Invalid remote_ip : " +
                                    str(remoteip))

                rem_cmd = self.OPTIONS_REMOTEIP + "=" + remoteip
                cmd = self._assemble_cmd([self.SET,
                                          self.INTERFACE,
                                          if_name,
                                          type_cmd,
                                          rem_cmd])
                return cmd

        except Exception as e:
            self.LOG.error("ERROR: %s", str(e))
            traceback.print_exc()
            return ""


    def cmd_add_port_internal(self, bridge_name=None, sudo=True):
        "sudo ovs-vsctl add-port fbr_TE int1 -- set interface int1 type=internal"



if __name__ == '__main__':

    level = logging.getLevelName("DEBUG")
    logging.basicConfig(filename='ovsbridgeman.log',
                        filemode='w',
                        level=level)
    console = logging.StreamHandler()
    LOG_FORMAT = ('%(levelname) -8s %(lineno) -5d: %(message)s')
    # LOG_FORMAT ='%(asctime)-15s %(name)-40s: %(levelname)-8s %(message)s'
    console.setLevel(level)
    formatter = logging.Formatter(LOG_FORMAT)

    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    LOG = logging.getLogger(__name__)

    ovs_bm = OVSBridgeManager("fbr_TE")
    LOG.info(ovs_bm._name)
    LOG.info(ovs_bm.cmd_create_bridge())
    LOG.info(ovs_bm.cmd_ifconfig_bridge())
    LOG.info(ovs_bm.cmd_set_datapath())
    LOG.info(ovs_bm.cmd_set_protocols())
    LOG.info(ovs_bm.cmd_set_controller('tcp', '127.0.0.1', 6653))
    LOG.info(ovs_bm.cmd_delete_controller())