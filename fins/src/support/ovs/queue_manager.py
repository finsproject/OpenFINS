# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback
import json
import copy

from fins.src.support.bash.utils import BashUtils


class ValueValidator:

    VTYPE__STRING = 'string'
    VTYPE__INT = 'int'
    VTYPE__BOOL = 'bool'
    VTYPE__DICT = 'dict'
    VTYPE__LIST = 'list'
    VTYPE__NONE = 'none'
    VTYPE__INT2STRING = 'int2string'
    VTYPE__INT2STRING_GENERIC = 'int2string.generic'
    VTYPE__BOOL2STRING = 'bool2string'
    VTYPE__BOOL2STRING_GENERIC = 'bool2string.generic'
    VTYPE__SELFVALIDATINGENTITY = 'selfvalidatingentity'


    DEFAULT_RESPONSE = False

    def __init__(self):
        self.LOG = logging.getLogger(__name__)

    def merge_vtype(self, vtype_list):
        assert isinstance(vtype_list, list)
        return "+".join(vtype_list)

    def validate(self, value, vtype):
        assert isinstance(vtype, str)
        rsp = self.DEFAULT_RESPONSE
        if vtype.rfind("+") >= 0:
            pos = vtype.rfind("+")
            vtype_first = vtype[:pos]
            vtype_last = vtype[pos+1:]
            rsp = self.validate(value, vtype_last) | \
                  self.validate(value, vtype_first)
        elif vtype == self.VTYPE__STRING:
            rsp = self.validate__string(value)

        elif vtype == self.VTYPE__INT:
            rsp = self.validate__int(value)

        elif vtype == self.VTYPE__BOOL:
            rsp = self.validate__bool(value)

        elif vtype == self.VTYPE__DICT:
            rsp = self.validate__dict(value)

        elif vtype == self.VTYPE__LIST:
            rsp = self.validate__list(value)

        elif vtype == self.VTYPE__NONE:
            rsp = self.validate__none(value)

        elif vtype == self.VTYPE__INT2STRING:
            rsp = self.validate__int2string(value)

        elif vtype == self.VTYPE__INT2STRING_GENERIC:
            rsp = self.validate__int2string(value) | \
                  self.validate__int(value)

        elif vtype == self.VTYPE__SELFVALIDATINGENTITY:
            rsp = self.validate__int2string(value)

        else:
            self.LOG.warning('Unknown type: %s', vtype)

        return rsp

    def validate__string(self, value):
        try:
            if not isinstance(value, str):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__int(self, value):
        try:
            if not isinstance(value, int):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__bool(self, value):
        try:
            if not isinstance(value, bool):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__dict(self, value):
        try:
            if not isinstance(value, dict):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__list(self, value):
        try:
            if not isinstance(value, list):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__none(self, value):
        try:
            if value is not None:
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__int2string(self, value):
        try:
            if not self.validate__string(value):
                return False
            int(value)
            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__int2string_generic(self, value):
        try:
            return self.validate__int2string(value) | \
                   self.validate__int(value)

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__bool2string(self, value):
        try:
            if not self.validate__string(value):
                return False
            bool(value)
            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__bool2string_generic(self, value):
        try:
            return self.validate__bool2string(value) | \
                   self.validate__bool(value)

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__selfvalidatingentity(self, value):
        try:
            if not isinstance(value, SelfValidatingEntity):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False


class KeyDescriptor:

    KEY = 0
    MANDATORY = 1
    TYPE = 2
    DEFAULT = 3
    FORCEDEFAULT = 4

    KEYS = [KEY, MANDATORY, TYPE, DEFAULT, FORCEDEFAULT]

    def __init__(self, key_descriptor):

        self.LOG = logging.getLogger(__name__)

        assert isinstance(key_descriptor, list)
        assert len(key_descriptor) == len(self.KEYS)

        self._key = None
        self._mandatory = None
        self._type = None
        self._default = None
        self._forcedefault = None

        self._validated = False

        assert self._validate(key_descriptor)

    def _validate(self, key_descriptor):
        try:
            assert isinstance(key_descriptor[self.KEY], str)
            assert isinstance(key_descriptor[self.MANDATORY], bool)
            assert isinstance(key_descriptor[self.TYPE], str)
            # TODO: check is a valid type

            self._key = key_descriptor[self.KEY]
            self._mandatory = key_descriptor[self.MANDATORY]
            self._type = key_descriptor[self.TYPE]
            self._default = key_descriptor[self.DEFAULT]
            self._forcedefault = key_descriptor[self.FORCEDEFAULT]

            self._validated = True

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s\n%s', str(e), str(key_descriptor))
            traceback.print_exc()

            self._validated = False

            return False

    def is_validated(self):
        return self._validated

    def k(self):
        return self._key

    def m(self):
        return self._mandatory

    def t(self):
        return self._type

    def d(self):
        return self._default

    def fd(self):
        return self._forcedefault

    def print(self, pretty=True):
        rsp = dict()
        rsp['k'] = self._key
        rsp['m'] = self._mandatory
        rsp['t'] = self._type
        rsp['d'] = self._default
        if pretty:
            return json.dumps(rsp, sort_keys=True, indent=3)
        return json.dumps(rsp)


class SelfValidatingEntity:

    def __init__(self, descriptor=None, extra_keys=None, value_validator=None):
        self._validated = False
        self._descriptor = descriptor
        self._keys = list()
        if value_validator is None:
            self.vv = ValueValidator()
        else:
            self.vv = value_validator
        self.LOG = logging.getLogger(__name__)
        keys = list()
        if extra_keys is not None:
            assert isinstance(keys, list)
            keys = keys + extra_keys

        for kd in keys:
            k = KeyDescriptor(kd)
            assert k.is_validated()
            self._keys.append(k)
        if descriptor is not None:
            assert self.validate()

    # def __new__(cls, *args, **kwargs):
    #     # newclass = super(SelfValidatingEntity, cls).__new__(cls)
    #     for key in cls._descriptor.keys():
    #         setattr(cls, "get_" + key,
    #                 classmethod(cls._create_get_function(key)))
    #
    # def _create_get_function(self, key):
    #     def _function():
    #         self._get_key_value(key)
    #     return _function

    def _get_key(self, key):
        try:
            v = self._descriptor[key]
            return v
        except Exception as e:
            raise

    def set_descriptor(self, descriptor, overwrite=True):
        try:
            assert descriptor is not None
            assert isinstance(descriptor, dict)

            if self._descriptor is not None:
                if overwrite:
                    self.LOG.warning("Overwriting previus descriptor")
                    self._descriptor = descriptor
                else:
                    raise Exception("Cannot overwrite previous descriptor")
            else:
                self._descriptor = descriptor

            assert self.validate()

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate(self, descriptor=None):
        try:
            if descriptor is None:
                if self._descriptor is not None:
                    descriptor = self._descriptor
                else:
                    raise Exception('No instance descriptor assigned')

            for kd in self._keys:
                assert isinstance(kd, KeyDescriptor)
                # self.LOG.debug(kd.k())
                if kd.k() not in descriptor:
                    if kd.m():
                        if kd.fd():
                            descriptor[kd.k()] = copy.copy(kd.d())
                        else:
                            raise Exception("Missing mandatory key: '" +
                                            str(kd.k()) + "'")
                if not self.vv.validate(descriptor[kd.k()], kd.t()):
                    raise Exception("Value for key '" + str(kd.k()) + "' " +
                                    "is not valid " + kd.t() + " [=" +
                                    str(descriptor[kd.k()]) + "]")

            self._validated = True

            self._descriptor = descriptor

            return self._validated

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            self._validated = False

            return self._validated

    def update_value(self, key, value, validate=True):
        try:
            vtype = None

            for kd in self._keys:
                if kd.k() == key:
                    vtype = kd.t()

            if vtype is None:
                raise Exception("Invalid key: " + key)

            if validate:
                if not self.vv.validate(value,vtype):
                    raise Exception("Invalid value for key '" + key + "' [=" +
                                    str(value) + "]")

            self._descriptor[key] = value

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def as_json(self, pretty=True):
        d = dict()
        # for key in self._descriptor:
        #     if self.vv.validate__selfvalidatingentity(self._descriptor[key]):
        #         d[key] = self._descriptor[key].as_dict()
        #     else:
        #         self.LOG.debug(str(type(self._descriptor[key])))
        #         d[key] = self._descriptor[key]
        d = self.as_dict(self._descriptor)

        if pretty:
            return json.dumps(d, sort_keys=True, indent=3)
        return json.dumps(d)

    # def as_dict2(self):
    #     d = dict()
    #     for key in self._descriptor:
    #         if self.vv.validate__selfvalidatingentity(self._descriptor[key]):
    #             d[key] = self._descriptor[key].as_dict()
    #         else:
    #             d[key] = self._descriptor[key]
    #     return d

    def as_dict(self, value):
        ret = None
        if self.vv.validate__selfvalidatingentity(value):
            ret = dict()
            descriptor = value._descriptor
            for k in descriptor:
                ret[k] = self.as_dict(descriptor[k])
        elif self.vv.validate__list(value):
            ret = list()
            for element in value:
                ret.append(self.as_dict(element))
        elif self.vv.validate__dict(value):
            ret = dict()
            for key in value:
                # self.LOG.debug(key)
                ret[key] = self.as_dict(value[key])
        else:
            ret = value

        return ret




class OvsValueValidator(ValueValidator):

    KEYS_SET__BRIDGE = "bridge"
    KEYS_SET__PORT = "port"
    KEYS_SET__QOS = "qos"
    KEYS_SET__QUEUE = "queue"
    KEYS_SET__METER = "meter"
    KEYS_SET__METER_BAND = "meter_band"

    VTYPE__OVS_BRIDGE = 'ovs_bridge'
    VTYPE__OVS_PORT = 'ovs_port'
    VTYPE__OVS_QOS = 'ovs_qos'
    VTYPE__OVS_QUEUE = 'ovs_queue'
    VTYPE__OVS_METER = 'ovs_meter'
    VTYPE__OVS_METER_BAND = 'ovs_meter_band'
    VTYPE__OVS_METER_UNIT = 'ovs_meter_unit'
    VTYPE__OVS_METER_BAND_TYPE = 'ovs_meter_band_type'

    METER_UNIT__KBPS = "kbps"
    METER_UNIT__PKTBS = "pktps"
    METER_UNITS = [METER_UNIT__KBPS, METER_UNIT__PKTBS]

    METER_BAND_TYPE__DROP = "drop"
    METER_BAND_TYPES = [METER_BAND_TYPE__DROP]

    def __init__(self):
        super(OvsValueValidator, self).__init__()

    def get_keys_set(self, keyset):
        if keyset == self.KEYS_SET__BRIDGE:
            return [
                [
                    OvsBridge.KEY__ID,
                    True,
                    self.VTYPE__STRING,
                    OvsBridge.DEFAULT__ID,
                    False
                ],
                [
                    OvsBridge.KEY__UUID,
                    True,
                    self.merge_vtype([
                        self.VTYPE__STRING,
                        self.VTYPE__NONE]),
                    OvsBridge.DEFAULT__UUID,
                    True
                ],
                [
                    OvsBridge.KEY__PORTS,
                    True,
                    self.VTYPE__DICT,
                    OvsBridge.DEFAULT__PORTS,
                    False
                ],
            ]
        elif keyset == self.KEYS_SET__PORT:
            return [
                [
                    OvsPort.KEY__ID,
                    True,
                    self.VTYPE__STRING,
                    OvsPort.DEFAULT__ID,
                    False
                ],
                [
                    OvsPort.KEY__OFPORT,
                    True,
                    self.VTYPE__INT,
                    OvsPort.DEFAULT__OFPORT,
                    False
                ],
                [
                    OvsPort.KEY__QOS,
                    True,
                    self.merge_vtype([
                        self.VTYPE__DICT,
                        self.VTYPE__NONE]),
                    OvsPort.DEFAULT__QOS,
                    True
                ]
            ]
        elif keyset == self.KEYS_SET__QOS:
            return [
                [
                    OvsQoS.KEY__NAME,
                    True,
                    self.merge_vtype([
                        self.VTYPE__STRING,
                        self.VTYPE__NONE]),
                    OvsQoS.DEFAULT__NAME,
                    True
                ],
                [
                    OvsQoS.KEY__UUID,
                    True,
                    self.merge_vtype([
                        self.VTYPE__STRING,
                        self.VTYPE__NONE]),
                    OvsQoS.DEFAULT__UUID,
                    True
                ],
                [
                    OvsQoS.KEY__TYPE,
                    True,
                    self.VTYPE__STRING,
                    OvsQoS.DEFAULT__TYPE,
                    True
                ],
                [
                    OvsQoS.KEY__MIN_RATE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT2STRING_GENERIC,
                        self.VTYPE__NONE]),
                    OvsQoS.DEFAULT__MIN_RATE,
                    True
                ],
                [
                    OvsQoS.KEY__MAX_RATE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT2STRING_GENERIC,
                        self.VTYPE__NONE]),
                    OvsQoS.DEFAULT__MAX_RATE,
                    True
                ],
                [
                    OvsQoS.KEY__QUEUES,
                    True,
                    self.VTYPE__DICT,
                    OvsQoS.DEFAULT__QUEUES,
                    True
                ]
            ]
        elif keyset == self.KEYS_SET__QUEUE:
            return [
                [
                    OvsQueue.KEY__NAME,
                    True,
                    self.merge_vtype([
                        self.VTYPE__STRING,
                        self.VTYPE__NONE]),
                    OvsQueue.DEFAULT__NAME,
                    True
                ],
                [
                    OvsQueue.KEY__UUID,
                    True,
                    self.merge_vtype([
                        self.VTYPE__STRING,
                        self.VTYPE__NONE]),
                    OvsQueue.DEFAULT__UUID,
                    True
                ],
                [
                    OvsQueue.KEY__MIN_RATE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT2STRING_GENERIC,
                        self.VTYPE__NONE]),
                    OvsQueue.DEFAULT__MIN_RATE,
                    True
                ],
                [
                    OvsQueue.KEY__MAX_RATE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT2STRING_GENERIC,
                        self.VTYPE__NONE]),
                    OvsQueue.DEFAULT__MAX_RATE,
                    True
                ]
            ]
        elif keyset == self.KEYS_SET__METER:
            return [
                [
                    OvsMeter.KEY__ID,
                    True,
                    self.VTYPE__INT,
                    OvsMeter.DEFAULT__ID,
                    False
                ],
                [
                    OvsMeter.KEY__UNIT,
                    True,
                    self.VTYPE__OVS_METER_UNIT,
                    OvsMeter.DEFAULT__UNIT,
                    True
                ],
                [
                    OvsMeter.KEY__BURST,
                    True,
                    self.VTYPE__BOOL,
                    OvsMeter.DEFAULT__BURST,
                    True
                ],
                [
                    OvsMeter.KEY__STATS,
                    True,
                    self.VTYPE__BOOL,
                    OvsMeter.DEFAULT__STATS,
                    True
                ],
                [
                    OvsMeter.KEY__BANDS,
                    True,
                    self.VTYPE__LIST,
                    OvsMeter.DEFAULT__BANDS,
                    True
                ],
            ]
        elif keyset == self.KEYS_SET__METER_BAND:
            return [
                [
                    OvsMeterBand.KEY__TYPE,
                    True,
                    self.VTYPE__OVS_METER_BAND_TYPE,
                    OvsMeterBand.DEFAULT__TYPE,
                    False
                ],
                [
                    OvsMeterBand.KEY__RATE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT,
                        self.VTYPE__NONE]),
                    OvsMeterBand.DEFAULT__RATE,
                    True
                ],
                [
                    OvsMeterBand.KEY__BURST_SIZE,
                    True,
                    self.merge_vtype([
                        self.VTYPE__INT,
                        self.VTYPE__NONE]),
                    OvsMeterBand.DEFAULT__BURST_SIZE,
                    True
                ]
            ]

    def validate(self, value, vtype):
        rsp = super(OvsValueValidator, self).validate(value, vtype)

        if vtype == self.VTYPE__OVS_BRIDGE:
            rsp = self.validate__bridge(value)
        elif vtype == self.VTYPE__OVS_PORT:
            rsp = self.validate__port(value)
        elif vtype == self.VTYPE__OVS_QOS:
            rsp = self.validate__qos(value)
        elif vtype == self.VTYPE__OVS_QUEUE:
            rsp = self.validate__queue(value)
        elif vtype == self.VTYPE__OVS_METER:
            rsp = self.validate__meter(value)
        elif vtype == self.VTYPE__OVS_METER_BAND:
            rsp = self.validate__meter(value)
        elif vtype == self.VTYPE__SELFVALIDATINGENTITY:
            rsp = super(OvsValueValidator, self).validate(value, vtype) |\
                  self.validate__queue(value) |\
                  self.validate__port(value) |\
                  self.validate__qos(value) |\
                  self.validate__queue(value) |\
                  self.validate__meter(value) |\
                  self.validate__meter_band(value)
        elif vtype == self.VTYPE__OVS_METER_UNIT:
            if self.validate__string(value):
                if value in self.METER_UNITS:
                    rsp = True
                else:
                    rsp = False
            else:
                rsp = False
        elif vtype == self.VTYPE__OVS_METER_BAND_TYPE:
            if self.validate__string(value):
                if value in self.METER_BAND_TYPES:
                    rsp = True
                else:
                    rsp = False
            else:
                rsp = False
        else:
            rsp = rsp

        return rsp

    def validate__bridge(self, value):
        try:
            if not isinstance(value, OvsBridge):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__port(self, value):
        try:
            if not isinstance(value, OvsPort):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__qos(self, value):
        try:
            if not isinstance(value, OvsQoS):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__queue(self, value):
        try:
            if not isinstance(value, OvsQueue):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__meter(self, value):
        try:
            if not isinstance(value, OvsMeter):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def validate__meter_band(self, value):
        try:
            if not isinstance(value, OvsMeterBand):
                return False

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False


class OvsBridge(SelfValidatingEntity):
    KEY__PORTS = 'ports'
    KEY__ID = 'id'
    KEY__UUID = 'uuid'

    DEFAULT__PORTS = []
    DEFAULT__ID = None
    DEFAULT__UUID = None

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__BRIDGE)
        super(OvsBridge, self).__init__(descriptor, EXTRA_KEYS, vv)


class OvsPort(SelfValidatingEntity):
    KEY__ID = 'id'
    KEY__OFPORT = 'ofport'
    KEY__QOS = 'qos'

    DEFAULT__ID = None
    DEFAULT__OFPORT = None
    DEFAULT__QOS = None

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__PORT)
        super(OvsPort, self).__init__(descriptor, EXTRA_KEYS, vv)

    def add_qos(self, qos):
        try:
            if isinstance(qos, dict):
                qos = OvsQoS(qos)
            elif not isinstance(qos, OvsQoS):
                raise Exception("Invalid queue type: " + str(type(qos)))

            self._descriptor[self.KEY__QOS] = qos

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def code__new_qos(self, qos, sudo=True):
        try:
            # self.LOG.debug(self._get_key(self.KEY__QUEUES))
            # self.LOG.debug("add_queue: %s, idx= %s", str(queue), str(index))
            if isinstance(qos, dict):
                qos = OvsQoS(qos)
            elif not isinstance(qos, OvsQoS):
                raise Exception("Invalid queue type: " + str(type(qos)))

            return qos.code__new_qos(self._get_key(self.KEY__ID), sudo)

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return None


class OvsQoS(SelfValidatingEntity):
    KEY__NAME = "name"
    KEY__UUID = "uuid"
    KEY__TYPE = "type"
    KEY__MIN_RATE = "min_rate"
    KEY__MAX_RATE = "max_rate"
    KEY__QUEUES = "queues"

    DEFAULT__NAME = None
    DEFAULT__UUID = None
    DEFAULT__TYPE = "linux-htb"
    DEFAULT__MIN_RATE = None
    DEFAULT__MAX_RATE = None
    DEFAULT__QUEUES = dict()

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__QOS)
        super(OvsQoS, self).__init__(descriptor, EXTRA_KEYS, vv)
        queues = descriptor[self.KEY__QUEUES]
        self._descriptor[self.KEY__QUEUES] = copy.copy(self.DEFAULT__QUEUES)
        # self.LOG.debug("test1: %s", str(self.DEFAULT__QUEUES))
        # self.LOG.debug("test2: %s", str(self._get_key(self.KEY__QUEUES)))
        assert isinstance(queues, dict)
        for index in queues:
            assert self.add_queue(queues[index], int(index))

    def add_queue(self, queue, index=-1):
        try:
            # self.LOG.debug(self._get_key(self.KEY__QUEUES))
            # self.LOG.debug("add_queue: %s, idx= %s", str(queue), str(index))
            if isinstance(queue, dict):
                queue = OvsQueue(queue)
            elif not isinstance(queue, OvsQueue):
                raise Exception("Invalid queue type: " + str(type(queue)))

            if not self.vv.validate(index, self.vv.VTYPE__INT):
                raise Exception("Invalid index type: " + str(type(index)))

            queues = self._get_key(self.KEY__QUEUES)
            assert isinstance(queues, dict)
            if index == -1:
                keys = sorted([int(i) for i in queues])
                for k in range(len(keys)):
                    if k != keys[k]:
                        return self.add_queue(queue, k)
                return self.add_queue(queue, len(keys))
            else:
                if str(index) in queues.keys():
                    raise Exception("Index already assigned: " + str(index))
                else:
                    queues[str(index)] = queue

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def code__new_qos(self, sudo=True):
        code_sudo =""
        if sudo:
            code_sudo = "sudo "
        # code_port = "ovs-vsctl set port %s " % (str(port))
        # code_qos_name = "QoS=@fins_qos --"
        # code_qos_def = "--id=@fins_qos create QoS type=%s" \
        #                % (self._get_key(self.KEY__TYPE))
        code_qos_def = "ovs-vsctl create QoS type=%s" \
                       % (self._get_key(self.KEY__TYPE))

        code_qos_minrate = ""
        if self._get_key(self.KEY__MIN_RATE) is not None:
            code_qos_minrate = "other-config:min-rate=%s" % \
                               (str(self._get_key(self.KEY__MIN_RATE)))

        code_qos_maxrate = ""
        if self._get_key(self.KEY__MAX_RATE) is not None:
            code_qos_maxrate = "other-config:max-rate=%s" % \
                               (str(self._get_key(self.KEY__MAX_RATE)))

        code_queues_list = []
        code_queues_def = []
        queues = self._get_key(self.KEY__QUEUES)
        for index in queues:
            queue = queues[index]
            assert isinstance(queue, OvsQueue)
            if queue._get_key(queue.KEY__UUID) is None:
                queue_name = "q" + str(index)
                queue.update_value(queue.KEY__NAME, queue_name)
                s = str(index) + "=@" + queue_name
                code_queues_list.append(s)
                s = queue.code__new_queue_for_qos()
                code_queues_def.append(s)
            else:
                queue_uuid = queue._get_key(queue.KEY__UUID)
                s = str(index) + "=" + queue_uuid
                code_queues_list.append(s)
        code_queues = "queues=" + ",".join(code_queues_list) + " " + \
                      " ".join(code_queues_def)

        # return " ".join([code_sudo, code_port, code_qos_name, code_qos_def,
        #                  code_qos_minrate, code_qos_maxrate, code_queues]).\
        #        replace("  ", " ")

        return " ".join([code_sudo, code_qos_def, code_qos_minrate,
                         code_qos_maxrate, code_queues]).replace("  ", " ")


class OvsQueue(SelfValidatingEntity):
    KEY__NAME = "name"
    KEY__UUID = "uuid"
    KEY__MIN_RATE = "min_rate"
    KEY__MAX_RATE = "max_rate"

    DEFAULT__NAME = None
    DEFAULT__UUID = None
    DEFAULT__MIN_RATE = None
    DEFAULT__MAX_RATE = None

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__QUEUE)
        super(OvsQueue, self).__init__(descriptor, EXTRA_KEYS, vv)

    def code__new_queue(self, sudo=True):
        code_sudo = ""
        if sudo:
            code_sudo = "sudo "
        # code = "-- --id@%s create Queue" % (self._get_key(self.KEY__NAME))
        code = "ovs-vsctl create Queue"

        min_rate_code = ""
        if self._get_key(self.KEY__MIN_RATE) is not None:
            min_rate_code = "other-config:min-rate=%s" % \
                            (str(self._get_key(self.KEY__MIN_RATE)))

        max_rate_code = ""
        if self._get_key(self.KEY__MAX_RATE) is not None:
            max_rate_code = "other-config:max-rate=%s" % \
                            (str(self._get_key(self.KEY__MAX_RATE)))

        return " ".join([code_sudo, code, min_rate_code, max_rate_code]).\
               replace("  ", " ")

    def code__new_queue_for_qos(self, sudo=True):
        code = "-- --id@%s create Queue" % (self._get_key(self.KEY__NAME))

        min_rate_code = ""
        if self._get_key(self.KEY__MIN_RATE) is not None:
            min_rate_code = " other-config:min-rate=%s" % \
                            (str(self._get_key(self.KEY__MIN_RATE)))

        max_rate_code = ""
        if self._get_key(self.KEY__MAX_RATE) is not None:
            max_rate_code = " other-config:max-rate=%s" % \
                            (str(self._get_key(self.KEY__MAX_RATE)))

        return " ".join([code, min_rate_code, max_rate_code]).\
               replace("  ", " ")


class OvsMeter(SelfValidatingEntity):
    KEY__ID = "id"
    KEY__UNIT = "unit"
    KEY__BURST = "burst"
    KEY__STATS = "stats"
    KEY__BANDS = "bands"

    DEFAULT__ID = None
    DEFAULT__UNIT = OvsValueValidator.METER_UNIT__KBPS
    DEFAULT__BURST = False
    DEFAULT__STATS = False
    DEFAULT__BANDS = list()

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__METER)
        super(OvsMeter, self).__init__(descriptor, EXTRA_KEYS, vv)
        bands = descriptor[self.KEY__BANDS]
        self._descriptor[self.KEY__BANDS] = copy.copy(self.DEFAULT__BANDS)
        # self.LOG.debug("test1: %s", str(self.DEFAULT__QUEUES))
        # self.LOG.debug("test2: %s", str(self._get_key(self.KEY__QUEUES)))
        assert isinstance(bands, list)
        for band in bands:
            assert self.add_band(band)

    def code__new_meter_for_bridge(self, bridge, sudo=True):
        code_compo = list()
        if sudo:
            code_sudo = "sudo"
            code_compo.append(code_sudo)
        code_ofctl = "ovs-ofctl -O OpenFlow13 add-meter"
        code_compo.append(code_ofctl)
        code_compo.append(bridge)
        code_meter_compo = list()
        code_meter = "meter=%s" % (self._get_key(self.KEY__ID))
        code_meter_compo.append(code_meter)
        code_meter_compo.append(self._get_key(self.KEY__UNIT))
        if self._get_key(self.KEY__BURST):
            code_meter_compo.append("burst")
        if self._get_key(self.KEY__STATS):
            code_meter_compo.append("stats")
        code_band = "band="
        for band in self._get_key(self.KEY__BANDS):
            assert isinstance(band, OvsMeterBand)
            code_band += band.code__new_meter_band()
        code_meter_compo.append(code_band)
        code_meter = ",".join(code_meter_compo)
        code_compo.append(code_meter)
        return " ".join(code_compo)

    def add_band(self, band):
        try:
            if isinstance(band, dict):
                band = OvsMeterBand(band)
            elif not isinstance(band, OvsMeterBand):
                raise Exception("Invalid queue type: " + str(type(band)))

            bands = self._get_key(self.KEY__BANDS)
            bands.append(band)

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False


class OvsMeterBand(SelfValidatingEntity):
    KEY__TYPE = "type"
    KEY__RATE = "rate"
    KEY__BURST_SIZE = "burst_size"

    DEFAULT__TYPE = OvsValueValidator.METER_BAND_TYPE__DROP
    DEFAULT__RATE = None
    DEFAULT__BURST_SIZE = None

    def __init__(self, descriptor=None):
        vv = OvsValueValidator()
        EXTRA_KEYS = vv.get_keys_set(vv.KEYS_SET__METER_BAND)
        super(OvsMeterBand, self).__init__(descriptor, EXTRA_KEYS, vv)

    def code__new_meter_band(self):
        code_compo = list()
        code_type = "type=%s" % (self._get_key(self.KEY__TYPE))
        code_compo.append(code_type)
        if self._get_key(self.KEY__RATE) is not None:
            code_rate = "rate=%s" % (self._get_key(self.KEY__RATE))
            code_compo.append(code_rate)
        if self._get_key(self.KEY__BURST_SIZE) is not None:
            code_bs = "burst_size=%s" % (self._get_key(self.KEY__BURST_SIZE))
            code_compo.append(code_bs)
        return ",".join(code_compo)


class SDNManager:

    def __init__(self):
        self.LOG = logging.getLogger(__name__)
        self.qos_list = []
        self.queue_list = []

    def instantiate_qos(self, qos, ipaddress):
        try:
            if isinstance(qos, dict):
                qos = OvsQoS(qos)
            elif isinstance(qos, OvsQoS):
                if qos._get_key(qos.KEY__UUID) is not None:
                    raise Exception("QoS already instantiated: " +
                                    str(qos._get_key(qos.KEY__UUID)))
            else:
                raise Exception("Invalid QoS type: " + str(type(qos)))

            cmd = qos.code__new_qos()
            self.LOG.debug(cmd)
            rsp = self.get_creation_response(cmd, ip)
            self.LOG.debug(rsp)


            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def instantiate_queue(self, queue, ipaddress):
        try:
            if isinstance(queue, dict):
                queue = OvsQueue(queue)
            elif isinstance(queue, OvsQueue):
                if queue._get_key(queue.KEY__UUID) is not None:
                    raise Exception("Queue already instantiated: " +
                                    str(queue._get_key(queue.KEY__UUID)))
            else:
                raise Exception("Invalid Queue type: " + str(type(queue)))

            cmd = queue.code__new_queue()
            self.LOG.debug(cmd)
            rsp = self.get_creation_response(cmd, ipaddress)
            self.LOG.debug(rsp)
            if rsp is not None:
                queue.update_value(queue.KEY__UUID, rsp[0])

            return True

        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return False

    def get_creation_response(self, command, ipaddress):
        try:
            rsp = BashUtils.ssh_command(command, ipaddress)
            self.LOG.info("rsp " + str(rsp))
            std_err = rsp[2]
            if len(std_err) > 0:
                raise Exception("something wrong happened with command")

            return rsp[1]


        except Exception as e:
            self.LOG.error('ERROR: %s', str(e))
            traceback.print_exc()

            return None














# class Bridge:
#
#     KEY_PORTS = 'ports'





# class Port:
#
#     def __init__(self):
#         pass
#
#
# class QoS:
#
#     def __init__(self):
#         pass
#
#
# class Queue:
#
#     def __init__(self):
#         pass
#
# class QueueManager:
#
#     def __init__(self):
#
#         bridge = dict(
#
#             ports=dict()
#         )
#         port = dict(
#             qos=dict()
#         )
#
#         qos = dict(
#             uuid=None,
#             type="linux-htb",
#             minrate=None,
#             maxrate=None,
#             queues=dict()
#         )
#         queue = dict(
#             uuid=None,
#             minrate=None,
#             maxrate=None
#         )

if __name__ == '__main__':

    level = logging.getLevelName("DEBUG")
    logging.basicConfig(filename='ovsbridgeman.log',
                        filemode='w',
                        level=level)
    console = logging.StreamHandler()
    LOG_FORMAT = ('%(levelname) -8s %(lineno) -5d: %(message)s')
    # LOG_FORMAT ='%(asctime)-15s %(name)-40s: %(levelname)-8s %(message)s'
    console.setLevel(level)
    formatter = logging.Formatter(LOG_FORMAT)

    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    LOG = logging.getLogger(__name__)

    desc = [
        ['name', True, ValueValidator.VTYPE__STRING, 'ciccio', True],
        ['surname', True, ValueValidator.VTYPE__INT2STRING_GENERIC, 1, True]
    ]
    # for d in desc:
    #     kd = KeyDescriptor(d)
    #     LOG.info(kd.print())

    # oe = OvsBridge(dict(), desc)
    #
    # LOG.info(oe.print())

    # ob = OvsBridge(dict(id="a", ports=dict()))
    #
    # LOG.info(ob.as_json())
    # LOG.info(ob.as_dict())

    oport = OvsPort(dict(
        id="gre0",
        ofport=1,
    ))

    oqos = OvsQoS(dict(
        min_rate=2000,
        #max_rate=10000,
    ))

    oqueue0 = OvsQueue(dict(
      min_rate=1000
    ))

    oqueue1 = OvsQueue(dict(
        max_rate=5000
    ))

    oqueue2 = OvsQueue(dict(
        min_rate=500,
        max_rate=4000
    ))

    ometer = OvsMeter(dict(
        id=100
    ))

    omband = OvsMeterBand(dict(
        type='drop',
        rate=30000
    ))

    oqos.add_queue(oqueue0, -1)
    oqos.add_queue(oqueue1, -1)
    # oqos.add_queue(oqueue2, -1)

    # oqueue2.update_value(OvsQueue.KEY__UUID,"asdalol")

    # LOG.info(oqos.as_json())
    LOG.info(oqos._descriptor)
    LOG.info("OvsQoS oqos: %s", oqos.as_json())

    oqos2 = OvsQoS(oqos._descriptor)
    LOG.info("OvsQoS oqos2: %s", oqos2.as_json())

    LOG.info("OvsPort oport: %s", oport.as_json())
    oport.add_qos(oqos2)
    LOG.info("OvsPort oport: %s", oport.as_json())

    LOG.debug("add QoS cmd: %s", oqos2.code__new_qos())
    LOG.debug("add Queue cmd: %s", oqueue2.code__new_queue())

    LOG.info("OvsMeter ometer: %s", ometer.as_json())

    ometer.add_band(omband)
    LOG.info("OvsMeter ometer: %s", ometer.as_json())
    LOG.debug(ometer.code__new_meter_for_bridge('fbr_myTE'))

    ip = "192.168.5.129"






    # oqos.update_value(OvsQoS.KEY__UUID, "1")
    #
    # LOG.info(oqos.as_json())
    #
    # oqos.update_value(OvsQoS.KEY__UUID, "wertyu34567")
    #
    # LOG.info(oqos.as_json())

    sm = SDNManager()
    sm.instantiate_queue(oqueue0, ip)
    sm.instantiate_queue(oqueue1, ip)
    sm.instantiate_qos(oqos2, ip)
