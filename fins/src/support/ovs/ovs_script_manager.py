# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging


class OvsScriptManager:

    BRIDGE_DEFAULT__PROTOCOLS = [
        "OpenFlow10",
        "OpenFlow11",
        "OpenFlow12",
        "OpenFlow13"]

    def __init__(self):
        self.LOG = logging.getLogger(__name__)
        pass

    def echo(self, text):
        cmd = "echo \"%s\"" % text

        return cmd

    # BRIDGEs

    def add_bridge(self, bridge_name, sudo=True):
        cmd = "ovs-vsctl add-br %s " % bridge_name

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def del_bridge(self, bridge_name, sudo=True):
        cmd = "ovs-vsctl del-br %s " % bridge_name

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_status__bridge(self, bridge_name, up=True, sudo=True):

        status = "down"
        if up:
            status = "up"
        cmd = "ifconfig %s %s" % (bridge_name, status)

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_protocols__bridge(self, bridge_name,
                              protocols=BRIDGE_DEFAULT__PROTOCOLS, sudo=True):
        if not isinstance(protocols, list):
            protocols = list()
        if len(protocols) == 0:
            protocols = ""
        else:
            protocols = "protocols=%s" % ",".join(protocols)

        cmd = "ovs-vsctl set bridge %s %s" \
              % (bridge_name, protocols)

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    # INTERFACEs & PORTs

    def add_port__internal_interface(self, bridge_name, if_name, sudo=True):

        cmd = "ovs-vsctl add-port %s %s -- set interface %s type=internal" \
                 % (bridge_name, if_name, if_name)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def create_gre_interface(self, if_name, remote_ip, local_ip, sudo=True):

        cmd = "ip link add %s type gretap remote %s local %s" \
                % (if_name, remote_ip, local_ip)

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def add_port__gre_interface(self, bridge_name, if_name, sudo=True):

        cmd = "ovs-vsctl add-port %s %s" % (bridge_name, if_name)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def delete_gre_interface(self, if_name, sudo=True):
        cmd = "ip link delete %s" % if_name

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def create_tap_interface(self, if_name, sudo=True):
        cmd = "ip tuntap add mode tap %s" % if_name

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def add_port__tap_interface(self, bridge_name, if_name, sudo=True):

        cmd = "ovs-vsctl add-port %s %s" % (bridge_name, if_name)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def delete_tap_interface(self, if_name, sudo=True):
        cmd = "ip link delete %s" % if_name

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def assign_ofport__interface(self, interface_name, ofport, sudo=True):

        ofport = str(ofport)

        cmd = "ovs-vsctl set Interface %s ofport_request=%s" \
              % (interface_name, str(ofport))
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_status__interface(self, interface_name,
                              ip_address=None, netmask=None,
                              up=True, sudo=True):

        status = "down"
        if up:
            status = "up"
        address_section = ""
        if ip_address is not None:
            ip = ip_address
            nm = ""
            if netmask is not None:
                nm = "netmask %s" % netmask
            else:
                nm = "netmask 255.255.255.0"
            address_section = "%s %s " %(ip, nm)
        cmd = "sudo ifconfig %s %s%s" % (interface_name,
                                          address_section,
                                          status)

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def delete_port(self, port_name, bridge_name=None, sudo=True):
        if bridge_name is None:
            bridge_name = ""
        # cmd = "ovs-vsctl --if-exists --with-iface del-port %s %s" \
        cmd = "ovs-vsctl --with-iface del-port %s %s" \
              % (bridge_name, port_name)

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    # QoS and Queue


    def add_internal_interface(self,bridge_name, interface_name, sudo=True):

        cmd = "ovs-vsctl add-port %s %s -- set interface %s type=internal" \
                 %(bridge_name, interface_name, interface_name)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def assign_ofport_to_interface(self,interface_name, ofport, sudo=True):

        ofport = str(ofport)

        cmd = "ovs-vsctl set Interface %s ofport_request=%s" \
              % (interface_name, ofport)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_interface_up(self, interface_name, interface_ip,
                         netmask="255.255.255.0", up=True, sudo=True):

        status = "down"
        if up:
            status = "up"

        cmd = "ifconfig %s %s netmask %s %s" % (interface_name, interface_ip, netmask,
                                        status)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_port_qos(self, port_name, max_rate=0, min_rate=0, queues=dict(),
                     sudo=True):

        cmd = "ovs-vsctl set port %s qos=@qos0 -- " \
              "--id=@qos0 %s" \
              % (port_name, self._create_qos(max_rate, min_rate, queues))
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def _create_qos(self, max_rate=0, min_rate=0, queues=dict()):

        cmd = "create qos type=linux-htb " \
              "other-config:min-rate=%s " \
              "other-config:max-rate=%s %s" \
              % (str(min_rate), str(max_rate),
                 self._generate_qos_queue_list(queues))

        return cmd

    def _generate_qos_queue_list(self, queues):
        assert isinstance(queues, dict)
        if len(queues) == 0:
            return ""

        q_list = list()
        qdesc_list = list()
        for index in queues:
            queue = queues[index]
            assert isinstance(queue, dict)
            if "uuid" in queue:
                # direct reference to existing queue
                q_list.append(index+"="+queue["uuid"])
            else:
                # placeholder for new queue definition
                q_list.append(index + "=" + "@q" + index)
                min_rate = 0
                if "min_rate" in queue:
                    min_rate = queue["min_rate"]
                max_rate = 0
                if "max_rate" in queue:
                    max_rate = queue["max_rate"]
                qdesc_list.append("-- --id=@q" + index + " " +
                                  self.create_queue(max_rate, min_rate))

        queue_seq = "queues="+",".join(q_list)
        queue_desc_seq = " ".join(qdesc_list)
        cmd = "queues=%s %s" % (queue_seq, queue_desc_seq)

        return cmd

    def create_queue(self, max_rate=0, min_rate=0):
        cmd = "create queue " \
              "other-config:min-rate=%s " \
              "other-config:max-rate=%s" \
              % (str(min_rate), str(max_rate))

        return cmd


    def destroy_qos(self, qos_uuid, sudo=True):
        cmd = "ovs-vsctl destroy qos %s " % (qos_uuid)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def destroy_port_qos(self, port_name, sudo=True):
        cmd = "ovs-vsctl -- destroy QoS %s -- clear Port %s qos"\
              % (port_name, port_name)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def update_port_qos(self, port_name, old_qos_uuid, max_rate=0, min_rate=0,
                        sudo=True):
        cmd = list()
        cmd.append(self.set_port_qos(port_name, max_rate, min_rate, sudo))
        cmd.append(self.destroy_qos(old_qos_uuid, sudo))

        return " && ".join(cmd)


    def set_ingress_policing_rate(self, interface_name, rate_kb, sudo=True):

        cmd = "ovs-vsctl set interface %s ingress_policing_rate=%s"\
              % (interface_name, str(rate_kb))
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_ingress_policing_burst(self, interface_name, burst_kb, sudo=True):

        cmd = "ovs-vsctl set interface %s ingress_policing_burst=%s" \
              % (interface_name, str(burst_kb))
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    # Flows


    def set_flow_head(self, bridge_name, priority, in_port, vlan_vid, out_port,
                      cookie, queue_index=None, explicit_oprotocol=None,
                      sudo=True):

        out_enqueue = "output:%s" % str(out_port)
        if queue_index is not None:
            out_enqueue = "enqueue:%s:%s" % (str(out_port), str(queue_index))
        oprotocol = ""
        if explicit_oprotocol is not None:
            oprotocol = '-O %s' % explicit_oprotocol

        cmd = "ovs-ofctl %s add-flow %s priority=%s,in_port=%s," \
              "cookie=%s,actions=mod_vlan_vid:%s,%s" \
              % (oprotocol, bridge_name, str(priority), str(in_port),
                 str(cookie), str(vlan_vid), out_enqueue)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_flow_tail(self, bridge_name, priority, in_port, vlan_vid, out_port,
                      cookie, queue_index=None, explicit_oprotocol=None,
                      sudo=True):

        out_enqueue = "output:%s" % str(out_port)
        if queue_index is not None:
            out_enqueue = "enqueue:%s:%s" % (str(out_port), str(queue_index))
        oprotocol = ""
        if explicit_oprotocol is not None:
            oprotocol = '-O %s' % explicit_oprotocol

        cmd = "ovs-ofctl %s add-flow %s priority=%s,in_port=%s," \
              "dl_vlan=%s,cookie=%s,actions=strip_vlan,%s" \
              % (oprotocol, bridge_name, str(priority), str(in_port),
                 str(vlan_vid), str(cookie), out_enqueue)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_flow_mid(self, bridge_name, priority, in_port, vlan_vid, out_port,
                     cookie, queue_index=None, explicit_oprotocol=None,
                     sudo=True):

        out_enqueue = "output:%s" % str(out_port)
        if queue_index is not None:
            out_enqueue = "enqueue:%s:%s" % (str(out_port), str(queue_index))
        oprotocol = ""
        if explicit_oprotocol is not None:
            oprotocol = '-O %s' % explicit_oprotocol

        cmd = "ovs-ofctl %s add-flow %s priority=%s,in_port=%s," \
              "dl_vlan=%s,cookie=%s,actions=%s" \
              % (oprotocol, bridge_name, str(priority), str(in_port),
                 str(vlan_vid), str(cookie), out_enqueue)
        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def destroy_flow_by_cookie(self, bridge_name, cookie,
                               explicit_oprotocol=None, sudo=True):

        oprotocol = ""
        if explicit_oprotocol is not None:
            oprotocol = '-O %s' % explicit_oprotocol

        cmd = "ovs-ofctl %s del-flows %s \"cookie=%s/-1\"" \
              % (oprotocol, bridge_name, str(cookie))

        if sudo:
            cmd = "sudo " + cmd

        return cmd

    def set_slice_head(self, bridge_name, in_ifname, in_ifip, in_netmask,
                       priority, in_port, vlan_vid, out_port, qos_minrate,
                       qos_maxrate, cookie, sudo=True):
        cmd = list()
        cmd.append(self.add_internal_interface(bridge_name, in_ifname, sudo))
        cmd.append(self.assign_ofport_to_interface(in_ifname, in_port, sudo))
        cmd.append(self.set_interface_up(in_ifname, in_ifip, in_netmask, True))
        cmd.append(self.set_port_qos(in_ifname, qos_maxrate, qos_minrate, sudo))

        cmd.append(self.set_flow_head(bridge_name, priority, in_port,
                                      vlan_vid, out_port, cookie, sudo))

        return " \n".join(cmd)

    def set_slice_tail(self, bridge_name, out_ifname, out_ifip, out_netmask,
                       priority, out_port, vlan_vid, in_port, cookie,
                       sudo=True):
        cmd = list()
        cmd.append(self.add_internal_interface(bridge_name, out_ifname, sudo))
        cmd.append(self.assign_ofport_to_interface(out_ifname, out_port, sudo))
        cmd.append(self.set_interface_up(out_ifname, out_ifip, out_netmask,
                                         True))
        #cmd.append(self.set_qos(out_ifname, qos_maxrate, qos_minrate, sudo))

        cmd.append(self.set_flow_tail(bridge_name, priority, in_port,
                                      vlan_vid, out_port, cookie, sudo))

        return " \n".join(cmd)

    def set_slice_mid(self, bridge_name, priority, in_port, out_port, vlan_vid,
                      cookie, sudo=True):
        cmd = list()

        cmd.append(self.set_flow_mid(bridge_name, priority, in_port,
                                     vlan_vid, out_port, cookie, sudo))

        return " \n".join(cmd)

    def destroy_slice_mid(self, bridge_name, cookie, sudo=True):

        return self.destroy_flow_by_cookie(bridge_name, cookie, sudo)

    def gencode_from_descriptor(self, fins_bridge_descriptor):
        code = dict()
        for resource_id in fins_bridge_descriptor:
            brd = fins_bridge_descriptor[resource_id]

            code[resource_id] = self.gencode_for_br(brd, True)

        return code


    def gencode_for_br(self, br_descriptor, commented=False):
        cmd_build = list()
        cmd_destroy = list()
        bridge_name = br_descriptor['id']
        local_ip = br_descriptor['ip']
        fins_res = br_descriptor['associated_resource']
        jfed_res = br_descriptor['associated_resource_jfed']
        # create bridge
        if commented:
            cmd_build.append(self.echo("Creating bridge %s at %s-%s(%s)" \
                                 % (bridge_name, fins_res, jfed_res, local_ip)))
        cmd_build.append(self.add_bridge(bridge_name))
        cmd_build.append(self.set_status__bridge(bridge_name))
        cmd_build.append(self.set_protocols__bridge(bridge_name))

        cmd_destroy.append(self.del_bridge(bridge_name))

        interfaces = br_descriptor['interfaces']

        # create gre ifs
        gre_ifs = interfaces['gre']
        for gre in gre_ifs:
            if_name = gre['id']
            remote_ip = gre['remote_ip']
            of_port = gre['of_port']

            if commented:
                cmd_build.append(self.echo(
                    "Creating gre interface %s (@%s -> %s) and assigning it to "
                    "bridge %s with of_port %s" \
                    % (if_name, local_ip, remote_ip, bridge_name, of_port)
                ))
            cmd_build.append(self.create_gre_interface(
                if_name, remote_ip, local_ip
            ))
            cmd_build.append(self.add_port__gre_interface(bridge_name,if_name))
            cmd_build.append(self.assign_ofport_to_interface(if_name,of_port))
            cmd_build.append(self.set_status__interface(if_name))

            cmd_destroy.append(self.delete_gre_interface(if_name))

        # create tap ifs
        tap_ifs = interfaces['tap']
        for tap in tap_ifs:
            if_name = tap['id']
            of_port = tap['of_port']

            if commented:
                cmd_build.append(self.echo(
                    "Creating tap interface %s and assigning it to "
                    "bridge %s with of_port %s" \
                    % (if_name, bridge_name, of_port)
                ))
            cmd_build.append(self.create_tap_interface(if_name))
            cmd_build.append(self.add_port__tap_interface(bridge_name, if_name))
            cmd_build.append(self.assign_ofport_to_interface(if_name, of_port))
            cmd_build.append(self.set_status__interface(if_name))

            cmd_destroy.append(self.delete_tap_interface(if_name))

        # create internal ifs
        int_ifs = interfaces['int']
        for internal in int_ifs:
            if_name = internal['id']
            of_port = internal['of_port']
            ip_address = None
            if 'ip' in internal:
                ip_address = internal['ip']
            netmask = None
            if 'netmask' in internal:
                netmask = internal['netmask']

            if commented:
                cmd_build.append(self.echo(
                    "Creating internal interface %s (ip %s, netmask %s) "
                    "and assigning it to bridge %s with of_port %s" \
                    % (if_name, ip_address, netmask, bridge_name, of_port)
                ))
                # if creation is embedded in "add_port" function
            cmd_build.append(self.add_port__internal_interface(bridge_name,
                                                               if_name))
            cmd_build.append(self.assign_ofport_to_interface(if_name, of_port))
            cmd_build.append(self.set_status__interface(if_name, ip_address,
                                                        netmask))

        cmd_build.append(self.set_status__bridge(bridge_name))
        cmd_destroy.reverse()

        return dict(
            build=cmd_build,
            destroy=cmd_destroy
        )

        # TODO: assign QoS to interfaces create new qos (get uuid)

        # sudo ovs-vsctl add-br br1
        #
        # sudo ifconfig br1 up
        #
        # sudo ovs-vsctl set bridge br1 protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13
        #
        # sudo ip link add gre_12 type gretap remote 192.168.5.222 local 192.168.5.147
        # sudo ovs-vsctl add-port br1 gre_12
        # sudo ovs-vsctl set Interface gre_12 ofport_request=1
        # sudo ifconfig gre_12 up
        #
        # sudo ovs-vsctl add-port br1 int0 -- set interface int0 type=internal
        # sudo ovs-vsctl set Interface int0 ofport_request=2
        # sudo ifconfig int0 10.0.1.1 up
        #
        # sudo ifconfig br1 up


if __name__ == '__main__':

    level = logging.getLevelName("DEBUG")
    logging.basicConfig(filename='ovsbridgeman.log',
                        filemode='w',
                        level=level)
    console = logging.StreamHandler()
    LOG_FORMAT = ('%(levelname) -8s %(lineno) -5d: %(message)s')
    # LOG_FORMAT ='%(asctime)-15s %(name)-40s: %(levelname)-8s %(message)s'
    console.setLevel(level)
    formatter = logging.Formatter(LOG_FORMAT)

    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    LOG = logging.getLogger(__name__)

    osm = OvsScriptManager()

    # bridge_name = "br0"
    # in_ifname = "int1"
    # in_ifip = "10.0.1.1"
    # in_netmask = "255.255.255.0"
    # priority = 100
    # in_port = 2
    # vlan_vid = 100
    # out_port = 1
    # qos_minrate = 0
    # qos_maxrate = 1000000
    # cookie = 666
    #
    # LOG.info("\n" +
    #          om.set_slice_head(
    #              bridge_name, in_ifname, in_ifip, in_netmask, priority,
    #              in_port, vlan_vid, out_port, qos_minrate, qos_maxrate,
    #              cookie)
    #          )
    #
    # bridge_name = "br1"
    # in_port = 1
    # out_port = 2
    #
    # LOG.info("\n" +
    #          om.set_slice_mid(
    #              bridge_name, priority, in_port, out_port, vlan_vid, cookie)
    #          )
    #
    # bridge_name = "br2"
    # out_ifname = "int2"
    # out_ifip = "10.0.1.2"
    # out_netmask = in_netmask
    # out_port = 2
    # in_port = 1
    # LOG.info("\n" +
    #          om.set_slice_tail(
    #              bridge_name, out_ifname, out_ifip, out_netmask, priority,
    #              out_port, vlan_vid, in_port, cookie)
    #          )


    queues = {
        "1": dict(
            uuid="zxcvbnm"
        ),
        "2": dict(
            uuid="asdfghjkl"
        ),
        "3": dict(
            max_rate=1000000
        ),
        "4": dict(
            max_rate=2000000
        )
    }

    fins_descriptor = {
        'H1': {
            'associated_resource': 'H1',
            'associated_resource_jfed': 'H1',
            'ip': '192.168.5.114',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [{
                        'id': 'gre_SW',
                        'remote_ip': '192.168.5.121',
                        'of_port': 1,
                        'remote_br_id': 'fbr_SW',
                        'remote_br_fins': 'SW',
                        'remote_br_jfed': 'SW',
                        'remote_of_port': 1
                    }
                ],
                'tap': [],
                'int': []
            },
            'id': 'fbr_H1'
        },
        'H2': {
            'associated_resource': 'H2',
            'associated_resource_jfed': 'H2',
            'ip': '192.168.5.224',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [{
                        'id': 'gre_SW',
                        'remote_ip': '192.168.5.121',
                        'of_port': 1,
                        'remote_br_id': 'fbr_SW',
                        'remote_br_fins': 'SW',
                        'remote_br_jfed': 'SW',
                        'remote_of_port': 2
                    }
                ],
                'tap': [],
                'int': []
            },
            'id': 'fbr_H2'
        },
        'SW': {
            'associated_resource': 'SW',
            'associated_resource_jfed': 'SW',
            'ip': '192.168.5.121',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [{
                        'id': 'gre_H1',
                        'remote_ip': '192.168.5.114',
                        'of_port': 1,
                        'remote_br_id': 'fbr_H1',
                        'remote_br_fins': 'H1',
                        'remote_br_jfed': 'H1',
                        'remote_of_port': 1
                    }, {
                        'id': 'gre_H2',
                        'remote_ip': '192.168.5.224',
                        'of_port': 2,
                        'remote_br_id': 'fbr_H2',
                        'remote_br_fins': 'H2',
                        'remote_br_jfed': 'H2',
                        'remote_of_port': 1
                    }, {
                        'id': 'gre_CU_SDN',
                        'remote_ip': '192.168.5.179',
                        'of_port': 3,
                        'remote_br_id': 'fbr_CU_SDN',
                        'remote_br_fins': 'CU_SDN',
                        'remote_br_jfed': 'CU',
                        'remote_of_port': 1
                    }
                ],
                'tap': [],
                'int': []
            },
            'id': 'fbr_SW'
        },
        'CU_SDN': {
            'associated_resource': 'CU_SDN',
            'associated_resource_jfed': 'CU',
            'ip': '192.168.5.179',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [{
                        'id': 'gre_SW',
                        'remote_ip': '192.168.5.121',
                        'of_port': 1,
                        'remote_br_id': 'fbr_SW',
                        'remote_br_fins': 'SW',
                        'remote_br_jfed': 'SW',
                        'remote_of_port': 3
                    }
                ],
                'tap': [{
                        'id': 'tap1',
                        'of_port': 2
                    }, {
                        'id': 'tap2',
                        'of_port': 3
                    }
                ],
                'int': []
            },
            'id': 'fbr_CU_SDN'
        },
        'RH1_SDN': {
            'associated_resource': 'RH1_SDN',
            'associated_resource_jfed': 'RH1',
            'ip': '192.168.5.115',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [],
                'tap': [{
                        'id': 'tap0',
                        'of_port': 1
                    }
                ],
                'int': []
            },
            'id': 'fbr_RH1_SDN'
        },
        'RH2_SDN': {
            'associated_resource': 'RH2_SDN',
            'associated_resource_jfed': 'RH2',
            'ip': '192.168.5.218',
            'userspace_dp': 'False',
            'controller': None,
            'interfaces': {
                'gre': [],
                'tap': [{
                        'id': 'tap0',
                        'of_port': 1
                    }
                ],
                'int': []
            },
            'id': 'fbr_RH2_SDN'
        }
    }



    LOG.info(osm.set_port_qos("if_test", 3000000, 0, queues))
    for index in fins_descriptor:
        brd = fins_descriptor[index]
        rsp = osm.gencode_for_br(brd,True)
        LOG.info("\n BUILD\n%s\n\n DESTROY\n%s\n ",
                 rsp['build'],
                 " && ".join(rsp['destroy']))
