# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.


class RMQUtils:

    PUBLISH = 'publish'
    CONSUME = 'consume'

    def __init__(self):
        pass

    @staticmethod
    def get_fins_exchange_name(module):
        assert isinstance(module, str)

        return "exchange_" + module

    @staticmethod
    def get_fins_queue_name(source, destination):
        assert isinstance(source, str)
        assert isinstance(destination, str)

        return "queue_" + source + "_to_" + destination

