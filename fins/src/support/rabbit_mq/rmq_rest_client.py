# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import requests
import logging
import traceback
# from third_party.stand_out import StandOut
from fins.src.support.comm.response_wrapper import ResponseWrapper

LOG = logging.getLogger(__name__)


class RMQRestClient(object):

    def __init__(self, management_url):
        self._management_url = management_url

    def get_user(self, user_name=''):

        try:

            user_url = 'users/' + user_name

            response = requests.get(self.rmq_management_url() + user_url)


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return None

        return response

    def check_rest(self):
        try:

            check_url = 'users/'

            response = requests.get(self.rmq_management_url() + check_url)

            rw = ResponseWrapper(response)
        except Exception as e:
            rw = ResponseWrapper(e, ResponseWrapper.TYPE_EXCEPTION)

        return rw

    def is_rest_ready(self):
        rw = self.check_rest()
        LOG.debug(rw.read_response())
        LOG.debug(rw.read_exception())
        if rw.is_response():
            if rw.status_code() == 200:
                return True
        return False

    def get_all_users(self):
        return self.get_user()

    def delete_user(self, user_name, accept_missing=False):
        try:

            # response = requests.get(self.rmq_management_url() + user_url)
            response = self.get_user(user_name)
            if response.status_code == 404:
                if accept_missing:
                    LOG.warning('User %s is not existing, no need to delete it',
                                user_name)
                    return True

            assert response.status_code == 200

            user_url = 'users/' + user_name

            response = requests.delete(self.rmq_management_url() + user_url)
            assert response.status_code == 204


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return False

        return True

    def get_exchange(self, exchange_name=''):

        try:

            exchange_url = 'exchanges/%2f/' + exchange_name

            response = requests.get(self.rmq_management_url() + exchange_url)


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return None

        return response

    def get_all_exchanges(self):
        return self.get_exchange()

    def create_exchange(self, exchange_name, accept_existing=False):

        try:

            # response = requests.get(self.rmq_management_url() + exchange_url)
            response = self.get_exchange(exchange_name)
            if response.status_code == 200:
                if accept_existing:
                    LOG.warning('Exchange %s already existing, going to use it',
                                exchange_name)
                    return True

            assert response.status_code == 404

            exchange_url = 'exchanges/%2f/' + exchange_name

            response = requests.put(self.rmq_management_url() + exchange_url,
                                    json={})
            assert response.status_code == 201

            LOG.info('CREATED new exchange: %s', exchange_name)


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return False

        return True

    def delete_exchange(self, exchange_name, accept_missing=False):
        try:

            exchange_url = 'exchanges/%2f/' + exchange_name

            response = requests.get(self.rmq_management_url() + exchange_url)
            if response.status_code == 404:
                if accept_missing:
                    LOG.warning('Exchange %s is not existing, no need to delete'
                                ' it',
                                exchange_name)
                    return True

            assert response.status_code == 200

            response = requests.delete(self.rmq_management_url() + exchange_url)
            assert response.status_code == 204


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return False

        return True

    def create_queue(self, queue_name, accept_existing=False):

        try:

            queue_url = 'queues/%2f/' + queue_name

            response = requests.get(self.rmq_management_url() + queue_url)
            if response.status_code == 200:
                if accept_existing:
                    LOG.warning('Queue %s already existing, going to use it',
                                queue_name)
                    return True

            assert response.status_code == 404

            response = requests.put(self.rmq_management_url() + queue_url,
                                    json={})
            assert response.status_code == 201

            LOG.info('CREATED new queue: %s', queue_name)

        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return False

        return True

    def get_queue(self, queue_name=''):

        try:

            queue_url = 'queues/%2f/' + queue_name

            response = requests.get(self.rmq_management_url() + queue_url)


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return None

        return response

    def get_all_queues(self):
        return self.get_queue()

    def delete_queue(self, queue_name, accept_missing=False):

        try:

            # response = requests.get(self.rmq_management_url() + queue_url)
            response = self.get_queue(queue_name)

            if response.status_code == 404:
                if accept_missing:
                    LOG.warning('Queue %s is not existing, no need to delete '
                                'it',
                                queue_name)
                    return True

            assert response.status_code == 200

            queue_url = 'queues/%2f/' + queue_name

            response = requests.delete(self.rmq_management_url() + queue_url)
            assert response.status_code == 204


        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            traceback.print_exc()
            return False

        return True

    def bind_queue_to_exchange(self, queue_name, exchange_name):

        try:

            response = requests.post(self.rmq_management_url()
                                     + 'bindings/%2f/e/'
                                     + exchange_name
                                     + '/q/'
                                     + queue_name,
                                     json={'routing_key': queue_name,
                                           'arguments': {}})

            assert response.status_code == 201

        except Exception as e:

            LOG.error('ERROR: %s', str(e))
            return False

        return True

    def rmq_management_url(self):
        return self._management_url
