# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import time
from fins.src.support.rabbit_mq.rmq_rest_client import RMQRestClient
from fins.src.support.rabbit_mq.rmq_endpoint import RabbitEndpoint
import requests
from importlib import import_module

LOG = logging.getLogger(__name__)


class RMQManager:
    RMQ_DATA_IS_OK = 'rmq_data_is_ok'
    RMQ_GET_DATA = 'rmq_get_data_is_ok'

    _RMQ_DATA_STRUCT = \
        dict(
            host_ip=str,
            port=int,
            config_port=int,
            username=str,
            password=str,
            log_level=str
        )

    def __init__(self, rmq_data, initialize=False):

        self.validate(rmq_data, self.RMQ_DATA_IS_OK)

        #####
        # LOG
        #

        level = logging.getLevelName(rmq_data['log_level'])
        logging.getLogger("pika").setLevel(level)

        #####
        # RMQ DATA
        #

        LOG.info("Setting RMQ data...")
        self._set_data(rmq_data)

        LOG.info("Instantiating RMQ Rest Client...")
        self._rrc = RMQRestClient(self.rmq_management_url())

        if initialize:
            LOG.info("RabbitMQ environment initialization: starting...")
            self.initialize_rmq()
            LOG.info("RabbitMQ environment initialization: completed")

    def _set_data(self, rmq_data):

        self._data = {}

        data_attr = ['host_ip', 'port', 'config_port', 'log_level']
        for key in data_attr:
            self._data['_' + key] = rmq_data[key]

        self._data['_'+'management_url'] = 'http://' \
                                           + rmq_data['username'] \
                                           + ':' + rmq_data['password'] \
                                           + '@' + rmq_data['host_ip'] \
                                           + ':' + \
                                           rmq_data['config_port'] + '/api/'

        self._data_endpoint = dict(
            username=rmq_data['username'],
            password=rmq_data['password'],
            rabbit_ip=rmq_data['host_ip'],
            rabbit_port=rmq_data['port'],

        )

    def _get_data(self, key):

        self.validate(key, self.RMQ_GET_DATA)
        return self._data['_'+key]

    def rmq_host_ip(self):
        return self._get_data('host_ip')

    def rmq_port(self):
        return self._get_data('port')

    def rmq_config_port(self):
        return self._get_data('config_port')

    def rmq_log_level(self):
        return self._get_data('log_level')

    def rmq_management_url(self):
        return self._get_data('management_url')

    def rmq_rest_client(self):
        return self._rrc

    def initialize_rmq(self):
        retry_interval = 5
        max_attempts = 60
        attempt_number = 1
        while (not self.rmq_rest_client().check_rest()) or \
                attempt_number > max_attempts:
            LOG.warning("RabbitMQ Rest check#%d: FAILED, retry in %ds...",
                        attempt_number,
                        retry_interval)
            attempt_number += 1
            time.sleep(retry_interval)

        if attempt_number > max_attempts:
            raise Exception("RabbitMQ Rest Interface not responsive")

        LOG.warning("RabbitMQ Rest check#%d: SUCCESS, proceed with "
                    "initialization...",
                    attempt_number)

        response = self.rmq_rest_client().get_all_users()
        assert response.status_code == 200
        data = response.json()
        toremove = [rest_user for rest_user in data
                    if rest_user['name'] != 'admin'
                    and rest_user['name'] != 'guest']
        for rest_user in toremove:
            response = self.rmq_rest_client().delete_user(rest_user['name'])

        response = self.rmq_rest_client().get_all_queues()
        assert response.status_code == 200
        data = response.json()
        for queue in data:
            if queue['name'] != 'debug':
                response = self.rmq_rest_client().delete_queue(queue['name'])

        response = self.rmq_rest_client().get_all_exchanges()
        assert response.status_code == 200
        data = response.json()
        for exchange in data:
            if exchange['name'].split('.')[0] not in ['amq', '']:
                response = self.rmq_rest_client().delete_exchange(
                    exchange['name'])

    def create_endpoint(self, exchange_name, queue_desc, callback):
        return RabbitEndpoint(self._data_endpoint,
                              self.rmq_rest_client(),
                              exchange_name,
                              queue_desc,
                              callback)

    def validate(self, tbv, check):

        if check == self.RMQ_DATA_IS_OK:
            for key in self._RMQ_DATA_STRUCT:
                LOG.debug('key: %s (class: %s)', key, type(key).__name__)
                assert key in tbv
                if not isinstance(tbv[key], self._RMQ_DATA_STRUCT[key]):
                    self._RMQ_DATA_STRUCT[key](tbv[key])

        elif check == self.RMQ_GET_DATA:
            assert (tbv == 'host_ip') \
                   or (tbv == 'port') \
                   or (tbv == 'config_port') \
                   or (tbv == 'log_level') \
                   or (tbv == 'management_url')
