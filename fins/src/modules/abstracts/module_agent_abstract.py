# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from abc import ABCMeta, abstractmethod
import logging
import time
from fins.src.support.rabbit_mq.rmq_manager import RMQManager
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.switchboard import SwitchBoard
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.packets import PacketHandler, \
    HelloPacket, ACKPacket


LOG = logging.getLogger(__name__)


class ModuleAgentAbstract:
    __metaclass__ = ABCMeta

    _ID = 'id'
    _TYPE = 'type'
    _MANAGER_ID = 'rm_id'
    _INITIALISER_ID = 'ei_id'
    _REST_ID = 'rest_id'

    @abstractmethod
    def __init__(self, rabbitmq_data, agent_data):

        # MODULE data
        assert self._ID in agent_data
        assert self._TYPE in agent_data
        assert self._MANAGER_ID in agent_data
        assert self._INITIALISER_ID in agent_data

        self._module_data = dict()
        module_data_params = [
            self._ID,
            self._TYPE,
            self._MANAGER_ID, self._INITIALISER_ID,
        ]

        for key in module_data_params:
            self._module_data[key] = agent_data[key]

        key = self._REST_ID
        if key in agent_data:
            LOG.debug("REST id: %s", agent_data[key])
            self._module_data[key] = agent_data[key]
        else:
            LOG.debug("REST id is None")
            self._module_data[key] = None

        # RABBIT MQ data

        assert isinstance(rabbitmq_data, dict)
        self._rmq_manager = RMQManager(rabbitmq_data)

        self._switchboard = SwitchBoard()
        self._packet_handler = PacketHandler(self.mod_id())
        self._exchange = None

        self._init_fins_communications()

    def assign_message_processors(self, process_ei_message,
                                  process_rm_message=None,
                                  process_rest_message=None):

        assert process_ei_message is not None

        if process_rm_message is None:
            process_rm_message = process_ei_message
            LOG.warning("RM messages are processed together with EI messages")

        if process_rest_message is None:
            process_rest_message = process_ei_message
            LOG.warning("REST messages are processed together with EI messages")

        self._assign_rmq_endpoints(process_ei_message,
                                   process_rm_message,
                                   process_rest_message)

    def _init_fins_communications(self):

        self._check_if_queues_exist()

        self._register_queues()

        if self.mod_rest_id() is not None:
            self._register_rest_queues()

        self._show_switchboard()

        self._create_exchange()

    def _check_if_queues_exist(self):
        LOG.info("")
        LOG.info("Checking if EC/RM communication queues have been created")

        initialiser_id = self.mod_init_id()
        manager_id = self.mod_man_id()
        agent_id = self.mod_id()
        rest_id = self.mod_rest_id()

        rmq_queues = list()
        rmq_queues.append(
            RMQUtils.get_fins_queue_name(initialiser_id, agent_id))
        rmq_queues.append(
            RMQUtils.get_fins_queue_name(manager_id, agent_id))
        rmq_queues.append(
            RMQUtils.get_fins_queue_name(agent_id, initialiser_id))
        rmq_queues.append(
            RMQUtils.get_fins_queue_name(agent_id, manager_id))
        if rest_id is not None:
            rmq_queues.append(
                RMQUtils.get_fins_queue_name(rest_id, manager_id))
            rmq_queues.append(
                RMQUtils.get_fins_queue_name(manager_id, rest_id))

        for queue in rmq_queues:
            found = False
            LOG.debug("Check if queue %s exists", queue)
            while not found:
                response = self.mod_rmq_manager().rmq_rest_client()\
                    .get_queue(queue)
                if response.status_code == 200:
                    found = True
                    LOG.debug("-> Queue %s FOUND!", queue)
                else:
                    # TODO: manage this error
                    # raise Exception('Queue %s NOT FOUND', queue)
                    time.sleep(1)

    def _register_queues(self):

        LOG.debug("")
        LOG.debug("Registering all queues...")

        initialiser_id = self.mod_init_id()
        manager_id = self.mod_man_id()
        agent_id = self.mod_id()

        queue_name = RMQUtils.get_fins_queue_name(initialiser_id, agent_id)
        assert self._switchboard.get_queue_name(initialiser_id,
                                                RMQUtils.CONSUME) is None
        self._register_queue(initialiser_id, queue_name, RMQUtils.CONSUME)

        queue_name = RMQUtils.get_fins_queue_name(manager_id, agent_id)
        assert self._switchboard.get_queue_name(manager_id,
                                                RMQUtils.CONSUME) is None
        self._register_queue(manager_id, queue_name, RMQUtils.CONSUME)

        queue_name = RMQUtils.get_fins_queue_name(agent_id, initialiser_id)
        assert self._switchboard.get_queue_name(initialiser_id,
                                                RMQUtils.PUBLISH) is None
        self._register_queue(initialiser_id, queue_name, RMQUtils.PUBLISH)

        queue_name = RMQUtils.get_fins_queue_name(agent_id, manager_id)
        assert self._switchboard.get_queue_name(manager_id,
                                                RMQUtils.PUBLISH) is None
        self._register_queue(manager_id, queue_name, RMQUtils.PUBLISH)

    def _register_rest_queues(self):

        assert self.mod_rest_id() is not None

        LOG.debug("")
        LOG.debug("Registrering all REST queues...")

        rest_id = self.mod_rest_id()
        agent_id = self.mod_id()

        queue = RMQUtils.get_fins_queue_name(rest_id, agent_id)
        assert self._switchboard.get_queue_name(rest_id, "consume") is None
        self._register_queue(rest_id, queue, "consume")

        queue = RMQUtils.get_fins_queue_name(agent_id, rest_id)
        assert self._switchboard.get_queue_name(rest_id, "publish") is None
        self._register_queue(rest_id, queue, "publish")

    def _register_queue(self, terminal_id, queue_name, direction, purpose=None):

        return self._switchboard.add_channel(terminal_id,
                                             queue_name,
                                             direction,
                                             purpose)

    def _create_exchange(self):

        LOG.info("")
        LOG.info("Creating EC exchange...")

        rmq_rc = self.mod_rmq_manager().rmq_rest_client()

        self._exchange = RMQUtils.get_fins_exchange_name(self.mod_id())

        rmq_rc.create_exchange(self._exchange)

    def _assign_rmq_endpoints(self, process_ei_message,
                                  process_rm_message=None,
                                  process_rest_message=None):

        LOG.debug("")
        LOG.debug("Assigning a RabbitMQ endpoint for each associated module")

        assert process_ei_message is not None
        assert process_rm_message is not None
        assert process_rest_message is not None

        assert callable(process_ei_message)
        assert callable(process_rm_message)
        assert callable(process_rest_message)

        initialiser_id = self.mod_init_id()
        manager_id = self.mod_man_id()
        rest_id = self.mod_rest_id()

        ids = self._switchboard.get_terminal_id_list()

        for terminal_id in ids:
            process_message = None
            if terminal_id == rest_id:
                process_message = process_rest_message
            elif terminal_id == manager_id:
                process_message = process_rm_message
            elif terminal_id == initialiser_id:
                process_message = process_ei_message
            else:
                LOG.warning("Unknown terminal %s, no msg processor assigned",
                            terminal_id)
            if process_message is not None:
                queue_desc = self._switchboard.get_terminal_queues(terminal_id)
                ep = self.mod_rmq_manager().create_endpoint(self._exchange,
                                                            queue_desc,
                                                            process_message)

                self._switchboard.assign_rmq_endpoint(terminal_id, ep)

                ep.start()

    def _show_switchboard(self):

        LOG.info("\n\nSWITCHBOARD:\n%s", self._switchboard.to_str())

    def new_message_id(self):
        return self._packet_handler.generate_msg_id()

    def send_message_to_EI(self, pkt, is_reply=False):
        pkt.set_msg_id(self.new_message_id(), is_reply)
        pkt.set_sender(self.mod_id(), is_reply)
        if is_reply:
            self._switchboard.send_to(self.mod_init_id(), pkt.get_reply())
        else:
            self._switchboard.send_to(self.mod_init_id(), pkt.get_packet())

    def send_message_to_RM(self, pkt, is_reply=False):
        pkt.set_msg_id(self.new_message_id(), is_reply)
        pkt.set_sender(self.mod_id(), is_reply)
        if is_reply:
            self._switchboard.send_to(self.mod_man_id(), pkt.get_reply())
        else:
            self._switchboard.send_to(self.mod_man_id(), pkt.get_packet())

    def send_message_to_REST(self, pkt, is_reply=False):
        pkt.set_msg_id(self.new_message_id(), is_reply)
        pkt.set_sender(self.mod_id(), is_reply)
        if is_reply:
            self._switchboard.send_to(self.mod_rest_id(), pkt.get_reply())
        else:
            self._switchboard.send_to(self.mod_rest_id(), pkt.get_packet())

    def message_to_packet(self, message):
        return self._packet_handler.get_packet(message)

    def mod_id(self):
        return self._module_data[self._ID]

    def mod_man_id(self):
        return self._module_data[self._MANAGER_ID]

    def mod_init_id(self):
        return self._module_data[self._INITIALISER_ID]

    def mod_rest_id(self):
        return self._module_data[self._REST_ID]

    def mod_rmq_manager(self):
        return self._rmq_manager



