# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from abc import ABCMeta, abstractmethod
import logging
import time
import json
import traceback

from fins.src.support.rabbit_mq.rmq_manager import RMQManager
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.switchboard import SwitchBoard
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.packets import PacketHandler, \
    HelloPacket, ACKPacket


class ModuleManagerAbstract:

    __metaclass__ = ABCMeta

    KEY_RMQ__IP = 'host_ip'
    KEY_RMQ__PORT = 'port'
    KEY_RMQ__CONFIG_PORT = 'config_port'
    KEY_RMQ__USERNAME = 'username'
    KEY_RMQ__PASSWORD = 'password'
    KEY_RMQ__LOG_LEVEL = 'log_level'

    KEY_MANAGER__ID = 'id'
    KEY_MANAGER__TYPE = 'type'

    KEY_EI__ID = 'id'

    KEY_RA__ID = 'id'
    KEY_RA__TYPE = 'type'

    KEY_REST__ID = 'id'

    @abstractmethod
    def __init__(self, rabbitmq_data, module_data, ei_data, ras_data,
                 rest_data=None):

        self.LOG = logging.getLogger(__name__)

        # RABBIT MQ
        rmq_data_keys = [
            self.KEY_RMQ__IP,
            self.KEY_RMQ__PORT,
            self.KEY_RMQ__CONFIG_PORT,
            self.KEY_RMQ__USERNAME,
            self.KEY_RMQ__PASSWORD,
            self.KEY_RMQ__LOG_LEVEL
        ]

        rmq_data = self.assign_key_values(rabbitmq_data,
                                          rmq_data_keys)
        self.LOG.debug("\nRabbit MQ data used: \n\n%s\n",
                       self.dict_pretty_format(rmq_data))
        self._rmq_manager = RMQManager(rmq_data)

        # MODULE DATA
        module_data_keys = [
            self.KEY_MANAGER__ID,
            self.KEY_MANAGER__TYPE
        ]

        self._module_data = self.assign_key_values(module_data,
                                                   module_data_keys)
        self.LOG.debug("\nModule data stored: \n\n%s\n",
                       self.dict_pretty_format(self._module_data))

        # Experiment Initialiser DATA
        ei_data_keys = [
            self.KEY_EI__ID
        ]

        self._ei_data = self.assign_key_values(ei_data,
                                               ei_data_keys)
        self.LOG.debug("\nEI data stored: \n\n%s\n",
                       self.dict_pretty_format(self._ei_data))

        # Resource Agents DATA
        ra_data_keys = [
            self.KEY_RA__ID,
            self.KEY_RA__TYPE
        ]
        ras = dict()
        for index in ras_data:
            ra = self.assign_key_values(ras_data[index],
                                        ra_data_keys)
            ras[index] = ra

        self._ras_data = ras
        self.LOG.debug("\nRAs data stored: \n\n%s\n",
                       self.dict_pretty_format(self._ras_data))

        # REST DATA - it is NOT mandatory
        if rest_data is None:
            self.LOG.warning("No FINS REST associated")
            self._rest_data = None
        else:
            rest_data_keys = [
                self.KEY_REST__ID
            ]
            self._rest_data = self.assign_key_values(rest_data,
                                                     rest_data_keys)

            self.LOG.debug("\nREST data stored: \n\n%s\n",
                           self.dict_pretty_format(self._rest_data))

        # SWITCHBOARD
        self._switchboard = SwitchBoard()

        # PACKETHANDLER
        self._packet_handler = PacketHandler(self.mod_id())

        # EXCHANGE (local)
        self._exchange = None
        if not self._create_exchange():
            raise Exception("RM exchange creation failed")

        # Associated RAs list
        self._ras_repository = dict()

        # RA expected number
        self._ra_expected_number = len(self._ras_data.keys())
        for index in self._ras_data:
            self.insert_new_ra_in_repository(self._ras_data[index])


        inits = [self.mod_ei_id()]
        if self.mod_rest_id() is not None:
            inits.append(self.mod_rest_id())
        for ra_id in self._ras_repository:
            inits.append(ra_id)
        self._init_component_communications(inits)

    ####
    # Module Initialisation
    #

    def _init_component_communications(self, component):

        try:
            if isinstance(component, list):
                for c in component:
                    if isinstance(c, str):
                        initialised = self._init_component_communications(c)
                        if not initialised:
                            raise Exception("Communication toward component %s" 
                                            " is NOT initialised", c)
                    else:
                        raise Exception("Invalid component: %s", c)
                return True
            elif not isinstance(component, str):
                raise Exception("Invalid component type: %s",
                                str(type(component)))

            compo_id = None
            if component == self.mod_ei_id() or\
               component == self.mod_rest_id() or\
               component in self._ras_repository:
                compo_id = component

            if compo_id is None:
                raise Exception("Invalid component id: %s", compo_id)

            associated_queues = self._check_if_component_queues_exist(compo_id)

            if not associated_queues:
                raise Exception("Queue check has failed")

            if not self._register_component_queues(compo_id, associated_queues):
                raise Exception("Queue registration failed for component " +
                                compo_id)

            self._show_switchboard()

            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def _check_if_component_queues_exist(self, component_id):

        try:
            if not isinstance(component_id, str):
                raise Exception("Component id is not a string: %s",
                                str(type(component_id)))

            if component_id == self.mod_ei_id():
                pass
            elif self.mod_rest_id() is not None and \
                 component_id == self.mod_rest_id():
                    pass
            elif component_id in self.mod_ras_repository():
                pass
            else:
                raise Exception("Invalid component id: " + component_id)

            associated_queues = list()
            associated_queues.append(
                dict(
                    name=RMQUtils.get_fins_queue_name(component_id,
                                                      self.mod_id()),
                    direction=RMQUtils.CONSUME
                )
            )
            associated_queues.append(
                dict(
                    name=RMQUtils.get_fins_queue_name(self.mod_id(),
                                                      component_id),
                    direction=RMQUtils.PUBLISH
                )
            )

            for queue in associated_queues:
                found = False
                q_name = queue['name']
                self.LOG.debug("Check if queue %s exists", q_name)
                while not found:
                    response = self.mod_rmq_manager().rmq_rest_client()\
                        .get_queue(q_name)
                    if response.status_code == 200:
                        found = True
                        self.LOG.debug("-> Queue %s FOUND!", q_name)
                    else:
                        raise Exception('Queue %s NOT FOUND', queue)

            return associated_queues

        except Exception as e:
            self.LOG.error(e)
            return None

    def _register_component_queues(self, compo_id, queues):

        try:
            if not isinstance(compo_id, str):
                raise Exception("Component id is not a string: %s",
                                str(type(compo_id)))

            if compo_id == self.mod_ei_id():
                pass
            elif self.mod_rest_id() is not None and \
                    compo_id == self.mod_rest_id():
                    pass
            elif compo_id in self.mod_ras_repository():
                pass
            else:
                raise Exception("Invalid component id: %s", compo_id)

            self.LOG.debug("")
            self.LOG.debug("Registering queues for component id : " + compo_id)

            for queue in queues:
                queue_name = queue['name']
                queue_dir = queue['direction']
                is_registered = self._switchboard.get_queue_name(
                    compo_id,
                    queue_dir
                )
                if is_registered is not None:
                    raise Exception(queue_dir + " queue for component " +
                                    compo_id + " is already registered")
                self._register_queue(compo_id,
                                     queue_name,
                                     queue_dir)
                                     #RMQUtils.CONSUME)
            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def _register_queue(self, terminal_id, queue_name, direction, purpose=None):

        return self._switchboard.add_channel(terminal_id,
                                             queue_name,
                                             direction,
                                             purpose)

    def _create_exchange(self):

        try:
            self.LOG.info("")
            self.LOG.info("Creating RM %s exchange...", self.mod_id())

            rmq_rc = self.mod_rmq_manager().rmq_rest_client()

            self._exchange = RMQUtils.get_fins_exchange_name(self.mod_id())

            rmq_rc.create_exchange(self._exchange)

            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def _assign_rmq_endpoints(self, process_ei_message,
                              process_rest_message,
                              process_ra_message):
        try:
            self.LOG.debug("")
            self.LOG.debug("Assigning a RabbitMQ endpoint for each "
                           "associated module (EI, REST, RAs)")

            if process_ei_message is None:
                raise Exception("Function processing EI messages is None")
            if not callable(process_ei_message):
                raise Exception("Function processing EI messages is NOT "
                                "callable")

            if process_rest_message is None:
                raise Exception("Function processing REST messages is None")
            if not callable(process_rest_message):
                raise Exception("Function processing REST messages is NOT "
                                "callable")

            if process_ra_message is None:
                raise Exception("Function processing RA messages is None")
            if not callable(process_ra_message):
                raise Exception("Function processing RA messages is NOT "
                                "callable")

            if not self.mod_exchange_is_defined():
                raise Exception("Module exchange NOT defined ")

            ei_id = self.mod_ei_id()
            rest_id = self.mod_rest_id()
            ra_ids_list = self.mod_ra_ids()

            ids = self._switchboard.get_terminal_id_list()

            for terminal_id in ids:

                if terminal_id == ei_id:
                    process_message = process_ei_message
                elif terminal_id == rest_id:
                    process_message = process_rest_message
                elif terminal_id in ra_ids_list:
                    process_message = process_ra_message
                else:
                    raise Exception(
                        "Registered component " + terminal_id +
                        " could NOT be assigned to any msg processor")

                queue_desc = self._switchboard.get_terminal_queues(terminal_id)
                ep = self.mod_rmq_manager().create_endpoint(self._exchange,
                                                            queue_desc,
                                                            process_message)

                self._switchboard.assign_rmq_endpoint(terminal_id, ep)

                ep.start()

            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def assign_message_processors(self, process_ei_message,
                                  process_rest_message=None,
                                  process_ra_message=None):

        assert process_ei_message is not None

        if process_rest_message is None:
            process_rest_message = process_ei_message
            self.LOG.warning("REST messages are processed together "
                             "with EI messages")

        if process_ra_message is None:
            process_ra_message = process_ei_message
            self.LOG.warning("RA messages are processed together "
                             "with EI messages")

        if self._ra_expected_number is None or \
           self._ra_expected_number != len(self._ras_repository):
            self.LOG.warning("Registered RAs are less than expected, assignment"
                             " is not performed")
            return False

        self._assign_rmq_endpoints(process_ei_message,
                                   process_rest_message,
                                   process_ra_message)

        return True

    ####
    # Support methods
    #

    def mod_id(self):
        return self._module_data[self.KEY_MANAGER__ID]

    def mod_ei_id(self):
        return self._ei_data[self.KEY_EI__ID]

    def mod_rest_id(self):
        if self._rest_data is None:
            return None
        return self._rest_data[self.KEY_REST__ID]

    def mod_ra_ids(self):
        ra_ids_list = []
        for ra_id in self.mod_ras_repository():
            ra_ids_list.append(ra_id)
        return ra_ids_list

    def mod_ras_repository(self):
        return self._ras_repository

    def insert_new_ra_in_repository(self, ra):
        try:
            assert isinstance(ra, dict)
            assert 'id' in ra
            assert ra['id'] not in self._ras_repository

            self._ras_repository[ra['id']] = ra

            return True

        except Exception as e:
            self.LOG.error("Invalid RA: " + str(ra))
            traceback.print_exc()

            return False


    def mod_rmq_manager(self):
        return self._rmq_manager

    def mod_exchange_is_defined(self):
        return self._exchange is not None

    def new_message_id(self):
        return self._packet_handler.generate_msg_id()

    def send_message_to_EI(self, pkt, is_reply=False):
        self._send_message_to(pkt, self.mod_ei_id(), is_reply)

    def send_message_to_REST(self, pkt, is_reply=False):
        self._send_message_to(pkt, self.mod_rest_id(), is_reply)

    def send_message_to_RA(self, ra_id, pkt, is_reply=False):
        if ra_id is None:
            self.LOG.debug("Broadcasting message to all RAs")
            for ra_id in self.mod_ra_ids():
                self._send_message_to(pkt, ra_id, is_reply)
        elif ra_id in self.mod_ra_ids():
            self._send_message_to(pkt, ra_id, is_reply)
        else:
            raise Exception("RA with id " + ra_id + " NOT known")

    def _send_message_to(self, pkt, dest_id, is_reply=False):
        pkt.set_msg_id(self.new_message_id(), is_reply)
        pkt.set_sender(self.mod_id(), is_reply)
        if is_reply:
            self._switchboard.send_to(dest_id, pkt.get_reply())
        else:
            self._switchboard.send_to(dest_id, pkt.get_packet())

    def message_to_packet(self, message):
        return self._packet_handler.get_packet(message)

    def dict_pretty_format(self, dictionary):
        return json.dumps(dictionary, sort_keys=True, indent=3)

    def assign_key_values(self, source, keylist):
        try:
            if not isinstance(source, dict):
                raise Exception('Source is not a dict')
            if not isinstance(keylist, list):
                raise Exception('Keylist is not a list')

            result = dict()
            for key in keylist:
                if key not in source:
                    raise Exception('Key is not in source')
                result[key] = source[key]

            return result

        except Exception as e:
            self.LOG.error(e)
            return None

    def _show_switchboard(self):

        self.LOG.info("\n\nSWITCHBOARD:\n%s", self._switchboard.to_str())
