# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

from abc import ABCMeta, abstractmethod


import logging
import json
from fins.src.support.comm.switchboard import SwitchBoard
from fins.src.support.rabbit_mq.rmq_utils import RMQUtils
from fins.src.support.comm.packets import PacketHandler
from fins.src.support.rabbit_mq.rmq_manager import RMQManager


class ModuleInitialiserAbstract:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self,
                 rabbit_data,
                 ei_data,
                 rms_data,
                 ras_data,
                 rest_data):

        self.LOG = logging.getLogger(__name__)

        self.LOG.info('Experiment Initialiser Manager is initializing...')

        # MODULE data
        self.module_data = {}
        module_data_params = [
            'id',
            'module'
        ]
        self._ei_data = dict()
        for key in module_data_params:
            self._ei_data[key] = ei_data[key]

        # RABBIT MQ data

        if isinstance(rabbit_data, RMQManager):
            self._rmq_manager = rabbit_data
            level = logging.getLevelName(self._rmq_manager.rmq_log_level())
            logging.getLogger("pika").setLevel(level)
            log = logging.getLogger("pika")

        elif isinstance(rabbit_data, dict):
            level = logging.getLevelName(rabbit_data['log_level'])
            logging.getLogger("pika").setLevel(level)
            log = logging.getLogger("pika")
            self._rmq_manager = RMQManager(rabbit_data)

        else:
            raise Exception(
                'Invalid rabbit_data: not a RMQManager or a dict')

        # RMs data
        self._rms_data = rms_data

        # RAs data
        self._ras_data = ras_data

        # REST data
        self._rest_data = rest_data

        # SWITCHBOARD
        self._switchboard = SwitchBoard()

        # PACKETHANDLER
        self._packet_handler = PacketHandler(self.mod_id())

        # EXCHANGE
        self._exchange = None

        # BRIDGE DESCRIPTOR
        # self._fins_bridge_descriptor = fins_bridge_descriptor

        self._init_fins_communications()

    def _init_fins_communications(self):

        self._create_all_queues()

        self._register_ei_related_queues()

        self._create_exchange()

        self._show_switchboard()

    def _create_all_queues(self):
        self.LOG.info("")
        self.LOG.info("Creating all the queues used by FINS modules...")

        rmq_rc = self._rmq_manager.rmq_rest_client()
        ei_id = self.mod_id()

        # Queues EI -> REST
        ei_rest_counter = 0
        if self._rest_data is not None:
            rest_id = self.mod_rest_id()

            queue = RMQUtils.get_fins_queue_name(ei_id, rest_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ei to rest: %s", queue)

            queue = RMQUtils.get_fins_queue_name(rest_id, ei_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rest to ei: %s", queue)

            ei_rest_counter += 2

        # Queues EI -> RM
        ei_rm_counter = 0
        for key in self._rms_data:
            rm_id = self._rms_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(ei_id, rm_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ei to rm %s: %s",
            #                rm_id,
            #                queue)

            queue = RMQUtils.get_fins_queue_name(rm_id, ei_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rm %s to ei: %s",
            #                rm_id,
            #                queue)

            ei_rm_counter += 2

        # Queues EI -> RA
        ei_ra_counter = 0
        for key in self._ras_data:
            ra_id = self._ras_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(ei_id, ra_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ei to ra %s: %s",
            #                ra_id,
            #                queue)

            queue = RMQUtils.get_fins_queue_name(ra_id, ei_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ra %s to ei: %s",
            #                ra_id,
            #                queue)

            ei_ra_counter += 2

        # Queues RM -> RA
        rm_ra_counter = 0
        for key in self._ras_data:
            ra_id = self._ras_data[key]['id']
            rm_id = self._ras_data[key]['managed_by']

            queue = RMQUtils.get_fins_queue_name(rm_id, ra_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rm %s to ra %s: %s",
            #                rm_id,
            #                ra_id,
            #                queue)

            queue = RMQUtils.get_fins_queue_name(ra_id, rm_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ra %s to rm %s: %s",
            #                ra_id,
            #                rm_id,
            #                queue)

            rm_ra_counter += 2

        # Queues REST -> RM
        rest_rm_counter = 0
        for key in self._rms_data:
            rest_id = self.mod_rest_id()
            rm_id = self._rms_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(rest_id, rm_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rest %s to rm %s: %s",
            #                rest_id,
            #                rm_id,
            #                queue)

            queue = RMQUtils.get_fins_queue_name(rm_id, rest_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rm %s to rest %s: %s",
            #                rm_id,
            #                rest_id,
            #                queue)

            rest_rm_counter += 2

        # Queues REST -> RA
        rest_ra_counter = 0
        for key in self._ras_data:
            rest_id = self.mod_rest_id()
            ra_id = self._ras_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(rest_id, ra_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from rest %s to ra %s: %s",
            #                rest_id,
            #                ra_id,
            #                queue)

            queue = RMQUtils.get_fins_queue_name(ra_id, rest_id)
            rmq_rc.create_queue(queue)
            # self.LOG.debug("Created queue from ra %s to rest %s: %s",
            #                ra_id,
            #                rest_id,
            #                queue)

            rest_ra_counter += 2

        queue_total = ei_rest_counter + ei_rm_counter + ei_ra_counter +\
                      rm_ra_counter + rest_rm_counter + rest_ra_counter

        self.LOG.info("\n CREATED QUEUES: "
                      "\n EI <-> REST: %s"
                      "\n EI <-> RMs: %s"
                      "\n EI <-> RAs: %s"
                      "\n RM <-> RA: %s"
                      "\n RM <-> REST: %s"
                      "\n RA <-> REST: %s"
                      "\n TOTAL: %s",
                      str(ei_rest_counter),
                      str(ei_rm_counter),
                      str(ei_ra_counter),
                      str(rm_ra_counter),
                      str(rest_rm_counter),
                      str(rest_ra_counter),
                      str(queue_total))

    def _register_ei_related_queues(self):
        self.LOG.info("")
        self.LOG.info("Registering all the queues associate to EI...")

        rmq_rc = self._rmq_manager.rmq_rest_client()
        ei_id = self.mod_id()

        # Queues EI -> REST
        if self._rest_data is not None:
            rest_id = self._rest_data['id']

            queue = RMQUtils.get_fins_queue_name(ei_id, rest_id)
            assert self._switchboard.get_queue_name(rest_id,
                                                    "publish") is None
            self._register_queue(rest_id, queue, "publish")
            self.LOG.debug("Registered queue (PUBLISH) from ei to rest: %s",
                           queue)

            queue = RMQUtils.get_fins_queue_name(rest_id, ei_id)
            assert self._switchboard.get_queue_name(rest_id,
                                                    "consume") is None
            self._register_queue(rest_id, queue, "consume")
            self.LOG.debug("Registered queue (CONSUME) from rest to ei: %s",
                           queue)

        # Queues EI -> RM
        for key in self._rms_data:
            rm_id = self._rms_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(ei_id, rm_id)
            assert self._switchboard.get_queue_name(rm_id,
                                                    "publish") is None
            self._register_queue(rm_id, queue, "publish")
            self.LOG.debug(
                "Registered queue (PUBLISH) from ei to rm %s: %s",
                rm_id,
                queue)

            queue = RMQUtils.get_fins_queue_name(rm_id, ei_id)
            assert self._switchboard.get_queue_name(rm_id,
                                                    "consume") is None
            self._register_queue(rm_id, queue, "consume")
            self.LOG.debug(
                "Registered queue (CONSUME) from rm %s to ei: %s",
                rm_id,
                queue)

        # Queues EI -> RA
        for key in self._ras_data:
            ra_id = self._ras_data[key]['id']

            queue = RMQUtils.get_fins_queue_name(ei_id, ra_id)
            assert self._switchboard.get_queue_name(ra_id,
                                                    "publish") is None
            self._register_queue(ra_id, queue, "publish")
            self.LOG.debug(
                "Registered queue (PUBLISH) from ei to ra %s: %s",
                ra_id,
                queue)

            queue = RMQUtils.get_fins_queue_name(ra_id, ei_id)
            assert self._switchboard.get_queue_name(ra_id,
                                                    "consume") is None
            self._register_queue(ra_id, queue, "consume")
            self.LOG.debug(
                "Registered queue (CONSUME) from ra %s to ei: %s",
                ra_id,
                queue)

    def _create_exchange(self):

        self.LOG.info("")
        self.LOG.info("Creating EI exchange...")

        rmq_rc = self._rmq_manager.rmq_rest_client()

        self._exchange = RMQUtils.get_fins_exchange_name(self.mod_id())

        rmq_rc.create_exchange(self._exchange)

    def _register_queue(self, terminal_id, queue_name, direction,
                        purpose=None):

        return self._switchboard.add_channel(terminal_id,
                                             queue_name,
                                             direction,
                                             purpose)

    def _assign_rmq_endpoints(self, process_rest_message,
                              process_rm_message,
                              process_ra_message):
        try:
            self.LOG.debug("")
            self.LOG.debug("Assigning a RabbitMQ endpoint for each "
                           "associated module (EI, REST, RAs)")

            if process_rest_message is None:
                raise Exception("Function processing REST messages is None")
            if not callable(process_rest_message):
                raise Exception("Function processing REST messages is NOT "
                                "callable")

            if process_rm_message is None:
                raise Exception("Function processing RM messages is None")
            if not callable(process_rm_message):
                raise Exception("Function processing RM messages is NOT "
                                "callable")

            if process_ra_message is None:
                raise Exception("Function processing RA messages is None")
            if not callable(process_ra_message):
                raise Exception("Function processing RA messages is NOT "
                                "callable")

            if not self.mod_exchange_is_defined():
                raise Exception("Module exchange NOT defined ")

            rest_id = self.mod_rest_id()
            rm_ids_list = self.mod_rm_ids()
            ra_ids_list = self.mod_ra_ids()

            ids = self._switchboard.get_terminal_id_list()

            for terminal_id in ids:

                if terminal_id == rest_id:
                    process_message = process_rest_message
                elif terminal_id in rm_ids_list:
                    process_message = process_rm_message
                elif terminal_id in ra_ids_list:
                    process_message = process_ra_message
                else:
                    raise Exception(
                        "Registered component " + terminal_id +
                        " could NOT be assigned to any msg processor")

                queue_desc = self._switchboard.get_terminal_queues(
                    terminal_id)
                ep = self._rmq_manager.create_endpoint(self._exchange,
                                                       queue_desc,
                                                       process_message)

                self._switchboard.assign_rmq_endpoint(terminal_id, ep)

                ep.start()

            return True

        except Exception as e:
            self.LOG.error(e)
            return False

    def assign_message_processors(self, process_rest_message,
                                  process_rm_message=None,
                                  process_ra_message=None):

        assert process_rest_message is not None

        if process_rm_message is None:
            process_rm_message = process_rest_message
            self.LOG.warning("RM messages are processed together "
                             "with REST messages")

        if process_ra_message is None:
            process_ra_message = process_rest_message
            self.LOG.warning("RA messages are processed together "
                             "with REST messages")

        self._assign_rmq_endpoints(process_rest_message,
                                   process_rm_message,
                                   process_ra_message)

        return True

    ####
    # Support methods
    #

    def mod_id(self):
        return self._ei_data['id']

    def mod_rest_id(self):
        return self._rest_data['id']

    def mod_rm_ids(self):
        rm_ids_list = []
        for idx in self._rms_data:
            rm_ids_list.append(self._rms_data[idx]['id'])
        return rm_ids_list

    def mod_ra_ids(self):
        ra_ids_list = []
        for idx in self._ras_data:
            ra_ids_list.append(self._ras_data[idx]['id'])
        return ra_ids_list

    def _show_switchboard(self):
        self.LOG.info("\n\nSWITCHBOARD:\n%s", self._switchboard.to_str())

    def mod_exchange_is_defined(self):
        return self._exchange is not None

    def new_message_id(self):
        return self._packet_handler.generate_msg_id()

    def send_message_to_RM(self, rm_id, pkt, is_reply=False):
        if rm_id is None:
            self.LOG.debug("Broadcasting message to all RMs")
            for rm_id in self.mod_rm_ids():
                self._send_message_to(pkt, rm_id, is_reply)
        elif rm_id in self.mod_rm_ids():
            self._send_message_to(pkt, rm_id, is_reply)
        else:
            raise Exception("RM with id " + rm_id + " NOT known")

    def send_message_to_REST(self, pkt, is_reply=False):
        self._send_message_to(pkt, self.mod_rest_id(), is_reply)

    def send_message_to_RA(self, ra_id, pkt, is_reply=False):
        if ra_id is None:
            self.LOG.debug("Broadcasting message to all RAs")
            for ra_id in self.mod_ra_ids():
                self._send_message_to(pkt, ra_id, is_reply)
        elif ra_id in self.mod_ra_ids():
            self._send_message_to(pkt, ra_id, is_reply)
        else:
            raise Exception("RA with id " + ra_id + " NOT known")

    def _send_message_to(self, pkt, dest_id, is_reply=False):
        pkt.set_msg_id(self.new_message_id(), is_reply)
        pkt.set_sender(self.mod_id(), is_reply)
        if is_reply:
            self._switchboard.send_to(dest_id, pkt.get_reply())
        else:
            self._switchboard.send_to(dest_id, pkt.get_packet())

    def message_to_packet(self, message):
        return self._packet_handler.get_packet(message)

    def dict_pretty_format(self, dictionary):
        return json.dumps(dictionary, sort_keys=True, indent=3)

