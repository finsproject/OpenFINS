# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
import logging
import traceback
import time

from fins.src.modules.abstracts.module_exp_initialiser_abstract import \
    ModuleInitialiserAbstract
from fins.src.support.comm.packets import HelloPacket

LOG = logging.getLogger(__name__)


class ModuleManager(ModuleInitialiserAbstract):

    def __init__(self, rabbit_data, ei_data, rms_data, ras_data, rest_data,
                 fins_bridge_descriptor):

        super(ModuleManager, self).__init__(rabbit_data,
                                            ei_data,
                                            rms_data,
                                            ras_data,
                                            rest_data)

        self.fins_bridge_descriptor = fins_bridge_descriptor

        self.LOG.info(self.fins_bridge_descriptor)

        self.assign_message_processors(self.process_message_from_REST,
                                       self.process_message_from_RM,
                                       self.process_message_from_RA)

        # time.sleep(30)

        # self.process_command("SEND_HELLO", dict())

    def process_command(self, command, params):

        assert isinstance(command, str)
        assert isinstance(params, dict)

        LOG.debug('Processing command: %s', command)

        try:
            if command == "SEND_HELLO":

                pkt = HelloPacket()
                pkt.build_packet(
                    None,
                    dict(
                        text='Hi there!'
                    ))

                self.send_message_to_REST(pkt)

        except Exception:
            traceback.print_exc()

    def process_message_from_RM(self, queue, message):

        LOG.debug('New message from RM (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from RM:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_RM(packet.get_sender(), packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_RA(self, queue, message):

        LOG.debug('New message from RA (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from RA:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_RA(packet.get_sender(), packet, True)

            elif packet.get_msg_type() == packet.MSG_TYPE_INPUT_REQ:
                b = packet.get_body()
                if 'need' in b:
                    if b['need'] == 'fins_bridge_descriptor':
                        # Create packet reply with a given body
                        packet.build_reply(
                            None, dict(what=b['need'],
                                       data=self.fins_bridge_descriptor))
                        # Send message with is_reply set to True
                        self.send_message_to_RA(packet.get_sender(), packet,
                                                True)
                else:
                    # Create packet reply with a given body
                    packet.build_reply(None, dict(data=None))
                    # Send message with is_reply set to True
                    self.send_message_to_RA(packet.get_sender(), packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_REST(self, queue, message):

        LOG.debug('New message from REST (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from REST:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_REST(packet, True)

        except Exception:
            traceback.print_exc()
