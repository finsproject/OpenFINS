# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback
import time

from fins.src.modules.abstracts.module_agent_abstract import ModuleAgentAbstract
from fins.src.support.comm.packets import HelloPacket, InputRequestPacket, \
    InputResponsePacket
from fins.src.support.bash.utils import BashUtils

from fins.src.support.ovs.ovs_script_manager import OvsScriptManager

LOG = logging.getLogger(__name__)


class ModuleAgent(ModuleAgentAbstract):

    MSG_KEY__SLICE_ID = 'slice_id'
    MSG_KEY__SLICE_NAME = 'slice_name'
    MSG_KEY__VLAN_ID = 'vlan_id'
    MSG_KEY__PRIORITY = 'priority'
    MSG_KEY__QOS_MINRATE = 'minrate'
    MSG_KEY__QOS_MAXRATE = 'maxrate'
    MSG_KEY__QOS_UP_MINRATE = 'up_minrate'
    MSG_KEY__QOS_UP_MAXRATE = 'up_maxrate'
    MSG_KEY__QOS_DOWN_MINRATE = 'down_minrate'
    MSG_KEY__QOS_DOWN_MAXRATE = 'down_maxrate'
    MSG_KEY__UP_COOKIE = 'up_cookie'
    MSG_KEY__DOWN_COOKIE = 'down_cookie'
    #MSG_KEY__ADD_VLANID_SRC_TAP = 'add_vlanid_src_tap'
    #MSG_KEY__STRIP_VLANID_DST_TAP = 'strip_vlanid_dst_tap'
    MSG_KEY__METHOD = "method"
    MSG_KEY__PATH = 'path'
    MSG_KEY__DUPLEX = 'duplex'
    MSG_KEY__STATUS = 'status'

    STATUS_VALUE__DEFINED = 'defined'
    STATUS_VALUE__INSTANTIATED = 'instantiated'
    STATUS_VALUE__RELEASED = 'released'

    METHOD__ASSIGN_KEEP_STRIP = "AKS"
    METHOD__ASSIGN_KEEP = "AK"
    METHOD__KEEP_KEEP = "KK"
    METHOD__KEEP_STRIP = "KS"
    METHOD__ASSIGN = "A"
    METHOD__KEEP = "K"
    METHOD__STRIP = "S"


    MSG_KEY__IF_NAME = 'if_name'
    MSG_KEY__IF_NAME_SRC = 'if_name_src'
    MSG_KEY__IF_NAME_DST = 'if_name_dst'
    MSG_KEY__IF_NETMASK = 'if_netmask'
    MSG_KEY__IF_TYPE = 'if_type'
    MSG_KEY__IF_TYPE_SRC = 'if_type_src'
    MSG_KEY__IF_TYPE_DST = 'if_type_dst'
    MSG_KEY__IF_IP = 'if_ip'
    MSG_KEY__IF_OFPORT = 'if_ofport'
    MSG_KEY__NODE_FINS = 'node_fins'
    MSG_KEY__NODE_JFED = 'node_jfed'
    MSG_KEY__NODE_IP = 'node_ip'
    MSG_KEY__BRIDGE_NAME = 'bridge_name'


    MSG_KEY__SRC_OFPORT = 'src_ofport'
    MSG_KEY__DST_OFPORT = 'dst_ofport'

    def __init__(self, rabbitmq_data, ra_data):

        super(ModuleAgent, self).__init__(rabbitmq_data,
                                          ra_data)
        #
        # A certain set of RabbitMQ queues is expected to be available for FINS
        # internal communication between RA and EI, RM and REST interface
        # The naming of the queues is assigned according to the rules given in
        # fins/support/rabbit_mq/rmq_utils.py->RMQUtils.get_fins_queue_name
        # i.e., "queue_" + source_id + "_to_" + destination_id
        # Each queue is meant for an unidirectional communication, thus for each
        # couple of connected entities, 2 queues are needed
        # During the init phase, the module checks if all the queue needed for
        # communication toward EI, RM and REST interface are already defined.
        # If not, RA repeat the check every second until all the needed queues
        # are found.
        #
        # When all the expected queues are found, RA configuration proceeds with
        # RabbitMQ communication manager setup.
        # At RA side, for each couple of connected RA - ModuleX, a RabbitMQ
        # Manager is defined for handling the communication between the two
        # modules, by retrieve/inject messages respectively from the output
        # queue (RA_to_X) and into the input queue (X_to_RA)
        # To each manager it is associated a Processing function, which performs
        # the operations that are supposed to be done when a message of a given
        # type is received on that specific Manager
        # Such a manager works as a thread, periodically checking the input
        # queue and automatically retrieving all messages in it
        # During the init phase, all configurations are performed, but the
        # assignment of message processing functions is left to the implementer,
        # to give him/her freedom in implementing the behavior of the module
        # Such an assignment is performed here with the following method
        #
        self.assign_message_processors(self.process_message_from_EI,
                                       self.process_message_from_RM,
                                       self.process_message_from_REST)

        self.fins_bridge_decriptor = None

        self.slice_repository = dict()

        time.sleep(5)

        self.process_command("REQUEST_BRIDGE_DESCRIPTOR",
                             dict(
                                 need='fins_bridge_descriptor'
                             ))

        # time.sleep(5)
        #
        # self.process_command("CREATE_NEW_INTERFACE",
        #                      dict(
        #                          node_jfed='RADIOHOST2',
        #                          if_name='int_test5',
        #                          if_type="internal",
        #                          if_ip="10.0.1.5",
        #                          if_netmask="255.255.255.0",
        #                          if_ofport="6"
        #                      ))



    # A command is a special, internal-generated message.
    # It is used to trigger a given reaction in the RA: a command can be hard-
    # coded into the py file or invoked on demand via message by EI/RM/REST
    def process_command(self, command, params):

        # command is a string identifying the type of command
        assert isinstance(command, str)
        # params is a dict containing the parameters associated to the command
        assert isinstance(params, dict)

        LOG.debug('Processing command: %s', command)

        try:
            if command == "REQUEST_BRIDGE_DESCRIPTOR":
                # Here a new packet (HelloPacket) is generated
                # You don't have to bother about header: proper filling is
                # performed automatically by Packet class.
                # If you need to create a reply to a given message, just use the
                # 'build_reply' method: the Packet class can create on its own
                # the reply packet header (see an example in ModuleAgent's
                # method 'self.process_message_from_EI' )
                # You have just to worry about the content of the body
                pkt = InputRequestPacket()
                pkt.build_packet(
                    None,
                    params
                )

                # send_message_to_XX takes care of sending the message to the
                # desired connected entity. It just takes 2 params:
                # @pkt              the Packet to be sent
                # @is_reply=False   if you are sending it as reply or not
                self.send_message_to_EI(pkt)

            elif command == "CREATE_NEW_INTERFACE":
                self.add_interface(params)

        except Exception:
            traceback.print_exc()


    def process_message_from_EI(self, queue, message):

        LOG.debug('New message from EI (queue: %s', queue)

        try:
            # Received message is a dict: it is converted into an instance of
            # Packet class (if recognized, into a specific child class instance
            # according to the message type)
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from EI:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message: here, since it is a reply, the is_reply
                # parameter among send_message_to_XX function's arguments shall
                # be set to True (whilst by default it is set to False)
                self.send_message_to_EI(packet, True)
            elif packet.get_msg_type() == packet.MSG_TYPE_INPUT_RSP:
                b = packet.get_body()
                if 'what' in b:
                    if b['what'] == 'fins_bridge_descriptor':
                        self.fins_bridge_decriptor = b['data']

        except Exception:
            traceback.print_exc()

    def process_message_from_RM(self, queue, message):

        LOG.debug('New message from RM (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from RM:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_RM(packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_REST(self, queue, message):

        LOG.debug('New message from REST (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from REST:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_REST(packet, True)

            if packet.get_msg_type() == 'OVS_ACTION':
                b = packet.get_body()
                # Create packet reply with a given body
                action = b['action']
                # if action == 'add_interface':
                #     params = b['params']
                #     rsp = self.add_interface(params)
                #     packet.build_reply(
                #         None,
                #         dict(action=b['action'],
                #              info=dict(
                #                  output="\n".join(rsp[1]),
                #                  error="\n".join(rsp[2])
                #              )))
                # elif action == 'del_interface':
                #     params = b['params']
                #     rsp = self.del_interface(params)
                #     packet.build_reply(
                #         None,
                #         dict(action=b['action'],
                #              info=dict(
                #                  output="\n".join(rsp[1]),
                #                  error="\n".join(rsp[2])
                #              )))
                if action == 'define_slice':
                    params = b['params']
                    rsp = self._define_slice(params)
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info=dict(
                                 output=rsp[1],
                                 error="\n".join(rsp[2])
                             )))
                elif action == 'show_slice':
                    params = b['params']
                    rsp = self._show_slice(params)
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info=dict(
                                 output=rsp[1],
                                 error="\n".join(rsp[2])
                             )))
                elif action == 'instantiate_slice':
                    params = b['params']
                    rsp = self._instantiate_slice(params)
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info=dict(
                                 output=rsp[1],
                                 error="\n".join(rsp[2])
                             )))
                elif action == 'update_slice_qos':
                    params = b['params']
                    rsp = self._update_slice_qos(params)
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info=dict(
                                 output=rsp[1],
                                 error="\n".join(rsp[2])
                             )))
                elif action == 'release_slice':
                    params = b['params']
                    rsp = self._release_slice(params)
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info=dict(
                                 output=rsp[1],
                                 error="\n".join(rsp[2])
                             )))
                else:
                    packet.build_reply(
                        None,
                        dict(action=b['action'],
                             info='action unknown: IGNORED'))
                self.send_message_to_REST(packet, True)

            # LOG.debug("Current bridge_descriptor: %s",
            #           str(self.fins_bridge_decriptor))

        except Exception:
            traceback.print_exc()

    def _assign_slice_id(self):
        return 100

    def _assign_slice_name(self, slice_id):
        return "SLICE_%s" % str(slice_id)

    def _assign_vlan_id(self):
        return 100

    def _assign_priority(self):
        return 100

    def _process_singleton_slice_creation_node(self, node):
        LOG.debug("Singleton node: %s", str(node))
        try:
            # ASSERT node identifier
            assert self.MSG_KEY__NODE_FINS in node or \
                   self.MSG_KEY__NODE_JFED in node or \
                   self.MSG_KEY__NODE_IP in node

            assert self.MSG_KEY__IF_NAME_SRC in node
            assert self.MSG_KEY__IF_NAME_DST in node
            assert node[self.MSG_KEY__IF_NAME_SRC] != \
                   node[self.MSG_KEY__IF_NAME_DST]

            node_found = False
            # CHECK NODE EXISTANCE and FILL/SET other node fields
            if self.MSG_KEY__NODE_FINS in node:
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    fins_id = br['associated_resource']
                    if fins_id == node[self.MSG_KEY__NODE_FINS]:
                        br = self.fins_bridge_decriptor[fins_id]
                        node[self.MSG_KEY__NODE_JFED] = \
                            br['associated_resource_jfed']
                        node[self.MSG_KEY__NODE_IP] = br['ip']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            elif self.MSG_KEY__NODE_JFED in node:
                LOG.debug("node_jfed search: %s", node[self.MSG_KEY__NODE_JFED])
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    jfed_id = br['associated_resource_jfed']
                    LOG.debug("node_jfed, candidate: %s", jfed_id)
                    if jfed_id == node[self.MSG_KEY__NODE_JFED]:
                        node[self.MSG_KEY__NODE_FINS] = \
                            br['associated_resource']
                        node[self.MSG_KEY__NODE_IP] = br['ip']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            elif self.MSG_KEY__NODE_IP in node:
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    ip = br['ip']
                    if ip == node[self.MSG_KEY__NODE_IP]:
                        node[self.MSG_KEY__NODE_FINS] = \
                            br['associated_resource']
                        node[self.MSG_KEY__NODE_JFED] = \
                            br['associated_resource_jfed']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            # IF NODE NOT FOUND --> ERROR!
            assert node_found

            if_src_found = False
            if_dst_found = False
            if_src_name = node[self.MSG_KEY__IF_NAME_SRC]
            if_dst_name = node[self.MSG_KEY__IF_NAME_DST]
            node_fins = node[self.MSG_KEY__NODE_FINS]
            br = self.fins_bridge_decriptor[node_fins]
            for iface in br['interfaces']['int']:
                LOG.debug("if_name, candidate: %s", iface['id'])
                if iface['id'] == if_src_name:
                    node[self.MSG_KEY__SRC_OFPORT] = iface['of_port']
                    node[self.MSG_KEY__IF_TYPE_SRC] = 'int'
                    if_src_found = True
                elif iface['id'] == if_dst_name:
                    node[self.MSG_KEY__DST_OFPORT] = iface['of_port']
                    node[self.MSG_KEY__IF_TYPE_DST] = 'int'
                    if_dst_found = True
                if if_src_found and if_dst_found:
                    break
            for iface in br['interfaces']['tap']:
                if if_src_found and if_dst_found:
                    break
                LOG.debug("if_name, candidate: %s", iface['id'])
                if iface['id'] == if_src_name:
                    node[self.MSG_KEY__SRC_OFPORT] = iface['of_port']
                    node[self.MSG_KEY__IF_TYPE_SRC] = 'tap'
                    if_src_found = True
                elif iface['id'] == if_dst_name:
                    node[self.MSG_KEY__DST_OFPORT] = iface['of_port']
                    node[self.MSG_KEY__IF_TYPE_DST] = 'tap'
                    if_dst_found = True
                if if_src_found and if_dst_found:
                    break
            return True

        except Exception as e:
            LOG.error(e)
            traceback.print_exc()

            return False

    def _process_slice_creation_node(self, node, is_terminal=False):
        LOG.debug("Current node: %s", str(node))
        try:
            # ASSERT node identifier
            assert self.MSG_KEY__NODE_FINS in node or \
                   self.MSG_KEY__NODE_JFED in node or \
                   self.MSG_KEY__NODE_IP in node

            # ASSERT interface identifier (IF slice terminal)
            if is_terminal:
                assert self.MSG_KEY__IF_NAME in node or \
                       self.MSG_KEY__IF_IP in node or \
                       self.MSG_KEY__IF_OFPORT in node

            if_type = None
            # ASSERT interface type: INT/TAP (IF type defined)
            if self.MSG_KEY__IF_TYPE in node:
                assert node[self.MSG_KEY__IF_TYPE] == 'int' or \
                       node[self.MSG_KEY__IF_TYPE] == 'tap'
                if_type = node[self.MSG_KEY__IF_TYPE]

            node_found = False
            # CHECK NODE EXISTANCE and FILL/SET other node fields
            if self.MSG_KEY__NODE_FINS in node:
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    fins_id = br['associated_resource']
                    if fins_id == node[self.MSG_KEY__NODE_FINS]:
                        br = self.fins_bridge_decriptor[fins_id]
                        node[self.MSG_KEY__NODE_JFED] = \
                            br['associated_resource_jfed']
                        node[self.MSG_KEY__NODE_IP] = br['ip']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            elif self.MSG_KEY__NODE_JFED in node:
                LOG.debug("node_jfed search: %s", node[self.MSG_KEY__NODE_JFED])
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    jfed_id = br['associated_resource_jfed']
                    LOG.debug("node_jfed, candidate: %s", jfed_id)
                    if jfed_id == node[self.MSG_KEY__NODE_JFED]:
                        node[self.MSG_KEY__NODE_FINS] = \
                            br['associated_resource']
                        node[self.MSG_KEY__NODE_IP] = br['ip']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            elif self.MSG_KEY__NODE_IP in node:
                for index in self.fins_bridge_decriptor:
                    br = self.fins_bridge_decriptor[index]
                    ip = br['ip']
                    if ip == node[self.MSG_KEY__NODE_IP]:
                        node[self.MSG_KEY__NODE_FINS] = \
                            br['associated_resource']
                        node[self.MSG_KEY__NODE_JFED] = \
                            br['associated_resource_jfed']
                        node[self.MSG_KEY__BRIDGE_NAME] = br['id']
                        node_found = True
                        break
            # IF NODE NOT FOUND --> ERROR!
            assert node_found

            if is_terminal:
                if_found = False
                # CHECK INTERFACE EXISTANCE and FILL/SET other interface fields
                if self.MSG_KEY__IF_NAME in node:
                    LOG.debug("if_name search: %s",
                              node[self.MSG_KEY__IF_NAME])
                    if_name = node[self.MSG_KEY__IF_NAME]
                    node_fins = node[self.MSG_KEY__NODE_FINS]
                    br = self.fins_bridge_decriptor[node_fins]
                    if if_type is not None:
                        ifaces = br['interfaces'][if_type]
                        for iface in ifaces:
                            if iface['id'] == if_name:
                                node[self.MSG_KEY__IF_OFPORT] = iface['of_port']
                                if if_type == 'int':
                                    node[self.MSG_KEY__IF_IP] = iface['ip']
                                if_found = True
                                break
                    else:
                        for iface in br['interfaces']['int']:
                            LOG.debug("if_name, candidate: %s", iface['id'])
                            if iface['id'] == if_name:
                                node[self.MSG_KEY__IF_OFPORT] = iface['of_port']
                                node[self.MSG_KEY__IF_IP] = iface['ip']
                                node[self.MSG_KEY__IF_TYPE] = 'int'
                                if_type = node[self.MSG_KEY__IF_TYPE]
                                if_found = True
                                break
                        for iface in br['interfaces']['tap']:
                            LOG.debug("if_name, candidate: %s", iface['id'])
                            if if_found:
                                break
                            elif iface['id'] == if_name:
                                node[self.MSG_KEY__IF_OFPORT] = iface['of_port']
                                node[self.MSG_KEY__IF_IP] = None
                                node[self.MSG_KEY__IF_TYPE] = 'tap'
                                if_type = node[self.MSG_KEY__IF_TYPE]
                                if_found = True
                                break
                elif self.MSG_KEY__IF_OFPORT in node:
                    if_ofport = node[self.MSG_KEY__IF_OFPORT]
                    node_fins = node[self.MSG_KEY__NODE_FINS]
                    br = self.fins_bridge_decriptor[node_fins]
                    if if_type is not None:
                        ifaces = br['interfaces'][if_type]
                        for iface in ifaces:
                            if iface['of_port'] == if_ofport:
                                node[self.MSG_KEY__IF_NAME] = iface['id']
                                if if_type == 'int':
                                    node[self.MSG_KEY__IF_IP] = iface['ip']
                                if_found = True
                                break
                    else:
                        for iface in br['interfaces']['int']:
                            if iface['of_port'] == if_ofport:
                                node[self.MSG_KEY__IF_NAME] = iface['id']
                                node[self.MSG_KEY__IF_IP] = iface['ip']
                                node[self.MSG_KEY__IF_TYPE] = 'int'
                                if_type = node[self.MSG_KEY__IF_TYPE]
                                if_found = True
                                break
                        for iface in br['interfaces']['tap']:
                            if if_found:
                                break
                            if iface['of_port'] == if_ofport:
                                node[self.MSG_KEY__IF_NAME] = iface['id']
                                node[self.MSG_KEY__IF_IP] = None
                                node[self.MSG_KEY__IF_TYPE] = 'tap'
                                if_type = node[self.MSG_KEY__IF_TYPE]
                                if_found = True
                                break
                elif self.MSG_KEY__IF_IP in node:

                    assert node[self.MSG_KEY__IF_IP] is not None

                    if_ip = node[self.MSG_KEY__IF_IP]
                    node[self.MSG_KEY__IF_TYPE] = 'int'
                    if_type = node[self.MSG_KEY__IF_TYPE]
                    node_fins = node[self.MSG_KEY__NODE_FINS]
                    br = self.fins_bridge_decriptor[node_fins]
                    if if_type is not None:
                        ifaces = br['interfaces'][if_type]
                        for iface in ifaces:
                            if iface['ip'] == if_ip:
                                node[self.MSG_KEY__IF_NAME] = iface['id']
                                node[self.MSG_KEY__IF_OFPORT] = iface['of_port']
                                if_found = True
                                break
                # IF INTERFACE NOT FOUND --> ERROR!
                assert if_found

            if is_terminal:
                if self.MSG_KEY__IF_NAME_SRC in node:
                    node[self.MSG_KEY__IF_NAME_SRC] = \
                        node[self.MSG_KEY__IF_NAME]
                    node[self.MSG_KEY__IF_TYPE_SRC] = \
                        node[self.MSG_KEY__IF_TYPE]
                else:
                    node[self.MSG_KEY__IF_NAME_DST] = \
                        node[self.MSG_KEY__IF_NAME]
                    node[self.MSG_KEY__IF_TYPE_DST] = \
                        node[self.MSG_KEY__IF_TYPE]

            return True

        except Exception as e:
            LOG.error(e)
            traceback.print_exc()

            return False

    def _eval_explicit_path(self, path):
        try:
            assert isinstance(path, list)
            if len(path) == 1:
                node = path[0]
                assert node[self.MSG_KEY__SRC_OFPORT] is not None and \
                       node[self.MSG_KEY__DST_OFPORT] is not None

                LOG.info(path)

                return True

            for index in range(len(path)):

                if index == 0:
                    LOG.debug("HEAD")
                    node = path[index]
                    src_ofport = node[self.MSG_KEY__IF_OFPORT]
                    node[self.MSG_KEY__SRC_OFPORT] = src_ofport
                    node_fins_id = node[self.MSG_KEY__NODE_FINS]
                    node_next = path[index + 1]
                    node_next_fins_id = node_next[self.MSG_KEY__NODE_FINS]
                    br = self.fins_bridge_decriptor[node_fins_id]
                    gres = br['interfaces']['gre']
                    for gre in gres:
                        if gre['remote_br_fins'] == node_next_fins_id:
                            dst_ofport = gre['of_port']
                            node[self.MSG_KEY__DST_OFPORT] = dst_ofport
                            rem_ofport = gre['remote_of_port']
                            node_next[self.MSG_KEY__SRC_OFPORT] = rem_ofport
                            break

                elif index == len(path) - 1:
                    LOG.debug("TAIL")
                    node = path[index]
                    dst_ofport = node[self.MSG_KEY__IF_OFPORT]
                    node[self.MSG_KEY__DST_OFPORT] = dst_ofport

                else:
                    LOG.debug("INTERMEDIATE")
                    node = path[index]
                    node_fins_id = node[self.MSG_KEY__NODE_FINS]
                    node_next = path[index + 1]
                    node_next_fins_id = node_next[self.MSG_KEY__NODE_FINS]
                    br = self.fins_bridge_decriptor[node_fins_id]
                    gres = br['interfaces']['gre']
                    for gre in gres:
                        if gre['remote_br_fins'] == node_next_fins_id:
                            dst_ofport = gre['of_port']
                            node[self.MSG_KEY__DST_OFPORT] = dst_ofport
                            rem_ofport = gre['remote_of_port']
                            node_next[self.MSG_KEY__SRC_OFPORT] = rem_ofport
                            break

                assert node[self.MSG_KEY__SRC_OFPORT] is not None and \
                       node[self.MSG_KEY__DST_OFPORT] is not None

                LOG.info(path)

            return True

        except Exception as e:
            LOG.error(e)
            traceback.print_exc()

            return False


    def _define_slice(self, req):

        try:

            assert isinstance(req, dict)
            assert self.MSG_KEY__PATH in req
            assert isinstance(req[self.MSG_KEY__PATH], list)
            assert len(req[self.MSG_KEY__PATH]) > 0
            if len(req[self.MSG_KEY__PATH]) == 1:
                node = req[self.MSG_KEY__PATH][0]
                if not self._process_singleton_slice_creation_node(node):
                    raise Exception("Invalid singleton path node ")
            else:
                for index in range(len(req[self.MSG_KEY__PATH])):
                    node = req[self.MSG_KEY__PATH][index]
                    is_terminal = False
                    if index == 0 or index == len(req[self.MSG_KEY__PATH]) - 1:
                        is_terminal = True
                        if index == 0:
                            node[self.MSG_KEY__IF_NAME_SRC] = None
                            node[self.MSG_KEY__IF_TYPE_SRC] = None
                        else:
                            node[self.MSG_KEY__IF_NAME_DST] = None
                            node[self.MSG_KEY__IF_TYPE_DST] = None

                    if not (self._process_slice_creation_node(node,
                                                              is_terminal)):
                        raise Exception("Invalid path node " + str(index))

            # LOG.info(req)

            if self.MSG_KEY__SLICE_ID not in req:
                req[self.MSG_KEY__SLICE_ID] = self._assign_slice_id()
            if self.MSG_KEY__SLICE_NAME not in req:
                req[self.MSG_KEY__SLICE_NAME] = self._assign_slice_name(
                    req[self.MSG_KEY__SLICE_ID])
            if self.MSG_KEY__VLAN_ID not in req:
                req[self.MSG_KEY__VLAN_ID] = self._assign_vlan_id()
            if self.MSG_KEY__PRIORITY not in req:
                req[self.MSG_KEY__PRIORITY] = self._assign_priority()
            if self.MSG_KEY__QOS_MINRATE not in req:
                req[self.MSG_KEY__QOS_MINRATE] = 0
            if self.MSG_KEY__QOS_MAXRATE not in req:
                req[self.MSG_KEY__QOS_MAXRATE] = 0

            if self.MSG_KEY__DUPLEX not in req:
                req[self.MSG_KEY__DUPLEX] = True

            if self.MSG_KEY__QOS_UP_MINRATE not in req:
                if req[self.MSG_KEY__DUPLEX]:
                    req[self.MSG_KEY__QOS_UP_MINRATE] = \
                        int(req[self.MSG_KEY__QOS_MINRATE]/2)
                else:
                    req[self.MSG_KEY__QOS_UP_MINRATE] = \
                        int(req[self.MSG_KEY__QOS_MINRATE])
            if self.MSG_KEY__QOS_UP_MAXRATE not in req:
                if req[self.MSG_KEY__DUPLEX]:
                    req[self.MSG_KEY__QOS_UP_MAXRATE] = \
                        int(req[self.MSG_KEY__QOS_MAXRATE]/2)
                else:
                    req[self.MSG_KEY__QOS_UP_MAXRATE] = \
                        int(req[self.MSG_KEY__QOS_MAXRATE])
            if self.MSG_KEY__QOS_DOWN_MINRATE not in req:
                if req[self.MSG_KEY__DUPLEX]:
                    req[self.MSG_KEY__QOS_DOWN_MINRATE] = \
                        int(req[self.MSG_KEY__QOS_MINRATE]/2)
                else:
                    req[self.MSG_KEY__QOS_DOWN_MINRATE] = 0
            if self.MSG_KEY__QOS_DOWN_MAXRATE not in req:
                if req[self.MSG_KEY__DUPLEX]:
                    req[self.MSG_KEY__QOS_DOWN_MAXRATE] = \
                        int(req[self.MSG_KEY__QOS_MAXRATE]/2)
                else:
                    req[self.MSG_KEY__QOS_DOWN_MAXRATE] = 0

            if self.MSG_KEY__UP_COOKIE not in req:
                req[self.MSG_KEY__UP_COOKIE] = \
                    int(req[self.MSG_KEY__SLICE_ID]) * 10
            if self.MSG_KEY__DOWN_COOKIE not in req:
                req[self.MSG_KEY__DOWN_COOKIE] = \
                    int(req[self.MSG_KEY__SLICE_ID]) * 10 + 1

            assert self._eval_explicit_path(req[self.MSG_KEY__PATH])

            if self.MSG_KEY__METHOD not in req:
                path = req[self.MSG_KEY__PATH]
                if len(req[self.MSG_KEY__PATH]) == 1:
                    node = path[0]
                    if_type_src = node[self.MSG_KEY__IF_TYPE_SRC]
                    if_type_dst = node[self.MSG_KEY__IF_TYPE_DST]

                    if if_type_src == 'int':
                        if if_type_dst == 'tap':
                            req[self.MSG_KEY__METHOD] = self.METHOD__ASSIGN
                        else:
                            raise Exception("Invalid singleton slice terminal "
                                            "ifs: int-->%s" % if_type_dst)
                    elif if_type_src == 'tap':
                        if if_type_dst == 'int':
                            req[self.MSG_KEY__METHOD] = self.METHOD__STRIP
                        elif if_type_dst == 'tap':
                            req[self.MSG_KEY__METHOD] = self.METHOD__KEEP
                        else:
                            raise Exception("Invalid singleton slice terminal "
                                            "ifs: tap-->%s" % if_type_dst)
                else:
                    node = path[0]
                    if_type_src = node[self.MSG_KEY__IF_TYPE]
                    node = path[len(path) - 1]
                    if_type_dst = node[self.MSG_KEY__IF_TYPE]

                    if if_type_src == 'int':
                        if if_type_dst == 'int':
                            req[self.MSG_KEY__METHOD] = \
                                self.METHOD__ASSIGN_KEEP_STRIP
                        elif if_type_dst == 'tap':
                            req[self.MSG_KEY__METHOD] = self.METHOD__ASSIGN_KEEP
                        else:
                            raise Exception("Invalid singleton slice terminal "
                                            "ifs: int-->%s" % if_type_dst)
                    elif if_type_src == 'tap':
                        if if_type_dst == 'int':
                            req[self.MSG_KEY__METHOD] = self.METHOD__KEEP_STRIP
                        elif if_type_dst == 'tap':
                            req[self.MSG_KEY__METHOD] = self.METHOD__KEEP_KEEP
                        else:
                            raise Exception("Invalid singleton slice terminal "
                                            "ifs: tap-->%s" % if_type_dst)

            req[self.MSG_KEY__STATUS] = self.STATUS_VALUE__DEFINED

            self.slice_repository[req[self.MSG_KEY__SLICE_ID]] = req

            return [[], [req], []]

        except Exception as e:
            LOG.error(e)
            traceback.print_exc()

            return [[], [], [str(e)]]

    def _show_slice(self, params):
        slice_id = params['slice_id']
        if slice_id not in self.slice_repository:
            return [list(),
                    list(),
                    ["Slice with slice_id %s undefined" % slice_id]]
        return [list(),
                [self.slice_repository[slice_id]],
                list()]

    def _process_method_singleton(self, slice):

        path = slice[self.MSG_KEY__PATH]

        instructions = dict()

        instructions["UP"] = list()

        osm = OvsScriptManager()

        method = slice[self.MSG_KEY__METHOD]
        node = path[0]

        if method == self.METHOD__ASSIGN:
            code = osm.set_flow_head(node[self.MSG_KEY__BRIDGE_NAME],
                                     slice[self.MSG_KEY__PRIORITY],
                                     node[self.MSG_KEY__SRC_OFPORT],
                                     slice[self.MSG_KEY__VLAN_ID],
                                     node[self.MSG_KEY__DST_OFPORT],
                                     slice[self.MSG_KEY__UP_COOKIE])
            instructions['UP'].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                code = osm.set_flow_tail(node[self.MSG_KEY__BRIDGE_NAME],
                                         slice[self.MSG_KEY__PRIORITY],
                                         node[self.MSG_KEY__DST_OFPORT],
                                         slice[self.MSG_KEY__VLAN_ID],
                                         node[self.MSG_KEY__SRC_OFPORT],
                                         slice[self.MSG_KEY__DOWN_COOKIE])
                instructions["DOWN"].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
        elif method == self.METHOD__KEEP:
            code = osm.set_flow_mid(node[self.MSG_KEY__BRIDGE_NAME],
                                    slice[self.MSG_KEY__PRIORITY],
                                    node[self.MSG_KEY__SRC_OFPORT],
                                    slice[self.MSG_KEY__VLAN_ID],
                                    node[self.MSG_KEY__DST_OFPORT],
                                    slice[self.MSG_KEY__UP_COOKIE])
            instructions['UP'].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                code = osm.set_flow_mid(node[self.MSG_KEY__BRIDGE_NAME],
                                        slice[self.MSG_KEY__PRIORITY],
                                        node[self.MSG_KEY__DST_OFPORT],
                                        slice[self.MSG_KEY__VLAN_ID],
                                        node[self.MSG_KEY__SRC_OFPORT],
                                        slice[self.MSG_KEY__DOWN_COOKIE])
                instructions["DOWN"].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
        elif method == self.METHOD__STRIP:
            code = osm.set_flow_tail(node[self.MSG_KEY__BRIDGE_NAME],
                                     slice[self.MSG_KEY__PRIORITY],
                                     node[self.MSG_KEY__SRC_OFPORT],
                                     slice[self.MSG_KEY__VLAN_ID],
                                     node[self.MSG_KEY__DST_OFPORT],
                                     slice[self.MSG_KEY__UP_COOKIE])
            instructions['UP'].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                code = osm.set_flow_head(node[self.MSG_KEY__BRIDGE_NAME],
                                         slice[self.MSG_KEY__PRIORITY],
                                         node[self.MSG_KEY__DST_OFPORT],
                                         slice[self.MSG_KEY__VLAN_ID],
                                         node[self.MSG_KEY__SRC_OFPORT],
                                         slice[self.MSG_KEY__DOWN_COOKIE])
                instructions["DOWN"].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
        else:
            LOG.warning("Unknown method: %s", method)
            return None

        return instructions

    def _process_method(self, slice):

        path = slice[self.MSG_KEY__PATH]
        if len(path) == 1:
            return self._process_method_singleton(slice)

        instructions = dict()
        instructions["UP"] = list()

        osm = OvsScriptManager()

        method = slice[self.MSG_KEY__METHOD]

        if method == self.METHOD__ASSIGN_KEEP_STRIP:
            for i, node in reversed(list(enumerate(path))):

                if i == len(path) - 1:
                    code = osm.set_flow_tail(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))

                elif i == 0:
                    code = osm.set_flow_head(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
                else:
                    code = osm.set_flow_mid(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                for i, node in list(enumerate(path)):
                    if i == len(path) - 1:
                        code = osm.set_flow_head(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))

                    elif i == 0:
                        code = osm.set_flow_tail(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))

                    else:
                        code = osm.set_flow_mid(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))

        elif method == self.METHOD__ASSIGN_KEEP:
            for i, node in reversed(list(enumerate(path))):

                if i == 0:
                    code = osm.set_flow_head(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
                else:
                    code = osm.set_flow_mid(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                for i, node in list(enumerate(path)):
                    if i == 0:
                        code = osm.set_flow_tail(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))

                    else:
                        code = osm.set_flow_mid(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))
        elif method == self.METHOD__KEEP_STRIP:
            for i, node in reversed(list(enumerate(path))):

                if i == len(path) - 1:
                    code = osm.set_flow_tail(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))

                else:
                    code = osm.set_flow_mid(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__UP_COOKIE])
                    instructions['UP'].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                for i, node in list(enumerate(path)):
                    if i == len(path) - 1:
                        code = osm.set_flow_head(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))

                    else:
                        code = osm.set_flow_mid(
                            node[self.MSG_KEY__BRIDGE_NAME],
                            slice[self.MSG_KEY__PRIORITY],
                            node[self.MSG_KEY__DST_OFPORT],
                            slice[self.MSG_KEY__VLAN_ID],
                            node[self.MSG_KEY__SRC_OFPORT],
                            slice[self.MSG_KEY__DOWN_COOKIE])
                        instructions["DOWN"].append(dict(
                            node_ip=node[self.MSG_KEY__NODE_IP],
                            code=code
                        ))
        elif method == self.METHOD__KEEP_KEEP:
            for i, node in reversed(list(enumerate(path))):
                code = osm.set_flow_mid(
                    node[self.MSG_KEY__BRIDGE_NAME],
                    slice[self.MSG_KEY__PRIORITY],
                    node[self.MSG_KEY__SRC_OFPORT],
                    slice[self.MSG_KEY__VLAN_ID],
                    node[self.MSG_KEY__DST_OFPORT],
                    slice[self.MSG_KEY__UP_COOKIE])
                instructions['UP'].append(dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))
            if slice[self.MSG_KEY__DUPLEX]:
                instructions["DOWN"] = list()
                for i, node in list(enumerate(path)):
                    code = osm.set_flow_mid(
                        node[self.MSG_KEY__BRIDGE_NAME],
                        slice[self.MSG_KEY__PRIORITY],
                        node[self.MSG_KEY__DST_OFPORT],
                        slice[self.MSG_KEY__VLAN_ID],
                        node[self.MSG_KEY__SRC_OFPORT],
                        slice[self.MSG_KEY__DOWN_COOKIE])
                    instructions["DOWN"].append(dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))
        return instructions

    def _instantiate_slice(self, params):

        slice_id = params['slice_id']
        if slice_id not in self.slice_repository:
            return [list(),
                    list(),
                    ["Slice with slice_id %s undefined" % slice_id]]

        slice = self.slice_repository[slice_id]

        if  slice[self.MSG_KEY__STATUS] == self.STATUS_VALUE__INSTANTIATED:

            return [list(),
                    list(),
                    ["Slice %s is already instantiated" % str(slice_id)]]

        instructions = self._process_method(slice)

        LOG.debug("INSTRUCTIONS: "+str(instructions))

        LOG.info("Setting UP flow")

        rsp_up = [list(), list(), list()]

        for block in instructions['UP']:

            code = block['code']
            node_ip = block['node_ip']

            LOG.info("\n[UP] Run @%s: %s" % (node_ip, code))

            rsp = BashUtils.ssh_command(code, node_ip)

            rsp_up[1] = rsp_up[1] + rsp[1]
            rsp_up[2] = rsp_up[2] + rsp[2]

        if slice[self.MSG_KEY__DUPLEX]:

            LOG.info("Setting DOWN flow")

            rsp_down = [list(), list(), list()]

            for block in instructions['DOWN']:
                code = block['code']
                node_ip = block['node_ip']

                LOG.info("\n[DOWN] Run @%s: %s" % (node_ip, code))

                rsp = BashUtils.ssh_command(code, node_ip)

                rsp_down[1] = rsp_down[1] + rsp[1]
                rsp_down[2] = rsp_down[2] + rsp[2]

            rsp_up[1] = rsp_up[1] + rsp_down[1]
            rsp_up[2] = rsp_up[2] + rsp_down[2]

        # QoS

        path = slice[self.MSG_KEY__PATH]

        src_ip = None
        dst_ip = None
        if_name_src = None
        if_name_dst = None
        up_minrate = slice[self.MSG_KEY__QOS_UP_MINRATE]
        up_maxrate = slice[self.MSG_KEY__QOS_UP_MAXRATE]
        down_minrate = slice[self.MSG_KEY__QOS_DOWN_MINRATE]
        down_maxrate = slice[self.MSG_KEY__QOS_DOWN_MAXRATE]
        if len(path) == 1:
            src_ip = path[0]['node_ip']
            dst_ip = path[0]['node_ip']
            if_name_src = path[0]['if_name_src']
            if_type_src = path[0]['if_type_src']
            if_name_dst = path[0]['if_name_dst']
            if_type_dst = path[0]['if_type_dst']

        else:
            last = len(path) -1
            src_ip = path[0]['node_ip']
            dst_ip = path[last]['node_ip']
            if_name_src = path[0]['if_name']
            if_type_src = path[0]['if_type_src']
            if_name_dst = path[last]['if_name']
            if_type_dst = path[last]['if_type_dst']

        if if_type_src == 'int':
            LOG.info("\n[UP] Add QoS to src %s@%s" % (if_name_src, src_ip))
            self._update_port_qos(src_ip, if_name_src, up_minrate, up_maxrate)
        else:
            LOG.info("\n[UP] src type is %s: no QoS assignement" % if_type_src)
        if if_type_dst == 'int':
            LOG.info("\n[DOWN] Add QoS to dst %s@%s" % (if_name_dst, dst_ip))
            self._update_port_qos(dst_ip, if_name_dst, down_minrate,
                                  down_maxrate)
        else:
            LOG.info("\n[DOWN] dst type is %s: no QoS assignement" %
                     if_type_dst)

        slice[self.MSG_KEY__STATUS] = self.STATUS_VALUE__INSTANTIATED

        return rsp_up


    def _update_port_qos(self, vm_ip, port_name, min_rate, max_rate):
        osm = OvsScriptManager()
        code = osm.set_port_qos(port_name, max_rate, min_rate)

        rsp = BashUtils.ssh_command(code, vm_ip)

        return rsp

    def _update_slice_qos(self, params):
        slice_id = params['slice_id']
        if slice_id not in self.slice_repository:
            return [list(),
                    list(),
                    ["Slice with slice_id %s undefined" % slice_id]]

        slice = self.slice_repository[slice_id]

        up_maxrate = slice[self.MSG_KEY__QOS_UP_MAXRATE]
        if self.MSG_KEY__QOS_UP_MAXRATE in params:
            up_maxrate = int(params[self.MSG_KEY__QOS_UP_MAXRATE])

        up_minrate = slice[self.MSG_KEY__QOS_UP_MINRATE]
        if self.MSG_KEY__QOS_UP_MINRATE in params:
            up_minrate = int(params[self.MSG_KEY__QOS_UP_MINRATE])

        down_maxrate = slice[self.MSG_KEY__QOS_DOWN_MAXRATE]
        if self.MSG_KEY__QOS_DOWN_MAXRATE in params:
            down_maxrate = int(params[self.MSG_KEY__QOS_DOWN_MAXRATE])

        down_minrate = slice[self.MSG_KEY__QOS_DOWN_MINRATE]
        if self.MSG_KEY__QOS_DOWN_MINRATE in params:
            down_minrate = int(params[self.MSG_KEY__QOS_DOWN_MINRATE])

        slice[self.MSG_KEY__QOS_UP_MAXRATE] = int(up_maxrate)
        slice[self.MSG_KEY__QOS_UP_MINRATE] = int(up_minrate)
        slice[self.MSG_KEY__QOS_DOWN_MAXRATE] = int(down_maxrate)
        slice[self.MSG_KEY__QOS_DOWN_MINRATE] = int(down_minrate)
        slice[self.MSG_KEY__QOS_MINRATE] = int(up_minrate + down_minrate)
        slice[self.MSG_KEY__QOS_MAXRATE] = int(up_maxrate + down_maxrate)

        rsp_main = [list(), list(), list()]

        if slice[self.MSG_KEY__STATUS] == self.STATUS_VALUE__INSTANTIATED:
            src_ip = None
            dst_ip = None
            if_name_src = None
            if_name_dst = None
            if_type_src = None
            if_type_dst = None

            path = slice[self.MSG_KEY__PATH]
            if len(path) == 1:
                src_ip = path[0]['node_ip']
                dst_ip = path[0]['node_ip']
                if_name_src = path[0]['if_name_src']
                if_name_dst = path[0]['if_name_dst']
                if_type_src = path[0]['if_type_src']
                if_type_dst = path[0]['if_type_dst']

            else:
                last = len(path) - 1
                src_ip = path[0]['node_ip']
                dst_ip = path[last]['node_ip']
                if_name_src = path[0]['if_name']
                if_name_dst = path[last]['if_name']
                if_type_src = path[0]['if_type_src']
                if_type_dst = path[last]['if_type_dst']

            if if_type_src == "int":
                rsp = self._update_port_qos(src_ip, if_name_src,
                                            up_minrate, up_maxrate)
                rsp_main[1] = rsp_main[1] + rsp[1]
                rsp_main[2] = rsp_main[2] + rsp[2]

            if if_type_dst == "int":
                rsp = self._update_port_qos(dst_ip, if_name_dst,
                                      down_minrate, down_maxrate)
                rsp_main[1] = rsp_main[1] + rsp[1]
                rsp_main[2] = rsp_main[2] + rsp[2]

        return rsp_main


    def _release_slice(self, params):

        slice_id = params['slice_id']
        if slice_id not in self.slice_repository:
            return [list(),
                    list(),
                    ["Slice with slice_id %s undefined" % slice_id]]

        slice = self.slice_repository[slice_id]

        instructions = dict()

        instructions["UP"] = list()

        if  slice[self.MSG_KEY__STATUS] == self.STATUS_VALUE__DEFINED or \
            slice[self.MSG_KEY__STATUS] == self.STATUS_VALUE__RELEASED:

            return [list(),
                    list(),
                    ["Slice is in status %s, cannot be released"\
                     % slice[self.MSG_KEY__STATUS]]]

        path = slice[self.MSG_KEY__PATH]

        osm = OvsScriptManager()

        for i, node in list(enumerate(path)):
            code = osm.destroy_flow_by_cookie(
                node[self.MSG_KEY__BRIDGE_NAME],
                slice[self.MSG_KEY__UP_COOKIE]
            )

            instructions["UP"].append(
                dict(
                    node_ip=node[self.MSG_KEY__NODE_IP],
                    code=code
                ))

        if slice[self.MSG_KEY__DUPLEX]:

            instructions["DOWN"] = list()

            for i, node in reversed(list(enumerate(path))):
                code = osm.destroy_flow_by_cookie(
                    node[self.MSG_KEY__BRIDGE_NAME],
                    slice[self.MSG_KEY__DOWN_COOKIE]
                )

                instructions["DOWN"].append(
                    dict(
                        node_ip=node[self.MSG_KEY__NODE_IP],
                        code=code
                    ))

        LOG.info("Releasing UP flow")

        rsp_up = [list(), list(), list()]
        rsp_down = [list(), list(), list()]

        for block in instructions['UP']:

            code = block['code']
            node_ip = block['node_ip']

            LOG.info("\n[UP] Run @%s: %s" % (node_ip, code))

            rsp = BashUtils.ssh_command(code, node_ip)

            rsp_up[1] = rsp_up[1] + rsp[1]
            rsp_up[2] = rsp_up[2] + rsp[2]

        if slice[self.MSG_KEY__DUPLEX]:

            LOG.info("Releasing DOWN flow")

            for block in instructions['DOWN']:
                code = block['code']
                node_ip = block['node_ip']

                LOG.info("\n[DOWN] Run @%s: %s" % (node_ip, code))

                rsp = BashUtils.ssh_command(code, node_ip)

                rsp_down[1] = rsp_down[1] + rsp[1]
                rsp_down[2] = rsp_down[2] + rsp[2]

        LOG.info("Removing QoS")

        node_src = path[0]
        LOG.debug("SRC NODE: %s " % str(node_src))
        node_dst = path[len(path)-1]
        LOG.debug("DST NODE: %s " % str(node_dst))

        if node_src[self.MSG_KEY__IF_TYPE_SRC] == 'int':
            if_name = node_src[self.MSG_KEY__IF_NAME_SRC]
            node_ip = node_src[self.MSG_KEY__NODE_IP]
            code = osm.destroy_port_qos(if_name)
            LOG.info("\n[UP] Remove QoS from src %s@%s" % (if_name, node_ip))
            rsp = BashUtils.ssh_command(code, node_ip)

            rsp_up[1] = rsp_up[1] + rsp[1]
            rsp_up[2] = rsp_up[2] + rsp[2]
        else:
            if_type = node_src[self.MSG_KEY__IF_TYPE_SRC]
            LOG.info("\n[UP] src type is %s: no QoS removal" % if_type)

        if node_dst[self.MSG_KEY__IF_TYPE_DST] == 'int':
            if_name = node_dst[self.MSG_KEY__IF_NAME_DST]
            node_ip = node_dst[self.MSG_KEY__NODE_IP]
            code = osm.destroy_port_qos(if_name)
            LOG.info("\n[DOWN] Remove QoS from dst %s@%s" % (if_name, node_ip))
            rsp = BashUtils.ssh_command(code, node_ip)

            rsp_down[1] = rsp_down[1] + rsp[1]
            rsp_down[2] = rsp_down[2] + rsp[2]
        else:
            if_type = node_src[self.MSG_KEY__IF_TYPE_SRC]
            LOG.info("\n[DOWN] dst type is %s: no QoS removal" % if_type)

        slice[self.MSG_KEY__STATUS] = self.STATUS_VALUE__RELEASED

        return rsp_up

    # def add_interface(self, params):
    #
    #     try:
    #
    #         assert isinstance(params, dict)
    #         assert self.MSG_KEY__NODE_FINS in params or \
    #                self.MSG_KEY__NODE_JFED in params
    #         br = None
    #
    #         if self.MSG_KEY__NODE_FINS in params:
    #             for index in self.fins_bridge_decriptor:
    #                 br = self.fins_bridge_decriptor[index]
    #                 fins_id = br['associated_resource']
    #                 if fins_id == params[self.MSG_KEY__NODE_FINS]:
    #                     br = self.fins_bridge_decriptor[index]
    #                     break
    #         elif self.MSG_KEY__NODE_JFED in params:
    #             for index in self.fins_bridge_decriptor:
    #                 br = self.fins_bridge_decriptor[index]
    #                 jfed_id = br['associated_resource_jfed']
    #                 if jfed_id == params[self.MSG_KEY__NODE_JFED]:
    #                     br = self.fins_bridge_decriptor[index]
    #                     break
    #
    #         assert br is not None
    #
    #         osm = OvsScriptManager()
    #         bridge_name = br['id']
    #         if_name = params[self.MSG_KEY__IF_NAME]
    #         if_ofport = params[self.MSG_KEY__IF_OFPORT]
    #         if_ip = params[self.MSG_KEY__IF_IP]
    #         if_netmask = params[self.MSG_KEY__IF_NETMASK]
    #
    #         already_in = False
    #         for iface in br['interfaces']['int']:
    #             if iface['id'] == if_name:
    #                 already_in = True
    #                 break
    #         if not already_in:
    #             br['interfaces']['int'].append(
    #                 dict(
    #                     id=if_name,
    #                     ip=if_ip,
    #                     netmask=if_netmask,
    #                     of_port=if_ofport
    #                 )
    #             )
    #             LOG.debug("inserted new interface %s", if_name)
    #
    #         cmd = list()
    #         cmd.append(
    #             osm.add_internal_interface(bridge_name, if_name))
    #         cmd.append(
    #             osm.assign_ofport_to_interface(if_name, if_ofport))
    #         cmd.append(
    #             osm.set_interface_up(if_name, if_ip, if_netmask))
    #
    #         cmd = " && ".join(cmd)
    #
    #         LOG.info("run @%s: \n%s", br['ip'], cmd)
    #
    #         rsp = ""  #  BashUtils.ssh_command(cmd, br['ip'])
    #
    #         return rsp
    #
    #     except Exception as e:
    #         LOG.error(e)
    #         traceback.print_exc()
    #
    #         return [[], [], [str(e)]]
    #
    # def del_interface(self, params):
    #
    #     try:
    #
    #         assert isinstance(params, dict)
    #         assert self.MSG_KEY__NODE_FINS in params or \
    #                self.MSG_KEY__NODE_JFED in params
    #
    #         assert self.MSG_KEY__IF_NAME in params
    #
    #         br = None
    #
    #         if self.MSG_KEY__NODE_FINS in params:
    #             for index in self.fins_bridge_decriptor:
    #                 br = self.fins_bridge_decriptor[index]
    #                 fins_id = br['associated_resource']
    #                 if fins_id == params[self.MSG_KEY__NODE_FINS]:
    #                     br = self.fins_bridge_decriptor[index]
    #                     break
    #         elif self.MSG_KEY__NODE_JFED in params:
    #             for index in self.fins_bridge_decriptor:
    #                 br = self.fins_bridge_decriptor[index]
    #                 jfed_id = br['associated_resource_jfed']
    #                 if jfed_id == params[self.MSG_KEY__NODE_JFED]:
    #                     br = self.fins_bridge_decriptor[index]
    #                     break
    #
    #         assert br is not None
    #
    #         osm = OvsScriptManager()
    #         bridge_name = br['id']
    #         if_name = params[self.MSG_KEY__IF_NAME]
    #
    #         elem = None
    #         for iface in br['interfaces']['int']:
    #             if iface['id'] == if_name:
    #                 elem = iface
    #                 break
    #         if elem is not None:
    #             br['interfaces']['int'].remove(elem)
    #             LOG.debug("removed interface %s", if_name)
    #
    #         cmd = osm.delete_port(if_name, bridge_name)
    #
    #         LOG.info("run @%s: \n%s", br['ip'], cmd)
    #
    #         rsp = "" #  BashUtils.ssh_command(cmd, br['ip'])
    #
    #         return rsp
    #
    #     except Exception as e:
    #         LOG.error(e)
    #         traceback.print_exc()
    #
    #         return [[], [], [str(e)]]
