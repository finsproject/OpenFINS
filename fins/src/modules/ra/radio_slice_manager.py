# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
RA Module for Wishful Controller

Notes: 
- the default path for config file is "./fins/confs", and 
it is stored in SliceManager.CONFIG_PATH 
- the default path for library recipes is SliceManager.CONFIG_PATH/recipe-libraries
"""

__docformat__ = "epytext" # http://epydoc.sourceforge.net/manual-epytext.html
__author__    = "Cristina Costa (CREATE-NET FBK)"
__copyright__ = "Copyright (c) 2018, CREATE-NET FBK"
__version__   = "FINS Radio Slices module V.1.0"
# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import logging
import traceback
import json
import os.path
import time
import configobj

# -------- Logging configuration constants
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(process)d-%(levelname)s-%(message)s'
# LOG_FORMAT          = '%(name)s.%(funcName)s() - %(levelname)s - %(message)s'
# LOG_FORMAT          = '%(asctime)s - %(name)s.%(funcName)s() - %(levelname)s - %(message)s'
LOG_FORMAT          = '%(asctime)s %(levelname)s: %(name)s.%(funcName)s() - %(message)s'
LOG_FORMAT_OUT      = '%(levelname)s: %(message)s'
LOG_FORMAT          = "[%(filename)s:%(lineno)s - %(funcName)s()] %(levelname)s: %(message)s"
LOG_FORMAT          = "[%(lineno)s] %(message)s"
LOG_DATE_FORMAT     = '%d-%b-%y %H:%M:%S'
LOG_LEVEL           = 'DEBUG'

LOG_FILE = "radio_slice_manager.log"
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
# Stream handler
# If no stream is specified, sys.stderr will be used.
# c_handler = logging.StreamHandler(stream=sys.stdout)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.DEBUG)
#c_format = logging.Formatter(LOG_FORMAT_OUT, LOG_DATE_FORMAT)   
c_format = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)   
c_handler.setFormatter(c_format)

# File handler
f_handler = logging.handlers.RotatingFileHandler(
              LOG_FILE, maxBytes=500000, backupCount=5)
#f_handler = logging.FileHandler(logfile)
f_handler.setLevel(logging.DEBUG)
f_format = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)
f_handler.setFormatter(f_format)

# Add handlers to the logger
LOG.addHandler(f_handler)    
LOG.addHandler(c_handler)
    
class RadioNode:
    """
    RadioNode Class
    ==========================
    Keeps track of node parameters and rabbitMQ queues associated to each node
    """
    
    TAP_BASE_NAME = "tap"
    TAP = 1
    XMLRTP_DEFAULT_PORT = 1235
    SIMRADIO_BASE_PORTS = 6000
    CU_BASE_PORT = 5000
    RU_BASE_PORT = 5100
    STATUS = {"CONNECTED":True,"NOT_CONNECTED":False}
    
    # --- Node Types
    """
    RADIO_NODE = 0 # node with one usrp
    VR_NODE = 1 # node with one usrp, implementing multiple virtual radios
    RU_NODE = 2 # node with one usrp, implementing a distributed unit node coupled with a central unit node
    CU_NODE = 3 # node without usrp, implementing a centra unit node
    """
    NODE_TYPE = ["DEFAULT","RADIO_NODE","VR_NODE","RU_NODE","CU_NODE"]
    RADIO_NAME = ["radio","vr","ru","cu"]
    RADIO_NAME_BASE = 0
    DEFAULT_NODE = NODE_TYPE.index("RADIO_NODE")
    
    def __init__(self, node_name, **kwargs):
        """
        Init RadioNode object
        
        @param node_name: node_name
        @type  node_name: str
        @param kwargs: kwargs
        @type  kwargs: **dict
        @param tap_names: kwargs (node_type=None, tap_names=None, xmlrtp_ports=None)
        """              
        config = dict()
        if kwargs:
            config = kwargs.copy() 
        
        self.name            = node_name   
        # --- TAP ifaces avaliable on node  
        self.__taps           = config.get("taps", [])
        # --- IP address and ports used on node
        self.__local_ports    = config.get("_local_ports", [])
        self.__local_ru_ports = config.get("_local_ru_ports", [])
        self.__local_cu_ports = config.get("_local_cu_ports", [])
        self.ip              = config.get("ip",  "*") # to check
        # --- Node information
        self.node_type       = config.get("node_type",  self.DEFAULT_NODE)
        self.xmlrtp_port     = config.get("xmlrtp_port", self.XMLRTP_DEFAULT_PORT)
        self.description     = config.get("description", "Radio host {} [type {}, ip {}, XMLRCP port {}]".format(self.name, self.node_type, self.ip, self.xmlrtp_port))
        # --- Wishful framework specific information
        self.info            = config.get("info", {})
        # --- Node status on controller
        self.status          = config.get("status", self.STATUS["NOT_CONNECTED"])
        
        LOG.info("------ Creating a new node \"%s\"", self.name)
        if self.description:
            LOG.info(self.description)
        if self.__taps:
            LOG.info("Avaliable taps: %s", repr(self.__taps))
        if self.xmlrtp_port:
            LOG.info("Program port (XMLRPC server): %d", self.xmlrtp_port)
        if self.node_type:
            LOG.info("Node type: %s", RadioNode.NODE_TYPE[self.node_type])
        LOG.info("-------------------------------------")
        # --- Radios
        self.radios = []
        radios      = config.get("radios", [{"radio1":{}}])
        self.__assigned_taps = []
                        
        for radio in radios:
            if "tap" in radio:
                if self.__taps.count(radio["tap"]) == 0:
                    LOG.warning("Assigned tap \"%s\" not valid",radio["tap"])
                self.__assigned_taps.append(radio["tap"])
            if "local_port" in radio:
                self.__local_ports.append(radio["local_port"])
            if "local_cu_port" in radio:
                self.__local_cu_ports.append(radio["local_cu_port"])
            if "local_ru_port" in radio:
                self.__local_ru_ports.append(radio["local_ru_port"])
            if "radio_name" in radio:
                radio_name = radio["radio_name"]
            else:
                radio_name = "radio" + str(len(self.radios))
                
            radio_id = self.__add_radio(radio_name, bool("cu_node" in radio), bool("ru_node" in radio))
            
            if "remote_host" in radio:
                self.radios[radio_id]["remote_host"] = radio["remote_host"]
            if "remote_radio" in radio:
                self.radios[radio_id]["remote_radio"] = radio["remote_radio"]
            #if self.node_type == self.NODE_TYPE.index("RU_NODE"):
            if "cu_node" in radio:
                self.radios[radio_id]["cu_node"] = radio["cu_node"]
                if "cu_radio" in radio:
                    self.radios[radio_id]["cu_radio"] = radio["cu_radio"]
            #if self.node_type == self.NODE_TYPE.index("CU_NODE"):
            if "ru_node" in radio:
                self.radios[radio_id]["ru_node"] = radio["ru_node"]
                if "ru_radio" in radio:
                    self.radios[radio_id]["ru_radio"] = radio["ru_radio"]
          
    def __repr__ (self):
        """
        Radio Node string representation
        """
        rval = self.get_dict()                
        LOG.debug("RadioNode: \"%s\"", json.dumps(rval, indent=3)) 
        return json.dumps(rval)

    def get_dict(self):
        """
        Radio Node dictionary representation
        """
        rval = dict()
        rval["node_name"]        = self.name
        rval["info"]             = self.info
        rval["status"]           = self.status
        rval["description"]      = self.description
        rval["type"]             = self.node_type
        rval["tap_list"]         = self.__taps
        rval["local_host"]       = self.ip
        rval["local_ports"]      = self.__local_ports
        rval["local_ru_ports"]   = self.__local_ru_ports
        rval["local_cu_ports"]   = self.__local_cu_ports
        rval["radios"] = []
        for radio in self.radios:
            rval["radios"].append(radio["radio_name"])
        LOG.debug("RadioNode: Dict rapresentation for node \"%s\"", self.name) 
        return rval
        
    def get_context(self):
        """
        Node context dictionary
        """
        rval = dict()
        rval["node_name"]        = self.name
        rval["info"]             = self.info
        rval["description"]      = self.description
        rval["type"]             = self.node_type
        rval["local_host"]       = self.ip
        return rval
        
    def assign_tap(self, radio_id, tap):
        if tap is not None and radio_id  <len(self.radios) and self.__assigned_taps[radio_id] != tap:
            LOG.info("Assigning new tap {} to radio {}.{}".format(tap, self.name, radio_id))
            self.__assigned_taps[radio_id] = tap
            self.radios[radio_id]["tap"] = tap
        
    def __add_radio(self, radio_name, has_ru_node = False, has_cu_node = False):
        """
        add radio to node
        
        @param radio_name: radio_name
        @type  radio_name: str   
        @return:         radio_id
        @rtype:          int
        """
        
        radio_id = len(self.radios)
        new_radio = dict()
        new_radio["id"]          = radio_id
        new_radio["radio_name"]  = radio_name
        new_radio["node_name"]   = self.name
        new_radio["local_host"]  = self.ip
        new_radio["remote_node"]  = None
        new_radio["remote_radio"] = None
        
        # --- Configuration of simulated link with remote radio host
        if radio_id >= len(self.__local_ports):
            new_radio["local_port"] = self.SIMRADIO_BASE_PORTS + radio_id
            self.__local_ports.append(new_radio["local_port"])
        else:
            new_radio["local_port"] = self.__local_ports[radio_id]
        new_radio["remote_node"] = None
        new_radio["remote_radio"] = None
            
        # --- Configuration of link with RU host
        #if self.node_type ==  self.NODE_TYPE.index("RU_NODE"):
        if radio_id >= len(self.__local_ru_ports):
            new_radio["local_ru_port"] = self.RU_BASE_PORT + radio_id
            self.__local_ru_ports.append(new_radio["local_ru_port"])
        else:
            new_radio["local_ru_port"] = self.__local_ru_ports[radio_id]
        new_radio["cu_node"]  = None
        new_radio["cu_radio"] = None
         
        # --- Configuration of link with CU host
        #if self.node_type ==  self.NODE_TYPE.index("CU_NODE"):
        if radio_id >= len(self.__local_cu_ports):
            new_radio["local_cu_port"] = self.CU_BASE_PORT + radio_id
            self.__local_cu_ports.append(new_radio["local_cu_port"])
        else:
            new_radio["local_cu_port"] = self.__local_cu_ports[radio_id]
        new_radio["ru_node"]  = None
        new_radio["ru_radio"] = None
        
        # --- tap interface
        #if self.node_type != self.NODE_TYPE.index("RU_NODE"):
        if radio_id >= len(self.__assigned_taps):
            new_radio["tap"] = self.TAP_BASE_NAME + str(self.TAP + radio_id)
            self.__assigned_taps.append(new_radio["tap"])
        else:
            new_radio["tap"] = self.__assigned_taps[radio_id]
        #else:
            #new_radio["tap"] = None
            #self.__assigned_taps.append(new_radio["tap"])
                
        self.radios.append(new_radio)
    
        LOG.debug("RadioNode: new radio %s.%d added, radio name = \"%s\", tap=%s", self.name, new_radio["id"], new_radio["radio_name"], new_radio["tap"]) 
        return radio_id
        
    def get_program_port(self):
        """
        returns XMLRTP server port
        
        @return:       port number
        @rtype:        int
        """
        return self.xmlrtp_port
        
    def get_radioid_by_name(self, radio_name):
        """
        returns radio
        
        @param radio_id: radio_name
        @type  radio_id: str       
        @return:         radio id
        @rtype:          int
        """
        for radio in self.radios:
            if radio["radio_name"] == radio_name:
                return radio["id"]      
        return None
        
    def get_radioid_by_tap(self, tap):
        """
        returns radio
        
        @param radio_id: tap
        @type  radio_id: str      
        @return:         radio id
        @rtype:          int
        """
        for radio in self.radios:
            if radio["tap"] == tap:
                return radio["id"]        
        return None
  
    def update_radio_context(self, context, radio_id=0):
        """
        updates radio context
        
        @param context:  context
        @type  context:  dict
        @param radio_id: radio id
        @type  radio_id: int
        """
        self.radios[radio_id].update(context)
        
        LOG.debug("RadioNode: Radio \"%s\" (%s) updated", self.name, str(radio_id)) 
        
    def delete_radio_context(self, radio_id):
        """
        updates radio context
        
        @param radio_id: radio id
        @type  radio_id: int
        """
        self.radios[radio_id] = dict()
        
        LOG.debug("RadioNode: Radio \"%s\" (%s) deleted", self.name, str(radio_id)) 
                

class RadioSlice:
    """
    RadioNode Class
    ==========================
    Keeps track of node parameters and rabbitMQ queues associated to each node
    """
    
    def __init__ (self, slice_name):
        """
        Init RadioSlice object
        
        @param slice_name: slice_name
        @type  slice_name: str
        @return: radios assigned to slice
        @type:   list of radios 
        """      
        
        self.name  = slice_name
        self.assigned_radios = []
        LOG.debug("-------- RadioSlice: Created a new slice \"%s\"", self.name) 
        
    def __repr__ (self):
        """
        Alternate representation of RadioSlice
        """
        rval  = "Slice name {}, assigned radios: {}".format(self.name, repr(self.assigned_radios))            
        return rval
                
    def clear(self):
        """
        Clear slice to default
        """      
        self.assigned_radios = []
        
    def assigned(self, radio):
        """
        Check if a radio is assigned to the slice
        """      
        if radio in self.assigned_radios:
            return True
        else:
            return False 
            
 
class SliceManager:
    # --- IRIS NODE TYPES
    NODE_TYPE = ["RES.DEFAULT","RES.IRIS_USRP","RES.IRIS_HYDRA","RES.IRIS_HYDRAEMU_SB_VR","RES.IRIS_HYDRAEMU_NB"]
    DEFAULT_NODE_TYPE = NODE_TYPE[0]
    
    # --- Default slice    
    DEFAULT_SLICE =  "default"
    
    # --- Config default path
    DEFAULT_NODES_FILE = "FINS_configuration.cfg"
    #CONFIG_PATH = "./"
    CONFIG_PATH = "./fins/confs/"
    DEFAULT_DESCRIPTOR_FILE = "radio_slices.json"
    DESCRIPTORS_PATH = CONFIG_PATH
    
    # --- Recipes Library Defaults
    LIBRARIES_PATH = CONFIG_PATH + "recipe-libraries/"
    
    # --- Recipe book defautls:
    DEFAULT_LIBRARY_NAME = "default_library"
    DEFAULT_RECIPES_BOOK = "init"
    
    DEFAULT_ASSIGN_BOOK = "update"
    DEFAULT_UPDATE_BOOK = "update"
    DEFAULT_INIT_BOOK = "init"
    
    DEFAULT_RADIO_PROGRAM = "test"
    DEFAULT_RADIO_PROGRAM_TYPE = "grc"
    DEFAULT_RADIO_PROGRAM_PORT = 1234
    
    DEFAULT_LIBRARY = {
            "info":{    
                "name":DEFAULT_LIBRARY_NAME,
                "description":"Default library",
                "version":"1.0",
                "node_types":[DEFAULT_NODE_TYPE]
                },
            "node_context":{
                "default":{
                    DEFAULT_NODE_TYPE:{"radio_program":DEFAULT_RADIO_PROGRAM, "program_port":DEFAULT_RADIO_PROGRAM_PORT, "program_type":DEFAULT_RADIO_PROGRAM_TYPE},
                    "*":{}
                    }
                },
            "radio_context":{        
                "default":{
                    DEFAULT_NODE_TYPE:{},
                    "*":{}
                    }
            },
            "recipes_library":{
                DEFAULT_INIT_BOOK:{DEFAULT_NODE_TYPE:{"action_list":[]}},
                DEFAULT_ASSIGN_BOOK:{DEFAULT_NODE_TYPE:{"action_list":[]}},
                DEFAULT_UPDATE_BOOK:{DEFAULT_NODE_TYPE:{"action_list":[]}}
            }
    }
    
    def __init__(self, **kwargs):
        """
        init radio slices data structure, and create default slice
        
        @param recipe_library:  recipe library
        @type  recipe_library:  dict
        """
        config = dict()
        self.recipe_library = dict()
        if kwargs:
            config = kwargs.copy() 
        
        LOG.debug("-------- SliceManager: Creating a new slice manager") 
        # ---- Recipes library
        nodes_info   = kwargs.get("nodes_info", dict() )
        library_info = kwargs.get("library_info", dict() )
        slices_info  = kwargs.get("slices_info", dict() )
        
        self.nodes  = dict()
        self.slices = dict()
             
        # --- load library
        self.load_recipe_library(library_info)   
        # --- add the default slice  
        self.add_radio_slice(RadioSlice(self.DEFAULT_SLICE))
                
        if nodes_info:
            self.load_nodes(nodes_info)
        
        if slices_info:
            self.load_slices(slices_info)   
            
        for node in self.nodes:
            for radio in self.nodes[node].radios:
                slice_name = self.get_radio_slice(radio)  
                if slice_name:
                    if self.nodes[node].node_type == RadioNode.NODE_TYPE.index("RU_NODE"):
                        if "cu_node" in radio and "cu_radio" in radio:                        
                            if type(radio["cu_radio"]) is str:
                                cu_radio_id = self.get_radio_from_name(radio["cu_node"], radio["cu_radio"])["id"]
                            else:
                                cu_radio_id = radio["cu_radio"]
                            self.assign_radio_slice(slice_name, [{"node_name":radio["cu_node"],"id":cu_radio_id}])
                    
            
        LOG.debug("-------- SliceManager: New slice manager created") 
        return
        
    def __repr__ (self):
        """
        Alternate representation for SliceManager
        
        @return:   Rado Manager info
        @rtype:    str
        """
        rval  = "SliceManager: "
        rval += "recipe_library     = {}; ".format(self.recipe_library_name)
        rval += "assigned_slices    = {}; ".format(self.assigned_slices())
        rval += "Nodes: "
        for node_name in self.nodes:
            rval += node_name + " "
        rval += "Slices: "
        for slice_name in self.slices:
            rval += slice_name + " "
                
        LOG.debug("SliceManager: Repr for SliceManager: %s", rval) 
        return rval
            
    def get_dict(self):
        """
        Dictionary representation for SliceManager
        
        @return:   Rado Manager info
        @rtype:    dict
        """
        rval = dict()
        rval["recipe_library"] = self.recipe_library_name 
        rval["recipe_books"]   = []
        for item in self.recipe_library["recipes_library"]:
            rval["recipe_books"].append(item)
        rval["nodes"] = dict()
        for node_name in self.nodes:
            for radio in self.nodes[node_name].radios:
                rval["nodes"][node_name] = {"radio_name":radio.get("radio_name"),"tap":radio.get("tap")}
        rval["slices"] = dict()
        for slice_name in self.slices:
            rval["slices"][slice_name] = self.slices[slice_name].assigned_radios
        
        LOG.debug("SliceManager: Dict rapresentation for SliceManager: %s", repr(rval)) 
        return rval
    
    def get_radio_from_tap(self, node_name, tap):
        radio = None
        if node_name in self.nodes:
            radio_id = self.nodes[node_name].get_radioid_by_tap(tap)
            if radio_id:
                radio = self.nodes[node_name].radios[radio_id]
        return radio
        
    def get_radio_from_name(self, node_name, name):
        radio = None
        if node_name in self.nodes:
            radio_id = self.nodes[node_name].get_radioid_by_name(name)
            if radio_id is not None:
                radio = self.nodes[node_name].radios[radio_id]
        return radio
        
    def assigned_slices(self):
        rval = dict()
        for slice_name in self.slices:
            rval[slice_name] = self.slices[slice_name].assigned_radios.copy()
        return slice_name
               
    def load_nodes(self, nodes):
        LOG.info("SliceManager: --- Loading nodes")
        for node_name, config in nodes.items():
            if node_name not in self.nodes:
                node = RadioNode(node_name, **config)  
                self.add_node(node)
            else:
                node = self.nodes[node_name]
                if "ip" in config:
                    node.ip = config["ip"]
                if "xmlrtp_port" in config:
                    node.xmlrtp_port = config["xmlrtp_port"]
                if "description" in config:
                    node.description = config["description"]
                if "info" in config:
                    node.info = config["info"]
                if "radios" in config:
                    radios = config["radios"]
                for radio in radios:
                    radio_id = None  
                    if "id" in radio:
                        radio_id = radio["id"]
                        if "radio_name" in radio:
                             node.radios[radio_id]["radio_name"] = radio["radio_name"]
                    else:
                        if "radio_name" in radio:
                            radio_id = node.get_radioid_by_name(radio["radio_name"])
                        if radio_id is None and "tap" in radio:
                            radio_id = node.get_radioid_by_tap(radio["tap"])    
                            
                    if radio_id is not None:                        
                        if "tap" in radio:
                            node.assign_tap(radio_id, radio["tap"])                            
                        if "cu_node" in radio:
                            node.radios[radio_id]["cu_node"] = radio["cu_node"]
                        if "cu_radio" in radio:
                            node.radios[radio_id]["cu_radio"] = radio["cu_radio"]
                        if "ru_node" in radio:
                            node.radios[radio_id]["ru_node"] = radio["ru_node"]
                        if "ru_radio" in radio:
                            node.radios[radio_id]["ru_radio"] = radio["ru_radio"]
                        if "peer_node" in radio:
                            node.radios[radio_id]["peer_node"] = radio["peer_node"]
                        if "peer_radio" in radio:
                            node.radios[radio_id]["peer_radio"] = radio["peer_radio"]
                            
        return
        
    def load_recipe_library(self, library_info = dict()):
        """
        Loads default recipe library from file.
        If recipe_library equals to None, loads the default library
        
        @param library_name:     name of the recipes book library
        @type  library_name:     string
        @param library_path:     path of the recipes book library
        @type  library_path:     string
        @return:       action_list
        @rtype:        int
        """
        self.recipe_library = dict()
        recipe_library = library_info.get("library_name")
        library_path   = library_info.get("library_path", self.LIBRARIES_PATH)
        node_context   = library_info.get("node_context")
            
        print(" -------- SliceManager: Loading recipe library")
        if recipe_library:
            LOG.info(" -------- SliceManager: Loading recipe library \"%s\"", recipe_library)
            print(" -------- SliceManager: Loading recipe library \"%s\"", recipe_library)
            filename = library_path + recipe_library + "_recipe_library.json"
            try:
                with open(filename, 'r') as data_file:
                    self.recipe_library = json.load(data_file)
            except Exception as e:
                LOG.error(">>>>>>>>>>>>>>>>>>>> SliceManager: Cannot load recipe library from file {} ({})".format(filename,e))
                self.recipe_library_name = self.DEFAULT_LIBRARY_NAME
                self.recipe_library = self.DEFAULT_LIBRARY
                LOG.info(">>>>>>>>>>>>>>>>>>>> SliceManager: Setting recipe library to default library")
            except:
                raise
            else:
                self.recipe_library_name = recipe_library
                
        if "recipes_library" not in self.recipe_library:
            self.recipe_library_name = self.DEFAULT_LIBRARY_NAME
            self.recipe_library = self.DEFAULT_LIBRARY
            LOG.info("SliceManager: --- Setting recipe library to default library")
        if self.recipe_library.get("info") is None:
            self.recipe_library["info"] = dict()
            self.recipe_library["info"]["name"] = self.recipe_library_name
            self.recipe_library["info"]["description"] = "NA"
            self.recipe_library["info"]["version"] = "NA"
            self.recipe_library["info"]["node_types"] = []  
        if "node_context" not in self.recipe_library:
            self.recipe_library["node_context"] = dict()
        if "radio_context" not in self.recipe_library:
            self.recipe_library["radio_context"] = dict()
            
        if node_context:
            #self.recipe_library["node_context"] = dict()
            self.recipe_library["node_context"].update(node_context)
        self.recipe_library["library_config"]      = library_info.get("library_config", "default")
        self.recipe_library["init_book"]           = library_info.get("init_book", self.DEFAULT_INIT_BOOK)
        self.recipe_library["update_book"]         = library_info.get("update_book", self.DEFAULT_UPDATE_BOOK)
        self.recipe_library["assign_book"]         = library_info.get("assign_book", self.DEFAULT_ASSIGN_BOOK)
        LOG.info("SliceManager: recipe library init book \"%s\", update book \"%s\", assign book \"%s\", config \"%s\"", self.recipe_library["init_book"], self.recipe_library["update_book"], self.recipe_library["assign_book"], self.recipe_library["library_config"])  
        

        return
        
    def load_slices(self, slices):
        self.slices = dict()
        self.add_radio_slice(RadioSlice(self.DEFAULT_SLICE))
        for slice_name, config in slices.items():
            self.add_radio_slice(RadioSlice(slice_name))
            if "radio_nodes" in config:
                self.assign_radio_slice(slice_name, config["radio_nodes"])
            if "slice_context" in config:
                self.update_radio_slice(slice_name, config["slice_context"])
        
                
    def add_radio_slice(self, radio_slice, slice_context=None): 
        """
        add a slice to the repo, if slice_name exists, overrite
        after an overrite, slice must be updated to reflect the change on nodes
        
        @param node_name:  slice
        @type  node_name:  RadioSlice
        @return:       error code: 0 no error, 1 context format not valid
        @rtype:        int
        """
        rval = 0
        if type(radio_slice) is RadioSlice:
            
            if radio_slice.name in self.slices:
                del self.slices[radio_slice.name]
            
            self.slices[radio_slice.name] = radio_slice
            
            #LOG.debug("SliceManager: Adding a new slice \"%s\"", radio_slice.name) 
            if slice_context:
                self.recipe_library["radio_context"][radio_slice.name] = slice_context
                LOG.debug("SliceManager: Updating slice context = %s", repr(slice_context))
        else:
            LOG.debug("SliceManager: Couldn't a new slice, wrong type") 
            rval = 1
        return rval
        
    def add_node(self, node):
        """
        add a node to the repo, if node_name exists, overrite
        after an overrite, assigned slice must be updated
        
        @param node_name:  node 
        @type  node_name:  RadioNode
        
        """
        rval = 0
        if type(node) is RadioNode:
            self.nodes[node.name] = node
            #LOG.debug("SliceManager: Adding new node \"%s\" to Slice Manager", node.name)  
            # --- assign node to the DEFAULT_SLICE 
            r = []
            for radio in node.radios:
                r.append((node.name, node.radios.index(radio)))
        else:
            LOG.debug("SliceManager: Couldn't add a new node, wrong type") 
            rval = 1
        return rval
            
    def get_node_list(self):
        """
        Return a list of nodes
        
        @return:       node list
        @rtype:        list of str
        """ 
        rval = []     
        for node in self.nodes:
            rval.append(node.name)
        LOG.debug("SliceManager: Getting node list %s", rval) 
        
        return rval
            
    def get_radios(self, slice_name):
        """
        get nodes assigned to a radio.
        
        @param slice_name: slice name
        @type  slice_name: str
        @return:           radios list
        @rtype:            list of tuples
        """
        LOG.debug("SliceManager: Getting nodes assigned to slice \"%s\": %s", slice_name, self.slices[slice_name].assigned_radios) 
        return self.slices[slice_name].assigned_radios
        
    def assign_radio_slice(self, slice_name, radio_list):
        """
        assign a radio on a node to a radio slice, 
        returns the action list for the node.
        does nothing if the slice does not exist (retunrs an empty list).
        
        @param slice_name: slice name
        @type  slice_name: str
        @param radio: radio list of dict
        @type  radio: tuple
        @param kwargs: kwargs
        """
        action_list = []
        node = None
        
        # --- check if it is valid slice name
        if not slice_name and slice_name not in self.slices:
            LOG.debug("SliceManager: slice name not valid") 
            error = True
        else:        
            for radio in radio_list:
                if radio["node_name"] in self.nodes:
                    if "id" not in radio:
                        if "tap" in radio:
                            rr = self.get_radio_from_tap(radio["node_name"], radio["tap"])
                        elif "radio_name" in radio:
                            rr = self.get_radio_from_name(radio["node_name"], radio["radio_name"])
                        else:
                            rr = self.nodes[radio["node_name"]].radios[0]  
                        if rr:  
                            radio.update(rr)  
                        else:
                            radio["id"] = 0
                            
                    #LOG.info("SliceManager: Assigning slice \"%s\" to radio %s", slice_name, radio)
                    LOG.info("SliceManager: Assigning radio %s.%d  to slice \"%s\"", radio["node_name"], radio["id"], slice_name)
                    
                    # --- check radios
                    radio = tuple([radio["node_name"],radio["id"]])
                    error = False
                    # RADIO_NODE or VR_NODE or RU_NODE
                    if type(radio) is not tuple:
                        LOG.info("SliceManager ERROR: radio node wrong format")
                        return []
                    elif not self.nodes.get(radio[0]):
                        LOG.info("SliceManager ERROR: radio host not valid")
                        return []
                    elif radio[1] > len(self.nodes[radio[0]].radios):
                        LOG.info("SliceManager ERROR: radio host not valid")
                        return []
                    else:
                        # --- remove radio from assigned slice
                        for sname in self.slices:
                            if radio in self.slices[sname].assigned_radios:
                                self.slices[sname].assigned_radios.remove(radio)
                                LOG.debug("SliceManager: removing radio %s.%d from slice \"%s\":", radio[0],radio[1], sname)            
                        self.slices[slice_name].assigned_radios.append(radio)
                                      
        return action_list
        
        
    def get_radio_slice(self, radio):
        if "id" in radio:
            radio_id = radio["id"]
        elif "radio_name" in radio:
            radio_id = self.get_radio_from_name(radio["node_name"], radio["radio_name"])
        elif "tap" in radio:
            radio_id = self.get_radio_from_tap(radio["node_name"], radio["tap"])
        else:
            return None
                 
        for slice_name in self.slices:
            if tuple([radio["node_name"],radio_id]) in self.slices[slice_name].assigned_radios:
                return slice_name
        return None        
        
    def info_recipe_library(self):
        """
        Loads default recipe library from file.
        If recipe_library equals to None, loads the default library
        
        @return:       info data
        @rtype:        int
        """
        
        rval = self.recipe_library.get("info")
        if rval is None:
            rval.setdefault("name", self.recipe_library_name)
            rval.setdefault("description","No info")
            rval.setdefault("version","No info")
            rval.setdefault("node_types",[])
        
        return rval       
        
    def update_recipe_library(self, node_context=None, lib_config="default"):
        """
        Loads default recipe library from file.
        If recipe_library equals to None, loads the default library
        
        @param library_name:     name of the recipes book library
        @type  library_name:     string
        @param library_path:     path of the recipes book library
        @type  library_path:     string
        @return:       action_list
        @rtype:        int
        """
        action_list = []
        LOG.info("SliceManager: --- Updating recipe library, update book  \"%s\"", update_book)

        if node_context:
            if lib_config not in self.recipe_library["node_context"]:
                self.recipe_library["radio_context"][lib_config] = dict()
            self.recipe_library["node_context"][lib_config].update(node_context)
        else:
            LOG.info("SliceManager: empty input node context")
                                    
        return self.commit_context(lib_config = lib_config, book = self.recipe_library["update_book"])
            
    
    def update_radio_slice(self, slice_name, slice_context):
        """
        update slice context of a slice
        
        @param slice_name: slice name
        @type  slice_name: str
        @param slice_context: slice_context
        @type  slice_context: dict
        @return:           action list, None if error
        @rtype:            list of dict
        """
        action_list = []
        
        if slice_context:
            if slice_name in self.recipe_library["radio_context"]:
                self.recipe_library["radio_context"][slice_name].update(slice_context)
            else:
                self.recipe_library["radio_context"][slice_name] = slice_context
            LOG.debug("SliceManager: updating slice \"%s\" context", slice_name) 
            
        action_list = self.commit_context(slice_name = slice_name, book = self.recipe_library["update_book"]) 
                                    
        return action_list
        
    def commit_context(self, **kwargs):
        
        commit_book  = kwargs.get("commit_book", self.recipe_library["init_book"]  ) 
        config       = kwargs.get("lib_config", self.recipe_library["library_config"]  ) 
        slice_name   = kwargs.get("slice_name", None)
        
        LOG.debug("SliceManager: commit context for slice \"%s\", lib config \"%s\", book \"%s\" - generating action list", slice_name, config, commit_book) 
        
        action_list = []
        if slice_name:
            radios = self.slices[slice_name].assigned_radios
            for radio in radios:
                action_list.extend(self.__get_action_list(node_name = radio[0], radio_id = radio[1], book = commit_book))
        else:
            for node in self.nodes:
                action_list.extend(self.__get_action_list(node_name = node, book = commit_book, lib_config = config))
        return action_list
        
    def commit_library(self, commit_book =  None, config = None): 
        commit_book =  self.recipe_library["init_book"]  
        if config:                                 
            return self.commit_context(commit_book = commit_book, lib_config = config)
        else:
            return self.commit_context(commit_book = commit_book)
    
    def commit_slice(self, slice_name, commit_book = None):
        commit_book =  self.recipe_library["assign_book"]  
        action_list = self.commit_context(commit_book = commit_book, slice_name = slice_name)
        return action_list
        
    def __get_action_list(self, **kwargs):
        """
        get action list for initializing nodes and radios       
        
        @param node_name:   node name
        @type  node_name:   str
        @param radio_       id: radio id
        @type  radio_       id: int
        @param book:        library book to apply
        @type  book:        str
        @param lib_config:  library configuration default
        @type  lib_config:  str
        @return:            action list, empty if error
        @rtype:             list
        """
        # ---- Defaults
        validated_action_list = []     
        
        node_name = kwargs.get("node_name")
        radio_id  = kwargs.get("radio_id")
        book      = kwargs.get("book", "init") 
        config    = kwargs.get("lib_config", self.recipe_library["library_config"]  ) 
        
        
        if config not in  self.recipe_library["node_context"]:
            config = None
        
        if book == "init":
            book = self.recipe_library["init_book"]
        elif book == "assign":
            book = self.recipe_library["assign_book"]
        elif book == "update":
            book = self.recipe_library["update_book"]
        
        if node_name in self.nodes:
            # ---- Defaults
            node = self.nodes[node_name]
            node_type = self.NODE_TYPE[node.node_type]      
            #node_type = RadioNode.NODE_TYPE[node.node_type]
            
            if book in self.recipe_library["recipes_library"]:
                if node_type not in self.recipe_library["recipes_library"][book]:
                    LOG.debug("RadioSlice: no recipe book for specified node type \"%s\", using default node type \"%s\"", node_type, self.DEFAULT_NODE_TYPE)
                    node_type = self.DEFAULT_NODE_TYPE
                action_list      = self.recipe_library["recipes_library"][book][node_type]["action_list"].copy()
            else:
                LOG.debug("RadioSlice: no recipe book  \"%s\"", book)
                return validated_action_list    
                
            # ---- Define CONTEXT for new actions --------
            context = dict()
            #  --- library node context
            if config in self.recipe_library["node_context"]:
                if "*" in self.recipe_library["node_context"][config]:
                    context.update(self.recipe_library["node_context"][config]["*"])
                if node_type in self.recipe_library["node_context"][config]:
                    context.update(self.recipe_library["node_context"][config][node_type])
            #  --- specific node context
            context.update(node.get_context())       
                 
            #  --- radio & slice context 
            if radio_id is None or book == "init":
                for radio in node.radios:
                    slice_name = self.get_radio_slice(radio)
                    radio_context = dict()
                    radio_context.update(radio)
                    # --- get remote radio address and port
                    remote_node  = radio_context.get("remote_node")
                    remote_radio = radio_context.get("remote_radio")
                    if type(remote_radio) == str:
                            remote_radio = self.nodes[remote_node].get_radioid_by_name(remote_radio)
                    if remote_node and remote_radio:
                        radio_context["remote_host"] = self.nodes[remote_node].radios[remote_radio]["local_host"]
                        radio_context["remote_port"] = self.nodes[remote_node].radios[remote_radio]["local_port"]
                    cu_node  = radio_context.get("cu_node")
                    cu_radio = radio_context.get("cu_radio")
                    ru_node  = radio_context.get("ru_node")
                    ru_radio = radio_context.get("ru_radio")
                    # --- get CU radio address and port
                    if cu_node and cu_radio:
                        if type(cu_radio) == str:
                            cu_radio = self.nodes[cu_node].get_radioid_by_name(cu_radio)
                        if cu_radio is not None:
                            radio_context["remote_cu_host"] = self.nodes[cu_node].radios[cu_radio]["local_host"]
                            radio_context["remote_cu_port"] = self.nodes[cu_node].radios[cu_radio].get("local_cu_port")
                    # --- get DU radio address and port
                    if ru_node and ru_radio:
                        if type(ru_radio) == str:
                            ru_radio = self.nodes[ru_node].get_radioid_by_name(ru_radio)
                        if ru_radio is not None:
                            radio_context["remote_ru_host"] = self.nodes[ru_node].radios[ru_radio]["local_host"]
                            radio_context["remote_ru_port"] = self.nodes[ru_node].radios[ru_radio].get("local_ru_port")
                    if slice_name in self.recipe_library["radio_context"]:
                        radio_context.update(self.recipe_library["radio_context"][slice_name]["*"])
                        if node_type in self.recipe_library["radio_context"][slice_name]:
                            radio_context.update(self.recipe_library["radio_context"][slice_name][node_type])
                    for key, value in radio_context.items():
                        if value is not None:
                            context[key+str(radio["id"])] = value                                  
            else:
                if len(node.radios) <= radio_id:
                    LOG.debug("RadioSlice: radio_id  \"%d\" not valid", radio_id)
                    return validated_action_list    
                radio = node.radios[radio_id]
                radio_context = dict()
                radio_context.update(radio)
                
                slice_name = self.get_radio_slice(radio)
                                
                #  --- library node context
                if slice_name in self.recipe_library["radio_context"]:
                    if "*" in self.recipe_library["radio_context"][slice_name]:
                        radio_context.update(self.recipe_library["radio_context"][slice_name]["*"])
                    if node_type in self.recipe_library["radio_context"][slice_name]:
                        radio_context.update(self.recipe_library["radio_context"][slice_name][node_type])
                
                # --- get remote radio address and port
                remote_node  = radio_context.get("remote_node")
                remote_radio = radio_context.get("remote_radio")
                
                if type(remote_radio) == str:
                    remote_radio = self.nodes[remote_node].get_radioid_by_name(remote_radio)
                if remote_node and remote_radio:
                    radio_context["remote_host"] = self.nodes[remote_node].radios[remote_radio]["local_host"]
                    radio_context["remote_port"] = self.nodes[remote_node].radios[remote_radio]["local_port"]
                cu_node  = radio_context.get("cu_node")
                cu_radio = radio_context.get("cu_radio")
                ru_node  = radio_context.get("ru_node")
                ru_radio = radio_context.get("ru_radio")
                # --- get CU radio address and port
                if cu_node and cu_radio:
                    if cu_node in self.nodes and cu_radio in self.nodes[cu_node].radios:
                        if type(cu_radio) == str:
                            cu_radio = self.nodes[cu_node].get_radioid_by_name(cu_radio)
                        radio_context["remote_cu_host"] = self.nodes[cu_node].radios[cu_radio]["local_host"]
                        radio_context["remote_cu_port"] = self.nodes[cu_node].radios[cu_radio]["local_cu_port"]
                # --- get DU radio address and port
                if ru_node and ru_radio:
                    if ru_node in self.nodes and ru_radio in self.nodes[ru_node].radios:
                        if type(ru_radio) == str:
                            ru_radio = self.nodes[ru_node].get_radioid_by_name(ru_radio)
                        radio_context["remote_ru_host"] = self.nodes[ru_node].radios[ru_radio]["local_host"]
                        radio_context["remote_ru_port"] = self.nodes[ru_node].radios[ru_radio]["local_ru_port"]
                if slice_name in self.recipe_library["radio_context"]:
                    radio_context.update(self.recipe_library["radio_context"][slice_name]["*"])
                    if node_type in self.recipe_library["radio_context"][slice_name]:
                        radio_context.update(self.recipe_library["radio_context"][slice_name][node_type])
                        
                for key, value in radio_context.items():
                    if value is not None:
                        context[key+str(radio["id"])] = value                        
            #LOG.debug("RadioSlice: Generating action list for radio (%s,%d) (%s)", node_name, radio["id"], node_type) 
            #LOG.debug("RadioSlice: Context data:\n %s", json.dumps(context, indent=3))
                
            # ---- Define action list   
            #LOG.debug("RadioSlice: Getting action list from recipes book \"%s\", node \"%s\"", book, node_name)    
            #LOG.debug("RadioSlice: action list for node \"%s\" and recipe book (%s):\n %s", node_name, book, json.dumps(validated_action_list, indent=3))
            validated_action_list = []
            
            for action in action_list:
                validated_action = self.__validate_action(action, context, node_name)
                if validated_action:
                    validated_action_list.append(validated_action)
        else:
            LOG.debug("node name not valid")
        
        return validated_action_list
                    
    def __validate_action(self, action, context, node_name):
        """
        load context into action for a node, substituting $ parameters with context values        
        
        @param action: action
        @type  action: dict
        @param context: context parameters
        @type  context: dict
        @param node_name: name of the node
        @type  node_name: str
        @return:       validated action, None if error
        @rtype:        dict
        """
        validated_action = None
        
        if "action" in action and type(action["action"]) is dict:
            validated_action = dict()
            validated_action["action"] = dict()
            validated_action["action"]["action_name"] = action["action"].get("action_name")
            
            #LOG.info("RadioSlice: loading action \"%s\" parameters", validated_action["action"]["action_name"])
            # ---- activate radio program ----
            if validated_action["action"]["action_name"] == "activate_radio_program":
                validated_action["action"]["action_params"] = dict()
                if "action_params" in action["action"]:                    
                    # --- get program name
                    program_name = action["action"]["action_params"].get("program_name","")
                    if program_name[:1] == '$':
                        if program_name[1:] in context:
                            validated_action["action"]["action_params"]["program_name"] = context.get(program_name[1:], "")
                    else:
                        validated_action["action"]["action_params"]["program_name"] = program_name
                    
                    # --- get program type
                    program_type = action["action"]["action_params"].get("program_type","grc")
                    if program_type[:1] == '$':
                        if program_type[1:] in context:
                            validated_action["action"]["action_params"]["program_type"] = context.get(program_type[1:], "grc")
                    else:
                        validated_action["action"]["action_params"]["program_type"] = program_type
                    
                    # --- get program type
                    program_port = action["action"]["action_params"].get("program_port", self.DEFAULT_RADIO_PROGRAM_PORT)
                    if type(program_port) is str and program_port[:1] == '$':
                            if program_port[1:] in context:
                                validated_action["action"]["action_params"]["program_port"] = int(context.get(program_port[1:], self.DEFAULT_RADIO_PROGRAM_PORT))
                    else:
                        validated_action["action"]["action_params"]["program_port"] = program_port
                    
                    # --- get program args
                    validated_action["action"]["action_params"]["program_args"] = []
                    tmp = action["action"]["action_params"].get("program_args",[])
                    program_args = zip(tmp[::2],tmp[1::2])
                    for arg in program_args:
                        arg = list(arg)
                        if arg[1][:1] == '$':
                            if arg[1][1:] in context:
                                arg[1] = context.get(arg[1][1:])
                                if arg[1] is not None:
                                    if type(arg[1]) is bool:
                                        arg[1] = json.dumps(arg[1])
                                    else:
                                        arg[1] = str(arg[1])
                                    validated_action["action"]["action_params"]["program_args"].append(arg[0])
                                    validated_action["action"]["action_params"]["program_args"].append(arg[1])
                        else:
                            validated_action["action"]["action_params"]["program_args"].append(arg[0])
                            validated_action["action"]["action_params"]["program_args"].append(arg[1])
                    
            # ---- set parameters ----
            elif  validated_action["action"]["action_name"] == "set_parameters":
                validated_action["action"]["action_params"] = dict()
                if "action_params" in action["action"]:
        
                    # --- get set params
                    validated_action["action"]["action_params"]["params"] = dict()
                    for key, value in action["action"]["action_params"].get("params", dict()).items():
                        if value[:1] == '$':
                            if value[1:] in context:
                                validated_action["action"]["action_params"]["params"][key] = context.get(value[1:])
                        else:
                            validated_action["action"]["action_params"]["params"][key] = value
                if not validated_action["action"]["action_params"]["params"]:
                    #LOG.debug("****** No params for %s ******", validated_action["action"]["action_name"])
                    validated_action = {}
            else:
                 validated_action["action"] =  action["action"].copy()
                                                 
            if validated_action and "node" in action and "node_name" in action["node"]:
                validated_action["node"] = dict()
                validated_action["node"]["node_name"] = node_name
                                    
        #if not validated_action:
        #    LOG.debug("RadioSlice: couldn't load action: action format not valid or empty action ")
            
        return validated_action

             
    def load_descriptor(filename=None,path=None):
        """
        Loads configuration file
        
        @param filename:     json file
        @type  filename:     string
        @return:       error code: 0 no error, 1 set to default
        @rtype:        int
        """
        rval = 0
        nodes_info   = None 
        library_info = None
        slices_info  = None
        
        if not filename:
            filename = SliceManager.DEFAULT_DESCRIPTOR_FILE
        if not path:
            path = SliceManager.DESCRIPTORS_PATH
        filename = path + filename
        descriptor = dict()
        
        LOG.info("SliceManager: Loading descriptor file \"%s\"", filename)
        try:
            with open(filename, 'r') as f:
                descriptor = json.load(f)  
        except:
            LOG.info("SliceManager: Cannot load data from file {}".format(filename))
            raise
        
        
        if "radio_nodes" in descriptor:
            nodes_info = descriptor["radio_nodes"]
            
        if "library" in descriptor:
            library_info = descriptor["library"]
        
        if "slices" in descriptor:
            slices_info = descriptor["slices"]
        
        return nodes_info, library_info, slices_info
        
    def load_nodes_configuration(ctrl_id = "wctrl", filename=None):
        """
        Loads nodes in slice manager from the FINS EI configuration file
        
        @param filename:     name of the file with node data
        @type  filename:     string
        @return:       error code: 0 no error, 1 set to default
        @rtype:        int
        """
        rval = 0
        
        path = SliceManager.CONFIG_PATH
        if not filename:
            filename = SliceManager.DEFAULT_NODES_FILE
        filename = path + filename
        nodes = dict()
        
        LOG.info("SliceManager: Loading Nodes from \"%s\"", filename)
        name, file_extension = os.path.splitext(filename)
        if file_extension == ".cfg":
            try:
                config = configobj.ConfigObj(filename)
            except Exception as e:
                LOG.info("SliceManager: Cannot nodes data from file {} ({})".format(filename, repr(e)))
                raise
            except:
                LOG.info("SliceManager: Cannot nodes data from file {}".format(filename))
                raise
            else:
                for key, resource in config["RESOURCES"]["Rs"].items():
                    if resource["managed_by"] == ctrl_id:
                        vm = resource["vm"].strip("VM.")
                        node_name = config["VMs"][vm]["id"]
                        nodes[node_name] = dict()
                        nodes[node_name]["ip"] = config["VMs"][vm]["ip"]
                        # TODO CHECK ON self.NODE_TYPE.index(resource["type"])
                        nodes[node_name]["node_type"]    = SliceManager.NODE_TYPE.index(resource["type"])
                        nodes[node_name]["taps"]         = resource["taps"]
                        nodes[node_name]["description"]  = resource["description"]
        elif file_extension == ".json":
            try:
                with open(filename, 'r') as f:
                    config = json.load(f)  
            except:
                LOG.info("SliceManager: Cannot nodes data from file {}".format(filename))
                raise
        
        out_msg  = "| {0:{fill}<15} | {0:{fill}^17} | {0:{fill}<26} | {0:{fill}<12} | {0:{fill}^30} | {0:{fill}^30} |\n".format("", fill="-",)
        out_msg += "| {0:{fill}^15} | {1:{fill}^17} | {2:{fill}^26} | {3:{fill}^12} | {4:{fill}^30} | {5:{fill}^30} |\n".format("Node name","IP","Type","FINS type","Description","taps", fill=" ")
        out_msg += "| {0:{fill}<15} | {0:{fill}^17} | {0:{fill}<26} | {0:{fill}<12} | {0:{fill}^30} | {0:{fill}^30} |\n".format("", fill="-")
        for node_name, node_data in nodes.items():  
           # out_msg += "| %-15s | %-15s | %-30s | %-30s | %-15d |\n".format(node_name, node_data.ip, self.NODE_TYPE[node_data.node_type] , node_data.description, len(node_data.radios))
            out_msg += "| {0:15} | {1:^17} | {2:26} | {3:12} | {4:30} | {5:^30} |\n".format(node_name, node_data["ip"], SliceManager.NODE_TYPE[node_data["node_type"]], RadioNode.NODE_TYPE[node_data["node_type"]], node_data["description"] , str(node_data["taps"]))
        out_msg += "| {0:{fill}<15} | {0:{fill}^17} | {0:{fill}<26} | {0:{fill}<12} | {0:{fill}^30} | {0:{fill}^30} |\n".format("", fill="-")
        
        #LOG.debug(out_msg)
        LOG.debug("\n"+out_msg)
        
        # --- return nodes
        #     node_name:{node_type, node_ip, description, taps}
        return nodes

    def validate_nodes(nodes):
        validated_nodes = dict()
        
        for node_name, node in nodes.items():
            validated_nodes[node_name] = dict()
            for key, value in node.items():
                if key == "type":
                    validated_nodes[node_name]["node_type"] = SliceManager.NODE_TYPE.index(value)
                elif key == "node_ip":
                    validated_nodes[node_name]["ip"] = value
                else:
                    validated_nodes[node_name][key] = value

        return validated_nodes
