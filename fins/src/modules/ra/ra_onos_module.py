# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

import logging
import traceback

from fins.src.modules.abstracts.module_agent_abstract import ModuleAgentAbstract
from fins.src.support.comm.packets import HelloPacket

LOG = logging.getLogger(__name__)


class ModuleAgent(ModuleAgentAbstract):

    def __init__(self, rabbitmq_data, ra_data):

        super(ModuleAgent, self).__init__(rabbitmq_data,
                                          ra_data)
        #
        # A certain set of RabbitMQ queues is expected to be available for FINS
        # internal communication between RA and EI, RM and REST interface
        # The naming of the queues is assigned according to the rules given in
        # fins/support/rabbit_mq/rmq_utils.py->RMQUtils.get_fins_queue_name
        # i.e., "queue_" + source_id + "_to_" + destination_id
        # Each queue is meant for an unidirectional communication, thus for each
        # couple of connected entities, 2 queues are needed
        # During the init phase, the module checks if all the queue needed for
        # communication toward EI, RM and REST interface are already defined.
        # If not, RA repeat the check every second until all the needed queues
        # are found.
        #
        # When all the expected queues are found, RA configuration proceeds with
        # RabbitMQ communication manager setup.
        # At RA side, for each couple of connected RA - ModuleX, a RabbitMQ
        # Manager is defined for handling the communication between the two
        # modules, by retrieve/inject messages respectively from the output
        # queue (RA_to_X) and into the input queue (X_to_RA)
        # To each manager it is associated a Processing function, which performs
        # the operations that are supposed to be done when a message of a given
        # type is received on that specific Manager
        # Such a manager works as a thread, periodically checking the input
        # queue and automatically retrieving all messages in it
        # During the init phase, all configurations are performed, but the
        # assignment of message processing functions is left to the implementer,
        # to give him/her freedom in implementing the behavior of the module
        # Such an assignment is performed here with the following method
        #
        self.assign_message_processors(self.process_message_from_EI,
                                       self.process_message_from_RM,
                                       self.process_message_from_REST)

    # A command is a special, internal-generated message.
    # It is used to trigger a given reaction in the RA: a command can be hard-
    # coded into the py file or invoked on demand via message by EI/RM/REST
    def process_command(self, command, params):

        # command is a string identifying the type of command
        assert isinstance(command, str)
        # params is a dict containing the parameters associated to the command
        assert isinstance(params, dict)

        LOG.debug('Processing command: %s', command)

        try:
            if command == "SEND_HELLO":
                # Here a new packet (HelloPacket) is generated
                # You don't have to bother about header: proper filling is
                # performed automatically by Packet class.
                # If you need to create a reply to a given message, just use the
                # 'build_reply' method: the Packet class can create on its own
                # the reply packet header (see an example in ModuleAgent's
                # method 'self.process_message_from_EI' )
                # You have just to worry about the content of the body
                pkt = HelloPacket()
                pkt.build_packet(
                    None,
                    dict(
                        text='Hi there!'
                    ))

                # send_message_to_XX takes care of sending the message to the
                # desired connected entity. It just takes 2 params:
                # @pkt              the Packet to be sent
                # @is_reply=False   if you are sending it as reply or not
                self.send_message_to_EI(pkt)

        except Exception:
            traceback.print_exc()


    def process_message_from_EI(self, queue, message):

        LOG.debug('New message from EI (queue: %s', queue)

        try:
            # Received message is a dict: it is converted into an instance of
            # Packet class (if recognized, into a specific child class instance
            # according to the message type)
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from EI:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message: here, since it is a reply, the is_reply
                # parameter among send_message_to_XX function's arguments shall
                # be set to True (whilst by default it is set to False)
                self.send_message_to_EI(packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_RM(self, queue, message):

        LOG.debug('New message from RM (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from RM:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_RM(packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_REST(self, queue, message):

        LOG.debug('New message from REST (queue: %s', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            LOG.debug("\n\nRECEIVED msg from REST:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_REST(packet, True)

        except Exception:
            traceback.print_exc()