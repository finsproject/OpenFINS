# Copyright 2018 FBK CREATE-NET (https://create-net.fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

"""
RA Module for Wishful Controller
"""

__docformat__ = "epytext" # http://epydoc.sourceforge.net/manual-epytext.html
__author__    = "Cristina Costa & Antonio Francescon (CREATE-NET FBK)"
__copyright__ = "Copyright (c) 2018, CREATE-NET FBK"
__version__   = "FINS RA Wishful Gnuradio V.1.0:"
# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import logging
import traceback
import json
import os.path
import time
WAIT_TIME = 10
TIMEOUT = 60

# FINS imports
from fins.src.modules.abstracts.module_agent_abstract import ModuleAgentAbstract
from fins.src.support.comm.packets import HelloPacket, RadioActionPacket
from fins.src.support.rabbit_mq.rmq_endpoint    import RabbitEndpoint
from fins.src.support.rabbit_mq.rmq_rest_client import RMQRestClient
#from fins_radio_slices import RadioSlice, RadioNode, SliceManager
from fins.src.modules.ra.radio_slice_manager import RadioSlice, RadioNode, SliceManager
# ----------------------------------------------------------------------
#  ModuleAgent Class
# ----------------------------------------------------------------------
class ModuleAgent(ModuleAgentAbstract):
    """ 
    Implements the Remote Agent (RA) for FINS Wishful Controller
    
    
    Class Attributes - Default values
    =================================
        - CONFIG_FILE, <str>: default value for wishful controller 
        configuration file, coded as .json file
        - CONFIG_FILE_BACKUP, <str>: default value for wishful controller 
        backup configuration file, coded as .json file
        - RABBITMQ_DEFAULT_CONF, <dict>: Default RabbitMQ configuration.
              
    Instance Attributes
    ===================
        - LOG, <Logger class>: __name__ logger
        - nodes, <list>: list of nodes (agents) connected to this
        controller. It contain also rabbitMQ queues info (for interacting
        with the FINS Controller).
    """
    
    # ------------------------------------------------------------------
    # Class attributes
    # ------------------------------------------------------------------
        
    # RA default configuration files            
    CONFIG_FILE = "FINS_RA_WCTRL_config.json"
    """
    <str>, default value for gnr wishful RA configuration file,
    coded as .json file
    """
    CONFIG_FILE_BACKUP = "FINS_RA_WCTRL_config_backup.json"
    """
    <str>, default value for gnr wishful RA backup configuration 
    file, coded as .json file (FINS_RA_wishful_config.json) 
    """
    # queues default values
    DEFAULT_QUEUES_DATA = {
        "prefix"            : "qra",
        "ra_id"             : "ra_wctl",
        "ctrl_id"           : "wc_ctrl"
        }

    """
    Default data for RabbitMQ queues (communication with controller):
    
    "prefix"           <str>, queues prefix
    "ra_id"            <str>, RA id.
    "ctrl_id"          <str>, Controller id.
    """
             
    class RadioActionError(Exception):
        """
        Exception class catching Radio Actions Errors
        """
        def __init__(self, value):
            self.value = value
        def __str__(self):
            return "RadioActionError: " + repr(self.value)
 
    # ------------------------------------------------------------------
    # Class initialization
    # ------------------------------------------------------------------
    def __init__(self, rabbitmq_data, ra_data):
        """
        ModuleAgent Class initialization
        
        @param rabbitmq_data: FINS RabbitMQ data
        @param ra_data:       RA data
        """
        super(ModuleAgent, self).__init__(rabbitmq_data, ra_data)
        # A certain set of RabbitMQ queues is expected to be available for FINS
        # internal communication between RA and EI, RM and REST interface
        # The naming of the queues is assigned according to the rules given in
        # fins/support/rabbit_mq/rmq_utils.py->RMQUtils.get_fins_queue_name
        # i.e., "queue_" + source_id + "_to_" + destination_id
        # Each queue is meant for an unidirectional communication, thus for each
        # couple of connected entities, 2 queues are needed
        # During the init phase, the module checks if all the queue needed for
        # communication toward EI, RM and REST interface are already defined.
        # If not, RA repeat the check every second until all the needed queues
        # are found.
        #
        # When all the expected queues are found, RA configuration proceeds with
        # RabbitMQ communication manager setup.
        # At RA side, for each couple of connected RA - ModuleX, a RabbitMQ
        # Manager is defined for handling the communication between the two
        # modules, by retrieve/inject messages respectively from the output
        # queue (RA_to_X) and into the input queue (X_to_RA)
        # To each manager it is associated a Processing function, which performs
        # the operations that are supposed to be done when a message of a given
        # type is received on that specific Manager
        # Such a manager works as a thread, periodically checking the input
        # queue and automatically retrieving all messages in it
        # During the init phase, all configurations are performed, but the
        # assignment of message processing functions is left to the implementer,
        # to give him/her freedom in implementing the behavior of the module
        # Such an assignment is performed here with the following method
        #
        self.assign_message_processors(self.process_message_from_EI,
                                       self.process_message_from_RM,
                                       self.process_message_from_REST)
        
        # -------------------
        # Set logging
        # -------------------
        self.LOG = logging.getLogger(__name__)
        log_level = logging.DEBUG
        logfile = None
        logging.basicConfig(filename=logfile, level=log_level,
        format='%(name)s.%(funcName)s() - %(levelname)s - %(message)s')
        
        self.LOG.info("\n\n**** FINS Remote Agent (RA) for Wishful Gnuradio Controller ****\n\n")  
        
        # -------------------
        # Globals
        # -------------------
        self.nodes = dict()
        
        if "ctrl_id" in ra_data:
            self.DEFAULT_QUEUES_DATA["ctrl_id"] = ra_data["ctrl_id"]
        
        if "nodes" in ra_data:
            try:
                # --- Load nodes (controlled by the Wishful Controller)
                self.LOG.info("Loading nodes info from ra_data")
                nodes =  ra_data["nodes"]
                print(nodes)
            except:
                raise        
        else:
            try:
                self.LOG.info("Loading nodes info from configuration file")
                # --- Load nodes (controlled by the Wishful Controller
                # --- from the EI configuration file and assign them to the "default" slice
                nodes = SliceManager.load_nodes(self.DEFAULT_QUEUES_DATA["ctrl_id"])
                # self.slice_manager.load_nodes(ra_data["nodes"])
            except:
                raise
        
        nodes_info, library_info, slices_info = SliceManager.load_descriptor()
        for node_name in nodes:
            if node_name in nodes_info:
                nodes[node_name].update(nodes_info[node_name])
        self.slice_manager = SliceManager(nodes_info = nodes, library_info = library_info, slices_info = slices_info)
        input("\n>>>>>>>>>> Slice Manager created, press any key...\n")
        # --- Initialize experiment with library init configuration
        self.pending_actions = self.slice_manager.commit_context(book = "init")
        print(">>>>>  ", json.dumps(self.pending_actions, indent=2))                            
        input("\n>>>>>>>>>> Library committed, press any key...\n")
        
        #  ----- self.pending_actions = {"slice_name":[actions,]}
        self.pending_actions = []
        #try:
            #self.slice_manager.load_recipe_library("IRISdefault")
        #except:
            #raise
        LOG.info("Controller id %s", ctrl_id)
        input("\n>>>>>>>>>> To start, press any key...\n")
            
        self.init_controller_comm(rabbitmq_data, ra_data)  
        
        # --- wait for nodes to show up
        #n_nodes = len(self.slice_manager.nodes)
        #c_nodes = len(self.__update_nodes())  
        #self.LOG.info("Waiting for nodes to show up")      
        #while n_nodes < c_nodes:
            #time.sleep(WAIT_TIME)
            #c_nodes = len(self.__update_nodes())
            
        #self.LOG.info("%d nodes connected", c_nodes)
        ## --- Add nodes from the controller and assign to the "default" slice
        #self.__add_nodes()
        
# === FINS Methods ===
   
    def process_command(self, command, params):
        """
        Process command
        
        @param command: a special, internal-generated message.
        It is used to trigger a given reaction in the RA, a command c
        an be hardcoded into the py file or invoked on demand via 
        message by EI, RM, or REST
        @type command: str
           
        @param params: command parameters
        @type params: dict
        """

        # command is a string identifying the type of command
        assert isinstance(command, str)
        # params is a dict containing the parameters associated to the command
        assert isinstance(params, dict)

        self.LOG.debug('Processing command: %s', command)

        try:
            if command == "SEND_HELLO":
                # Here a new packet (HelloPacket) is generated
                # You don't have to bother about header: proper filling is
                # performed automatically by Packet class.
                # If you need to create a reply to a given message, just use the
                # 'build_reply' method: the Packet class can create on its own
                # the reply packet header (see an example in ModuleAgent's
                # method 'self.process_message_from_EI' )
                # You have just to worry about the content of the body
                pkt = HelloPacket()
                pkt.build_packet(
                    None,
                    dict(
                        text='Hi there!'
                    ))

                # send_message_to_XX takes care of sending the message to the
                # desired connected entity. It just takes 2 params:
                # @pkt              the Packet to be sent
                # @is_reply=False   if you are sending it as reply or not
                self.send_message_to_EI(pkt)
            elif command == "RADIO_ACTION":
                packet = RadioActionPacket()
                packet.set_action_name(params)
                packet.set_action_params(params)
                return_value = self.__process_radio_action(packet.get_message())
                self.LOG.info(return_value)
                packet.set_return_value(return_value)
                packet.build_reply()  
                # Send message with is_reply set to True
                self.send_message_to_EI(packet, True)
        except Exception:
            traceback.print_exc()


    def process_message_from_EI(self, queue, message):
        """
        Process message from Experiment Init. (EI)
        
        @param queue:   message queue
        @param message: delivered message
        """

        self.LOG.debug('New message from EI (queue: %s', queue)

        try:
            # Received message is a dict: it is converted into an instance of
            # Packet class (if recognized, into a specific child class instance
            # according to the message type)
            packet = self.message_to_packet(message)

            self.LOG.debug("\n\nRECEIVED msg from EI:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message: here, since it is a reply, the is_reply
                # parameter among send_message_to_XX function's arguments shall
                # be set to True (whilst by default it is set to False)
                self.send_message_to_EI(packet, True)
                
            elif packet.get_msg_type() == 'RADIO_ACTION':
                return_value = self.__process_radio_action(packet.get_message())
                self.LOG.info(return_value)
                packet.set_return_value(return_value)
                packet.build_reply()  
                # Send message with is_reply set to True
                self.send_message_to_EI(packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_RM(self, queue, message):
        """
        Process message from the Remote Agent (RM)
        
        @param queue:   message queue
        @param message: delivered message
        """

        self.LOG.debug('New message from RM (queue: %s)', queue)

        try:
            # Convert message into an instance of Packet
            packet = self.message_to_packet(message)

            self.LOG.debug("\n\nRECEIVED msg from RM:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == 'HELLO':
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi'))
                # Send message with is_reply set to True
                self.send_message_to_RM(packet, True)
                
            elif packet.get_msg_type() == 'RADIO_ACTION':
                return_value = self.__process_radio_action(packet.get_message())
                self.LOG.info(return_value)
                packet.set_return_value(return_value)
                packet.build_reply()    
                # Send message with is_reply set to True
                self.send_message_to_RM(packet, True)

        except Exception:
            traceback.print_exc()

    def process_message_from_REST(self, queue, message):
        """
        Process message from the REST
        
        @param queue:   message queue
        @param message: delivered message
        """

        self.LOG.debug('New message from REST (queue: %s)', queue)

        try:
            # Convert message into an instance of Packet
            self.LOG.info("=======================================================")
            self.LOG.info(message)
            packet = self.message_to_packet(message)

            self.LOG.debug("\n\nRECEIVED msg from REST:\n%s\n",
                      packet.to_string())

            if packet.get_msg_type() == packet.MSG_TYPE_HELLO:
                # Create packet reply with a given body
                packet.build_reply(None, dict(text='Hi there! Here is the wishful controller!'))
                # Send message with is_reply set to True
                self.send_message_to_REST(packet, True)
             
            elif packet.get_msg_type() == packet.MSG_TYPE_RADIO_ACTION:
                return_value = self.__process_radio_action(packet.get_message())
                self.LOG.info(return_value)
                packet.set_return_value(return_value)
                packet.build_reply(None, None)
                # Send message with is_reply set to True
                self.send_message_to_REST(packet, True)

        except Exception as e:
            packet.build_reply(None, dict(text='ERROR while processing REST: {}'.format(str(e))))
            # Send message with is_reply set to True
            self.send_message_to_REST(packet, True)
            traceback.print_exc()

# === Wishful Controller communication methods ===

    def init_controller_comm(self, rmq_data, ra_data):
        """
        Set up controller communication via rmq_endpoint

        @param rmq_data: rabbitmq data
        @type  rmq_data: dict()
        @param ra_data: RA data
        @type  ra_data: dict()
        """
        # ---- Init globals     
        self.blocked = False
        self.recieved_message = None
        
        prefix  = self.DEFAULT_QUEUES_DATA["prefix"]
        ra_id   = self.DEFAULT_QUEUES_DATA["ra_id"]
        ctrl_id = self.DEFAULT_QUEUES_DATA["ctrl_id"]
        
        if "prefix"  in ra_data:
            prefix  = ra_data["prefix"]
        if "id"      in ra_data:
            ra_id  = ra_data["id"]
        if "ctrl_id" in ra_data:
            ctrl_id = ra_data["ctrl_id"]
        
        self.from_RA_queue      = "{}_from_{}_to_{}_queue".format(prefix, ra_id, ctrl_id)
        self.to_RA_queue        = "{}_from_{}_to_{}_queue".format(prefix, ctrl_id, ra_id)
            
        # ---- Creating channel with rabbitMQ (via rmq_endpoit)
        self.LOG.debug("\nConnecting to RabbitMQ server using rmq_endpoint module....")

        queues = {self.from_RA_queue:"publish", self.to_RA_queue:"consume"}
        data_endpoint = dict(
                            username    = rmq_data['username'],
                            password    = rmq_data['password'],
                            rabbit_ip   = rmq_data['host_ip'],
                            rabbit_port = rmq_data['port']
                           )
        rmq_management_url = 'http://' \
                           + rmq_data['username'] \
                           + ':' + rmq_data['password'] \
                           + '@' + rmq_data['host_ip'] \
                           + ':' + \
                           rmq_data['config_port'] + '/api/'
        rrc = RMQRestClient(rmq_management_url)
        exchange = "exchange_" + ra_id + "_" + ctrl_id
        queues = {self.from_RA_queue:"publish", self.to_RA_queue:"consume"}
        self.rmq_endpoint = RabbitEndpoint(
                           data_endpoint, 
                           rrc, 
                           exchange, 
                           queues, 
                           self.process_message_from_controller)
        self.rmq_endpoint.start()
        self.LOG.info("\t-- RabbitMQ connected: IP address {},\n".format(rmq_data["host_ip"]))
                           
    def process_message_from_controller(self, queue, message):
        """
        process message from controller

        @param queue: queue name
        @type  queue: str
        @param message: delivered message
        @type  message: str
        """        
        self.LOG.debug('New message from queue %s (%s)', queue, repr(message))
        if queue == self.to_RA_queue:
            self.LOG.debug('New message from Controller recieved')
            # Convert message into an instance of Packet
            self.LOG.info("=======================================================")
            self.LOG.info(message)
            packet = self.message_to_packet(message)
            
            if packet.get_msg_type() == packet.MSG_TYPE_RADIO_ACTION:
                self.recieved_message = packet.get_return_value()
                self.LOG.info(self.recieved_message)
            else:
                self.LOG.info("The format of the reply packet from controller is not valid (packet type must be RADIO_ACTION)")
                self.recieved_message = dict(error_msg = "The format of the reply packet from controller is not valid")        
        
    def send_action_to_controller(self, action_name, action_params=None, node_name=None):
        """
        Generic function for forwarding radio actions to the controller

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - name <str>:    Name of the action
            - params <dict>: Action parameters

        @param action_name: action name
        @type  action_name: str
        @param action_params: action params
        @type  action_params: dict
        @param node_name: name name
        @type  node_name: str
        @return:       action return value
        @rtype:        dict
        """
        # ---- Set defaults ----
        return_value = dict()
        if node_name:
            if node_name not in self.nodes:
                self.nodes[node_name] = dict()
        
        # ----- Prepare packet to be sent to controller ----  
        pkt = RadioActionPacket()
        pkt.set_action(dict(action_params = action_params, action_name = action_name))
        pkt.set_node  (dict(node_name = node_name))
        pkt.set_msg_id(self.new_message_id(), False)
        pkt.set_sender(self.mod_id(), False)
        pkt.build_packet(None, None) 
        msg = json.dumps(pkt.get_packet())   
             
        
        # ----- Send packet to controller ----    
        self.LOG.info('\t-- Sending action <{}> to controller'.format(action_name))
        while self.blocked:
            pass
        self.blocked = True
        self.recieved_message = None
        try:
            self.rmq_endpoint.send(self.from_RA_queue, msg)
            self.LOG.debug("\t--> Sent message to <{}> queue".format(self.from_RA_queue))
        except Exception as e:
            return_value.update(error_msg = repr(e))
            self.LOG.info("Error while sending action <{}> to <{}>".format(action_name, node_name))
        except:
            raise
        else:
            # ----- Wait for reply ----
            start_time = time.time()
            self.LOG.info("\t--> Fetching reply message from <{}> queue".format(self.to_RA_queue))
            while not self.recieved_message:
               elapsed_time = time.time() - start_time
               # if elapsed_time > WAIT_TIME:
               #    self.LOG.info("\t--> Fetching reply message from <{}> queue".format(self.to_RA_queue))
               # ---- check timeout
               if elapsed_time > TIMEOUT:
                   return_value = dict(error_msg = "Timeout while fetching reply from node {}".format(time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
                   self.LOG.info("\t--> Timeout while fetching reply from node from <{}> queue".format(self.to_RA_queue))
                   break
            else: 
                return_value = self.recieved_message
                # TODO: check msg id for reply           
            self.LOG.info("Recieved return value for action <{}> from node <{}>".format(action_name, node_name)) 
        finally:
            self.recieved_message = None
            self.blocked = False
                
        self.LOG.info("Return values: <{}>".format(repr(return_value)))
        return return_value
                
# === Wishful RA support methods ===                
    def __process_radio_action(self, msg):	
        """
        Generic function for processing all radio actions

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - name <str>:    Name of the action
            - params <dict>: Action parameters

        Raises
        ======
            - Wishful Controller exception
            - RadioActionError        

        @param msg: action dictionary
        @type  msg: dict
        @return:       action return value
        @rtype:        dict
        """
        # ---- Set defaults ----
        return_value = dict()
        return_value.setdefault("rval")
        return_value.setdefault("return_msg")
        return_value.setdefault("warning_msg")
        return_value.setdefault("error_msg")
        
        action_params = None
        node_name     = None
        if type(msg) is dict:
            action = msg.get("action", dict())
            node   = msg.get("node",   dict())

            action_name   = action.get('action_name')
            action_params = action.get('action_params')
            node_name     = node.get('node_name')
        else:
            raise self.RadioActionError('Message format not correct')

        if action_name is None:
            raise self.RadioActionError('Action name missing.')

            
        self.LOG.info("Action params: {}, {}, {}".format(action_name, action_params, node_name))
        
        # ---- Call action ----
        try:
            method_to_call = getattr(self, action_name, None)
            if method_to_call is None:
                self.LOG.info("Action %s (%s, %s) not implemented in the RA, forwarding to the controller....", action_name, repr(action_params), node_name)       
                return_value.update(self.send_action_to_controller(action_name, action_params, node_name))
            else:
                return_value.update(method_to_call(action_params, node_name))
        except self.RadioActionError as e:          
            return_value.update(error_msg =  repr(e.value))
        except AttributeError as e:
            return_value.update(error_msg = "AttributeError ({})".format(e))
        except IOError as e:
            errno, strerror = e.args
            return_value.update(error_msg = "I/O error({0}): {1}".format(errno, strerror))
        except Exception as e:
            return_value.update(error_msg = "Error while executing action {} ({})".format(action_name, e))
        except:
            return_value.update(error_msg = "Error while executing action {}".format(action_name))
            
        self.LOG.info(self.__prettyprint_return_value(return_value, action_name))
        
        return return_value    
    
       
        
    def __load_actions(self, config, context):  
        """
        Load actions
        
        @param config: actions
        @type  config: dict
        @return:       validated configuration
        @rtype:        dict
        """  
        """
            - node_name <str>:     name of the node to be loaded.
            - program_name <str>:  name of the program to be loaded.
            - program_args <dict>: (gnuradio m.) args of the radio program 
            - program_type <str>:  (gnuradio m.) default is 'grc'
            - program_port <int>:  (gnuradio m.) XMLRPC server port in radio program
        """
        self.LOG.debug("--- Loading actions list ...") 
        validated_list = []
        
        # ---- Setting default values ----
        if config and type(config) is dict:
            if "action_list" in config and type(config["action_list"]) is list:
                # --- validate action list
                self.LOG.info(" -- Validating action list")
                for item in config["action_list"]:
                    self.LOG.info(" -- Validating action:")
                    item_node_name = None
                    item_action_name = None
                    item_action_params = None
                    # --- check if there is a action_name ---
                    for key, value in item.items():
                        if key == "action" and type(value) is dict:
                            for action_key, action_value in value.items():                                        
                                if action_key == "action_name":
                                    self.LOG.info(" --->> action_name found %s", action_value)
                                    item_action_name = action_value
                                if action_key == "action_params" and type(item["action"]["action_params"]) is dict:
                                    self.LOG.info(" --->> action_params found")
                                    item_action_params = action_value
                        if key == "node" and type(value) is dict:
                            for node_key, node_value in value.items():                                        
                                if node_key == "node_name":
                                    self.LOG.info(" --->> node_name found %s", node_value)
                                    item_node_name = node_value
                    # --- check if there is a action_name ---
                    if not item_action_name:
                        self.LOG.warning("Warning: action format not valid (no action_name), skipping value ...")
                    else:
                        if "recipe_name" in context:
                            del context["recipe_name"]
                        for key, value in context.items():
                            # --- check context for node_name ---
                            if key == item_node_name:
                                self.LOG.info(" --->> substituting node_name:  from %s to %s", item_node_name, value)
                                item_node_name = value
                            if item_action_params:
                                for par_key, par_value in item_action_params.items():
                                    if key == par_value:
                                        self.LOG.info(" --->> substituting action_params %s:  from %s to %s", par_key, par_value, value)
                                        item_action_params[par_key] = value
                                    
                        if item_node_name:
                            item["node"]["node_name"] = item_node_name
                        if item_action_params:
                            item["action"]["action_params"] = item_action_params
                        
                        self.LOG.info(" --->> appending action %s to return_value",item_action_name)
                        validated_list.append(item)
            else:
                self.LOG.warning("Warning: action_list format not valid (or no action_list), returning empty list ...")
        else:
            self.LOG.warning("Warning: data format not valid, returning empty list ...")
                    
        return validated_list
        
    def __prettyprint_return_value(self, return_value, info=None):
        """
        pretty print return value from actions
        
        @return pretty printed string
        @rtype str
        """
        if info:
            header = ">>> Return Value (" + info + "): "
        else:
            header = ">>> Return Value: "
        rmsg = ""
        if not return_value:
            return header + "empty"
        if type(return_value) is not dict:
            return header + "not a valid format"
        if return_value.get("error_msg"):
            rmsg  = "\n    ERROR: " + return_value["error_msg"]
        if return_value.get("warning_msg"):
            rmsg += "\n    WARNING: " + return_value["warning_msg"]
        if return_value.get("out_msg"):
            rmsg += "\n    MSG: " + return_value["out_msg"]
        if return_value.get("rval"):
            rmsg += "\n    VALUES:\n "
            if type(return_value["rval"]) is dict and not return_value["rval"]:
                rmsg += json.dumps(return_value["rval"], indent=3)
            else:
                rmsg += repr(return_value["rval"])
        if not rmsg:
            return header + "not a valid format"
        
        return header + rmsg

        
    def __update_nodes(self):
        """
        update node status (if connected to controller)
        
        @return: names of the connected nodes
        @rtype : list
        """
        
        rval = []
        self.LOG.info("Check nodes connected to the controller")                
        out = self.send_action_to_controller("get_nodes")     
        
        if "rval" in out:        
            for node in self.slice_manager.nodes:
                if node.name in out["rval"]:
                    rval = out["rval"]
                    self.LOG.debug("Node %s connected to the controller" , node.name)
                    node.status = RadioNode.STATUS["CONNECTED"]
                    node.info.update(c_nodes[node.name])
                    rval.append(node)
                else:
                    node.status = RadioNode.STATUS["NOT_CONNECTED"]
        elif "error_msg" in out:
            self.LOG.info(out["error_msg"])
        else:
            self.LOG.info("no imput")
                
        return rval
        
    def __add_nodes(self):
        """
        update node status (if connected to controller)
        
        @return: names of the connected nodes
        @rtype : list
        """
        
        rval = []
        self.LOG.info("Add nodes connected to the controller to slice manager")                
        c_nodes = self.__update_nodes()   
        
        if c_nodes:
            for node in c_nodes:
                if node.name not in self.slice_manager.nodes:
                    new_node = RadioNode()
                    new_node.info.update(c_nodes[node.name])
                    new_node.status = RadioNode.STATUS["CONNECTED"]
                    node.ip                  = c_nodes[node.name].get("node_ip")
                    node.info["type"]        = RadioNode.DEFAULT_TYPE
                    node.info.update(c_nodes[node.name])
                    self.slice_manager.add_node(new_node)
                
        return rval
        # ==== Slice Management Methods ====

    def empty_action(self, action_params, node_name=None):
        """
        Does nothing, for testing or template

        All parameters are passed through the action_params dictionary
    
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        # --- DO THINGS
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def load_descriptor(self, action_params, node_name=None):
        """
        Loads slices descriptor

        All parameters are passed through the action_params dictionary
    
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        # --- 
        
        if not action_params or type(action_params) is not dict:
            action_params = dict()
            
        descriptor_file     = action_params.get("descriptor_file",    SliceManager.DEFAULT_DESCRIPTOR_FILE)
        descriptor_path     = action_params.get("descriptor_path",    SliceManager.DESCRIPTORS_PATH)
            
        nodes_info, library_info, slices_info = SliceManager.load_descriptor(descriptor_file, descriptor_path)
        
        if library_info:
            self.slice_manager.load_recipe_library(library_info)   
        
        if nodes_info:
            self.slice_manager.load_nodes(nodes_info)
        
        if slices_info:
            self.slice_manager.load_slices(slices_info)   
            
        self.pending_actions = self.slice_manager.commit_context(book = "init")
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def deactivate_radio_slice(self, action_params, node_name): 
        """
        Deactivate the radio programs currently running on slice nodes (except CU and VR nodes), if any.
        When executed, this function stops the radio program specified in the node.
        To reactivate the slice, use activate_radio_slice.

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the node to be loaded.

        Raises
        ======
            - RadioActionError: if node_name not specified.
            If node not found.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        slice_name = None
        
        if action_params and type(action_params) is dict:
            slice_name     = action_params.get("slice_name",    None)
        
        if not slice_name:
            out_msg =  "Deactivating nodes in all slices"
            for node_name in self.slice_manager.nodes:
                node_type = self.slice_manager.nodes[node_name].node_type
                if node_type != RadioNode.NODE_TYPE.index("VR_NODE") and node_type != RadioNode.NODE_TYPE.index("CU_NODE"):
                    try:
                        return_value = self.deactivate_radio_program(None, node_name)
                    except Exception as e:
                        if not error_msg:
                            error_msg = "Errors while deactivating node \"{}\"".format(node_name)
                        error_msg += repr(e)
                    else:
                        out_msg += ", \"{}\"".format(node_name)
        elif slice_name in self.slice_manager.slices:
            out_msg = "Deactivating slice \"{}\" nodes ".format(slice_name)
            for radio in self.slice_manager.slices[slice_name].assigned_radios:
                node_name = radio[0]
                node_type = self.slice_manager.nodes[node_name].node_type
                if node_type != RadioNode.NODE_TYPE.index("VR_NODE") and node_type != RadioNode.NODE_TYPE.index("CU_NODE"):
                    try:
                        return_value = self.deactivate_radio_program(None, node_name)
                    except Exception as e:
                        if not error_msg:
                            error_msg = "Errors while deactivating node \"{}\"".format(node_name)
                        error_msg += repr(e)
                    else:
                        out_msg += ", \"{}\"".format(node_name)
    
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
        
    def activate_radio_slice(self, action_params, node_name): 
        """
        Re-activate radio programs on slice nodes (except CU and VR nodes), if any.        

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the node to be loaded.

        Raises
        ======
            - RadioActionError: if node_name not specified.
            If node not found.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        action_list = []
        
        slice_name = None
        commit_actions = True
        
        if action_params and type(action_params) is dict:
            slice_name     = action_params.get("slice_name",    None)
        
        if slice_name and not self.slice_manager.slices[slice_name].assigned_radios:
            out_msg = "Activating slice \"{}\" nodes ".format(slice_name)
            action_list = self.slice_manager.commit_slice(slice_name)
        else:
            warning_msg = "Slice  \"{}\" not present or empty".format(slice_name)
        
        rval = dict(action_list = action_list, commit = commit_actions)
                  
        if commit_actions:
            # --- commit actions
            rval["commit"] = self.execute_action_list(dict(action_list=action_list)).get("rval")
            
            if rval["commit"]:
                out_msg = "Slice  \"{}\" assigned to radios  \"{}\"".format(slice_name, out_msg)
                self.LOG.info(out_msg)
            else:
                out_msg = "Couldn't commit slice  \"{}\" assigned to radios  \"{}\"".format(slice_name, out_msg)
                self.LOG.info(error_msg)
        else:
            # --- or actions to pending_actions
            self.pending_actions.extend(action_list)
            out_msg = "Slice  \"{}\" assigned to radios  \"{}\", commit actions to nodes to make them valid.".format(slice_name, out_msg)   
    
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def add_radio_slice(self, action_params, node_name=None):
        """
        Add radio slice to slice manager. 

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the slice
            - slice_context <dict>: context parameters for the slice, default None
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        slice_name    = None
        slice_context = None
        
        if action_params and type(action_params) is dict:
            slice_name     = action_params.get("slice_name",    None)
            slice_context  = action_params.get("slice_context", None)
            
        if not slice_name:
            slice_name = "slice" + str(len(self.slice_manager.slices))
            warning_msg =  "Slice name missing, setting to {}".format(slice_name)
            self.LOG.warning(warning_msg)
            
        if  slice_name in self.slice_manager.slices:
            warning_msg = "Slice \"{}\" already exist, nothing to do)".format(slice_name)
        else:
            out_msg = "New slice added \"{}\" (radio_context:{})".format(slice_name, slice_context)
            rval = self.slice_manager.add_radio_slice(RadioSlice(slice_name, slice_context))                    
        self.LOG.info(out_msg)
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def assign_radio_slice(self, action_params, node_name=None):
        """
        Assign radio slice to radio nodes. 

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the slice, required
            
            - node    <str>: name of the node, required for simple configurations
            - radio_tap <str> or <int>: name or number of the node tap, None if the node is an RU
            - peer_node <str>: name of the Peer node, optional (for simulated radio links)
            - peer_radio_tap  <str> or <int>: name or number of the peer node tap
            
            - cu_node <str>: name of the Central Unit node, required for RU-CU configurations
            - cu_radio_tap  <str> or <int>: name or number of the CU tap
            
            - recipes_book <str>:  name of the recipes_book, default "assign"
            - commit_actions <bool>: commit actions, default False
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        commit_actions = False
        commit_actions_default = False
        recipe_book_default = SliceManager.DEFAULT_ASSIGN_BOOK
        
        slice_name = None
        default_tap  = 0
        action_list  = []
        
        if action_params and type(action_params) is dict:
            
            recipes_book   = action_params.get("recipes_book",   recipe_book_default)
            commit_actions = action_params.get("commit_actions", commit_actions_default)
                
            if not recipes_book:
                recipes_book = recipe_book_default
            ## TODO radios list
            radio_list = action_params.get("radios", [])
            for radio_item in radio_list:
                node = radio_item.get("node", None)
                radio_tap = radio_item.get("radio_tap", None)
                radio_id = radio_item.get("radio_id", None)
                radio_name = radio_item.get("radio_name", None)
                
                cu_node = radio_item.get("cu_node", None)
                cu_radio_tap = radio_item.get("cu_radio_tap", None)
                cu_radio_id = radio_item.get("cu_radio_id", None)
                cu_radio_name = radio_item.get("cu_radio_name", None)
                
                peer_node = radio_item.get("peer_node", None)
                peer_radio_tap = radio_item.get("peer_radio_tap", None)
                peer_radio_id = radio_item.get("peer_radio_id", None)
                peer_radio_name = radio_item.get("peer_radio_name", None)
                
                radio = None
                cu_radio = None
                peer_radio = None
                
                if node is not None:
                    if radio_tap is not None:
                        radio = self.slice_manager.get_radio_from_tap(node, radio_tap)
                    elif radio_name is not None:
                        radio = self.slice_manager.get_radio_from_name(node, node_name)
                    elif radio_id is not None:
                        radio = (node, radio_id)
                    else:
                        radio = (node, 0)
                        
                if cu_node is not None:
                    if cu_radio_tap is not None:
                        cu_radio = self.slice_manager.get_radio_from_tap(cu_node, cu_radio_tap)
                    elif cu_radio_name is not None:
                        cu_radio = self.slice_manager.get_radio_from_name(cu_node, cu_node_name)
                    elif cu_radio_id is not None:
                        cu_radio = (cu_node, cu_radio_id)
                    else:
                        cu_radio = (cu_node, 0)
                
                if peer_node is not None:
                    if peer_radio_tap is not None:
                        peer_radio = self.slice_manager.get_radio_from_tap(peer_node, peer_radio_tap)
                    elif peer_radio_name is not None:
                        peer_radio = self.slice_manager.get_radio_from_name(peer_node, peer_node_name)
                    elif peer_radio_id is not None:
                        peer_radio = (peer_node, peer_radio_id)
                    else:
                        peer_radio = (peer_node, 0)
                        
                slice_name  = action_params.get("slice_name",None)
                recipe_book = action_params.get("recipe_book",None)
                
                
                # Parameters check
                if not slice_name or slice_name not in self.slice_manager.slices:
                    error_msg = "The slice \"{}\" does not exist or not valid, create it first".format(slice_name) 
                elif not radio:
                    error_msg = "Couldn't assign radio slice \"{}\": node/radio not specified or not valid ({})".format(slice_name, radio)
                else:
                    radios = [{"node_name":radio[0],"id":radio[1]}]
                    out_msg = "({}:{})".format(radio[0], radio[1])
                    node = self.slice_manager.nodes[radio[0]]                
                    
                    if node.node_type == RadioNode.NODE_TYPE.index("RU_NODE") and cu_radio is None:
                            warning_msg = "RU node type, but CU not specified"
                    
                    if cu_radio:
                        cu_node = self.slice_manager.nodes[cu_radio[0]]
                        if node.node_type == RadioNode.NODE_TYPE.index("RU_NODE") and cu_node.node_type == RadioNode.NODE_TYPE.index("CU_NODE"):
                            node.radios[radio[1]]["cu_host"]  = cu_radio[0]
                            node.radios[radio[1]]["cu_radio"] = cu_radio[1]
                            cu_node.radios[cu_radio[1]]["du_host"]  = radio[0]
                            cu_node.radios[cu_radio[1]]["du_radio"] = radio[1]
                            radios.append({"node_name":cu_radio[0],"id":cu_radio[1]})
                    
                    if peer_radio:
                        peer_node = self.slice_manager.nodes[peer_radio[0]]
                        node.radios[radio[1]]["remote_host"]  = peer_radio[0]
                        node.radios[radio[1]]["remote_radio"] = peer_radio[1]
                        peer_node.radios[peer_radio[1]]["remote_host"]  = radio[0]
                        peer_node.radios[peer_radio[1]]["remote_radio"] = radio[1]
                        radios.append({"node_name":peer_radio[0],"id":peer_radio[1]})
                            
                    action_list = self.slice_manager.assign_radio_slice(slice_name, radios)
                
                    if action_list is None:
                        error_msg = "Error while assigning slice  \"{}\" to radios  \"{}\"".format(slice_name, out_msg)
                        out_msg = None
                    else:
                        rval = dict(action_list = action_list, commit = commit_actions)
                        if commit_actions:
                            # --- commit actions
                            rval["commit"] = self.execute_action_list(dict(action_list=action_list)).get("rval")
                            
                            if rval["commit"]:
                                out_msg = "Slice  \"{}\" assigned to radios  \"{}\"".format(slice_name, out_msg)
                                self.LOG.info(out_msg)
                            else:
                                out_msg = "Couldn't commit slice  \"{}\" assigned to radios  \"{}\"".format(slice_name, out_msg)
                                self.LOG.info(error_msg)
                        else:
                            # --- or actions to pending_actions
                            self.pending_actions.extend(action_list)
                            out_msg = "Slice  \"{}\" assigned to radios  \"{}\", commit actions to nodes to make them valid.".format(slice_name, out_msg)   
        else:
            error_msg =  "Parameters missing or not valid"
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def update_radio_slice(self, action_params, node_name=None):
        """
        Update radio slice. 

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the slice
            - slice_context <dict>: context with slice parameters, default None
            - recipes_book <str>:  name of the recipes_book, default "update"
            - commit_actions <bool>: commit actions, default False
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        action_list = []
        commit_actions = False
        commit_actions_default = False
        recipe_book_default = SliceManager.DEFAULT_ASSIGN_BOOK
        
        if action_params and type(action_params) is dict:
            recipes_book    = action_params.get("recipes_book",   recipe_book_default)
            commit_actions  = action_params.get("commit_actions", commit_actions_default)
                
            if not recipes_book:
                recipes_book = recipe_book_default
            
            slice_name      = action_params.get("slice_name",  None)
            slice_context   = action_params.get("slice_context",  None)
                
            if slice_name:
                # --- Update slice and nodes
                action_list = self.slice_manager.update_radio_slice(slice_name, slice_context, recipes_book)
                if action_list:
                    rval = dict(action_list = action_list, commit = commit_actions)
                    if commit_actions:
                        # --- commit actions
                        rval["commit"] = self.execute_action_list(dict(action_list=action_list)).get("rval")
                        
                        if rval["commit"]:
                            out_msg = "Slice  \"{}\" updated.".format(slice_name)
                            self.LOG.info(out_msg)
                        else:
                            error_msg = "Couldn't commit slice updates \"{}\"".format(slice_name)
                            self.LOG.info(error_msg)
                    else:
                        # --- or actions to pending_actions
                        self.pending_actions.extend(action_list)
                        out_msg = "Slice  \"{}\" updated, commit actions to nodes to make them valid.".format(slice_name)
                    self.LOG.info(out_msg)
                else:
                    warning_msg = "Warning while updating radio slice {0} with new contex ({1}), action list empty".format(slice_name,slice_context)
                    self.LOG.info(warning_msg)
            else:
                error_msg = "Error while updating radio slice  with new context"
                self.LOG.info(error_msg)
        else:
            error_msg =  "Parameters missing or not valid"
            self.LOG.info(error_msg)
                
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
        
    def load_library(self, action_params, node_name=None):
        """
        Load library

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - library_name <str>: name of the library
            - library_path <str>: name of the path
            - library_context <str>: context parameteres for the library
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        action_list = []
        library_context = dict()
        
        if action_params and type(action_params) is dict:
        
            commit_actions = action_params.get("commit_actions",  False)
            
            library_info = dict()
            library_info["library_name"] = action_params.get("library_name",   None)
            library_info["library_path"] = action_params.get("init_book",   "./recipe-libraries/")
            library_info["node_context"] = action_params.get("node_context",   None)
            library_info["init_book"]    = action_params.get("init_book",   SliceManager.DEFAULT_INIT_BOOK)
            library_info["assign_book"]  = action_params.get("assign_book", SliceManager.DEFAULT_ASSIGN_BOOK)
            library_info["upload_book"]  = action_params.get("upload_book", SliceManager.DEFAULT_UPDATE_BOOK)
            
            
            
            if library_info["library_name"]:
                # -- Loading library                
                try:
                    self.slice_manager.load_recipe_library(library_info)
                    action_list = self.slice_manager.commit_context()
                except Exception as e:
                    error_msg = repr(e)
                except:
                    error_msg = "Couldn't load library \"{}\" from file  \"{}\" ".format(library_info["library_name"], library_info["library_path"]+library_info["library_name"]+"_recipe_library.json")
                else:
                    rval = dict(action_list = action_list, commit = commit_actions)
                    if commit_actions:
                        # --- commit actions
                        rval["commit"] = self.execute_action_list(dict(action_list=action_list)).get("rval")
                        if rval["commit"]:
                            out_msg = "Library  \"{}\" loaded.".format(library_info["library_name"])
                            self.LOG.info(out_msg)
                        else:
                            error_msg = "Couldn't commit library \"{}\" changes".format(library_info["library_name"])
                            self.LOG.info(error_msg)
                    
                    else:
                        # --- or actions to pending_actions
                        self.pending_actions.extend(action_list)
                        out_msg = "Library  \"{}\" loaded, commit actions to nodes to make them valid.".format(library_info["library_name"])
                    self.LOG.info(out_msg)
            else:
                error_msg = "Library name not specified"
                self.LOG.info(error_msg)
        else:
            error_msg =  "Parameters missing or not valid"
            self.LOG.info(error_msg)
        
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
        
        
    def update_library(self, action_params, node_name=None):
        """
        Load library

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - library_context <str>: context parameteres for the library
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        action_list = []
        commit_actions = False
        commit_actions_default = False
        recipe_book_default = SliceManager.DEFAULT_UPDATE_BOOK
        library_context = dict()
        
        if action_params and type(action_params) is dict:
            
            
            recipes_book   = action_params.get("recipes_book",   recipe_book_default)
            commit_actions = action_params.get("commit_actions",  commit_actions_default)
                
            if not recipes_book:
                recipes_book = recipe_book_default
            
            library_context = action_params.get("library_context",  None)
                
            action_list = self.slice_manager.update_recipe_library(library_context, recipes_book)
            rval = dict(action_list = action_list, commit = commit_actions)
            if commit_actions:
                # --- commit actions
                rval["commit"] = self.execute_action_list(dict(action_list=action_list)).get("rval")
                if rval["commit"]:
                    out_msg = "Library updated."
                    self.LOG.info(out_msg)
                else:
                    error_msg = "Couldn't update library"
                    self.LOG.info(error_msg)
            
            else:
                # --- or actions to pending_actions
                self.pending_actions.extend(action_list)
                out_msg = "Library updated, commit actions to nodes to make them valid."
            self.LOG.info(out_msg)
        else:
            error_msg =  "Parameters missing or not valid"
            self.LOG.info(error_msg)
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def get_library_info(self, action_params, node_name=None):
        """
        Does nothing, for testing or template

        All parameters are passed through the action_params dictionary
    
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        # --- 
        try:
            rval = self.slice_manager.info_recipe_library()
        except Exception as e:
            error_msg = repr(e)
        else:
            out_msg = "Currently running library \"{0[name]}\", version {0[version]} ({0[description]}). Accepted node types: {0[node_types]}".format(rval)
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
        
    def commit_actions(self, action_params, node_name=None):
        """
        Update radio slice. 

        All parameters are passed through the action_params dictionary
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
            
        rval = self.execute_action_list(dict(action_list=self.pending_actions)).get("rval")
        self.pending_actions = []
        
        if rval:
            out_msg = "Pending actions executed "
        else:
            error_msg = "Couldn't commit pending actions"
            self.LOG.info(error_msg)
                
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def get_radio_slice_info(self, action_params, node_name=None):
        """
        Get radio slice info.

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the slice
            - info <list>: information requested
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        if action_params and type(action_params) is dict:
            
            slice_name    = action_params.get("slice_name",  None)
            info_request  = action_params.get("info",  [])
            
            if slice_name and slice_name in self.slice_manager.slices:
                rval = self.slice_manager.slices[slice_name].get_dict()
                out_msg = "Getting info from slice \"{}\""
                if "pending_actions" in info_request:
                    rval.update(pending_actions = self.pending_actions.get(slice_name))
                    out_msg += ", pending actions"
                if "nodes" in info_request: 
                    rval.update(nodes = self.slice_manager.assigned_slices.get(slice_name,[]))
                    out_msg += "and associated nodes"
            else:
                error_msg = "Error while getting radio slice info, no slice specified"
                self.LOG.info(error_msg)
        else:
            error_msg =  "Parameters missing or not valid"
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value

    def get_connected_nodes(self, action_params=None, node_name=None):
        """
        Get nodes connected to the controller. 

        All parameters are passed through the action_params dictionary
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        rval = self.__update_nodes()
        out_msg = "Number of connected nodes: {}".format(len(rval))
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
                
    def add_connected_nodes(self, action_params=None, node_name=None):
        """
        Get nodes connected to the controller. 

        All parameters are passed through the action_params dictionary
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        rval = self.__add_nodes()
        out_msg = "Number of added nodes: {}".format(len(rval))
        
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def get_nodes(self, action_params=None, node_name=None):
        """
        Get nodes managed by RA
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        rval = dict()
        try:
            temp = self.slice_manager.nodes
        except:
            error_msg = "Couldn't get nodes from RA slice manager"
        
        for key, value in temp.items():
            value = value.get_dict()
            value.pop('local_ports')
            value.pop('local_rucu_ports')
            rval[key] = value
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def get_slices(self, action_params=None, node_name=None):
        """
        Get slices managed by RA
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        try:
            rval = self.slice_manager.slices
        except:
            error_msg = "Couldn't get slices from RA slice manager"
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
    def get_assigned_slices(self, action_params=None, node_name=None):
        """
        Get slices managed by RA
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       return values
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        warning_msg = None
        out_msg     = None
        
        try:
            rval = self.slice_manager.assigned_slices()
        except:
            error_msg = "Couldn't get assigned slices from RA slice manager"
            
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
# ==== RA Configuration Methods ====
        
    def execute_recipe_from_file(self, action_params, node_name=None):
        """
        Save the RA configuration to file.

        All parameters are passed through the action_params dictionary
        
        Keys requested in action dict
        =============================
            - recipe_name <str>: name of the configuration file
            - parameters <dict>: parameters
        
        @return   return_msg: return message
        @rtype:   return_msg: str
        """   
        # -------------------
        # Get info from file
        # -------------------
        rvals = []
        error_msg  = None
        return_msg = None
        return_value = dict()
        
        
        if action_params and type(action_params) is dict:
            recipe = action_params.get("recipe_name")
            if recipe:
                filename = action_params.get("recipe_name")+".json"
            try:
                with open(filename, 'r') as f:
                    recipe = json.load(f)
            except:
                rvals = []
                error_msg = "Cannot load recipe from file: {}.".format(filename)
                self.LOG.info(error_msg)
            else:
                return_msg = "Recipe {} loaded from file {}".format(recipe, filename)
                self.LOG.info(return_msg)
                
                action_list = self.__load_actions(recipe, action_params)
                
                self.LOG.info("Executing recipe: <%s>", recipe)
                return_value = self.execute_action_list(action_list)
                #for item in action_list:
                    #return_value = self.__process_radio_action(item) 
                    #rvals.append(return_value)
                #return_value = dict(rvals = rvals)
        else:
            return_value.update(error_msg =  "Parameters missing or not valid")
        return return_value
        
    def execute_action_list(self, action_params, node_name=None):
        """
        Execute an action_list from json file. If file does not exist, or
        filename is None, execute action list from action_params

        All parameters are passed through the action_params dictionary
        
        Keys requested in action dict
        =============================
            - action_list_name <str>: name of the action list,
            filename is <action_list_name>.json
            - action_list <list>: list of actions
        
        @return   return_msg: return message
        @rtype:   return_msg: str
        """   
        # -------------------
        # Get info from file
        # -------------------
        rvals = []
        action_list  = action_params.get("action_list",dict())
        error_msg  = None
        return_msg = None
        
        if action_params.get("action_list_name"):
            filename = action_params.get("action_list_name") + ".json"
            try:
                with open(filename, 'r') as f:
                    action_list = json.load(f)
            except:
                error_msg = "Warning: cannot load action list from file: {}.".format(filename)
                self.LOG.info(error_msg)
            
        for item in action_list:
            return_value = self.__process_radio_action(item) 
            rvals.append(return_value)
                
        return_value = dict(rvals = rvals)
        return return_value
        

    # ==== Radio Programs Repository actions ====
    def add_radio_program(self, action_params, node_name=None):  
        """
        Adds radio program.

        All parameters are passed through the action_params dictionary
    
        Keys in action dict
        ====================
            program_name <str>:   name of the program to be loaded.
            program_port <int>:   port number of XMLRTP server
            parameter_list <int>: list of configurable parameters (with set_parameters), if any, default is None
            monitor_list <int>:   list of monitorable parameters (with get_parameters), if any, default is None

        Raises
        ======
            - RadioActionError: if program_name not specified.
        
        
        @param action_params: action dictionary
        @type  action_params: dict
        @return:       0, operation sucessful
                       1, error
        @rtype:        int
        """
        # ---- Check parameters and set defaults ----        
        try:
            program_name = action_params["program_name"]
        except:
            raise self.RadioActionError("No program name specified")
            
        return_value = self.send_action_to_controller("add_radio_program", action_params)

        return return_value
        
    def get_radio_program_list(self, action_params=None, node_name=None):      
        """
        Gets radio program list.
        
        @return:       dictionary with list of program names, 
                       radio_program_list (iterable).
        @rtype:        dict
        
        """
        return_value = self.send_action_to_controller("get_radio_program_list")
    
        return return_value
        
    def get_radio_program(self, action_params, node_name=None):        
        """
        Gets radio program data.

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - program_name <str>: name of the program to be retrived.
            - get_code <bool>, optional: if True, gets also code, 
            if stored in the controller. Default is set to False.

        Raises
        ======
            - RadioActionError: if program not found or program_name 
            not specified.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @return:       dictionary with radio program data, 
                       radio_program (dict).
        @rtype:        dict
        """
        # ---- Check parameters and set defaults ----
        return_value = dict()
         
        try:
            program_name     = action_params['program_name']
        except:
            raise self.RadioActionError("No program_name")

        # ---- Get program ----  
        return_value = self.send_action_to_controller("get_radio_program", action_params)
                 
        return return_value
        
    def delete_radio_program(self, action_params, node_name=None):        
        """
        Deletes radio program from controller.

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - program_name <str>: name of the program to be deleted.

        Raises
        ======
            - RadioActionError: if program not found or program_name 
            not specified.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @return:       0, operation sucessful
                       1, error
        @rtype:        int
        """
        # ---- Check parameters and set defaults ----
        return_value = 1
        try:
            program_name = action_params['program_name']
        except:
            raise self.RadioActionError("No node_name")
        
        return_value = self.send_action_to_controller("delete_radio_program", action_params)
        return return_value
        
    def delete_all_radio_programs(self, action_params=None, node_name=None):        
        """
        Delets all radio programs stored in the controller.

        
        @param action_params: action dictionary
        @type  action_params: dict
        @return:       dictionary with list of programs delete.
                       radio_program_list
        @rtype:        dict
        
        """
        return_value = self.send_action_to_controller("delete_all_radio_programs")
        return return_value
        
           
# ==== Wishful UPI_Rs Actions ====
        
    def get_running_radio_program(self, action_params, node_name):
        
        """
        Get a radio program running on a node
        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - node_name <str>:    name of the node to be loaded.

        Raises
        ======
            - RadioActionError: if program_name is not
            specified. If node are not found.
        
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
    
        """
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        
        # Check if there is a program already running on the agent            
        return_value = self.send_action_to_controller("get_running_radio_program", action_params, node_name)
        pname = return_value.get("vals", None)
        
        if pname is not None:
            out_msg = "Program {} running on node {}".format(pname, node_name)
        else:
            out_msg = "No program running on node {}".format(pname)
        
        self.LOG.debug(out_msg)
        if "return_msg" in return_value:
            self.LOG.debug(return_value["return_msg"])
        if "error_msg" in return_value:
            self.LOG.debug(return_value["error_msg"])
            
        return return_value
                
    
    def activate_radio_program(self, action_params, node_name):
        """
        Activate a radio program on a node:
            - load the program code in the controller,
            - deactive any radio program currently running of the node,
            - activate the radio program.
        This function activates the specified radio program.
        When executed, this function stops the current radio program and enables the execution of the radio program specified in the parameter name.
    
        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - node_name <str>:     name of the node to be loaded.
            - program_name <str>:  name of the program to be loaded.
            - program_args <dict>: (gnuradio m.) args of the radio program 
            - program_type <str>:  (gnuradio m.) default is 'grc'
            - program_port <int>:  (gnuradio m.) XMLRPC server port in radio program

        Raises
        ======
            - RadioActionError: if the node_name or the program_name are  specified. 
            If the node_name or the program_name are not found. 
            If running the radio program on node is impossible (e.g. gnuradio not installed).
        
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
    
        """
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")
        try:
            program_name    = action_params["program_name"]
        except:
            raise self.RadioActionError("No program name specified")        
            
        # ---- Check if there is a program already running on the agent, if yes, deactivate it first ----
        return_value = self.send_action_to_controller("get_running_radio_program", None, node_name)
        try:
            pname = return_value["vals"]
        except:
            pname = None
        
        if "return_msg" in return_value:
            self.LOG.debug(return_value["return_msg"])
        if "error_msg" in return_value:
            self.LOG.debug(return_value["error_msg"])

        if pname is not None:
            # Deactivate radio program currently running on node
            self.LOG.info('\t-- Deactivate running programs on node {}'.format(node_name))
            rval = self.send_action_to_controller("deactivate_radio_program", dict(program_name = pname), node_name)
            out_msg = 'Program {}--{} deactivated'.format(node_name, pname)
            self.LOG.debug(out_msg)
            if "return_msg" in rval:
                self.LOG.debug(rval["return_msg"])
            if "error_msg" in rval:
                self.LOG.debug(rval["error_msg"])

        
        # ---- Send the program to the agent and activate it ----
        # Parameters:
        #   - program_name: The name of the grc program. Used to identify the program when starting/stopping it.
        #   - program_code: String with the GRC file content
        #   - program_type: default 'grc', or 'py' (we can also pass a '.py' file instead of a GRC.
        #   - program_port: Port that the XMLRPCServer will run (check the XMLRPC Server block on tx.grc file)  (default is default_socket_port)
        #   - program_args  #TODO add args            
        self.LOG.info('\tActivating program {} in node {}'.format(program_name, node_name))  
        return_value = self.send_action_to_controller("activate_radio_program", dict(radio_program = action_params), node_name)
        
        if "return_msg" in return_value:
            self.LOG.debug(return_value["return_msg"])
        if "error_msg" in return_value:
            self.LOG.debug(return_value["error_msg"])
        
        return return_value
    
    def deactivate_radio_slice(self, action_params, node_name): 
        """
        Deactivate the radio programs currently running on slice nodes (except CU and VR nodes), if any.
        When executed, this function stops the radio program specified in the node.
        To reactivate the slice, use activate_radio_slice.

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the node to be loaded.

        Raises
        ======
            - RadioActionError: if node_name not specified.
            If node not found.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        slice_name = None
        
        if action_params and type(action_params) is dict:
            slice_name     = action_params.get("slice_name",    None)
        
        if not slice_name:
            slice_name = "slice" + str(len(self.slice_manager.slices))
            out_msg =  "Slice name missing, deactivating nodes in all slices"
            for node_name in self.slice_manager.nodes:
                node_type = self.slice_manager.nodes[node_name].node_type
                if node_type != RadioNode.NODE_TYPE.index("VR_NODE") and node_type != RadioNode.NODE_TYPE.index("CU_NODE"):
                    try:
                        return_value = self.deactivate_radio_program(None, node_name)
                    except Exception as e:
                        if not error_msg:
                            error_msg = "Errors while deactivating slice \"{}\"".format(slice_name)
                        error_msg += repr(e)
                    else:
                        out_msg += ", \"{}\"".format(node_name)
        elif slice_name in self.slice_manager.assigned_slices:
            out_msg = "Deactivating slice \"{}\" nodes ".format(slice_name)
            for radio in self.slice_manager.assigned_slices[slice_name]:
                node_name = radio[0]
                node_type = self.slice_manager.nodes[node_name].node_type
                if node_type != RadioNode.NODE_TYPE.index("VR_NODE") and node_type != RadioNode.NODE_TYPE.index("CU_NODE"):
                    try:
                        return_value = self.deactivate_radio_program(None, node_name)
                    except Exception as e:
                        if not error_msg:
                            error_msg = "Errors while deactivating slice \"{}\"".format(slice_name)
                        error_msg += repr(e)
                    else:
                        out_msg += ", \"{}\"".format(node_name)
    
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
        
        
    def activate_radio_slice(self, action_params, node_name): 
        """
        Re-activate radio programs on slice nodes (except CU and VR nodes), if any.        

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - slice_name <str>: name of the node to be loaded.

        Raises
        ======
            - RadioActionError: if node_name not specified.
            If node not found.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
        
        """
        return_value = dict()
        rval        = None
        error_msg   = None
        out_msg     = None
        warning_msg = None
        
        slice_name = None
        
        if action_params and type(action_params) is dict:
            slice_name     = action_params.get("slice_name",    None)
        
        if not slice_name:
            slice_name = "slice" + str(len(self.slice_manager.slices))
            out_msg =  "Slice name missing, deactivating nodes in all slices"
            for slice_name in self.slice_manager.assigned_slices:
                out_msg = "Activating slice \"{}\" nodes ".format(slice_name)
                self.slice_manager.activate_slice(slice_name)
        elif slice_name in self.slice_manager.assigned_slices:
            out_msg = "Activating slice \"{}\" nodes ".format(slice_name)
            self.slice_manager.activate_slice(slice_name)
    
        if warning_msg:
            return_value.update(warning_msg = warning_msg)
        if out_msg:
            return_value.update(out_msg = out_msg)
        if error_msg:
            return_value.update(error_msg = error_msg)
        if rval:
            return_value.update(rval = rval)
        return return_value
    
    def deactivate_all_nodes(self, action_params=None, node_name = None):
        """
        Deactivate the radio program currently running on all node, if any.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @return:       nodes deactivated
        @rtype:        dict
        
        """
        # ---- Check parameters and set defaults ----
        vals = 0
        
        if node_name:
            if node_name not in self.nodes:
                self.nodes[node_name] = dict()
                
        for node_name in self.nodes:
            self.LOG.info("Deactivating node %s", node_name) 
            return_value = self.deactivate_radio_program(None, node_name) 
            if "return_msg" in return_value:
                self.LOG.debug(return_value["return_msg"])
            if "error_msg" in return_value:
                self.LOG.debug(return_value["error_msg"])
            vals += 1
                
        return dict(vals = vals)
    
    def get_parameters(self, action_params, node_name):
        
        """
        Gets values of parameters from a wishful agent/node.
        The UPI_R interface is able to obtain the current radio and MAC configuration by getting parameter values.
        Parameters correspond to the configuration registers of the hardware platform and to the variables used in the
        radio programs. This function get(s) the value(s) of the parameters specified in the list argument.
        The list of available parameters supported by all platforms are defined in the gnuradio program.
        Parameters specific to a subgroup of platforms are defined in the corresponding submodules.
        A list of supported parameters can be dynamically obtained using the get_radio_info function. # TODO

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - node_name <str>: node name from which retrive the parameters values.
            - params, <dict>: parameters values to be retrieved.

        Raises
        ======
            - RadioActionError: if node not found or node name not 
            specified.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       parameter values from the get_parameters wishful UPI_R ().
        @rtype:        dict
        """       
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")        
        try:
            params = action_params["params"]
        except:
            raise self.RadioActionError("No parameter specified")
            
        self.LOG.info('\t\tGet values {} from node {}'.format(params, node_name))
        
        return_value = self.send_action_to_controller("get_parameters", params, node_name)
        
        if "return_msg" in return_value:
            self.LOG.debug(return_value["return_msg"])
        if "error_msg" in return_value:
            self.LOG.debug(return_value["error_msg"])
            
        return return_value
        
    def set_parameters(self, action_params, node_name):
        
        """
        Set values of parameters in a wishful agent/node.
        The UPI_R interface is able to configure the radio and MAC behavior by changing parameters.
        Parameters correspond to the configuration registers of the hardware platform and to the variables used in
        the radio programs. This function (re)set the value(s) of the parameters specified in
        the dictionary argument. The list of available parameters supported by all platforms are defined in the gnuradio program.
        A list of supported parameters can be dynamically obtained using the get_radio_info function. #TODO

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - node_name <str>: node name where parameters the should be set 
            - params, <dict>: parameters values to be set.

        Raises
        ======
            - RadioActionError: if node not found or node name not specified.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output
        @rtype:        dict
        """
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")
        
        try:
            params = action_params["params"]
        except:
            raise self.RadioActionError("No parameter specified")
            
        self.LOG.info('\t\tSet values {} on node {}'.format(params, node_name))
        
        return_value = self.send_action_to_controller("set_parameters", params, node_name)
        
        if "return_msg" in return_value:
            self.LOG.debug(return_value["return_msg"])
        if "error_msg" in return_value:
            self.LOG.debug(return_value["error_msg"])
            
        return return_value
        
        
# ==== Radio Nodes wishful UPI_R Methods ====


    def get_node_info(self, action_params, node_name):
        """
        Gets the radio capabilities of a given network card radio_platform_t in terms of supported measurement and supported
        parameter and list of supported radio program. The information elements used by the UPI_R interface, to manage
        parameters, measurements and radio program, are organized into data structures, which provide information
        on the platform type and radio capabilities. When executed, this function return information about available
        radio capabilities (measurements and parameters) of each interface (radio_platform_t) on the available radio programs
        (radio_prg_t) available for transmissions over the radio interface.

        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - node: node_name

        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output, <dict>. 
        Returns a dictionary data type (key: value), in which are present the keys showed below:
                - 'radio_info' --> a list of pair value, the first value is the interface identifier and the second is the supported platforms.
                - 'monitor_list' --> a list of supported measurements between the attribute of the class UPI_R
                - 'param_list' --> a list of supported Parameters between the attribute of the class UPI_R
                - 'exec_engine_list_name' --> a list of supported execution environment name
                - 'exec_engine_list_pointer' --> a list of supported execution environment path
                - 'radio_prg_list_name'--> a list of supported radio program name
                - 'radio_prg_list_pointer' --> a list of supported radio program path
        @rtype:        dict
        """
        action_name = "get_radio_info"
        # ---- Check parameters and set defaults ----
        return_value = dict()             
        if node_name is None:
            raise self.RadioActionError("No node specified")
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
                
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        if return_value.get("rval"):
            for node_name in self.nodes:
                if node_name == node_name:
                    self.nodes["node_name"]["info"] = return_value["rval"]
                    
        return return_value
        
    def play_waveform(self, action_params, node_name):
        """
        Starts transmitting a radio waveform on signal generator
        
        All parameters are passed through the action_params dictionary
    
        Keys requested in action dict
        =============================
            - iface (String): Address ip of the interface connected with the signal generator.
            - freq (int): Frequency of the generated waveform.
            - power_lvl (int): power level of the generated waveform.
            - dict_args: Extra argumentes for the signal generator.

        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "play_waveform"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")
        try:
            node_name = action_params["iface"]
        except:
            raise self.RadioActionError("IP address (String) of the interface connected with the signal generator not specified")
            
        try:
            node_name = action_params["freq"]
        except:
            raise self.RadioActionError("Frequency (int) of the generated waveform not specified")
            
        node    = self.get_node_by_name(node_name)

        try:
            node_name = action_params["power_lvl"]
        except:
            raise self.RadioActionError("Power level (int) of the generated waveform not specified")
            
        #try:
            #node_name = action_params["dict_args"]
        #except:
            #raise self.RadioActionError("Extra arguments for the signal generator not specified")
            
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def stop_waveform(self, action_params, node_name):
        """
        Stops the radio waveform transmitting on signal generator

        All parameters are passed through the action_params dictionary
        
        Keys requested in action dict
        =============================
            - iface (String): Address ip of the interface connected with the signal generator.
            - dict_args: Extra argumentes for the signal generator.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "stop_waveform"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified") 
        try:
            node_name = action_params["iface"]
        except:
            raise self.RadioActionError("IP address (String) of the interface connected with the signal generator not specified")
            
        #try:
            #node_name = action_params["dict_args"]
        #except:
            #raise self.RadioActionError("Extra arguments for the signal generator not specified")
            
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def set_tx_power(self, action_params, node_name):
        """ 
        Sets the transmit power in dBm. If W is the power in Watt, the power in dBm is P = 30 + 10.log(W).

        In addition auto and fixed enable and disable power control (if those features are available).

        All parameters are passed through the action_params dictionary
        
        Keys requested in action dict
        =============================
            - power_dBm (String): Specify transmit power level in dBm and setting type
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "set_txchannel"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified") 
        try:
            node_name = action_params["power_dBm"]
        except:
            raise self.RadioActionError("Power dBm not specified")           
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value

    def get_tx_power(self, action_params, node_name):
        """ 
        Gets the current transmit power and the list of various Transmit Powers available on the device
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <dict>: transmission power values in dB
        @rtype:        dict
        """
        action_name = "get_tx_power"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value

        
    def get_noise(self, action_params, node_name):
        """  
        Background noise level (when no packet is transmitted). May be arbitrary units or dBm, this framework uses
        driver meta information to interpret the raw value given by interface and display it.
                
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>: noise level value
        @rtype:        dict
        """
        action_name = "get_noise"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def start_csi_measurements(self, action_params, node_name):
        """ 
        Start the thread to receive the channel state information. (amplitude + phase)

        When call, this UPI uses a separate thread to logs channel state estimation for each of the 56 ofdm subcarriers
        (HT20 case). The channel state info is the I and Q values for the subcarriers. When a burst of CSI information is
        ready, a callback is called.

        All parameters are passed through the action_params dictionary

        Keys requested in action dict
        =============================
            - callback (function): Specify the function to call when CSI information have been reported

        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "start_csi_measurements"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
 
        try:
            node_name = action_params["callback"]
        except:
            raise self.RadioActionError("Callback not specified")       
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def stop_csi_measurements(self, action_params, node_name):
        """
        Stop the thread to receive the channel state information. (amplitude + phase)

        When call, this UPI uses a separate thread to logs channel state estimation for each of the 56 ofdm subcarriers
        (HT20 case). The channel state info is the I and Q values for the subcarriers. When a burst of CSI information is
        ready, a callback is called.
                
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "stop_csi_measurements"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def get_radio_platforms(self, action_params, node_name):
        """ 
        Gets available radio platforms. The information elements used by the UPI_R
        interface, to manage parameters, measurements and radio program, are organized into data structures,
        which provide information on the platform type and radio capabilities.
        When executed, this function return information about available interfaces on node, the name or the identifier
        of the interface and the supported platform type.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <current_NIC_list>:
                a list of pair value, the first value is the interface identifier and the second is the supported platforms.
        @rtype:        dict
        """
        action_name = "get_radio_platforms"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value



    def set_rxchannel(self, action_params, node_name):
        """
        Set the operating reception frequency or channel.

        A value below 1000 indicates a channel number, a value greater than 1000 is a frequency in Hz. It is possible to use the suffix k, M or G to the value
        ("2.42G" for 2.42 GHz frequency). Channels are usually numbered starting at 1, it is possible to use get_rxchannel() UPI to get the total number of channels,
        list the available frequencies, and display the current channel.

    
        Keys requested in action dict
        =============================
            - freq_Hz(int): frequency in Hz or channel number
            - bandwidth(int): bandwidth in Hz
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "set_rxchannel"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        try:
            node_name = action_params["bandwidth"]
        except:
            raise self.RadioActionError("Bandwidth not specified")
            
        try:
            node_name = action_params["freq_Hz"]
        except:
            raise self.RadioActionError("Frequency (Hz) not specified")
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def get_rxchannel(self, action_params, node_name):
        """
        Return the list of available frequencies in the device and the number of defined channels for the reception module.
        Also return the current frequency/channel setted.
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <list>: list of available frequencies/channels and current frequency/channel setted
        @rtype:        dict
        """
        action_name = "get_txchannel"
        return_value = dict()
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node        
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value


    def set_txchannel(self, action_params, node_name):
        """
        Set the operating transmission frequency or channel.

        A value below 1000 indicates a channel number, a value greater than 1000 is a frequency in Hz. It is possible to use the suffix k, M or G to the value
        ("2.42G" for 2.42 GHz frequency). Channels are usually numbered starting at 1, it is possible to use get_rxchannel() UPI to get the total number of channels,
        list the available frequencies, and display the current channel.

        All parameters are passed through the action_params dictionary

    
        Keys requested in action dict
        =============================
            - freq_Hz(int): frequency in Hz or channel number
            - bandwidth(int): bandwidth in Hz
        
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <int>:
                - 0 if the  call was successfully performed
                - 1 partial success
                - 2 error.
        @rtype:        dict
        """
        action_name = "set_txchannel"
        return_value = dict()
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        try:
            node_name = action_params["bandwidth"]
        except:
            raise self.RadioActionError("Bandwidth (Hz) not specified")
            
        try:
            node_name = action_params["freq_Hz"]
        except:
            raise self.RadioActionError("Frequency (Hz) not specified")
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value

    def get_txchannel(self, action_params, node_name):
        """ 
        Return the list of available frequencies in the device and the number of defined channels for the transmission module.
        Also return the current frequency/channel setted.
                
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <list>: list of available frequencies/channels and current frequency/channel setted
        @rtype:        dict
        """
        action_name = "get_txchannel"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value

    def get_hwaddr(self, action_params, node_name):
        """ 
        Return the Hardware address of the device
                
        @param action_params: action dictionary
        @type  action_params: dict
        @param node_name:     destination node name
        @type  node_name:     string
        @return:       dictionary with UPI_R output <String>: HW address in the IEEE: MAC-48 format
        @rtype:        dict
        """
        action_name = "get_hwaddr"
        # ---- Check parameters and set defaults ----
        return_value = dict() 
        if node_name is None:
            raise self.RadioActionError("No node specified")             
        
        # ---- Forward action to node                
        return_value = self.send_action_to_controller(action_name, action_params, node_name)
        
        out_msg = "Action {} sent to node {}".format(action_name, str(node_name))
        self.LOG.debug(out_msg)
        
        return return_value

    def default_action(self, action_name, action_params, node_name):
        """ 
        Default action, forwarded to the controller
        """
        return_value = dict()             
        
        # ---- Forward action to node        
        return_value = self.send_action_to_controller(action_name, action_params, node_name)        
        if node_name is None:
            out_msg = "Action {} sent to controller".format(action_name) 
        else:
            out_msg = "Action {} for node {} sent to controller".format(action_name, str(node_name))
            
        self.LOG.debug(out_msg)
        return return_value
    
    
    def __del__(self):
        # disconnect from RabbitMQ
        return        
