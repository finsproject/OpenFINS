# FINS: Federated Interface for Network Slicing

**FINS** (_Federated Interface for Network Slicing_) is a supporting tool for 
the creation and management of _end-to-end_ slices in the IRIS testbed (TCD 
CONNECT, Dublin).

## Architecture

FINS architecture is characterized by **modules**, **services**, **resource 
controllers** and **resources**.  
Modules and services are strictly related to the control-plane, whilst 
resources and resource controllers are mostly part of the data-plane. 

### Modules

A module is a FINS specific component, with its own specific tasks in the FINS 
control-plane. Three module types have been defined:

- _**Experiment Initialiser**_ (**EI**): the module in charge of verifying if 
all the experiment components (modules, services, resources) are in place and 
functioning, of retrieving their significative parameters (in the light of 
slice management), of setting up all the configuration to ensure that the 
expected initial conditions for starting the experiment are met (which includes 
configuration both at control and data plane level) and of providing all other 
modules in FINS with the initial amount of information needed for their own 
configuration and "boot" activities.  
This module plays a fundamental role in the functioning of the whole FINS 
system and has to be carefully provided with a configuration file providing a 
full description of the experiment control and data plane

- _**Resource Manager**_ (**RM**): the core module of the control-plane, it is 
the brain coordinating its Agent activities according to the experiment 
requirements and evolution. It is in charge of translating incoming request in 
instructions to be sent to its agents to be translated into instructions for 
the underlying controlled resources. Also interaction among multiple RMs has 
been envisioned, but this functionality has been delayed, the priority being 
the set up of the hierarchical core architecture of FINS.
**NB**: In the current implementation of FINS, RM has a marginal role, since 
through the REST interface the user can interact directly with the RA . 

- _**Resource Agent**_ (**RA**): an agent works as an adapter for the RM toward 
controlled resources. Each agent has a dedicated communication channel (RA 
Northbound interface) toward its managing RM, receiving RM-incoming requests 
and translating them into a set of instructions that can be understand and 
performed by a set of the underlying resources (compatible with the RA 
Southbound interface). Responses received by controlled resources are then 
translated into responses that are sent back to the RM, to keep it updated 
regarding the current status of requested actions.
Via REST interface, users may send requests directly to each RA  

### Services

As their name says, they are services that are supporting FINS modules in the 
execution of their tasks. They are basically 3:

- _**Rabbit MQ**_: a queue-based message broker, used for setting up reliable 
communication between FINS components. From a strict FINS architectural point 
of view, Rabbit MQ is used for the RM-RAs communication, but it has also 
adopted for the internal communication between EI and all other modules and 
also for interfacing the REST module to all modules. Moreover, Rabbit MQ can be 
used by RAs for communicating with controlled resources, if supported by them.

- _**MongoDB**_: a NO-SQL database program, is used as storage support for the 
RM. This service is currently NOT USED in FINS.

- _**REST**_: FINS REST Interface allows user to interface with each of the 
control-plane modules (EI, RM, RAs), to retrieve information from them an to 
inject requests that can lead to operation performed over the controlled 
resources.

### Resource Controllers and Resources

_**Resource Controllers**_ are software components in charge of controlling a 
set of underlying resources, but accordingly to the instructions received from 
their managing RA. Resource Controllers are at the border-components of the
control-plane, having the capability of interact with underlying resources,
but also exposing a control interface which allow other entities to interact 
with them and to manage their activities.
Examples of Resource Controllers are ONOS (which finally has not been used in 
FINS), the WiSHFUL Controller and the OVS Controller (ad-hoc developed in FINS
for controlling the OVS bridges at data-plane side.)     


_**Resources**_ are basically all those VMs that are not part of the FINS 
control-plane (i.e., VMs not running Modules or Services). Resources are 
configured by RAs directly or indirectly though other specific controlling 
resources (see Resource Controllers) that are available in the system. 
Talking of IRIS testbed, OVS bridges and USRPs are considered resources


## Setting up and configuring environment for FINS installation

Please visit dedicated wiki here:
[OpenFINS Wiki](https://gitlab.com/finsproject/OpenFINS/wikis/home)


###### BY FBK CREATE-NET, 2018